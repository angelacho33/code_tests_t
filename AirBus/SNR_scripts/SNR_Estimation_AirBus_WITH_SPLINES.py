from __future__ import print_function, division
from PyAstronomy import pyasl
import numpy as np

from tk_databank import * 
from tk_plot import Plot
import numpy as np
import glob
import csv
import read_airbus_sar_data
import sys
import matplotlib

import tk_err
import gc
import sys,os

matplotlib.rcParams.update({'font.size': 20})

import matplotlib.pyplot as plt
from scipy.interpolate import UnivariateSpline


#folder='/home/angelh/Documents/DataScienceRelated/Teraki/AirBus_Output_All_sensors/file1/file1_run4/'
#folder='/home/angelh/Documents/DataScienceRelated/Teraki/AirBus_Output_All_sensors/file1/file1_run_FINAL_2/'
#file_name='output_file_id_0.dbk' 

file_name=str(sys.argv[1])

E=DataBank()
#E.load(folder+file_name)
E.load(file_name)
sfactors=[0,0.1,1]
output_file='output_Signal_To_Noise_ratio_estimation.csv'
with open(output_file, 'w',0) as f_recon:
    #Output file
    writer_recon = csv.writer(f_recon,quoting=csv.QUOTE_NONNUMERIC)
    writer_recon.writerow(('npoints','sensor_id', 'sensor_name','smoothing_factor','rrmse % (data-spline)',"SNR_estimate",'SNR_estimate(dB)','file_name') )
    for sensor in E.get_sensor_list():
        sensor_id=sensor['sensor_id']
        sensor_name=sensor['sensor_name']
        
        #if sensor_name!="UBLATACC":
        #    #if sensor_name!="ROLL":
        #    continue
            
        ##Raw data:
        raw_data=E.get_sensor_raw_data(sensor_id)
        values_raw=raw_data['values']
        values_raw_array=np.asarray(values_raw).T[0] 
        tss_raw=raw_data['tss']
        tss_raw_array=np.asarray(tss_raw)
        npoints=raw_data['count']
        #number_of_proceses=len(D.get_sensor_info(sensor_id)['processed_data'])-1
        #for process_id in range(0,number_of_processes):
        ##Recon data
        #recon_data=E.get_sensor_recon_data(sensor_id,process_id)
        #values_recon=recon_data['values']
        #values_recon_array=np.asarray(values_recon).T[0]
        #tss_recon=recon_data['tss']
        #tss_recon_array=np.asarray(tss_recon)
        
        x=tss_raw_array
        y=values_raw_array
        
        spl = UnivariateSpline(x, y)
        
        for sfactor in sfactors:
            if sfactor!=0:
                spl.set_smoothing_factor(sfactor)
            else:
                sfactor='default'    
                
            ##Plots and spline fit+SNR estimation
            fig,ax =plt.subplots(3,sharex=True,figsize=(20,15))
            
            std_data=round(y.std(),3)
            std_signal=round(spl(x).std(),3)
            
            ax[0].plot(x,y,'g',label=sensor_name+' data (file1)'+' ,std: '+str(std_data),marker='o')
            ax[0].plot(x, spl(x), 'b', lw=3,label='spline , smoothing factor:'+str(sfactor)+' ,std(spline):'+str(std_signal))
            #plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,ncol=2, mode="expand", borderaxespad=0.)
            #ax[0].legend(loc='best')
            ax[0].legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,ncol=1, mode="expand", borderaxespad=0.)
            
            ##Difference
            rrmse=tk_err.rrmse(y,spl(x))
            
            std_noise=(spl(x)-y).std()
            ax[1].plot(x, spl(x)-y, 'y', lw=3,label='std(spline-data)='+str(round(std_noise,3))+' ,rrmse='+str(round(rrmse,2))+'%',marker='o')
            ax[1].set_ylabel('spline(t)-data')
            ax[1].legend(loc='best')
            
            
            
            #SNR estimation for the whole rage
            variance_signal=std_signal*std_signal
            variance_noise=std_noise*std_noise
            SNR_estimate=round(variance_signal/variance_noise,3)
            SNR_estimate=10*np.log10(SNR_estimate) ##in dB!
            #print "SNR (std(signal)**2/std(noise)**2):", SNR_estimate
            print ('***sensor_id: ', sensor_id,' ',sensor_name)
            print ("**********************SNR estimate:", round(variance_signal/variance_noise,3))
            print ("**********************SNR estimate(dB):", SNR_estimate)
            print ("rrmse: ",rrmse)
            ##SNR by point?
            
            ax[2].plot(x, spl(x)/std_noise, 'y', lw=3,label='S/N Ratio estimate=10*log(std(spline)/std(spline-data))='+str(SNR_estimate),marker='o')
            ax[2].legend(loc='best')
            ax[2].set_ylabel('spline(t)/std(differences)')
            ax[2].set_xlabel('Timestamp')
            #plt.show()
            if not os.path.exists('./plots_SNR'):
                    os.makedirs('./plots_SNR')
            fig.savefig('plots_SNR/sensor_id_'+str(sensor_id)+'_'+sensor_name+'_spline_interpolation_smoothing_factor_'+str(sfactor)+'.png')
            
            #plt.show()
            plt.close('all')
            gc.collect()
                                                                                         #ratio                                   #10log(ratio)
            writer_recon.writerow((npoints,sensor_id, sensor_name,sfactor,round(rrmse,3),round(variance_signal/variance_noise,3),SNR_estimate))
        
        # Estimate the signal to noise ratio. Check whether the
        # estimate fits the input...
        # Use a chunk length of 20 data points, a polynomial of degree
        # one, and produce a "control plot".
        #snrEsti = pyasl.estimateSNR(x, y, 10, deg=1, controlPlot=True)
        #print("Estimate of the SNR RAW: ", snrEsti["SNR-Estimate"])
        
        
        
