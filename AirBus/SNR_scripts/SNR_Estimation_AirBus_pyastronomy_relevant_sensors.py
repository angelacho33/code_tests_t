from __future__ import print_function, division

import matplotlib
matplotlib.use('Agg') ### Force matplotlib to not use any Xwindows backend.

#from PyAstronomy import pyasl
import numpy as np

from tk_databank import * 
from tk_plot import Plot
import numpy as np
import glob
import csv
import read_airbus_sar_data
import sys

import tk_err
import gc
import sys,os

##This is the script I modified in:code_tests_teraki/AirBus/SNR_scripts/PyAstronmy_scripts
##and aded tp the python path
from SNR_Estimation_Function import estimateSNR

#matplotlib.rcParams.update({'font.size': 20})

import matplotlib.pyplot as plt
#from scipy.interpolate import UnivariateSpline


#folder='/home/angelh/Documents/DataScienceRelated/Teraki/AirBus_Output_All_sensors/file1/file1_run4/'
#folder='/home/angelh/Documents/DataScienceRelated/Teraki/AirBus_Output_All_sensors/file1/file1_run_FINAL_2/'
#file_name='output_file_id_0.dbk' 

relevant =['BLATACC', 'BNACC', 'BPTCHACC', 'BROLLACC', 'BYAWACC', 'BPTCHR', 'BROLLR', 'BYAWR', 'AOAC', 'SDSLPC', 'ELEV.R', 'ELEV.L']

file_name=str(sys.argv[1]) ##input dbk file!!

E=DataBank()
#E.load(folder+file_name)
E.load(file_name)
number_of_points=[10,30,50]
#number_of_points=[10]
output_file='output_Signal_To_Noise_ratio_estimation.csv'
with open(output_file, 'w',0) as f_recon:
    #Output file
    writer_recon = csv.writer(f_recon,quoting=csv.QUOTE_NONNUMERIC)
    writer_recon.writerow(('npoints','sensor_id', 'sensor_name','points','rrmse % (data-model)',"SNR_estimate(dB)",'SNR_estimate','file_name') )
    #for sensor in E.get_sensor_list()[:2]: ##Test first two sensors
    for sensor in E.get_sensor_list():
        sensor_id=sensor['sensor_id']
        sensor_name=sensor['sensor_name']
        
        if sensor_name not in relevant:
            continue
        #if sensor_name!="UBLATACC":
            #    #if sensor_name!="ROLL":
        #    continue
            
        ##Raw data:
        raw_data=E.get_sensor_raw_data(sensor_id)
        values_raw=raw_data['values']
        values_raw_array=np.asarray(values_raw).T[0] 
        tss_raw=raw_data['tss']
        tss_raw_array=np.asarray(tss_raw)
        npoints=raw_data['count']
        #number_of_proceses=len(D.get_sensor_info(sensor_id)['processed_data'])-1
        #for process_id in range(0,number_of_processes):
        ##Recon data
        #recon_data=E.get_sensor_recon_data(sensor_id,process_id)
        #values_recon=recon_data['values']
        #values_recon_array=np.asarray(values_recon).T[0]
        #tss_recon=recon_data['tss']
        #tss_recon_array=np.asarray(tss_recon)
        
        x=tss_raw_array
        y=values_raw_array
        
        #spl = UnivariateSpline(x, y)
        
        #for sfactor in sfactors:
        for points in number_of_points:
            title='sensor_id_'+str(sensor_id)+'_'+sensor_name+'_npoints'+str(points)
            print(title)
            #snrEsti = pyasl.estimateSNR(x, y, points, deg=2, controlPlot=True,title=title)
            snrEsti = estimateSNR(x, y, points, deg=2, controlPlot=True,title=title)
            
            data_points=snrEsti['data_points']
            model_points=snrEsti['model_points']
            
            ##The output of estimateSNR is an array of arrays. each array contains arrays of len=points
            ##So I have to convert that to a single array:
            data_points_single_array=[]
            model_points_single_array=[]
            for interval in data_points:
                for i in interval:
                    data_points_single_array.append(i)

            for interval in model_points:
                for i in interval:
                    model_points_single_array.append(i)
                
            rrmse=tk_err.rrmse(data_points_single_array,model_points_single_array)
            
            print("Estimate of the SNR: ", snrEsti["SNR-Estimate"])
            SNR_Estimate=snrEsti["SNR-Estimate"] ##Mean of SNR in each interval
            SNR_Estimate_dB=10*np.log10(np.abs(SNR_Estimate))
            writer_recon.writerow((npoints,sensor_id, sensor_name,points,round(rrmse,3),SNR_Estimate_dB,SNR_Estimate))
            ##
            
            ##Added by AngelH
            #plt.suptitle(title,fontsize=15)
            #if not os.path.exists('./plots_SNR'):
            #    os.makedirs('./plots_SNR')
            #plt.savefig('plots_SNR/'+title+'.png')
            
        #plt.close('all')
        #gc.collect()
      
        
