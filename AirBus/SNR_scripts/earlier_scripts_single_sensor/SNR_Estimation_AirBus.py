from __future__ import print_function, division
from PyAstronomy import pyasl
import numpy as np

from tk_databank import * 
from tk_plot import Plot
import numpy as np
import glob
import csv
import read_airbus_sar_data
import sys


folder='/home/angelh/Documents/DataScienceRelated/Teraki/AirBus_Output_All_sensors/file1/file1_run4/'
file_name='output_file_id_0.dbk' 
E=DataBank()
E.load(folder+file_name)

output_file='output_Signal_To_Noise_ratio_estimation.csv'
with open(output_file, 'w',0) as f_recon:
    #Output file
    writer_recon = csv.writer(f_recon,quoting=csv.QUOTE_NONNUMERIC)
    writer_recon.writerow(('npoints','sensor_id', 'sensor_name','SNR_estimation','file_name') )
    for sensor in E.get_sensor_list():
        sensor_id=sensor['sensor_id']
        sensor_name=sensor['sensor_name']
        
        ##Raw data:
        raw_data=E.get_sensor_raw_data(sensor_id)
        values_raw=raw_data['values']
        values_raw_array=np.asarray(values_raw).T[0] 
        tss_raw=raw_data['tss']
        tss_raw_array=np.asarray(tss_raw)
        
        number_of_proceses=len(D.get_sensor_info(sensor_id)['processed_data'])-1
        for process_id in range(0,number_of_processes):
            ##Recon data
            recon_data=E.get_sensor_recon_data(sensor_id,process_id)
            values_recon=recon_data['values']
            values_recon_array=np.asarray(values_recon).T[0]
            tss_recon=recon_data['tss']
            tss_recon_array=np.asarray(tss_recon)
            
            ##Estimate SNR..
            #sensor_id=25
            #process_id=0
            # Number of data points
            #N = 10000
            # Signal to noise ratio
            #SNR = 50.0
            
            # Create some data with noise and a sinusoidal
            # variation.
            #x = np.arange(N)
            #y = np.random.normal(0.0, 1.0/SNR, N) + 1.0
            #y += np.sin(x/500.0*2.*np.pi)*0.1
            x=tss_raw_array
            y=values_raw_array
            
            # Estimate the signal to noise ratio. Check whether the
            # estimate fits the input...
            # Use a chunk length of 20 data points, a polynomial of degree
            # one, and produce a "control plot".
            snrEsti = pyasl.estimateSNR(x, y, 10, deg=1, controlPlot=True)
            print("Estimate of the SNR RAW: ", snrEsti["SNR-Estimate"])
            
            #x=tss_recon_array
            #y=values_recon_array
            
            # Use a chunks with a length of 20, a polynomial of degree
            # two, and produce a "control plot".
            #snrEsti = pyasl.estimateSNR(x, y, 27, deg=2, controlPlot=False, xlenMode="excerpt")
            #snrEsti = pyasl.estimateSNR(x, y, 10, deg=1, controlPlot=True)
            #print("Estimate of the SNR: RECON", snrEsti["SNR-Estimate"])
