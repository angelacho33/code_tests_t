from __future__ import print_function, division
from PyAstronomy import pyasl
import numpy as np

from tk_databank import * 
from tk_plot import Plot
import numpy as np
import glob
import csv
import read_airbus_sar_data
import sys


folder='/home/angelh/Documents/DataScienceRelated/Teraki/AirBus_Output_All_sensors/file1/file1_run4/'
file_name='output_file_id_0.dbk' 

E=DataBank()
E.load(folder+file_name)

sensor_id=25 ##UBLATACC:25, BYAWACC: , DA:6, 
process_id=0


##Raw data:
raw_data=E.get_sensor_raw_data(sensor_id)
values_raw=raw_data['values']
values_raw_array=np.asarray(values_raw).T[0] 
tss_raw=raw_data['tss']
tss_raw_array=np.asarray(tss_raw)

##Recon data
recon_data=E.get_sensor_recon_data(sensor_id,process_id)
values_recon=recon_data['values']
values_recon_array=np.asarray(values_recon).T[0]
tss_recon=recon_data['tss']
tss_recon_array=np.asarray(tss_recon)

################################################################
# Number of data points
#N = 10000
# Signal to noise ratio
#SNR = 50.0

# Create some data with noise and a sinusoidal
# variation.
#x = np.arange(N)
#y = np.random.normal(0.0, 1.0/SNR, N) + 1.0
#y += np.sin(x/500.0*2.*np.pi)*0.1
x=tss_raw_array
y=values_raw_array

# Estimate the signal to noise ratio. Check whether the
# estimate fits the input...
# Use a chunk length of 20 data points, a polynomial of degree
# one, and produce a "control plot".
snrEsti = pyasl.estimateSNR(x, y, 20, deg=6, controlPlot=True)
print("Estimate of the SNR RAW: ", snrEsti["SNR-Estimate"])

#x=tss_recon_array
#y=values_recon_array

# Use a chunks with a length of 20, a polynomial of degree
# two, and produce a "control plot".
#snrEsti = pyasl.estimateSNR(x, y, 27, deg=2, controlPlot=False, xlenMode="excerpt")
#snrEsti = pyasl.estimateSNR(x, y, 10, deg=1, controlPlot=True)
#print("Estimate of the SNR: RECON", snrEsti["SNR-Estimate"])
