from __future__ import print_function, division
from PyAstronomy import pyasl
import numpy as np

from tk_databank import * 
from tk_plot import Plot
import numpy as np
import glob
import csv
import read_airbus_sar_data
import sys
import matplotlib

import tk_err

matplotlib.rcParams.update({'font.size': 20})

import matplotlib.pyplot as plt
from scipy.interpolate import UnivariateSpline

#smoothing factor:
#sfactors=[0,0.1,0.5,1.0,2.0]
#sfactors=[1.0]


folder='/home/angelh/Documents/DataScienceRelated/Teraki/AirBus_Output_All_sensors/file1/file1_run4/'
file_name='output_file_id_0.dbk' 

E=DataBank()
E.load(folder+file_name)

sensor_id=25 ##UBLATACC:25, BYAWACC: , DA:6, 
process_id=0


##Raw data:
raw_data=E.get_sensor_raw_data(sensor_id)
values_raw=raw_data['values']
values_raw_array=np.asarray(values_raw).T[0] 
tss_raw=raw_data['tss']
tss_raw_array=np.asarray(tss_raw)

##Recon data
recon_data=E.get_sensor_recon_data(sensor_id,process_id)
values_recon=recon_data['values']
values_recon_array=np.asarray(values_recon).T[0]
tss_recon=recon_data['tss']
tss_recon_array=np.asarray(tss_recon)

################################################################
# Number of data points
#N = 10000
# Signal to noise ratio
#SNR = 50.0

# Create some data with noise and a sinusoidal
# variation.
#x = np.arange(N)
#y = np.random.normal(0.0, 1.0/SNR, N) + 1.0
#y += np.sin(x/500.0*2.*np.pi)*0.1
x=tss_raw_array
y=values_raw_array


#for sfactor in sfactors:
fig,ax =plt.subplots(3,sharex=True,figsize=(20,15))

ax[0].plot(x,y,'g',label='UBLATACC data (file1)',marker='o')

spl = UnivariateSpline(x, y)

#if sfactor=!0:
sfactor=1.0 ##smoothing factor for all the sensors
spl.set_smoothing_factor(sfactor)
#sfactor=spl.get_smoothing_factor()
#else:
#sfactor='default'    
ax[0].plot(x, spl(x), 'b', lw=3,label='spline , smoothing factor:'+str(sfactor))
ax[0].legend(loc='best')

##Difference
rrmse=tk_err.rrmse(y,spl(x))

std_noise=(spl(x)-y).std()
ax[1].plot(x, spl(x)-y, 'y', lw=3,label='spline(t)-data'+' ,std(differences)='+str(round(std_noise,3))+' ,rrmse='+str(round(rrmse,2))+'%',marker='o')
ax[1].legend(loc='best')

std_signal=spl(x).std()

#SNR estimation for the whole rage
variance_signal=std_signal*std_signal
variance_noise=std_noise*std_noise
SNR_estimate=variance_signal/variance_noise
#print "SNR (std(signal)**2/std(noise)**2):", SNR_estimate
print ("SNR estimate:", SNR_estimate)
print ("rrmse: ",rrmse)
##SNR by point?

ax[2].plot(x, spl(x)/std_noise, 'y', lw=3,label='spline(t)/std(differences), SNR estimation:(std(signal)**2/std(noise)**2)='+str(SNR_estimate),marker='o')
ax[2].legend(loc='best')

#plt.show()
fig.savefig('UBLATACC_spline_interpolation_smoothing_factor_'+str(sfactor)+'.png')
# Estimate the signal to noise ratio. Check whether the
# estimate fits the input...
# Use a chunk length of 20 data points, a polynomial of degree
# one, and produce a "control plot".
#snrEsti = pyasl.estimateSNR(x, y, 20, deg=6, controlPlot=True)
#print("Estimate of the SNR RAW: ", snrEsti["SNR-Estimate"])

#x=tss_recon_array
#y=values_recon_array

# Use a chunks with a length of 20, a polynomial of degree
# two, and produce a "control plot".
#snrEsti = pyasl.estimateSNR(x, y, 27, deg=2, controlPlot=False, xlenMode="excerpt")
#snrEsti = pyasl.estimateSNR(x, y, 10, deg=1, controlPlot=True)
#print("Estimate of the SNR: RECON", snrEsti["SNR-Estimate"])
