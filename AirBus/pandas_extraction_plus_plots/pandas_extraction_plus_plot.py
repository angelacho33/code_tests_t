#!/usr/bin/env python

import matplotlib.pyplot as plt
import sys
import pandas as pd

#if len(sys.argv)<4:
#    sys.exit("Error!: Missing input parameters, Usage: pandas_extraction.py <File> <scenario> <number_files> <quantity>")

file=str(sys.argv[1]) ##summary csv file
#scenario=str(sys.argv[2])
#number_files=str(sys.argv[3])
#sensor_id=int(sys.argv[2]) ##sensor id!
#sensor_name=str(sys.argv[3]) ##sensor name!
deviation='rrmse'

sensors = ['BLATACC', 'BNACC', 'BPTCHACC', 'BROLLACC', 'BYAWACC', 'BPTCHR', 'BROLLR', 'BYAWR', 'AOAC', 'SDSLPC', 'ELEV.R', 'ELEV.L']
#sensors = ['BLATACC', 'BNACC']
dfa=pd.read_csv(file,sep=',')

sensor_id=0
for sensor_name in sensors:
    df=dfa[dfa['sensor_name']==sensor_name]
    #print df.sensor_name
    ##Test to select the minimum rrmse at 0.9 reduction
    df_09=df[df['reduction']==0.9]
    a_09=df_09.groupby(['sensor_name','framesize','reduction','mae'])[deviation].min()
    a_09r=a_09.reset_index()
    
    #print a_09r
    print 'Minimum value of rrmse'
    print a_09r[a_09r['rrmse']==a_09r['rrmse'].min()]
    ####################################################################
    a=df.groupby(['framesize','reduction'])[deviation].mean()
    ar=a.reset_index()
    
    f1=ar[ar['framesize']==200]
    f2=ar[ar['framesize']==500]
    f3=ar[ar['framesize']==1000]

    plt.plot(f1.reduction,f1[deviation],label="200",marker='o')
    plt.plot(f2.reduction,f2[deviation],label="500",marker='o')
    plt.plot(f3.reduction,f3[deviation],label="1000",marker='o')

    title=sensor_name
    plt.suptitle(title,fontsize=20)
    plt.xlabel('reduction')
    plt.ylabel(deviation+' %')
    plt.legend(loc='best',title='Framesize')
    
    plt.savefig('sensor_'+str(sensor_id)+'_'+sensor_name+'_'+deviation+'.png')
    plt.close()
    sensor_id=sensor_id+1

    
    




