#!/usr/bin/env python

import matplotlib.pyplot as plt
import sys
import pandas as pd
import numpy as np

#if len(sys.argv)<4:
#    sys.exit("Error!: Missing input parameters, Usage: pandas_extraction.py <File> <scenario> <number_files> <quantity>")

file=str(sys.argv[1]) ##summary csv file
#scenario=str(sys.argv[2])
#number_files=str(sys.argv[3])
#sensor_id=int(sys.argv[2]) ##sensor id!
#sensor_name=str(sys.argv[3]) ##sensor name!
deviation='rrmse'
#blocks=np.arange(10,26,1)
#blocks=np.arange(10,26,5)
blocks=np.arange(10,26,3)

sensors = ['ELEV.R', 'ELEV.L']
#sensors = ['ELEV.R']
dfa=pd.read_csv(file,sep=',')

sensor_id=0
#framesize_i=200
framesizes=[200,300,400]

for sensor_name in sensors:
    df1=dfa[dfa['sensor_name']==sensor_name]
    for framesize_i in framesizes:
        df2=df1[df1['framesize']==framesize_i]
        
        ###Print the row with minimum rrmse
        df_09=df2[df2['reduction']==0.9]
        a_09=df_09.groupby(['sensor_name','framesize','reduction','block','mae'])[deviation].min()
        a_09r=a_09.reset_index()
        #print a_09r
        print 'Minimum value of rrmse'
        print a_09r[a_09r['rrmse']==a_09r['rrmse'].min()]
        ####
        
        a=df2.groupby(['block','reduction'])[deviation].mean()
        ar=a.reset_index()
        #print ar
        f=[]
        i=0
        for block in blocks:
            f.append(ar[ar['block']==block])
            plt.plot(f[i].reduction,f[i][deviation],label=str(block),marker='o')
            i=i+1
    
        title=sensor_name+' '+'framesize: '+str(framesize_i)
        plt.suptitle(title,fontsize=20)
        plt.xlabel('reduction')
        plt.ylabel(deviation+' %')
        #plt.ylim(0,6.0) #7 for file 1
        #plt.ylim(0,4.0)  #4 for file 2
        plt.ylim(0,13.0)  #13 for file 3?
        
        plt.legend(loc='best',title='block')
        
        plt.savefig('sensor_'+str(sensor_id)+'_'+sensor_name+'_'+'framesize_'+str(framesize_i)+'_'+deviation+'.png')
        plt.close()
    sensor_id=sensor_id+1

    
    




