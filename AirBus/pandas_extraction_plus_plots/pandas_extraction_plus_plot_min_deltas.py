#!/usr/bin/env python

import matplotlib.pyplot as plt
import sys
import pandas as pd

#if len(sys.argv)<4:
#    sys.exit("Error!: Missing input parameters, Usage: pandas_extraction.py <File> <scenario> <number_files> <quantity>")

file=str(sys.argv[1]) ##summary csv file
#deviation='rrmse'

sensors = ['BLATACC', 'BNACC', 'BPTCHACC', 'BROLLACC', 'BYAWACC', 'BPTCHR', 'BROLLR', 'BYAWR', 'AOAC', 'SDSLPC', 'ELEV.R', 'ELEV.L']
#sensors = ['BLATACC', 'BNACC']
dfa=pd.read_csv(file,sep=',')

sensor_id=0
for sensor_name in sensors:
    df=dfa[dfa['sensor_name']==sensor_name]
    if sensor_name in sensors[:4]:
        print df[(df['rrmse']<=2.0)][['sensor_id','sensor_name','process_id','min_delta','red_info_ratio','rrmse']]
    else:
        print df[(df['rrmse']<=2.0)][['sensor_id','sensor_name','process_id','min_delta','red_info_ratio','rrmse']]
    sensor_id=sensor_id+1

    
    




