import os, sys, time
from tk_databank import DataBank
from tk_plot import Plot
import numpy as np
import glob
import csv
import read_airbus_sar_data

file_id=int(sys.argv[1])
folder='/home/angelh/Documents/DataScienceRelated/Teraki/code_tests/test_Airbus/'
filenamelist=sorted(glob.glob(folder+'*.csv'),key=os.path.getsize,reverse=True)

filenamelist=[filenamelist[file_id]]#Test just the largest file
print filenamelist
#relevant = ['BLATACC'] ##Test just one sensor
relevant = ['BLATACC', 'BNACC', 'BPTCHACC', 'BROLLACC', 'BYAWACC', 'BPTCHR', 'BROLLR', 'BYAWR', 'AOAC', 'SDSLPC', 'ELEV.R', 'ELEV.L']
block=     [15       ,  15    ,  15       ,  15       ,  15      ,  15     ,  15     ,  15    ,   5   ,  10     ,  20     , 15      ]

##Process cases
framesizes=[200,500,1000]
reductions=[0.6,0.8,0.9]   
#Short test
#framesizes=[100]
#reductions=[0.9]

process_cases=[]
for framesize in framesizes:
    for reduction in reductions:
        process_cases.append([framesize,reduction])
#D=[]

for file in filenamelist:
    
    data=read_airbus_sar_data.get_data(file)
    #D.append(DataBank()) #One Databank instance for a single file..in each one 13 sensors.
    D=DataBank() #One Databank instance for a single file..in each one 13 sensors.
    
    #Add sensor data to a DataBank
    for sensor_id in range(len(relevant)):
        timestamps=data[relevant[sensor_id]][0] ##timestamps are in datetime format
        timestamps_in_miliseconds=[ (time.mktime(dt.timetuple()) + (dt.microsecond / 1000000.0))*1000 for dt in timestamps]
        values=data[relevant[sensor_id]][1]
        #Convert values to a list of lists
        values_array=[[value] for value in values]
        value_names=[[relevant[sensor_id]]]
        
        #Add data to a Databank()
        #S = D.add_sensor_data(values_array,timestamps, sensor_id, relevant[sensor_id], "Airbus Data Set",value_names=value_names,quantity=relevant[sensor_id])
        S = D.add_sensor_data(values_array,timestamps_in_miliseconds, sensor_id, relevant[sensor_id], "Airbus Data Set",value_names=value_names)
        
        #Raw data info
        raw_data=D.get_sensor_raw_data(sensor_id)
        tss=raw_data['tss']
        count=raw_data['count']
        max=raw_data['max_v']
        min=raw_data['min_v']
        
        ##Print here raw_data
        
        
    ###########Process
    pre_process = {
        "multiple": [100000]
    }
    post_process = {
        "divide": [100000]
    }
    #block=5 ##As suggested by Daniel on slack Di 26. Apr 17:08:03 CEST 2016. for ELEVR you should study other settings
    
    output_file='summary_'+'file_id_'+str(file_id)+'_output_recon.csv'
    
    with open(output_file, 'w',0) as f_recon:
        #Output file
        writer_recon = csv.writer(f_recon,quoting=csv.QUOTE_NONNUMERIC)
        writer_recon.writerow(('file_id','npoints','sensor_id', 'sensor_name','value_name','process_id' ,'framesize','reduction', 
                               'tss_recon_min','tss_recon_max', 'values_recon_min','values_recon_max', 'values_recon.mean', 
                               'zlib_ratio', 'count_red','red_info_ratio',
                               'dev_max','dev_mean','dev_min',
                               'relerr','mape','wpe','rrmse','mae',
                               'file_name') )
        
        process_id=0
        for item in process_cases:        
            framesize = item[0]
            reduction = item[1]
            
            for sensor in D.get_sensor_list():
                index=sensor['index']
                dof=sensor['dof']
                sensor_id=sensor['sensor_id']
                sensor_name=sensor['sensor_name']
                
                #Raw data info
                raw_data=D.get_sensor_raw_data(sensor_id)
                tss=np.asarray(raw_data['tss'])
                values=np.asarray(raw_data['values'])
                npoints=raw_data['count']
                if npoints<framesize:
                    break
                    
                ##Process Data
                D.sensor_data_process(sensor_id, framesize, reduction,'cii_dct_v011', pre_process,post_process,block[sensor_id])
                print sensor_name, block[sensor_id]
                ##Plots
                P=Plot(D)
                P.plot(sensor_id,process_id,deviation='difference',dt_format='%M:%S',save_png=True) 
                #P.print_sensor_summary()
                
                #Recon data info
                recon_data=D.get_sensor_recon_data(sensor_id,process_id)
                tss_recon=np.asarray(recon_data['tss'])
                values_recon=np.asarray(recon_data['values'])
                zlib_ratio=D.get_zip_reduction_ratio(sensor_id,framesize)
                
                #Reduce info
                reduce_info=D.get_sensor_process_info(sensor_id, process_id)['reduce_info']
                count_red=reduce_info['count_red']
                red_info_ratio=reduce_info['ratio']
                
                #Deviation_info
                deviations=D.get_deviations(sensor_id,process_id)
                
                for i in range(dof):
                    value_name=sensor['value_names'][i]
                    values_comp=values_recon.T[i]
                    dev_max=deviations['E'][i]['max']
                    dev_mean=deviations['E'][i]['mean']
                    dev_min=deviations['E'][i]['min']
                    
                    relerr=deviations['relerr'][i]
                    mape=deviations['MAPE%'][i]
                    wpe=deviations['WAPE%'][i]
                    rrmse=deviations['RRMSE%'][i]
                    mae=deviations['MAE'][i]
                    
                    #Write output
                    writer_recon.writerow((file_id,npoints,sensor_id,sensor_name, value_name,process_id ,framesize,reduction, 
                                           tss_recon.min(),tss_recon.max(), float("%.3f"%values_comp.min()), 
                                           float("%.3f"%values_comp.max()), float("%.3f"%values_comp.mean()), 
                                           float("%.3f"%zlib_ratio), count_red,float("%.3f"%red_info_ratio), 
                                           float("%.3f"%dev_max),float("%.3f"%dev_mean),float("%.3f"%dev_min),
                                           relerr,mape,wpe,rrmse,mae,
                                           file))
            
            
            print '** File_id ',file_id,'','** Goes in sensor: ',sensor_id,'','** sensor_name: ',sensor_name,'',' process_id: ',process_id
            process_id=process_id+1
            
    ##Save D instance to a file
    name='test_file_id_'+str(file_id)+'.dbk'
    D.save(name)
    file_id=file_id+1
    

