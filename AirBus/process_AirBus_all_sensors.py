import matplotlib
matplotlib.use('Agg') ### Force matplotlib to not use any Xwindows backend.

import matplotlib.pyplot as plt 

import os, sys, time
from tk_databank import DataBank
from tk_plot import Plot
import numpy as np
import glob
import csv
import read_airbus_sar_data
import gc

file_id=int(sys.argv[1])
#folder='/home/angelh/Documents/DataScienceRelated/Teraki/code_tests/test_Airbus/'
folder='/home/angel/Airbus_Data/Airbus_DataFiles/'
filenamelist=sorted(glob.glob(folder+'*.csv'),key=os.path.getsize,reverse=True)

filenamelist=[filenamelist[file_id]]#Test just the input file
print filenamelist

all_sensors = ['ROLL','R1GYR','Q1GYR','PTCH','P1GYR','GS','DA','BYAWR','BYAWACC','BROLLR','BROLLACC','BPTCHR'
            ,'BPTCHACC','BNACC','BLONGACC','BLATACC','ACCVER','ACCLONG','ACCLAT','WS','WD','VELONS','VELOEW','UBNACC'
            ,'UBLONGACC','UBLATACC','THDG','TAS','SDSLPC','NZ1IRS','NZ1ACCFP','NZ1ACC','NY1IRS','NY1ACCFP','NY1ACC','NX1IRS'
            ,'NX1ACC','MN','IRS313','IALTROC','GYR5CF','GYR4CF','GYR3CF','GYR2CF','GYR1CF','FPFWS','CAS','AOAC'
            ,'ALTROC','ACCFWD4','ACCFWD3','ACCFWD2','ACCFWD1','ACCENGR3','ACCENGR2','ACCENGR1','ACCENGL3','ACCENGL2','ACCENGL1','ACCAFT3'
            ,'ACCAFT2','ACCAFT1','ACC6CFP','ACC5CFP','ACC4CFP','ACC3CFP','ACC2CFP','ACC1CFP','STAB','SPAC','SP.R7','AXCG'
            ,'SP.R6','SP.R5','SP.R4','SP.R3','SP.R2','SP.R1','SP.L7','SP.L6','SP.L5','SP.L4','SP.L3','SP.L2'
            ,'SP.L1','SLAT','SAT','RUDD','RMLGUPL','RMLGDNL','RHGONG','RHFEXT','RALT.1','NLGUPL','NLGFEXT','NLGDNL'
            ,'N1.2','N1.1','LMLGUPL','LMLGDNL','LHGONG','LHFEXT','IALT','GOGNLG','FRHOBSA','FLHOBSA','FLAP.I','ENGFAIL.2'
            ,'ENGFAIL.1','ELEV.R','ELEV.L','ALT','AIL.RO','AIL.RI','AIL.LO','AIL.LI','VMOW','VLEW','VFEW'
            ,'TLAV.2','TLAV.1','STKROLL.FO','STKROLL.CP','STKPTCH.FO','STKPTCH.CP','RUDDTRIM','RUDDPED','NORMALLAW','FLEVF','FLEV3','FLEV2'
            ,'FLEV1','FLEV0','BFP3','BFP2','BFP1','AP2ENG','AP1ENG','FTFQ.R','FTFQ.L','FTFQ.C','FGW','FFOB']

#all_sensors=['ELEV.R'] ##Test a single sensors...
#all_sensors=['GS'] ##Test a single sensor
#all_sensors=['BYAWACC'] ##Test a single sensor
print 'len(all_sensors)', len(all_sensors)

###### Daniel classification:
##For these sensors set a higher reduction 
#nOTE: Some sensors did not have a 'good profile' moved to BROLL profile: ACCFWD1
#Note: sensors added to good profile:N1.2, N1.1,SP.L2, SP.L1,STAB
sensors_with_good_profile=["GS","VELONS","VELOEW","THDG","IRS313","IALTROC","FPFWS","SLAT",
                           "RMLGUPL","RHFEXT","RALT.1","NLGUPL","NLGUPL","NLGFEXT","LMLGUPL","LHFEXT",'ROLL','PTCH','DA','SPAC',"N1.2","N1.1"
                           ,"SP.L2","SP.L1","STAB", "SP.R2","SP.R1","IALT"]

##For this sensors the exception is set at delta>0.5
##Sensor added to this category: WD
sensors_for_exception=["SP.R7","SP.R6","SP.R5","SP.R4","SP.R3","SP.L7","SP.L6","SP.L5","SP.L4","SP.L3",'WD']
##Note on exception I see from the second_run plots that applying exception detection to these sensors introduced spikes...
#so...I add them to sensors with good profile and below I "pass" the condition to process these sensors with expection detection:
#sensors_with_good_profile.extend(sensors_for_exception) Bad desicion for run two so commented and...back to exeption but with less reduction

#Note: some sensors reached smaller errors if included in sensors with good profile...so I removed them, ROLL,PTCH,'DA' 
#Note1: some sensors were not in any category and they need min delta: Added sensors: "GYR5CF"
sensors_with_BROLLR_profile=["R1GYR","Q1GYR","P1GYR","BYAWR","BROLLR","BPTCHR","BNACC","BLONGACC","ACCVER","ACCLONG","UBNACC","UBLONGACC","TAS","NZ1IRS","NZ1ACCFP","NZ1ACC","NX1IRS","NX1ACC","MN","GYR5CF","GYR4CF","GYR3CF","GYR2CF","GYR1CF","CAS","AOAC","ALTROC","ACCFWD4","ACCFWD3","ACCFWD2","ACCFWD1"]

##Sensors with BYAWACC and ELEV_L profile: I apply min delta (function set_pre_process_min_delta below set min_delta=0.05*max!)
##Sensors removed from this category and moved to sensors with good profile: SPAC, N1.2 ,N1.1, SP.L1, SP.L2,STAB,SP.R2,SP.R1,IALT
sensors_with_ELEV_L_profile=["ELEV.R","ELEV.L","ACCAFT3","ACCAFT2","ACCAFT1","ACC6CFP","ACC5CFP","ACC4CFP","ACC3CFP","ACC2CFP","ACC1CFP","SAT","RUDD"]

#Sensors added to this category: 'AIL.RO','AIL.RI','AIL.LO','AIL.LI'
##Sensors that seem to need a combination of min delta and exception detection: 'WD'
sensors_with_BYAWACC_profile=["BYAWACC","BROLLACC","BPTCHACC","BLATACC","ACCLAT","WS","UBLATACC","SDSLPC","NY1IRS","NY1ACCFP","NY1ACC",'AIL.RO','AIL.RI','AIL.LO','AIL.LI']

#sensors_for_exception=['WD'] ##Overwrites the above list of sensors_for_expection

##AngelH classification:
#sensors_noisy_for_min_delta=[]

##########

#######
##The next were the first 12 sensors tested and the corresponding optimal block and framesizes (Not used below)
relevant =         ['BLATACC', 'BNACC', 'BPTCHACC', 'BROLLACC', 'BYAWACC', 'BPTCHR', 'BROLLR', 'BYAWR', 'AOAC', 'SDSLPC', 'ELEV.R', 'ELEV.L']
#************************* Need_updated_framesizes for file1!! These optimal that Daniel sent for wich file? file1? 
#Note on blocks: Daniel values or all sensors expect ELEV.R and ELEV.L which I optimized in a separate run
block=             [15       ,  15    ,  15       ,  15       ,  15      ,  15     ,  15     ,  15    ,   5   ,  10     ,  10    , 10      ]
#************************* Need_updated_framesizes for file1: 
#Note on framesizes: Taken from the first run without any pre_processing i.w output_updated2/ for file2!!
optimal_framesizes=[1000      , 1000  ,  1000     ,  1000     ,  1000    ,  1000   ,  1000   ,  1000  ,   200 ,  200    ,  200  , 200     ]

#######
##Better implementation of the above optimal parameters (Not used below)
dict_optimal_parameters={
    #'sensor_name:' [optimal_block,optimal_framesize],
    'BLATACC': [15,1000], #npoints= 2671
    'BNACC':   [15,1000], #npoints= 2671
    'BPTCHACC':[15,1000], #npoints= 2671
    'BROLLACC':[15,1000], #npoints= 2671
    'BYAWACC': [15,1000], #npoints= 2671
    'BPTCHR':  [15,1000], #npoints= 2671
    'BROLLR':  [15,1000], #npoints= 2671
    'BYAWR':   [15,1000], #npoints= 2671
    'AOAC':    [5,  200], #npoints= 1336
    'SDSLPC':  [10, 200], #npoints= 1336
    'ELEV.R':  [10, 200], #npoints= 666
    'ELEV.L':  [10, 200], #npoints= 666
}



##################
#Set process cases (atm commented because each sensor has an optimal framesize and a core reduction depending on the pre_process parameters too :S)
#framesizes=[200,400,1000]
reductions=[0.4,0.5,0.6,0.7,0.8]
min_deltas=[0,0.01,0.05,0.10] #in percentage!! 
process_cases=[]
for reduction in reductions:
#   for reduction in reductions:
    for min_delta in min_deltas:
        #process_cases.append([framesize,reduction])
        process_cases.append([reduction,min_delta])

#####################################################

######### Pre and post process, exception point function
## Function to set a delta parameter for pre_process
## a multiple=1 is used!!!
def set_pre_process_exception(multiple,delta):
    pre_process = {
        "multiple": [multiple],
        #'integer': [1],
        "value_exceptions": [
            [
                {
                    "e_name": "red_peak_points",
                    "e_id":  4,
                    "v_types": ["red"],
                    "condition": "(abs(delta_v)>"+str(delta)+")",
                    "para": "delta_v"
                },
                {
                    "e_name": "rest_peak_points",
                    "e_id":  5,
                    "v_types": ["rest"],
                    "condition": "(abs(delta_v)>"+str(delta)+")",
                    "para": "str(delta_v)+'_'+str(ts)+'_'+str(v)"
                }
            ]
        ]
    }
    post_process = {
        "divide": [float(multiple)]
    }
    return pre_process, post_process


def set_pre_process_min_delta(multiple, min_delta):
    pre_process = {
        "multiple": [multiple],
        "min_delta":[min_delta]
        #'integer': [1],
    }
    post_process = {
        "divide": [float(multiple)]
    }
    return pre_process, post_process

###############################################################    
output_file='summary_file_id_'+str(file_id)+'_output_recon.csv'

with open(output_file, 'w',0) as f_recon:
    #Output file
    writer_recon = csv.writer(f_recon,quoting=csv.QUOTE_NONNUMERIC)
    writer_recon.writerow(('file_id','npoints','sensor_id', 'sensor_name','value_name','process_id' ,'framesize','reduction',
                           'multiple','min_delta','delta(excp.)', 
                           'tss_recon_min','tss_recon_max',
                           'raw_min_value','values_recon_min',
                           'raw_max_value','values_recon_max', 
                           'values_recon.mean', 
                           'zlib_ratio', 'count_red','red_info_ratio',
                           'dev_max','dev_mean','dev_min',
                           'relerr','mape','smape','wpe','wape','rrmse','relative_to_the_max_error','mae'
                           'file_name') )
    
    
    file_id=0 
    for file in filenamelist:    
        D = DataBank()
        data=read_airbus_sar_data.get_data(file)
        ##Add sensors data to a DataBank
        #for sensor_id in range(len(relevant)): #relevant only has 12 sensors...
        for sensor_id in range(len(all_sensors)):
            #print 'sensor_id: ', sensor_id , 'sensor_name:', relevant[sensor_id]
            print 'sensor_id: ', sensor_id , 'sensor_name:', all_sensors[sensor_id]
            
            #########Add data only for sensors with actual data
            ##Note: the read_airbus_sar_data function returns a dictionary only with certain keys (sensor_names)
            ##Those are the ones that actually have data I guess.
            #if data.has_key(relevant[sensor_id]): 
            if data.has_key(all_sensors[sensor_id]): 
                print '*************sensor_id (with entries): ', sensor_id,' ,sensor_name:', all_sensors[sensor_id]
                timestamps=data[all_sensors[sensor_id]][0] ##timestamps are in datetime format
                timestamps_in_miliseconds=[ (time.mktime(dt.timetuple()) + (dt.microsecond / 1000000.0))*1000 for dt in timestamps]
                values=data[all_sensors[sensor_id]][1]
                #Convert values to a list of lists
                values_array=[[value] for value in values]
                value_names=[all_sensors[sensor_id]]
                
                #Add data to a Databank()                                                #This is sensor_name 
                S = D.add_sensor_data(values_array,timestamps_in_miliseconds, sensor_id, all_sensors[sensor_id], "Airbus Data Set",value_names=value_names)
                #Raw data info
                raw_data=D.get_sensor_raw_data(sensor_id)
                tss=raw_data['tss']
                count=raw_data['count']
                max=raw_data['max_v']
                min=raw_data['min_v']
            else:
                pass ########Here should I add timestamps with empty data??
        print '************************************************************************************'
        print '*********** There are ', len(D.get_sensor_list()), ' sensors in DataBank ***********'
        print '************************************************************************************'
        ##Process each sensor added to the DataBank...
        for sensor in D.get_sensor_list():
            index=sensor['index']
            dof=sensor['dof']
            sensor_id=sensor['sensor_id']
            sensor_name=sensor['sensor_name']
            
            #Raw data info
            raw_data=D.get_sensor_raw_data(sensor_id)
            tss=np.asarray(raw_data['tss'])
            values=np.asarray(raw_data['values'])
            npoints=raw_data['count']
            
            ##The next works if there dof=1
            max=raw_data['max_v'][0]
            min=raw_data['min_v'][0]
            
            #print 'max: ',max
            if npoints<=400: ##Not process sensors with less than 400 data points
                continue
            #counter_process_exception=0
            counter_process_good_profile=0 ##This variables are to process exception and good profile only once!...no need to loop over reductions
            #process_id=0 #Not used as an additive variable! just for the fist process
            for item in process_cases:  
                #framesize=item[0]
                reduction=item[0]
                min_delta=item[1]
                
                if npoints>1000:
                    framesize=1000 ##Note!!For file 1, I saw that for sensors with npoints~2000 framesize 1000 worked best (i.e smaller rrmse) 
                    
                    ##Optimal block Daniel saw for these sensors [BLATACC','BNACC','BPTCHACC','BROLLACC', 'BYAWACC','BPTCHR','BROLLR','BYAWR']
                    ##All of which have npoints>2000
                    block=15       
                else:
                    ##I saw this worked best for ['AOAC', 'SDSLPC', 'ELEV.R', 'ELEV.L']
                    ##All of which have less than 2000 points...666 for the file1...but then...200 data points are missing from the reconstructed!!:S
                    framesize=400 
                    block=10
            
                ##Default reduction used for all sensors...If applying min_delta makes sense (see below), the core reduction is reset to 60%
                ##Since with min_delta the effective reducion will increase...
                #reduction=0.8
            
                #################### Set process parameters depending on the kind of sensor....
                ###########    Note!! If these conditions are changed they also have to be changed below in the suptitle for the plots...
                if sensor_name in sensors_for_exception:
                    #Default value for exception
                    multiple=1
                    if sensor_name=='WD':
                        delta=20 ##This is for the sensor WD
                    else:
                        delta=0.5
                        #reduction=0.7 ##The initial value was the above 80 but there are some artifacts :S
                    pre_process,post_process=set_pre_process_exception(multiple=multiple,delta=delta)
                    #pass ##Commented for run three...lets try these sensors as sensors with good profile....
                    min_delta='' #No min_delta to print in the summary table
                    
                    
                if (sensor_name in sensors_with_BYAWACC_profile)  or (sensor_name in sensors_with_ELEV_L_profile) or (sensor_name in sensors_with_BROLLR_profile):
                    #min delta is set as 1% of the maximum for the corresponding sensors
                    multiple=1000
                    if np.abs(min)>np.abs(max): ##some sensors have negative values and the min_delta should be relative to the min
                        #min_delta=np.abs(min)*0.01*multiple 
                        min_delta=np.abs(min)*min_delta*multiple 
                    else:
                        #min_delta=max*0.01*multiple
                        min_delta=max*min_delta*multiple
                    pre_process,post_process=set_pre_process_min_delta(multiple=multiple,min_delta=min_delta)
                    #reduction=0.6
                    delta=''#No delta(excep.) to print in the summary table

                if (sensor_name in sensors_with_good_profile): 
                    pre_process={}
                    post_process={}
                    #Note on framesizes: The recon data will be smaller than the raw data...how to handle this when writting to their format?
                    #reduction=0.9
                    multiple='' #No multiple nor min_delta nor delta to print in the summary table
                    min_delta=''
                    delta=''
                    #counter_process_good_profile=counter_process_good_profile+1
                    #print counter_process_good_profile
                    #if counter_process_good_profile>=2: ##Just process these sensors once for reduction=9
                    #    continue

                ##Process Data
                D.sensor_data_process(sensor_id, framesize, reduction,'cii_dct_v011', pre_process=pre_process, post_process=post_process,block=block)
                process_id=len(D.get_sensor_info(sensor_id)['processed_data'])-1
                print 'Processed sensor, ',sensor_id, ' ,', sensor_name, ', framesize: ', framesize, ' ,reduction: ', reduction
                print 'process_id=', process_id
                print 'block:' , block
                print 'pre_process:', pre_process

                
                #Recon data info
                recon_data=D.get_sensor_recon_data(sensor_id,process_id)
                tss_recon=np.asarray(recon_data['tss'])
                values_recon=np.asarray(recon_data['values'])
                zlib_ratio=D.get_zip_reduction_ratio(sensor_id,framesize)
                
                #Reduce info
                reduce_info=D.get_sensor_process_info(sensor_id, process_id)['reduce_info']
                count_red=reduce_info['count_red']
                red_info_ratio=reduce_info['ratio']
                
                ##Plots
                P=Plot(D)
                suptitle='sensor '+str(sensor_id)+', '+sensor_name+' ,process_id:'+str(process_id)+' ,framesize: '+str(framesize)+' ,reduction: '+str(round(red_info_ratio*100,3))+'%'
                
                ##Set titles of the plots depending on the pre_process used...
                if sensor_name in sensors_for_exception:
                    #pre_process,post_process=set_pre_process_exception(multiple=1,delta=0.5)
                    suptitle='sensor '+str(sensor_id)+', '+sensor_name+' ,process_id:'+str(process_id)+' ,framesize: '+str(framesize)+' ,reduction: '+str(round(red_info_ratio*100,3))+'%'+' ,Delta for exception: '+str(delta)
                    
                if (sensor_name in sensors_with_BYAWACC_profile)  or (sensor_name in sensors_with_ELEV_L_profile)  or (sensor_name in sensors_with_BROLLR_profile):
                    #min delta is set as 5% of the maximum for the corresponding sensors
                    #pre_process,post_process=set_pre_process_min_delta(multiple=1,min_delta=max*0.05)
                    #reduction=0.5
                    suptitle='sensor '+str(sensor_id)+', '+sensor_name+' ,process_id:'+str(process_id)+' ,framesize: '+str(framesize)+' ,reduction: '+str(round(red_info_ratio*100,3))+'%'+'\n, min_delta: '+str(round(min_delta,5))+' ,multiple: ' +str(multiple)
                
                if sensor_name in sensors_with_good_profile:
                    #reduction=0.9
                    suptitle='sensor '+str(sensor_id)+', '+sensor_name+' ,process_id:'+str(process_id)+' ,framesize: '+str(framesize)+' ,reduction: '+str(round(red_info_ratio*100,3))+'%'
                    
                P.plot(sensor_id,process_id,deviation='difference',save_png=True,suptitle=suptitle) 
                P.plot_rec_inspection(sensor_id, process_id, output='png', fft_yscale='log', dt_format='%M:%S')
                ##Close plt figures to avoid a massive memory leak...
                plt.close('all')
                gc.collect()
                
                #Deviation_info
                deviations=D.get_deviations(sensor_id,process_id)
            
                for i in range(dof):
                    value_name=sensor['value_names'][i]
                    values_comp=values_recon.T[i]
                    dev_max=deviations['E'][i]['max']
                    dev_mean=deviations['E'][i]['mean']
                    dev_min=deviations['E'][i]['min']
                
                    relerr=deviations['relerr'][i]
                    mae=deviations['MAE'][i]
                    mape=deviations['MAPE%'][i]
                    smape=deviations['SMAPE%'][i]
                    rrmse=deviations['RRMSE%'][i]
                    wape=deviations['WAPE%'][i]
                    wpe=deviations['WPE%'][i]
                    
                    if np.abs(min)>np.abs(max):
                        relative_error=np.abs(dev_max/min)*100 ##A relative error measurement
                    else:
                        relative_error=(dev_max/max)*100 ##A relative error measurement
                        
                    #Write output
                    writer_recon.writerow((file_id,npoints,sensor_id,sensor_name, value_name,process_id ,framesize,reduction, 
                                           multiple, min_delta,delta,
                                           tss_recon.min(),tss_recon.max(),min,float("%.3f"%values_comp.min()),max, float("%.3f"%values_comp.max()), 
                                           float("%.3f"%values_comp.mean()), float("%.3f"%zlib_ratio), count_red,float("%.3f"%red_info_ratio), 
                                           dev_max,dev_mean,dev_min,
                                           relerr,mape,smape,wpe,wape,rrmse,relative_error,mae,
                                           file))
                    
                    #process_id=process_id+1
        print '** Goes in file: ',file_id, ' ', file
        name='output_file_id_'+str(file_id)+'.dbk'
        D.save(name)
        file_id=file_id+1
            
