import matplotlib
matplotlib.use('Agg') ### Force matplotlib to not use any Xwindows backend.

import matplotlib.pyplot as plt 

import os, sys, time
from tk_databank import DataBank
from tk_plot import Plot
import numpy as np
import glob
import csv
import read_airbus_sar_data
import gc

file_id=int(sys.argv[1])
folder='/home/angelh/Documents/DataScienceRelated/Teraki/code_tests/test_Airbus/'
#folder='/home/angel/Airbus_Data/Airbus_DataFiles/'
filenamelist=sorted(glob.glob(folder+'*.csv'),key=os.path.getsize,reverse=True)

filenamelist=[filenamelist[file_id]]
print filenamelist
all_sensors = ['ROLL','R1GYR','Q1GYR','PTCH','P1GYR','GS','DA','BYAWR','BYAWACC','BROLLR','BROLLACC','BPTCHR'
               ,'BPTCHACC','BNACC','BLONGACC','BLATACC','ACCVER','ACCLONG','ACCLAT','WS','WD','VELONS','VELOEW','UBNACC'
               ,'UBLONGACC','UBLATACC','THDG','TAS','SDSLPC','NZ1IRS','NZ1ACCFP','NZ1ACC','NY1IRS','NY1ACCFP','NY1ACC','NX1IRS'
               ,'NX1ACC','MN','IRS313','IALTROC','GYR5CF','GYR4CF','GYR3CF','GYR2CF','GYR1CF','FPFWS','CAS','AOAC'
               ,'ALTROC','ACCFWD4','ACCFWD3','ACCFWD2','ACCFWD1'
               ,'ACCENGR3','ACCENGR2','ACCENGR1','ACCENGL3','ACCENGL2','ACCENGL1'
               ,'ACCAFT3','ACCAFT2','ACCAFT1','ACC6CFP','ACC5CFP','ACC4CFP','ACC3CFP','ACC2CFP','ACC1CFP','STAB','SPAC','SP.R7'
               ,'SP.R6','SP.R5','SP.R4','SP.R3','SP.R2','SP.R1','SP.L7','SP.L6','SP.L5','SP.L4','SP.L3','SP.L2'
               ,'SP.L1','SLAT','SAT','RUDD'
               ,'RMLGUPL','RMLGDNL','RHGONG','RHFEXT','RALT.1','NLGUPL','NLGFEXT','NLGDNL'
               ,'N1.2','N1.1','LMLGUPL'
               ,'LMLGDNL','LHGONG','LHFEXT'
               ,'IALT'
               ,'GOGNLG','FRHOBSA','FLHOBSA','FLAP.I','ENGFAIL.2','ENGFAIL.1'
               ,'ELEV.R','ELEV.L','ALT','AIL.RO','AIL.RI','AIL.LO','AIL.LI'
               ,'VMOW','VLEW','VFEW'
               ,'TLAV.2','TLAV.1'
               ,'STKROLL.FO','STKROLL.CP'
               ,'STKPTCH.FO','STKPTCH.CP'
               ,'RUDDTRIM','RUDDPED','NORMALLAW','FLEVF','FLEV3','FLEV2'
               ,'FLEV1','FLEV0','BFP3','BFP2','BFP1','AP2ENG','AP1ENG'
               ,'FTFQ.R','FTFQ.L','FTFQ.C','FGW','FFOB','AXCG']

units=["deg","No Unit","No Unit","deg","No Unit","kt","deg","deg/s","deg/s2","deg/s","deg/s2","deg/s","deg/s2","g","g","g","g","g","g","kt","deg","kt","kt","g","g","g","deg","kt","deg","No Unit","No Unit","g","No Unit","No Unit","g","No Unit","g","No Unit","deg","ft/min","deg/s","deg/s","deg/s","deg/s","deg/s","No Unit","kt","deg","ft/min","g","g","g","g","g","g","g","g","g","g","g","g","g","g","g","g","g","g","g","deg","mbar","deg","deg","deg","deg","deg","deg","deg","deg","deg","deg","deg","deg","deg","deg","deg","deg C","deg","","","","","ft","","","","%","%","","","","","ft","","deg","deg","deg","","","deg","deg","ft","deg","deg","deg","deg","","","","deg","deg","deg","deg","deg","deg","deg","deg","","","","","","","","","","","","kg","kg","kg","kg","kg","%MAC"]

                                ##min delta is the factor multiplying the maximum for each sensor
                                #[framesize,reduction, multiple, min_delta, delta] 
all_sensors_parameters = {'ROLL'      :[1000,     0.9,        1,         0,  0]
                          ,'R1GYR'    :[1000,     0.7,     1000,      0.01,  0]
                          ,'Q1GYR'    :[1000,     0.7,     1000,      0.01,  0]
                          ,'PTCH'     :[1000,     0.9,        1,      0.00,  0]
                          ,'P1GYR'    :[1000,     0.7,     1000,      0.01,  0]
                          ,'GS'       :[1000,     0.9,        1,      0.00,  0]
                          ,'DA'       :[1000,     0.8,        1,      0.00,  0]
                          ,'BYAWR'    :[1000,     0.8,     1000,      0.01,  0]
                          ,'BYAWACC'  :[1000,     0.2,     1000,      0.01,  0]
                          ,'BROLLR'   :[1000,     0.8,     1000,      0.01,  0]
                          ,'BROLLACC' :[1000,     0.2,     1000,      0.1 ,  0]
                          ,'BPTCHR'   :[1000,     0.7,     1000,      0.01,  0]
                          ,'BPTCHACC' :[1000,     0.2,     1000,      0.01,  0]
                          ,'BNACC'    :[1000,     0.2,     1000,       0.08,  0]
                          ,'BLONGACC' :[1000,     0.7,     1000,      0.01,  0]
                          ,'BLATACC'  :[1000,     0.2,     1000,      0.01,  0]
                          ,'ACCVER'  :[1000,     0.2,     1000,      0.01, 0]
                          ,'ACCLONG'  :[1000,     0.6,     1000,      0.03,  0]
                          ,'ACCLAT'   :[1000,     0.2,     1000,      0.05,  0]
                          ,'WS'       :[1000,     0.2,     1000,      0.01,  0]
                          ,'WD'      :[200,      0.2,        1,      0.01,  0]
                          ,'VELONS'   :[1000,     0.9,        1,      0.00,  0]
                          ,'VELOEW'   :[1000,     0.9,        1,      0.00,  0]
                          ,'UBNACC'   :[1000,     0.4,     1000,     0.08,  0]
                          ,'UBLONGACC':[1000,     0.2,     1000,     0.03,  0]
                          ,'UBLATACC' :[1000,     0.2,     1000,     0.01,  0]
                          ,'THDG'     :[400,     0.9,        1,      0.00,  0]
                          ,'TAS'      :[1000,    0.7,     1000,      0.001,  0]
                          ,'SDSLPC'   :[1000,     0.6,     1000,      0.01,  0]
                          ,'NZ1IRS'   :[200,     0.2,     1000,      0.003,  0]
                          ,'NZ1ACCFP' :[200,     0.5,     1000,      0.003,  0]
                          ,'NZ1ACC'   :[200,     0.5,     1000,      0.003,  0]
                          ,'NY1IRS'   :[1000,     0.1,     1000,      0.02,  0]
                          ,'NY1ACCFP' :[1000,     0.2,    1000,      0.05,  0]
                          ,'NY1ACC'   :[1000,     0.2,    1000,      0.05,  0]
                          ,'NX1IRS'   :[1000,     0.6,     1000,      0.03,  0]
                          ,'NX1ACC'   :[1000,     0.6,     1000,      0.03,  0]
                          ,'MN'       :[1000,     0.7,     1000,      0.001,  0]
                          ,'IRS313'   :[1000,     0.9,     1,       0.00,  0]
                          ,'IALTROC'  :[1000,     0.8,     1,       0.00,  0]
                          ,'GYR5CF'   :[1000,     0.6,     1000,      0.01,  0]
                          ,'GYR4CF'   :[1000,     0.6,     1000,      0.01,  0]
                          ,'GYR3CF'   :[1000,     0.6,     1000,      0.01,  0]
                          ,'GYR2CF'   :[1000,     0.6,     1000,      0.01,  0]
                          ,'GYR1CF'   :[1000,     0.6,     1000,      0.01,  0]
                          ,'FPFWS'    :[1000,     0.9,        1,      0.00,  0]
                          ,'CAS'      :[1000,     0.7,     1000,      0.001,  0]
                          ,'AOAC'     :[1000,     0.6,     1000,      0.03,  0]
                          
                          ,'ALTROC'   :[1000,     0.6,     1000,      0.05,  0]
                          ,'ACCFWD4'  :[1000,     0.1,     1000,      0.002,  0]
                          ,'ACCFWD3'  :[1000,     0.1,     1000,      0.002,  0]
                          ,'ACCFWD2'  :[1000,     0.1,     1000,      0.002,  0]
                          ,'ACCFWD1'  :[1000,     0.1,     1000,      0.02,  0]
                          
                          ,'ACCENGR3' :[1000,     0.9,     1,      0.00,  0]
                          ,'ACCENGR2' :[1000,     0.9,     1,      0.00,  0]
                          ,'ACCENGR1' :[1000,     0.9,     1,      0.00,  0]
                          ,'ACCENGL3' :[1000,     0.9,     1,      0.00,  0]
                          ,'ACCENGL2' :[1000,     0.9,     1,      0.00,  0]
                          ,'ACCENGL1' :[1000,     0.9,     1,      0.00,  0]
                          
                          ,'ACCAFT3'  :[1000,     0.1,     1000,      0.02,  0]
                          ,'ACCAFT2'  :[1000,     0.1,     1000,      0.02,  0]
                          ,'ACCAFT1'  :[1000,     0.1,     1000,      0.02,  0]
                          ,'ACC6CFP'   :[1000,     0.1,     1000,      0.01,  0]
                          ,'ACC5CFP'   :[1000,     0.1,     1000,      0.01,  0]
                          ,'ACC4CFP'   :[1000,     0.1,     1000,      0.01,  0]
                          ,'ACC3CFP'   :[1000,     0.1,     1000,      0.01,  0]
                          
                          ,'ACC2CFP'  :[1000,     0.1,     1000,      0.01,  0]
                          ,'ACC1CFP'  :[1000,     0.1,     1000,      0.01,  0]
                          ,'STAB'     :[200,      0.9,      1000,      0.00,  0]
                          ,'SPAC'     :[200,      0.9,         1,      0.00,  0]
                          
                          ,'SP.R7'     :[200,     0.7,        1,      0.00,  0.0]
                          ,'SP.R6'     :[200,     0.7,        1,      0.00,  0.0]
                          ,'SP.R5'     :[200,     0.7,        1,      0.00,  0.0]
                          ,'SP.R4'     :[200,     0.7,        1,      0.00,  0.0]
                          ,'SP.R3'     :[200,     0.7,        1,      0.00,  0.0]
                          
                          ,'SP.R2'     :[200,     0.7,        1,      0.00,  0.0]
                          ,'SP.R1'     :[200,     0.7,        1,      0.00,  0.0]
                          
                          ,'SP.L7'    :[200,     0.7,        1,      0.00,  0.0]
                          ,'SP.L6'    :[200,     0.7,        1,      0.00,  0.0]
                          ,'SP.L5'    :[200,     0.7,        1,      0.00,  0.0]
                          ,'SP.L4'    :[200,     0.7,        1,      0.00,  0.0]
                          ,'SP.L3'    :[200,     0.7,        1,      0.00,  0.0]
                          
                          ,'SP.L2'    :[200,     0.7,        1,      0.00,  0.0]
                          ,'SP.L1'    :[200,     0.7,        1,      0.00,  0.0]
                          
                          ,'SLAT'      :[200,     0.85,         1,      0.00,  0]
                          ,'SAT'       :[200,     0.6,      1000,      0.01,  0]
                          ,'RUDD'     :[400,     0.2,      1000,      0.03,  0]
                          
                          ,'RMLGUPL'   :[200,     0.9,     1,      0.00,  0]
                          ,'RMLGDNL':[200,     0.9,        1,      0.00,  0]
                          ,'RHGONG' :[200,     0.9,        1,      0.00,  0]
                          ,'RHFEXT' :[200,     0.9,        1,      0.00,  0]
                          ,'RALT.1' :[200,     0.9,        1,      0.00,  0]
                          ,'NLGUPL' :[200,     0.9,        1,      0.00,  0]
                          ,'NLGFEXT':[200,     0.9,        1,      0.00,  0]
                          ,'NLGDNL' :[200,     0.9,        1,      0.00,  0]
                          
                          ,'N1.2'   :[200,     0.8,     1000,      0.00,  0]
                          ,'N1.1'   :[200,     0.8,     1000,      0.00,  0]
                          
                          ,'LMLGUPL':[200,     0.9,        1,      0.00,  0]
                          ,'LMLGDNL':[200,     0.9,        1,      0.00,  0]
                          ,'LHGONG' :[200,     0.9,        1,      0.00,  0]
                          ,'LHFEXT' :[200,     0.9,        1,      0.00,  0]                          
                          ,'IALT'   :[200,     0.9,        1,      0.00,  0]
                          ,'GOGNLG' :[200,     0.9,        1,      0.00,  0]
                          ,'FRHOBSA':[200,     0.9,        1,      0.00,  0]
                          ,'FLHOBSA':[200,     0.9,        1,      0.00,  0]
                          ,'FLAP.I' :[200,     0.9,        1,      0.00,  0]
                          ,'ENGFAIL.2':[200,     0.9,      1,      0.00,  0]
                          ,'ENGFAIL.1':[200,     0.9,      1,      0.00,  0]
                          
                          ,'ELEV.R':[200,     0.6,     1000,      0.01,  0]
                          ,'ELEV.L':[200,     0.6,     1000,      0.01,  0]
                          
                          ,'ALT'   :[200,     0.8,     1000,       0.00,  0]
                          
                          ,'AIL.RO':[200,     0.6,     1000,      0.01,  0]
                          ,'AIL.RI':[200,     0.3,     1000,       0.05,  0]
                          
                          ,'AIL.LO':[400,     0.25,     1000,       0.03,  0]
                          ,'AIL.LI':[400,     0.25,     1000,       0.03,  0]
                          
                          ,'VMOW':[200,     0.9,        1,      0.00,  0]
                          ,'VLEW':[200,     0.9,        1,      0.00,  0]
                          ,'VFEW':[200,     0.9,        1,      0.00,  0]
                          ,'TLAV.2':[200,     0.9,      1,      0.00,  0]
                          ,'TLAV.1':[200,     0.9,      1,      0.00,  0]
                          ,'STKROLL.FO':[200,     0.9,      1,      0.00,  0]
                          ,'STKROLL.CP':[200,     0.9,      1,      0.00,  0]
                          ,'STKPTCH.FO':[200,     0.9,      1,      0.00,  0]
                          ,'STKPTCH.CP':[200,     0.9,      1,      0.00,  0]
                          ,'RUDDTRIM':[200,     0.9,      1,      0.00,  0]
                          ,'RUDDPED':[200,     0.9,      1,      0.00,  0]
                          ,'NORMALLAW':[200,     0.9,      1,      0.00,  0]
                          ,'FLEVF':[200,     0.9,      1,      0.00,  0]
                          ,'FLEV3':[200,     0.9,      1,      0.00,  0]
                          ,'FLEV2':[200,     0.9,      1,      0.00,  0]
                          ,'FLEV1':[200,     0.9,      1,      0.00,  0]
                          ,'FLEV0':[200,     0.9,      1,      0.00,  0]
                          ,'BFP3':[200,     0.9,      1,      0.00,  0]
                          ,'BFP2':[200,     0.9,      1,      0.00,  0]
                          ,'BFP1':[200,     0.9,      1,      0.00,  0]
                          ,'AP2ENG':[200,     0.9,      1,      0.00,  0]
                          ,'AP1ENG':[200,     0.9,      1,      0.00,  0]
                          ,'FTFQ.R':[200,     0.9,      1,      0.00,  0]
                          ,'FTFQ.L':[200,     0.9,      1,      0.00,  0]
                          ,'FTFQ.C':[200,     0.9,      1,      0.00,  0]
                          ,'FGW':[200,     0.9,      1,       0.00,  0]
                          ,'FFOB':[200,     0.9,      1,      0.00,  0]
                          ,'AXCG':[200,     0.9,      1,      0.00,  0]
                      }
detectors_with_zero_entries=['ACCENGR3','ACCENGR2','ACCENGR1','ACCENGL3','ACCENGL2','ACCENGL1'
                             ,'RMLGDNL','RHGONG','NLGDNL'
                             ,'LMLGDNL','LHGONG'
                             ,'GOGNLG'
                             ,'ENGFAIL.2','ENGFAIL.1'
                             ,'VMOW','VLEW','VFEW'
                             ,'STKROLL.FO','STKROLL.CP'
                             ,'RUDDTRIM','RUDDPED','NORMALLAW'
                             ,'FLEVF','FLEV3','FLEV2'
                             ,'FLEV1','FLEV0'
                             ,'BFP3','BFP2','BFP1'
                             ,'AP2ENG','AP1ENG'
                         ]
detectors_with_zero_entries_file2=['ACCENGR3','ACCENGR2','ACCENGR1','ACCENGL3','ACCENGL2','ACCENGL1'
                                   ,'RMLGDNL','RHGONG','NLGDNL'
                                   ,'LMLGDNL','LHGONG'
                                   ,'GOGNLG'
                                   ,'FRHOBSA','FLHOBSA','FLAP.I'
                                   ,'ENGFAIL.2','ENGFAIL.1'
                                   ,'VMOW','VLEW','VFEW'
                                   ,'STKROLL.FO','STKROLL.CP'
                                   ,'STKPTCH.FO','STKPTCH.CP'
                                   ,'RUDDTRIM','RUDDPED'
                                   ,'FLEVF','FLEV3','FLEV2'
                                   ,'BFP3','BFP2','BFP1'
                                   ,'AP2ENG'
                                   ,'FTFQ.C'
                               ]

if file_id==1:
    ##file2 i.e file_id=1 have other sensors with zero entries
    detectors_with_zero_entries=detectors_with_zero_entries_file2
       
#Values taken from the xls file I sent to Edouard where I optimized these parameters...with
#the script process_AirBus_min_deltas.py
                                 #[framesize,core_reduction, multiple, min_delta,block]
relevant_optimal_dict ={'BLATACC' :[1000   ,0.5           ,1000     ,10        ,15], 
                        'BNACC'   :[1000   ,0.6           ,1000     ,10        ,15], 
                        'BPTCHACC':[1000   ,0.6           ,100      ,10        ,15], 
                        'BROLLACC':[1000   ,0.7           ,10       ,5         ,15], 
                        'BYAWACC' :[1000   ,0.5           ,10       ,5         ,15], 
                        'BPTCHR'  :[1000   ,0.5           ,100      ,5         ,15], 
                        'BROLLR'  :[1000   ,0.7           ,100      ,5         ,15], 
                        'BYAWR'   :[1000   ,0.6           ,100      ,5         ,15], 
                        'AOAC'    :[200    ,0.6           ,100      ,10        , 5], 
                        'SDSLPC'  :[200    ,0.5           ,100      ,10        ,10], 
                        'ELEV.R'  :[400   ,0.5           ,100      ,10        ,10], 
                        'ELEV.L'  :[400   ,0.5           ,100      ,10        ,10] 
                        }
#all_sensors=['BLATACC'] ##Test a single sensor
#all_sensors=all_sensors[:5] ##Test the first 5 sensors
#all_sensors=detectors_with_zero_entries ##Test sensors with zero entries

print 'len(all_sensors)', len(all_sensors)

def set_pre_process_exception(multiple,delta):
    pre_process = {
        "multiple": [multiple],
        #'integer': [1],
        "value_exceptions": [
            [
                {
                    "e_name": "red_peak_points",
                    "e_id":  4,
                    "v_types": ["red"],
                    "condition": "(abs(delta_v)>"+str(delta)+")",
                    "para": "delta_v"
                },
                {
                    "e_name": "rest_peak_points",
                    "e_id":  5,
                    "v_types": ["rest"],
                    "condition": "(abs(delta_v)>"+str(delta)+")",
                    "para": "str(delta_v)+'_'+str(ts)+'_'+str(v)"
                }
            ]
        ]
    }
    post_process = {
        "divide": [float(multiple)]
    }
    return pre_process, post_process


def set_pre_process_min_delta(multiple, min_delta):
    pre_process = {
        "multiple": [multiple],
        "min_delta":[min_delta]
        #'integer': [1],
    }
    post_process = {
        "divide": [float(multiple)]
    }
    return pre_process, post_process

###############################################################    
output_file='summary_file_id_'+str(file_id)+'_output_recon.csv'

with open(output_file, 'w',0) as f_recon:
    #Output file
    writer_recon = csv.writer(f_recon,quoting=csv.QUOTE_NONNUMERIC)
    writer_recon.writerow(('file_id','npoints','sensor_id', 'sensor_name','value_name','process_id' ,'framesize','reduction',
                           'multiple','min_delta','delta(excp.)', 
                           'tss_recon_min','tss_recon_max',
                           'raw_min_value','values_recon_min',
                           'raw_max_value','values_recon_max', 
                           'values_recon.mean', 
                           'zlib_ratio', 'count_red','red_info_ratio',
                           'dev_max','dev_mean','dev_min','amplitude',
                           'relerr','mape','smape','wpe','wape','rrmse','relative_to_the_max_error','mae'
                           'file_name') )
    
    
    file_id=0 
    for file in filenamelist:    
        D = DataBank()
        data=read_airbus_sar_data.get_data(file)
        ##Add sensors data to a DataBank
        #for sensor_id in range(len(relevant)): #relevant only has 12 sensors...
        for sensor_id in range(len(all_sensors)):
            #print 'sensor_id: ', sensor_id , 'sensor_name:', relevant[sensor_id]
            print 'sensor_id: ', sensor_id , 'sensor_name:', all_sensors[sensor_id]
            
            #########Add data only for sensors with actual data
            ##Note: the read_airbus_sar_data function returns a dictionary only with certain keys (sensor_names)
            ##Those are the ones that actually have data I guess.
            #if data.has_key(relevant[sensor_id]): 
            if data.has_key(all_sensors[sensor_id]): 
                print '*************sensor_id (with entries): ', sensor_id,' ,sensor_name:', all_sensors[sensor_id]
                timestamps=data[all_sensors[sensor_id]][0] ##timestamps are in datetime format
                timestamps_in_miliseconds=[ (time.mktime(dt.timetuple()) + (dt.microsecond / 1000000.0))*1000 for dt in timestamps]
                values=data[all_sensors[sensor_id]][1]
                #Convert values to a list of lists
                values_array=[[value] for value in values]
                value_names=[all_sensors[sensor_id]]
                quantity=[units[sensor_id]]##The units are taken from the file!!
                
                #Add data to a Databank()                                                #This is sensor_name 
                S = D.add_sensor_data(values_array,timestamps_in_miliseconds, sensor_id, all_sensors[sensor_id], "Airbus Data Set",value_names=value_names,quantity=quantity)
                #Raw data info
                raw_data=D.get_sensor_raw_data(sensor_id)
                tss=raw_data['tss']
                count=raw_data['count']
                max=raw_data['max_v']
                min=raw_data['min_v']
            else:
                pass ########Here should I add timestamps with empty data??
        print '************************************************************************************'
        print '*********** There are ', len(D.get_sensor_list()), ' sensors in DataBank ***********'
        print '************************************************************************************'
        ##Process each sensor added to the DataBank...
        for sensor in D.get_sensor_list():
            index=sensor['index']
            dof=sensor['dof']
            sensor_id=sensor['sensor_id']
            sensor_name=sensor['sensor_name']
            
            #Raw data info
            raw_data=D.get_sensor_raw_data(sensor_id)
            tss=np.asarray(raw_data['tss'])
            values=np.asarray(raw_data['values'])
            npoints=raw_data['count']
            
            ##The next works if there dof=1
            max=raw_data['max_v'][0]
            min=raw_data['min_v'][0]
            
            #print 'max: ',max
            if npoints<=400: ##Not process sensors with less than 400 data points
                continue
            
            if npoints>1000:
                #framesize=1000 ##Note!!For file 1, I saw that for sensors with npoints~2000 framesize 1000 worked best (i.e smaller rrmse) 
                ##Optimal block Daniel saw for these sensors [BLATACC', 'BNACC', 'BPTCHACC', 'BROLLACC', 'BYAWACC', 'BPTCHR', 'BROLLR', 'BYAWR']
                ##All of which have npoints>2000
                block=15       
            else:
                ##I saw this worked best for ['AOAC', 'SDSLPC', 'ELEV.R', 'ELEV.L']
                ##If set to 400 a chucnk of the signal is cutted away for npoints=660
                #framesize=200 
                block=10
            
            framesize=all_sensors_parameters[sensor_name][0]
            reduction=all_sensors_parameters[sensor_name][1]
            multiple=all_sensors_parameters[sensor_name][2]
            min_delta=all_sensors_parameters[sensor_name][3]
            delta=all_sensors_parameters[sensor_name][4]
            
            pre_process={}
            post_process={}
            if min_delta!= 0:
                if np.abs(min)>np.abs(max): ##some sensors have negative values and the min_delta should be relative to the min
                    #print 'The maximum is negative'
                    #min_delta=np.abs(min)*0.01*multiple 
                    min_delta=np.abs(min)*min_delta*multiple 
                    print min_delta
                else:
                    #min_delta=max*0.01*multiple
                    min_delta=max*min_delta*multiple
                pre_process,post_process=set_pre_process_min_delta(multiple, min_delta)
            
            if delta != 0:
                pre_process,post_process=set_pre_process_exception(multiple,delta)
            
            ##Optimal parameters from a previous study playing with min_delta:
            if sensor_name in relevant_optimal_dict.keys():
                framesize =relevant_optimal_dict[sensor_name][0]
                reduction =relevant_optimal_dict[sensor_name][1]
                multiple  =relevant_optimal_dict[sensor_name][2]
                min_delta =relevant_optimal_dict[sensor_name][3]
                block     =relevant_optimal_dict[sensor_name][4]
                pre_process,post_process=set_pre_process_min_delta(multiple, min_delta)
                
            print 'processing sensor, ',sensor_id, ' ,', sensor_name, ', framesize: ', framesize, ' ,reduction: ', reduction
            print 'block:' , block
            print 'pre_process:', pre_process
            print 'post_process:', post_process
            
            #if sensor_name!='':
            #    continue
            
            ##Process Data
            D.sensor_data_process(sensor_id, framesize, reduction,'cii_dct_v011', pre_process=pre_process, post_process=post_process,block=block)
            process_id=len(D.get_sensor_info(sensor_id)['processed_data'])-1
            
            #Recon data info
            recon_data=D.get_sensor_recon_data(sensor_id,process_id)
            tss_recon=np.asarray(recon_data['tss'])
            values_recon=np.asarray(recon_data['values'])
            zlib_ratio=D.get_zip_reduction_ratio(sensor_id,framesize)
            
            #Reduce info
            reduce_info=D.get_sensor_process_info(sensor_id, process_id)['reduce_info']
            count_red=reduce_info['count_red']
            red_info_ratio=reduce_info['ratio']
            
            #Deviation_info
            deviations=D.get_deviations(sensor_id,process_id)
            
            for i in range(dof):
                value_name=sensor['value_names'][i]
                values_comp=values_recon.T[i]
                dev_max=deviations['E'][i]['max']
                dev_mean=deviations['E'][i]['mean']
                dev_min=deviations['E'][i]['min']
                
                relerr=deviations['relerr'][i]
                mae=deviations['MAE'][i]
                mape=deviations['MAPE%'][i]
                smape=deviations['SMAPE%'][i]
                rrmse=deviations['RRMSE%'][i]
                wape=deviations['WAPE%'][i]
                wpe=deviations['WPE%'][i]
                
                ##A relative error measurement they want to see
                amplitude=np.abs(max-min)
                if np.abs(dev_min)>np.abs(dev_max):
                    relative_error=np.abs(dev_min/amplitude)*100 
                else:
                    relative_error=np.abs(dev_max/amplitude)*100 
                    
                
                #####Plots
                P=Plot(D)
                suptitle=sensor_name+' ,reduction: '+str(int(red_info_ratio*100))+'%'+ '  ,rrmse:'+str(round(rrmse,1))+'%'+' ,sid:'+str(sensor_id)
                P.plot(sensor_id,process_id,deviation='difference',dt_format='%M:%S',save_png=True,suptitle=suptitle) 
                
                #if sensor_name in detectors_with_zero_entries:
                #    P.plot_rec_inspection(sensor_id, process_id, output='png', fft_yscale='linear', dt_format='%M:%S')
                #else:
                #    P.plot_rec_inspection(sensor_id, process_id, output='png', fft_yscale='log', dt_format='%M:%S')
                    ##Close plt figures to avoid a massive memory leak...
                plt.close('all')
                gc.collect()
                
                
                #Write output
                writer_recon.writerow((file_id,npoints,sensor_id,sensor_name, value_name,process_id ,framesize,reduction, 
                                       multiple, min_delta,delta,
                                       tss_recon.min(),tss_recon.max(),min,float("%.3f"%values_comp.min()),max, float("%.3f"%values_comp.max()), 
                                       float("%.3f"%values_comp.mean()), float("%.3f"%zlib_ratio), count_red,float("%.3f"%red_info_ratio), 
                                       dev_max,dev_mean,dev_min,amplitude,
                                       relerr,mape,smape,wpe,wape,rrmse,relative_error,mae,
                                       file))
                
                #process_id=process_id+1
        print '** Goes in file: ',file_id, ' ', file
        name='output_file_id_'+str(file_id)+'.dbk'
        D.save(name)
        file_id=file_id+1
            
