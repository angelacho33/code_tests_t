import matplotlib
matplotlib.use('Agg') ### Force matplotlib to not use any Xwindows backend.

import matplotlib.pyplot as plt 

import os, sys, time
from tk_databank import DataBank
from tk_plot import Plot
import numpy as np
import glob
import csv
import read_airbus_sar_data
import gc

file_id=int(sys.argv[1])
folder='/home/angelh/Documents/DataScienceRelated/Teraki/code_tests/test_Airbus/'
#folder='/home/angel/Airbus_Data/Airbus_DataFiles/'
filenamelist=sorted(glob.glob(folder+'*.csv'),key=os.path.getsize,reverse=True)

filenamelist=[filenamelist[file_id]]
print filenamelist
all_sensors = ['ROLL','R1GYR','Q1GYR','PTCH','P1GYR','GS','DA','BYAWR','BYAWACC','BROLLR','BROLLACC','BPTCHR'
               ,'BPTCHACC','BNACC','BLONGACC','BLATACC','ACCVER','ACCLONG','ACCLAT','WS','WD','VELONS','VELOEW','UBNACC'
               ,'UBLONGACC','UBLATACC','THDG','TAS','SDSLPC','NZ1IRS','NZ1ACCFP','NZ1ACC','NY1IRS','NY1ACCFP','NY1ACC','NX1IRS'
               ,'NX1ACC','MN','IRS313','IALTROC','GYR5CF','GYR4CF','GYR3CF','GYR2CF','GYR1CF','FPFWS','CAS','AOAC'
               ,'ALTROC','ACCFWD4','ACCFWD3','ACCFWD2','ACCFWD1'
               ,'ACCENGR3','ACCENGR2','ACCENGR1','ACCENGL3','ACCENGL2','ACCENGL1'
               ,'ACCAFT3','ACCAFT2','ACCAFT1','ACC6CFP','ACC5CFP','ACC4CFP','ACC3CFP','ACC2CFP','ACC1CFP','STAB','SPAC','SP.R7'
               ,'SP.R6','SP.R5','SP.R4','SP.R3','SP.R2','SP.R1','SP.L7','SP.L6','SP.L5','SP.L4','SP.L3','SP.L2'
               ,'SP.L1','SLAT','SAT','RUDD'
               ,'RMLGUPL','RMLGDNL','RHGONG','RHFEXT','RALT.1','NLGUPL','NLGFEXT','NLGDNL'
               ,'N1.2','N1.1','LMLGUPL'
               ,'LMLGDNL','LHGONG','LHFEXT'
               ,'IALT'
               ,'GOGNLG','FRHOBSA','FLHOBSA','FLAP.I','ENGFAIL.2','ENGFAIL.1'
               ,'ELEV.R','ELEV.L','ALT','AIL.RO','AIL.RI','AIL.LO','AIL.LI'
               ,'VMOW','VLEW','VFEW'
               ,'TLAV.2','TLAV.1'
               ,'STKROLL.FO','STKROLL.CP'
               ,'STKPTCH.FO','STKPTCH.CP'
               ,'RUDDTRIM','RUDDPED','NORMALLAW','FLEVF','FLEV3','FLEV2'
               ,'FLEV1','FLEV0','BFP3','BFP2','BFP1','AP2ENG','AP1ENG'
               ,'FTFQ.R','FTFQ.L','FTFQ.C','FGW','FFOB','AXCG']
units=["deg","No Unit","No Unit","deg","No Unit","kt","deg","deg/s","deg/s2","deg/s","deg/s2","deg/s","deg/s2","g","g","g","g","g","g","kt","deg","kt","kt","g","g","g","deg","kt","deg","No Unit","No Unit","g","No Unit","No Unit","g","No Unit","g","No Unit","deg","ft/min","deg/s","deg/s","deg/s","deg/s","deg/s","No Unit","kt","deg","ft/min","g","g","g","g","g","g","g","g","g","g","g","g","g","g","g","g","g","g","g","deg","mbar","deg","deg","deg","deg","deg","deg","deg","deg","deg","deg","deg","deg","deg","deg","deg","deg C","deg","","","","","ft","","","","%","%","","","","","ft","","deg","deg","deg","","","deg","deg","ft","deg","deg","deg","deg","","","","deg","deg","deg","deg","deg","deg","deg","deg","","","","","","","","","","","","kg","kg","kg","kg","kg","%MAC"]

                                ##min delta is the factor multiplying the maximum for each sensor
                                #[framesize,reduction, multiple, min_delta, delta] 
default_exception=1e15 ##If I do not want to use exception detection as suggested by Qing I just set a very large number....

all_sensors_parameters = {'ROLL'      :[400,     0.85,        1,         0,  default_exception]
                          ,'R1GYR'    :[400,    0.7,     1000,      0.01,  default_exception]
                          ,'Q1GYR'    :[400,     0.6,     1000,      0.01,  default_exception]
                          ,'PTCH'     :[400,     0.85,        1,      0.00,  default_exception]
                          ,'P1GYR'    :[400,     0.6,     1000,      0.01,  default_exception]
                          ,'GS'       :[400,     0.85,        1,      0.00,  default_exception]
                          ,'DA'       :[400,     0.8,        1,      0.00,  default_exception]
                          ,'BYAWR'    :[400,     0.6,     1000,      0.01,  default_exception]
                          ,'BYAWACC'  :[400,     0.6,     1000,      0.02,  1000]
                          ,'BROLLR'   :[200,     0.6,     1000,      0.01,  default_exception]
                          ,'BROLLACC' :[400,     0.6,     1000,      0.03 , 1000]
                          ,'BPTCHR'   :[400,     0.7,     1000,      0.01,  default_exception]
                          ,'BPTCHACC' :[400,     0.6,     1000,      0.03,  1000]
                          ,'BNACC'    :[300,     0.6,     1000,      0.001,  100]
                          ,'BLONGACC' :[400,     0.6,     1000,      0.01,  10]
                          ,'BLATACC'  :[400,     0.6,     1000,      0.03,  10]
                          ,'ACCVER'   :[400,     0.6,     1000,      0.003,  100]
                          ,'ACCLONG'  :[400,     0.6,     1000,      0.01,  10]
                          ,'ACCLAT'   :[400,     0.6,     1000,      0.03,  10]
                          ,'WS'       :[200,     0.4,     1000,      0.01,  default_exception]
                          ,'WD'       :[200,     0.4,     1000,      0.01,  default_exception]
                          ,'VELONS'   :[400,     0.9,        1,      0.00,  default_exception]
                          ,'VELOEW'   :[400,     0.9,        1,      0.00,  default_exception]
                          ,'UBNACC'   :[400,     0.4,     1000,      0.001,  200]
                          ,'UBLONGACC':[400,     0.6,     1000,      0.01,  10]
                          ,'UBLATACC' :[200,     0.8,     1000,      0.04,  15]
                          ,'THDG'     :[400,     0.85,        1,      0.00,  default_exception]
                          ,'TAS'      :[400,    0.7,     1000,      0.001,  default_exception]
                          ,'SDSLPC'   :[200,     0.3,     1000,      0.01,  default_exception]
                          ,'NZ1IRS'   :[200,     0.3,     1000,      0.003,  100]
                          ,'NZ1ACCFP' :[200,     0.35,     1000,      0.003,  100]
                          ,'NZ1ACC'   :[200,     0.35,     1000,      0.003,  100]
                          ,'NY1IRS'   :[200,     0.4,     1000,      0.001,  10]
                          ,'NY1ACCFP' :[400,     0.6,     1000,      0.03,  10]
                          ,'NY1ACC'   :[400,     0.5,     1000,      0.03,  20]
                          ,'NX1IRS'   :[400,     0.7,     1000,      0.01,  5]
                          ,'NX1ACC'   :[400,     0.7,     1000,      0.01,  5]
                          ,'MN'       :[400,     0.7,     1000,      0.001,  default_exception]
                          ,'IRS313'   :[400,     0.65,     1,         0.00,  default_exception]
                          ,'IALTROC'  :[400,     0.7,     1,       0.00,  default_exception]
                          ,'GYR5CF'   :[400,     0.45,     1000,      0.01,  default_exception]
                          ,'GYR4CF'   :[400,     0.5,     1000,      0.01,  default_exception]
                          ,'GYR3CF'   :[400,     0.4,     1000,      0.01,  500]
                          ,'GYR2CF'   :[400,     0.4,     1000,      0.01,  500]
                          ,'GYR1CF'   :[400,     0.3,     1000,      0.01,  default_exception]
                          ,'FPFWS'    :[400,     0.8,        1,      0.00,  0.5]
                          ,'CAS'      :[400,     0.7,     1000,      0.001,  default_exception]
                          ,'AOAC'     :[400,     0.4,     1000,      0.01,  default_exception]
                          
                          ,'ALTROC'   :[400,     0.4,     1000,      0.01,  default_exception]
                          
                          ,'ACCFWD4'  :[200,     0.3,     1000,      0.003,  100]
                          ,'ACCFWD3'  :[200,     0.3,     1000,      0.003,  100]
                          ,'ACCFWD2'  :[200,     0.3,     1000,      0.003,  100]
                          ,'ACCFWD1'  :[200,     0.5,     1000,      0.005,  10]
                          ,'ACCENGR3' :[400,     0.9,     1,      0.00,  default_exception]
                          ,'ACCENGR2' :[400,     0.9,     1,      0.00,  default_exception]
                          ,'ACCENGR1' :[400,     0.9,     1,      0.00,  default_exception]
                          ,'ACCENGL3' :[400,     0.9,     1,      0.00,  default_exception]
                          ,'ACCENGL2' :[400,     0.9,     1,      0.00,  default_exception]
                          ,'ACCENGL1' :[400,     0.9,     1,      0.00,  default_exception]
                          
                          ,'ACCAFT3'  :[200,     0.5,     1000,      0.005,  10]
                          ,'ACCAFT2'  :[200,     0.5,     1000,      0.005,  10]
                          ,'ACCAFT1'  :[200,     0.5,     1000,      0.005,  10]
                          ,'ACC6CFP'  :[200,     0.3,     1000,      0.003,  100]
                          ,'ACC5CFP'  :[200,     0.3,     1000,      0.003,  100]
                          ,'ACC4CFP' :[400,     0.6,     1000,      0.01,  10]
                          ,'ACC3CFP' :[200,     0.30,     1000,      0.003,  100]
                          
                          ,'ACC2CFP'  :[400,     0.6,     1000,      0.01,  10]
                          ,'ACC1CFP'  :[200,     0.30,     1000,      0.003,  100]
                          ,'STAB'     :[200,      0.7,      1000,      0.00,  default_exception]
                          ,'SPAC'     :[200,      0.7,         1,      0.0001,  default_exception]
                          
                          ,'SP.R7'     :[200,     0.6,        1,      0.00,  0.5]
                          ,'SP.R6'     :[200,     0.6,        1,      0.00,  0.5]
                          ,'SP.R5'     :[200,     0.6,        1,      0.00,  0.5]
                          ,'SP.R4'     :[200,     0.6,        1,      0.00,  0.5]
                          ,'SP.R3'     :[200,     0.6,        1,      0.00,  0.5]
                          
                          ,'SP.R2'     :[200,     0.6,        1,      0.00,  0.5]
                          ,'SP.R1'     :[200,     0.6,        1,      0.00,  0.5]
                          
                          ,'SP.L7'    :[200,     0.6,        1,      0.00,  0.5]
                          ,'SP.L6'    :[200,     0.6,        1,      0.00,  0.5]
                          ,'SP.L5'    :[200,     0.6,        1,      0.00,  0.5]
                          ,'SP.L4'    :[200,     0.6,        1,      0.00,  0.5]
                          ,'SP.L3'    :[200,     0.6,        1,      0.00,  0.5]
        
                          ,'SP.L2'    :[200,     0.6,        1,      0.00,  0.5]
                          ,'SP.L1'    :[200,     0.6,        1,      0.00,  0.5]
                          
                          ,'SLAT'      :[200,     0.85,         1,      0.00,  default_exception]
                          ,'SAT'       :[400,     0.5,     1000,      0.001,  default_exception]
                          ,'RUDD'     :[400,     0.5,      1000,      0,  default_exception]
                          
                          ,'RMLGUPL'   :[200,     0.9,     1,      0.00,  default_exception]
                          ,'RMLGDNL':[200,     0.9,        1,      0.00,  default_exception]
                          ,'RHGONG' :[200,     0.9,        1,      0.00, 0.5]
                          ,'RHFEXT' :[200,     0.9,        1,      0.00,  0.5]
                          ,'RALT.1' :[200,     0.7,        1,      0.0001,  default_exception]
                          ,'NLGUPL' :[200,     0.9,        1,      0.00,  default_exception]
                          ,'NLGFEXT':[200,     0.9,        1,      0.00,  0.5]
                          ,'NLGDNL' :[200,     0.9,        1,      0.00,  default_exception]
                          
                          ,'N1.2'   :[100,     0.7,     1000,      0.00,  default_exception]
                          ,'N1.1'   :[100,     0.7,     1000,      0.00,  default_exception]
                          
                          ,'LMLGUPL':[200,     0.9,        1,      0.00,  default_exception]
                          ,'LMLGDNL':[200,     0.9,        1,      0.00,  default_exception]
                          ,'LHGONG' :[200,     0.9,        1,      0.00,  0.5]
                          ,'LHFEXT' :[200,     0.9,        1,      0.00,  0.5]                          
                          ,'IALT'   :[100,     0.85,        1,      0.00,  default_exception]
                          ,'GOGNLG' :[200,     0.9,        1,      0.00,  0.5]
                          ,'FRHOBSA':[200,     0.9,        1,      0.00,  default_exception]
                          ,'FLHOBSA':[200,     0.9,        1,      0.00,  default_exception]
                          ,'FLAP.I' :[200,     0.9,        1,      0.00,  default_exception]
                          ,'ENGFAIL.2':[200,     0.9,      1,      0.00,  default_exception]
                          ,'ENGFAIL.1':[200,     0.9,      1,      0.00,  default_exception]
                          
                          ,'ELEV.R':[400,     0.3,     1000,      0.01,  default_exception]
                          ,'ELEV.L':[400,     0.3,     1000,      0.01,  default_exception]
                          
                          ,'ALT'   :[100,     0.7,     1000,       0.00,  default_exception]
                          
                          ,'AIL.RO':[400,     0.3,     1000,       0.01,  default_exception]
                          ,'AIL.RI':[400,     0.3,     1000,       0.01,  default_exception]
                          
                          ,'AIL.LO':[400,     0.3,     1000,       0.01,  default_exception]
                          ,'AIL.LI':[400,     0.3,     1000,       0.01,  default_exception]
                          
                          ,'VMOW':[200,     0.9,        1,      0.00,  default_exception]
                          ,'VLEW':[200,     0.9,        1,      0.00,  default_exception]
                          ,'VFEW':[200,     0.9,        1,      0.00,  default_exception]
                          ,'TLAV.2':[200,     0.9,      1,      0.00,  default_exception]
                          ,'TLAV.1':[200,     0.9,      1,      0.00,  default_exception]
                          ,'STKROLL.FO':[200,     0.9,      1,      0.00,  default_exception]
                          ,'STKROLL.CP':[200,     0.9,      1,      0.00,  default_exception]
                          ,'STKPTCH.FO':[200,     0.9,      1,      0.00,  default_exception]
                          ,'STKPTCH.CP':[200,     0.9,      1,      0.00,  default_exception]
                          ,'RUDDTRIM':[200,     0.9,      1,      0.00,  default_exception]
                          ,'RUDDPED':[200,     0.9,      1,      0.00,  default_exception]
                          ,'NORMALLAW':[200,     0.9,      1,      0.00,  default_exception]
                          ,'FLEVF':[200,     0.9,      1,      0.00,  default_exception]
                          ,'FLEV3':[200,     0.9,      1,      0.00,  default_exception]
                          ,'FLEV2':[200,     0.9,      1,      0.00,  default_exception]
                          ,'FLEV1':[200,     0.9,      1,      0.00,  default_exception]
                          ,'FLEV0':[200,     0.9,      1,      0.00,  default_exception]
                          ,'BFP3':[200,     0.9,      1,      0.00,  default_exception]
                          ,'BFP2':[200,     0.9,      1,      0.00,  default_exception]
                          ,'BFP1':[200,     0.9,      1,      0.00,  default_exception]
                          ,'AP2ENG':[200,     0.9,      1,      0.00,  default_exception]
                          ,'AP1ENG':[200,     0.9,      1,      0.00,  default_exception]
                          ,'FTFQ.R':[200,     0.9,      1,      0.00,  default_exception]
                          ,'FTFQ.L':[200,     0.9,      1,      0.00,  default_exception]
                          ,'FTFQ.C':[200,     0.9,      1,      0.00,  default_exception]
                          ,'FGW':[200,     0.9,      1,       0.00,  default_exception]
                          ,'FFOB':[200,     0.9,      1,      0.00,  default_exception]
                          ,'AXCG':[200,     0.9,      1,      0.00,  default_exception]
                      }
detectors_with_zero_entries=['ACCENGR3','ACCENGR2','ACCENGR1','ACCENGL3','ACCENGL2','ACCENGL1'
                             ,'RMLGDNL','RHGONG','NLGDNL'
                             ,'LMLGDNL','LHGONG'
                             ,'GOGNLG'
                             ,'ENGFAIL.2','ENGFAIL.1'
                             ,'VMOW','VLEW','VFEW'
                             ,'STKROLL.FO','STKROLL.CP'
                             ,'RUDDTRIM','RUDDPED','NORMALLAW'
                             ,'FLEVF','FLEV3','FLEV2'
                             ,'FLEV1','FLEV0'
                             ,'BFP3','BFP2','BFP1'
                             ,'AP2ENG','AP1ENG'
                         ]
detectors_with_zero_entries_file2=['ACCENGR3','ACCENGR2','ACCENGR1','ACCENGL3','ACCENGL2','ACCENGL1'
                                   ,'RMLGDNL','RHGONG','NLGDNL'
                                   ,'LMLGDNL','LHGONG'
                                   ,'GOGNLG'
                                   ,'FRHOBSA','FLHOBSA','FLAP.I'
                                   ,'ENGFAIL.2','ENGFAIL.1'
                                   ,'VMOW','VLEW','VFEW'
                                   ,'STKROLL.FO','STKROLL.CP'
                                   ,'STKPTCH.FO','STKPTCH.CP'
                                   ,'RUDDTRIM','RUDDPED'
                                   ,'FLEVF','FLEV3','FLEV2'
                                   ,'BFP3','BFP2','BFP1'
                                   ,'AP2ENG'
                                   ,'FTFQ.C'
                               ]

if file_id==1:
    ##file2 i.e file_id=1 have other sensors with zero entries
    detectors_with_zero_entries=detectors_with_zero_entries_file2
       
#Values taken from the xls file I sent to Edouard where I optimized these parameters...with
#the script process_AirBus_min_deltas.py
                                 #[framesize,core_reduction, multiple, min_delta,block]
relevant_optimal_dict ={'BLATACC' :[1000   ,0.5           ,1000     ,10        ,15], 
                        'BNACC'   :[1000   ,0.6           ,1000     ,10        ,15], 
                        'BPTCHACC':[1000   ,0.6           ,100      ,10        ,15], 
                        'BROLLACC':[1000   ,0.7           ,10       ,5         ,15], 
                        'BYAWACC' :[1000   ,0.5           ,10       ,5         ,15], 
                        'BPTCHR'  :[1000   ,0.5           ,100      ,5         ,15], 
                        'BROLLR'  :[1000   ,0.7           ,100      ,5         ,15], 
                        'BYAWR'   :[1000   ,0.6           ,100      ,5         ,15], 
                        'AOAC'    :[200    ,0.6           ,100      ,10        , 5], 
                        'SDSLPC'  :[200    ,0.5           ,100      ,10        ,10], 
                        'ELEV.R'  :[400   ,0.5           ,100      ,10        ,10], 
                        'ELEV.L'  :[400   ,0.5           ,100      ,10        ,10] 
                        }
#all_sensors=['BLATACC'] ##Test a single sensor
#all_sensors=all_sensors[:5] ##Test the first 5 sensors
#all_sensors=detectors_with_zero_entries ##Test sensors with zero entries

print 'len(all_sensors)', len(all_sensors)

def set_pre_process_exception(multiple,delta):
    pre_process = {
        "multiple": [multiple],
        #'integer': [1],
        "value_exceptions": [
            [
                {
                    "e_name": "red_peak_points",
                    "e_id":  4,
                    "v_types": ["red"],
                    "condition": "(abs(delta_v)>"+str(delta)+")",
                    "para": "delta_v"
                },
                {
                    "e_name": "rest_peak_points",
                    "e_id":  5,
                    "v_types": ["rest"],
                    "condition": "(abs(delta_v)>"+str(delta)+")",
                    "para": "str(delta_v)+'_'+str(ts)+'_'+str(v)"
                }
            ]
        ]
    }
    post_process = {
        "divide": [float(multiple)]
    }
    return pre_process, post_process


def set_pre_process_min_delta(multiple, min_delta):
    pre_process = {
        "multiple": [multiple],
        "min_delta":[min_delta]
        #'integer': [1],
    }
    post_process = {
        "divide": [float(multiple)]
    }
    return pre_process, post_process

def set_pre_process(multiple,min_delta,delta):
    pre_process = {
        "multiple": [multiple],
        #'integer': [1],
        "min_delta":[min_delta],
        "value_exceptions": [
            [
                {
                    "e_name": "red_peak_points",
                    "e_id":  4,
                    "v_types": ["red"],
                    "condition": "(abs(delta_v)>"+str(delta)+")",
                    "para": "delta_v"
                },
                {
                    "e_name": "rest_peak_points",
                    "e_id":  5,
                    "v_types": ["rest"],
                    "condition": "(abs(delta_v)>"+str(delta)+")",
                    "para": "str(delta_v)+'_'+str(ts)+'_'+str(v)"
                }
            ]
        ]
    }
    post_process = {
        "divide": [float(multiple)]
    }
    return pre_process, post_process

###############################################################    
output_file='summary_file_id_'+str(file_id)+'_output_recon.csv'

with open(output_file, 'w',0) as f_recon:
    #Output file
    writer_recon = csv.writer(f_recon,quoting=csv.QUOTE_NONNUMERIC)
    writer_recon.writerow(('file_id','npoints','sensor_id', 'sensor_name','value_name','process_id' ,'framesize','reduction',
                           'multiple','min_delta','delta(excp.)', 
                           'tss_recon_min','tss_recon_max',
                           'raw_min_value','values_recon_min',
                           'raw_max_value','values_recon_max', 
                           'values_recon.mean', 
                           'zlib_ratio', 'count_red','red_info_ratio',
                           'dev_max','dev_mean','dev_min','amplitude',
                           'relerr','mape','smape','wpe','wape','rrmse','relative_to_the_max_error','mae',
                           'file_name') )
    
    
    file_id=0 
    for file in filenamelist:    
        D = DataBank()
        data=read_airbus_sar_data.get_data(file)
        ##Add sensors data to a DataBank
        #for sensor_id in range(len(relevant)): #relevant only has 12 sensors...
        for sensor_id in range(len(all_sensors)):
            #print 'sensor_id: ', sensor_id , 'sensor_name:', relevant[sensor_id]
            print 'sensor_id: ', sensor_id , 'sensor_name:', all_sensors[sensor_id]
            
            #########Add data only for sensors with actual data
            ##Note: the read_airbus_sar_data function returns a dictionary only with certain keys (sensor_names)
            ##Those are the ones that actually have data I guess.
            #if data.has_key(relevant[sensor_id]): 
            if data.has_key(all_sensors[sensor_id]): 
                print '*************sensor_id (with entries): ', sensor_id,' ,sensor_name:', all_sensors[sensor_id]
                timestamps=data[all_sensors[sensor_id]][0] ##timestamps are in datetime format
                timestamps_in_miliseconds=[ (time.mktime(dt.timetuple()) + (dt.microsecond / 1000000.0))*1000 for dt in timestamps]
                values=data[all_sensors[sensor_id]][1]
                #Convert values to a list of lists
                values_array=[[value] for value in values]
                value_names=[all_sensors[sensor_id]]
                quantity=[units[sensor_id]]##The units are taken from the file!!
                #Add data to a Databank()                                                #This is sensor_name 
                S = D.add_sensor_data(values_array,timestamps_in_miliseconds, sensor_id, all_sensors[sensor_id], "Airbus Data Set",value_names=value_names, quantity=quantity)
                #Raw data info
                raw_data=D.get_sensor_raw_data(sensor_id)
                tss=raw_data['tss']
                count=raw_data['count']
                max=raw_data['max_v']
                min=raw_data['min_v']
            else:
                pass ########Here should I add timestamps with empty data??
        print '************************************************************************************'
        print '*********** There are ', len(D.get_sensor_list()), ' sensors in DataBank ***********'
        print '************************************************************************************'
        ##Process each sensor added to the DataBank...
        for sensor in D.get_sensor_list():
            index=sensor['index']
            dof=sensor['dof']
            sensor_id=sensor['sensor_id']
            sensor_name=sensor['sensor_name']
            
            #Raw data info
            raw_data=D.get_sensor_raw_data(sensor_id)
            tss=np.asarray(raw_data['tss'])
            values=np.asarray(raw_data['values'])
            npoints=raw_data['count']
            
            ##The next works if there dof=1
            max=raw_data['max_v'][0]
            min=raw_data['min_v'][0]
            
            #print 'max: ',max
            if npoints<=400: ##Not process sensors with less than 400 data points
                continue
            
            if npoints>1000:
                #framesize=1000 ##Note!!For file 1, I saw that for sensors with npoints~2000 framesize 1000 worked best (i.e smaller rrmse) 
                ##Optimal block Daniel saw for these sensors [BLATACC', 'BNACC', 'BPTCHACC', 'BROLLACC', 'BYAWACC', 'BPTCHR', 'BROLLR', 'BYAWR']
                ##All of which have npoints>2000
                block=15       
            else:
                ##I saw this worked best for ['AOAC', 'SDSLPC', 'ELEV.R', 'ELEV.L']
                ##If set to 400 a chucnk of the signal is cutted away for npoints=660
                #framesize=200 
                block=10
            
            framesize=all_sensors_parameters[sensor_name][0]
            reduction=all_sensors_parameters[sensor_name][1]
            multiple=all_sensors_parameters[sensor_name][2]
            min_delta=all_sensors_parameters[sensor_name][3]
            delta=all_sensors_parameters[sensor_name][4]
            
            pre_process={}
            post_process={}
            if min_delta!= 0:
                if np.abs(min)>np.abs(max): ##some sensors have negative values and the min_delta should be relative to the min
                    #print 'The maximum is negative'
                    #min_delta=np.abs(min)*0.01*multiple 
                    min_delta=np.abs(min)*min_delta*multiple 
                    print min_delta
                else:
                    #min_delta=max*0.01*multiple
                    min_delta=max*min_delta*multiple
                #pre_process,post_process=set_pre_process_min_delta(multiple, min_delta)
            
            if delta != 0:
                pass
                #pre_process,post_process=set_pre_process_exception(multiple,delta)
            pre_process,post_process=set_pre_process(multiple,min_delta,delta)
            ##Optimal parameters from a previous study playing with min_delta:
            #if sensor_name in relevant_optimal_dict.keys():
            #    framesize =relevant_optimal_dict[sensor_name][0]
            #    reduction =relevant_optimal_dict[sensor_name][1]
            #    multiple  =relevant_optimal_dict[sensor_name][2]
            #    min_delta =relevant_optimal_dict[sensor_name][3]
            #    block     =relevant_optimal_dict[sensor_name][4]
            #    pre_process,post_process=set_pre_process_min_delta(multiple, min_delta)
                
            print 'processing sensor, ',sensor_id, ' ,', sensor_name, ', framesize: ', framesize, ' ,reduction: ', reduction
            print 'block:' , block
            print 'pre_process:', pre_process
            print 'post_process:', post_process
            
            #if sensor_name not in ['UBLONGACC','UBLATACC','TAS','SDSLPC','NZ1IRS','NZ1ACCFP','NZ1ACC','NY1IRS','NY1ACCFP','NY1ACC','NX1IRS']:
            #if sensor_name not in ['UBLATACC','SDSLPC','NZ1IRS','NZ1ACCFP','NZ1ACC','NY1IRS','NY1ACC']:
            #if sensor_name not in ['UBLATACC']:
            #if sensor_name not in ['BLONGACC','ACCLONG','WS','WD',]:
            #if sensor_name not in ['WS','WD']:
            #if sensor_name not in ['NX1ACC','MN','IALTROC','GYR5CF','GYR4CF','GYR3CF','GYR2CF','GYR1CF','FPFWS','CAS','AOAC']:
            #if sensor_name not in ['GYR5CF','GYR3CF','GYR2CF','GYR1CF','AOAC']:
            #if sensor_name not in ['GYR1CF','AOAC']:
            #if sensor_name not in ['ALTROC','ACCFWD4','ACCFWD3','ACCFWD2','ACCFWD1','ACCAFT3','ACCAFT2','ACCAFT1','ACC6CFP','ACC5CFP']:
            #if sensor_name not in ['ALTROC','ACCFWD1','ACCAFT3','ACCAFT2','ACCAFT1','ACC6CFP','ACC5CFP']:
            #if sensor_name not in ['ALTROC','ACCFWD1','ACCAFT3','ACCAFT2','ACCAFT1']:
            #if sensor_name not in ['ALTROC']:
            #if sensor_name not in ['ACC4CFP','ACC3CFP','ACC2CFP','ACC1CFP','SP.R7','SP.R6','SP.R5','SP.R4','SP.R3','SP.R2' ,'SP.R1']:
            #if sensor_name not in ['SP.R7','SP.R6','SP.R5','SP.R4','SP.R3','SP.R2' ,'SP.R1']:
            #if sensor_name not in ['SAT','RUDD','ELEV.R','ELEV.L','AIL.RO','AIL.RI','AIL.LO','AIL.LI']:
            #if sensor_name not in ['RUDD','ELEV.R','ELEV.L']:
            #if sensor_name not in ['RUDD']:
             #   continue
                
            
            ##Process Data
            D.sensor_data_process(sensor_id, framesize, reduction,'cii_dct_v011', pre_process=pre_process, post_process=post_process,block=block)
            process_id=len(D.get_sensor_info(sensor_id)['processed_data'])-1
            
            #Recon data info
            recon_data=D.get_sensor_recon_data(sensor_id,process_id)
            tss_recon=np.asarray(recon_data['tss'])
            values_recon=np.asarray(recon_data['values'])
            zlib_ratio=D.get_zip_reduction_ratio(sensor_id,framesize)
            
            #Reduce info
            reduce_info=D.get_sensor_process_info(sensor_id, process_id)['reduce_info']
            count_red=reduce_info['count_red']
            red_info_ratio=reduce_info['ratio']
            
            #Deviation_info
            deviations=D.get_deviations(sensor_id,process_id)
            
            for i in range(dof):
                value_name=sensor['value_names'][i]
                values_comp=values_recon.T[i]
                dev_max=deviations['E'][i]['max']
                dev_mean=deviations['E'][i]['mean']
                dev_min=deviations['E'][i]['min']
                
                relerr=deviations['relerr'][i]
                mae=deviations['MAE'][i]
                mape=deviations['MAPE%'][i]
                smape=deviations['SMAPE%'][i]
                rrmse=deviations['RRMSE%'][i]
                wape=deviations['WAPE%'][i]
                wpe=deviations['WPE%'][i]
                
                ##A relative error measurement they want to see
                amplitude=np.abs(max-min)
                if np.abs(dev_min)>np.abs(dev_max):
                    relative_error=np.abs(dev_min/amplitude)*100 
                else:
                    relative_error=np.abs(dev_max/amplitude)*100 
                
                #####Plots
                P=Plot(D)
                suptitle=sensor_name+' ,reduction: '+str(int(red_info_ratio*100))+'%'+ ' ,rrmse:'+str(round(rrmse,1))+'%'+' ,sid:'+str(sensor_id)
                
                P.plot(sensor_id,process_id,deviation='difference',dt_format='%M:%S',save_png=True,suptitle=suptitle) 
                
                #if sensor_name in detectors_with_zero_entries:
                #    P.plot_rec_inspection(sensor_id, process_id, output='png', fft_yscale='linear', dt_format='%M:%S')
                #else:
                #    P.plot_rec_inspection(sensor_id, process_id, output='png', fft_yscale='log', dt_format='%M:%S')
                    ##Close plt figures to avoid a massive memory leak...
                plt.close('all')
                gc.collect()
                
                
                #Write output
                writer_recon.writerow((file_id,npoints,sensor_id,sensor_name, value_name,process_id ,framesize,reduction, 
                                       multiple, min_delta,delta,
                                       tss_recon.min(),tss_recon.max(),min,float("%.3f"%values_comp.min()),max, float("%.3f"%values_comp.max()), 
                                       float("%.3f"%values_comp.mean()), float("%.3f"%zlib_ratio), count_red,float("%.3f"%red_info_ratio), 
                                       dev_max,dev_mean,dev_min,amplitude,
                                       relerr,mape,smape,wpe,wape,rrmse,relative_error,mae,
                                       file))
                
                #process_id=process_id+1
        print '** Goes in file: ',file_id, ' ', file
        name='output_file_id_'+str(file_id)+'.dbk'
        D.save(name)
        file_id=file_id+1
            
