import matplotlib
matplotlib.use('Agg') ### Force matplotlib to not use any Xwindows backend.

import os, sys, time
from tk_databank import DataBank
from tk_plot import Plot
import numpy as np
import glob
import csv
import read_airbus_sar_data
import gc
import matplotlib.pyplot as plt


file_id=int(sys.argv[1])
folder='/home/angelh/Documents/DataScienceRelated/Teraki/code_tests/test_Airbus/'
#folder='/home/angel/Airbus_Data/Airbus_DataFiles/'
filenamelist=sorted(glob.glob(folder+'*.csv'),key=os.path.getsize,reverse=True)

filenamelist=[filenamelist[file_id]]#Test just the largest file
print filenamelist
#relevant = ['BLATACC'] ##Test just one sensor
relevant =           ['BLATACC', 'BNACC', 'BPTCHACC', 'BROLLACC', 'BYAWACC', 'BPTCHR', 'BROLLR', 'BYAWR', 'AOAC', 'SDSLPC', 'ELEV.R', 'ELEV.L']
#relevant = ['ELEV.R', 'ELEV.L']
#Note on blocks: Daniel values or all sensors expect ELEV.R and ELEV.L which I optimized in a separate run
block=               [15       ,  15    ,  15       ,  15       ,  15      ,  15     ,  15     ,  15    ,   5   ,  10     ,  10    , 10      ]
#Note on framesizes: Taken from the first run without any pre_processing i.w output_updated2/ 
optimal_framesizes=  [1000      , 1000  ,  1000     ,  1000     ,  1000    ,  1000   ,  1000   ,  1000  ,   200 ,  200    ,  400  , 400     ]
#Note on multiples: Optimized by looking by eye small variations...
multiple=            [1000.     , 1000. ,  100.      ,  10.      ,  10.     ,  100.   ,  100.   ,  100.  ,   100.,  100.   ,  100.  , 100.    ]

#blocks=np.arange(10,26,1)

#min_deltas=[60]
#blocks=     [15,20,25]
#blocks=     [15]
#blocks=     [10]

##Process cases
#framesizes=[200,300,400]

framesizes=[400]
#reductions=[0.6,0.65,0.7,0.75,0.8]
reductions=[0.5,0.6,0.7]
min_deltas=[0, 5, 10]
#min_deltas=np.arange(5,11,1)

#Short test
#framesizes=[400]
#reductions=[0.8]

process_cases=[]
for framesize in framesizes:
    for reduction in reductions:
        process_cases.append([framesize,reduction])
#D=[]

for file in filenamelist:
    
    data=read_airbus_sar_data.get_data(file)
    #D.append(DataBank()) #One Databank instance for a single file..in each one 13 sensors.
    D=DataBank() #One Databank instance for a single file..in each one 13 sensors.
    
    #Add sensor data to a DataBank
    for sensor_id in range(len(relevant)):
        timestamps=data[relevant[sensor_id]][0] ##timestamps are in datetime format
        timestamps_in_miliseconds=[ (time.mktime(dt.timetuple()) + (dt.microsecond / 1000000.0))*1000 for dt in timestamps]
        values=data[relevant[sensor_id]][1]
        #Convert values to a list of lists
        values_array=[[value] for value in values]
        value_names=[[relevant[sensor_id]]]
        
        #Add data to a Databank()
        #S = D.add_sensor_data(values_array,timestamps, sensor_id, relevant[sensor_id], "Airbus Data Set",value_names=value_names,quantity=relevant[sensor_id])
        S = D.add_sensor_data(values_array,timestamps_in_miliseconds, sensor_id, relevant[sensor_id], "Airbus Data Set",value_names=value_names)
        
        #Raw data info
        raw_data=D.get_sensor_raw_data(sensor_id)
        tss=raw_data['tss']
        count=raw_data['count']
        max=raw_data['max_v']
        min=raw_data['min_v']
        
        ##Print here raw_data
        
    #block=5 ##As suggested by Daniel on slack Di 26. Apr 17:08:03 CEST 2016. for ELEVR you should study other settings
    
    #min_deltas=[80]
    
    output_file='summary_'+'file_id_'+str(file_id)+'_output_recon.csv'
    
    with open(output_file, 'w',0) as f_recon:
        #Output file
        writer_recon = csv.writer(f_recon,quoting=csv.QUOTE_NONNUMERIC)
        writer_recon.writerow(('file_id','npoints','sensor_id', 'sensor_name','value_name','process_id' ,'framesize','reduction','multiple','min_delta', 
                               'tss_recon_min','tss_recon_max', 'values_recon_min','values_recon_max', 'values_recon.mean', 
                               'zlib_ratio', 'count_red','red_info_ratio',
                               'dev_max','dev_mean','dev_min',
                               'relerr','mape','wpe','rrmse','mae',
                               'file_name') )
        
        #process_id=0
        for item in process_cases:        
            #framesize = item[0] ##The optimal framesize is taken below depending on the sensor_id            
            reduction = item[1]
            
            for sensor in D.get_sensor_list():
                
                index=sensor['index']
                dof=sensor['dof']
                sensor_id=sensor['sensor_id']
                sensor_name=sensor['sensor_name']
                framesize=optimal_framesizes[sensor_id]
                
                #Raw data info
                raw_data=D.get_sensor_raw_data(sensor_id)
                tss=np.asarray(raw_data['tss'])
                values=np.asarray(raw_data['values'])
                npoints=raw_data['count']
                if npoints<framesize:
                    break
                
                ###########Pre_process
                pre_process = {
                    "multiple": [multiple[sensor_id]]
                }
                post_process = {
                    "divide": [multiple[sensor_id]]
                }
                for min_delta in min_deltas:    
                    pre_process['min_delta'] = [min_delta]
                    ##Process Data
                    D.sensor_data_process(sensor_id, framesize, reduction,'cii_dct_v011', pre_process,post_process,block[sensor_id])
                    #D.sensor_data_process(sensor_id, framesize, reduction,'cii_dct_v011', pre_process,post_process,block_n)
                    process_id=len(D.get_sensor_info(sensor_id)['processed_data'])-1
                    #print sensor_name, block_n, 'process_id: ',process_id
                    print sensor_name, ' ,process_id:', process_id, 'multiple: ',multiple[sensor_id],' ,min_delta: ', min_delta, ' ,framesize:', framesize, ' ,reduction', reduction
                    #print 'Len processed_data-1',len(D.get_sensor_info(sensor_id)['processed_data'])-1
                    #print 'process_id',process_id
                    
                    #Recon data info
                    recon_data=D.get_sensor_recon_data(sensor_id,process_id)
                    tss_recon=np.asarray(recon_data['tss'])
                    values_recon=np.asarray(recon_data['values'])
                    zlib_ratio=D.get_zip_reduction_ratio(sensor_id,framesize)
                    
                    #Reduce info
                    reduce_info=D.get_sensor_process_info(sensor_id, process_id)['reduce_info']
                    count_red=reduce_info['count_red']
                    red_info_ratio=reduce_info['ratio']
                    
                    ####Plots!                                                                                                                       
                    P=Plot(D)
                    suptitle='sensor '+str(sensor_id)+', '+sensor_name+' ,process_id:'+str(process_id)+' ,framesize: '+str(framesize)+' ,reduction: '+str(round(red_info_ratio*100,3))+'%'+'\n, min_delta: '+str(min_delta)+' ,multiple: ' +str(multiple[sensor_id])
                    #Difference plots                                                                                                                
                    P.plot(sensor_id,process_id,deviation='difference',dt_format='%M:%S',save_png=True,suptitle=suptitle)
                    #Relative Error plots
                    P.plot(sensor_id,process_id,deviation='rel_error',dt_format='%M:%S',save_png=True,suptitle=suptitle)
                    #Rec Inspection including box plots and FFT plots
                    P.plot_rec_inspection(sensor_id, process_id, output='png', fft_yscale='log', dt_format='%Y-%m-%d %H:%M:%S')
                    #Deviation_info
                    deviations=D.get_deviations(sensor_id,process_id)
                    
                    ##Clean matplotlib!!
                    plt.close('all')
                    gc.collect()
                    
                    for i in range(dof):
                        value_name=sensor['value_names'][i]
                        values_comp=values_recon.T[i]
                        dev_max=deviations['E'][i]['max']
                        dev_mean=deviations['E'][i]['mean']
                        dev_min=deviations['E'][i]['min']
                        
                        relerr=deviations['relerr'][i]
                        mape=deviations['MAPE%'][i]
                        wpe=deviations['WAPE%'][i]
                        rrmse=deviations['RRMSE%'][i]
                        mae=deviations['MAE'][i]
                        
                        #Write output
                        writer_recon.writerow((file_id,npoints,sensor_id,sensor_name, value_name,process_id ,framesize,reduction,multiple[sensor_id],min_delta, 
                                               tss_recon.min(),tss_recon.max(), float("%.3f"%values_comp.min()), 
                                               float("%.3f"%values_comp.max()), float("%.3f"%values_comp.mean()), 
                                               float("%.3f"%zlib_ratio), count_red,float("%.3f"%red_info_ratio), 
                                               float("%.3f"%dev_max),float("%.3f"%dev_mean),float("%.3f"%dev_min),
                                               relerr,mape,wpe,rrmse,mae,
                                               file))
                        
                        
                print '** File_id ',file_id,'','** Goes in sensor: ',sensor_id,'','** sensor_name: ',sensor_name,'',' process_id: ',process_id
            #process_id=process_id+1
            
    ##Save D instance to a file
    name='test_file_id_'+str(file_id)+'.dbk'
    D.save(name)
    file_id=file_id+1
    

