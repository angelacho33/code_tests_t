from tk_databank import DataBank
from tk_plot import Plot
import read_airbus_sar_data
import time, datetime

##########Short test for one sensor
relevant = ['ELEV.L']
block=     [10]
multiple=  [100]
framesizes=[400]
reductions=[0.8]
min_deltas=[5,10]

process_cases=[]
for framesize in framesizes:
    for reduction in reductions:
        process_cases.append([framesize,reduction])

file='/home/angelh/Documents/DataScienceRelated/Teraki/code_tests/test_Airbus/Decoded_SAR00074300160323122709.csv'
print file    
data=read_airbus_sar_data.get_data(file)
D=DataBank() 

################# Add sensor data to a DataBank
for sensor_id in range(len(relevant)):
    timestamps=data[relevant[sensor_id]][0] ##timestamps are in datetime format
    timestamps_in_miliseconds=[ (time.mktime(dt.timetuple()) + (dt.microsecond / 1000000.0))*1000 for dt in timestamps]
    values=data[relevant[sensor_id]][1]
    #Convert values to a list of lists
    values_array=[[value] for value in values]
    value_names=[[relevant[sensor_id]]]
    #Add data to a Databank()
    S = D.add_sensor_data(values_array,timestamps_in_miliseconds, sensor_id, relevant[sensor_id], "Airbus Data Set",value_names=value_names)
    
################ Process data
for sensor in D.get_sensor_list():
    for item in process_cases:        
        framesize = item[0]
        reduction = item[1]
        
        sensor_id=sensor['sensor_id']
        sensor_name=sensor['sensor_name']
        
        #Pre_process
        pre_process = {
            "multiple": [multiple[sensor_id]]
        }
        post_process = {
            "divide": [multiple[sensor_id]]
        }
        for min_delta in min_deltas:    
            pre_process['min_delta'] = [min_delta]
            
            ######################################   Process Data
            D.sensor_data_process(sensor_id, framesize, reduction,'cii_dct_v011', pre_process,post_process,block[sensor_id])
            sensor_info=D.get_sensor_info(sensor_id)
            process_id=len(sensor_info['processed_data'])-1
            
            P=Plot(D)
            
            suptitle='sensor '+str(sensor_id)+', '+sensor_name+' ,process_id '+str(process_id)+' ,framesize: '+str(framesize)+' ,reduction: '+str(reduction)+' ,min_delta: '+str(min_delta)+' ,multiple: '+str(multiple[sensor_id])+', May 25th Branch!'
            
            P.plot(sensor_id,process_id,deviation='difference',dt_format='%M:%S',save_png=True,suptitle=suptitle) 
            
            print sensor_name,' ,process_id: ',process_id, ',multiple: ',multiple[sensor_id],',min_delta: ' , min_delta,',framesize: ', framesize,' ,reduction', reduction

####Write files..            
D.write_sensor_data_files(0)

    

