import os, sys, time
from tk_databank import DataBank
from tk_plot import Plot
import numpy as np
import glob
import csv
import read_airbus_sar_data


folder='/home/angelh/Documents/DataScienceRelated/Teraki/code_tests/test_Airbus/'
filenamelist=sorted(glob.glob(folder+'*.csv'),key=os.path.getsize,reverse=True)

#filenamelist=[filenamelist[0]]#Test just the largest file

relevant = ['BLATACC', 'BNACC', 'BPTCHACC', 'BROLLACC', 'BYAWACC', 'BPTCHR', 'BROLLR', 'BYAWR', 'AOAC', 'SDSLPC', 'ELEV.R', 'ELEV.L']
#relevant = ['BLATACC'] ##Test just one sensor

D=[]

file_id=0
for file in filenamelist:
    
    data=read_airbus_sar_data.get_data(file)
    D.append(DataBank()) #One Databank instance for a single file..in each one 13 sensors.
    
    #Add sensor data to a DataBank
    for sensor_id in range(len(relevant)):
        timestamps=data[relevant[sensor_id]][0] ##timestamps are in datetime format
        timestamps_in_miliseconds=[ (time.mktime(dt.timetuple()) + (dt.microsecond / 1000000.0))*1000 for dt in timestamps]
        values=data[relevant[sensor_id]][1]
        #Convert values to a list of lists
        values_array=[[value] for value in values]
        value_names=[[relevant[sensor_id]]]
        
        #Add data to a Databank()
        #S = D[file_id].add_sensor_data(values_array,timestamps, sensor_id, relevant[sensor_id], "Airbus Data Set",value_names=value_names,quantity=relevant[sensor_id])
        S = D[file_id].add_sensor_data(values_array,timestamps_in_miliseconds, sensor_id, relevant[sensor_id], "Airbus Data Set",value_names=value_names)
        
        #Raw data info
        raw_data=D[file_id].get_sensor_raw_data(sensor_id)
        tss=raw_data['tss']
        count=raw_data['count']
        max=raw_data['max_v']
        min=raw_data['min_v']
        
        ##Print here raw_data
        
     
    ##Plots
    P=Plot(D[file_id])
    #P.plot(sensor_id,process_id,deviation='difference',dt_format='%M:%S',save_png=False) 
    print file_id, file
    P.print_sensor_summary()
    
    file_id=file_id+1
    

