import os, sys, time
from tk_databank import DataBank
from tk_plot import Plot
import numpy as np
import glob
import csv
import read_airbus_sar_data


folder='/home/angelh/Documents/DataScienceRelated/Teraki/code_tests/test_Airbus/'
filenamelist=sorted(glob.glob(folder+'*.csv'),key=os.path.getsize,reverse=True)

file_id=int(sys.argv[1])
filenamelist=[filenamelist[file_id]]


relevant = ['ROLL','R1GYR','Q1GYR','PTCH','P1GYR','GS','DA','BYAWR','BYAWACC','BROLLR','BROLLACC','BPTCHR'
            ,'BPTCHACC','BNACC','BLONGACC','BLATACC','ACCVER','ACCLONG','ACCLAT','WS','WD','VELONS','VELOEW','UBNACC'
            ,'UBLONGACC','UBLATACC','THDG','TAS','SDSLPC','NZ1IRS','NZ1ACCFP','NZ1ACC','NY1IRS','NY1ACCFP','NY1ACC','NX1IRS'
            ,'NX1ACC','MN','IRS313','IALTROC','GYR5CF','GYR4CF','GYR3CF','GYR2CF','GYR1CF','FPFWS','CAS','AOAC'
            ,'ALTROC','ACCFWD4','ACCFWD3','ACCFWD2','ACCFWD1','ACCENGR3','ACCENGR2','ACCENGR1','ACCENGL3','ACCENGL2','ACCENGL1','ACCAFT3'
            ,'ACCAFT2','ACCAFT1','ACC6CFP','ACC5CFP','ACC4CFP','ACC3CFP','ACC2CFP','ACC1CFP','STAB','SPAC','SP.R7','AXCG'
            ,'SP.R6','SP.R5','SP.R4','SP.R3','SP.R2','SP.R1','SP.L7','SP.L6','SP.L5','SP.L4','SP.L3','SP.L2'
            ,'SP.L1','SLAT','SAT','RUDD','RMLGUPL','RMLGDNL','RHGONG','RHFEXT','RALT.1','NLGUPL','NLGFEXT','NLGDNL'
            ,'N1.2','N1.1','LMLGUPL','LMLGDNL','LHGONG','LHFEXT','IALT','GOGNLG','FRHOBSA','FLHOBSA','FLAP.I','ENGFAIL.2'
            ,'ENGFAIL.1','ELEV.R','ELEV.L','ALT','AIL.RO','AIL.RI','AIL.LO','AIL.LI','VMOW','VLEW','VFEW'
            ,'TLAV.2','TLAV.1','STKROLL.FO','STKROLL.CP','STKPTCH.FO','STKPTCH.CP','RUDDTRIM','RUDDPED','NORMALLAW','FLEVF','FLEV3','FLEV2'
            ,'FLEV1','FLEV0','BFP3','BFP2','BFP1','AP2ENG','AP1ENG','FTFQ.R','FTFQ.L','FTFQ.C','FGW','FFOB']

print 'len(relevant): ', len(relevant)
D=[]

file_id=0
for file in filenamelist:
    
    data=read_airbus_sar_data.get_data(file)
    D.append(DataBank()) #One Databank instance for a single file..in each one 13 sensors.
    
    #Add sensors data to a DataBank
    for sensor_id in range(len(relevant)):
        print 'sensor_id: ', sensor_id , 'sensor_name:', relevant[sensor_id]
        if data.has_key(relevant[sensor_id]):
            print '*************sensor_id (with entries): ', sensor_id,'sensor_name:', relevant[sensor_id]
            timestamps=data[relevant[sensor_id]][0] ##timestamps are in datetime format
            timestamps_in_miliseconds=[ (time.mktime(dt.timetuple()) + (dt.microsecond / 1000000.0))*1000 for dt in timestamps]
            values=data[relevant[sensor_id]][1]
            #Convert values to a list of lists
            values_array=[[value] for value in values]
            value_names=[relevant[sensor_id]]
            
            #Add data to a Databank()
            #S = D[file_id].add_sensor_data(values_array,timestamps, sensor_id, relevant[sensor_id], "Airbus Data Set",value_names=value_names,quantity=relevant[sensor_id])
            S = D[file_id].add_sensor_data(values_array,timestamps_in_miliseconds, sensor_id, relevant[sensor_id], "Airbus Data Set",value_names=value_names)
        
            #Raw data info
            raw_data=D[file_id].get_sensor_raw_data(sensor_id)
            tss=raw_data['tss']
            count=raw_data['count']
            max=raw_data['max_v']
            min=raw_data['min_v']
        
            ##Print here raw_data

     
            ##Plots
            P=Plot(D[file_id])
            P.plot_raw_inspection(sensor_id, output='png', fft_yscale='log', dt_format='%M:%S')
            #P.plot(sensor_id,process_id,deviation='difference',dt_format='%M:%S',save_png=False) 
    
    print file_id, file
    P.print_sensor_summary()
    
    file_id=file_id+1
    

