import matplotlib
matplotlib.use('Agg') ### Force matplotlib to not use any Xwindows backend.

import os, sys, time, shutil
from tk_databank import DataBank
import numpy as np
import glob
import csv
import read_airbus_sar_data
import gc

import matplotlib.pyplot as plt 
from tk_plot import Plot


#Function to write a csv file with the data I need
def write_result_to_csv_file(D,Dname,file_id,file_name,output_file): ##And make plots for each sensor too!
    """
    Args:
    
    D:           DataBank Instance with processed data
    Dname:       A name of the Databank (just used to put the plots in a folder named plos_<Dname>/)
    file_id:     Id of the input file (keeps track of the file used)
    file_name:   Name of the input file (just used to keep track of the processed file and printed in the csv output file) 
    output_file: name of the output file (name of the output csv file)
    
    """
    with open(output_file, 'w',0) as f_recon:
        #Output file
        writer_recon = csv.writer(f_recon,quoting=csv.QUOTE_NONNUMERIC)
        writer_recon.writerow(('file_id','npoints','sensor_id', 'sensor_name','value_name','process_id' ,'framesize','reduction', 
                               'tss_recon_min','tss_recon_max', 'values_recon_min','values_recon_max', 'values_recon.mean', 
                               'zlib_ratio', 'count_red','red_info_ratio',
                               'dev_max','dev_mean','dev_min',
                               'relerr','mape','wpe','rrmse','mae',
                               'file_name') )
        for sensor in D.get_sensor_list():
            index=sensor['index']
            dof=sensor['dof']
            sensor_id=sensor['sensor_id']
            sensor_name=sensor['sensor_name']
            for item in D.get_sensor_process_info(sensor_id):
                process_id = item['process_id'] 
                framesize = item['framesize']
                reduction = item['reduction']
                print '****************** Writing csv file...', output_file
                print 'sensor_id', sensor_id,'****','Process_id', process_id
                
                #What I need for each DataBank instance...
                #D, sensor_id, process_id
                
                #Raw data info
                raw_data=D.get_sensor_raw_data(sensor_id)
                tss=np.asarray(raw_data['tss'])
                values=np.asarray(raw_data['values'])
                npoints=raw_data['count']
                if npoints<framesize:
                    break
                    
                #Recon data info
                recon_data=D.get_sensor_recon_data(sensor_id,process_id)
                tss_recon=np.asarray(recon_data['tss'])
                values_recon=np.asarray(recon_data['values'])
                zlib_ratio=D.get_zip_reduction_ratio(sensor_id,framesize)
                
                #Reduce info
                reduce_info=D.get_sensor_process_info(sensor_id, process_id)['reduce_info']
                #print 'reduce_info:', reduce_info
                count_red=reduce_info['count_red']
                red_info_ratio=reduce_info['ratio']
                
                if Dname=='sub_and_upsamplig':
                    ##New reduction due to sub and upsampling by a factor of two!
                    factor=(1/(1-red_info_ratio))*2
                    red_info_ratio=1-(1/factor)
            
                ####Plots!                                                                                                                       
                P=Plot(D)
                suptitle='sensor '+str(sensor_id)+', '+sensor_name+' ,process_id:'+str(process_id)+' ,framesize: '+str(framesize)+' ,reduction: '+str(round(red_info_ratio*100,3))+'%'+'\n ,'+Dname
                #Difference plots                                                                                                                
                P.plot(sensor_id,process_id,deviation='difference',dt_format='%M:%S',save_png=True,suptitle=suptitle)
                #Relative Error plots
                P.plot(sensor_id,process_id,deviation='rel_error',dt_format='%M:%S',save_png=True,suptitle=suptitle)
                #Rec Inspection including box plots and FFT plots
                P.plot_rec_inspection(sensor_id, process_id, output='png', fft_yscale='log', dt_format='%Y-%m-%d %H:%M:%S')
                
                #Deviation_info
                deviations=D.get_deviations(sensor_id,process_id)
                
                for i in range(dof):
                    value_name=sensor['value_names'][i]
                    values_comp=values_recon.T[i]
                    dev_max=deviations['E'][i]['max']
                    dev_mean=deviations['E'][i]['mean']
                    dev_min=deviations['E'][i]['min']
                    
                    relerr=deviations['relerr'][i]
                    mape=deviations['MAPE%'][i]
                    wpe=deviations['WAPE%'][i]
                    rrmse=deviations['RRMSE%'][i]
                    mae=deviations['MAE'][i]
                    
                    #Write output
                    writer_recon.writerow((file_id,npoints,sensor_id,sensor_name, value_name,process_id ,framesize,reduction, 
                                           tss_recon.min(),tss_recon.max(), float("%.3f"%values_comp.min()), 
                                           float("%.3f"%values_comp.max()), float("%.3f"%values_comp.mean()), 
                                           float("%.3f"%zlib_ratio), count_red,float("%.3f"%red_info_ratio), 
                                           float("%.3f"%dev_max),float("%.3f"%dev_mean),float("%.3f"%dev_min),
                                           relerr,mape,wpe,rrmse,mae,
                                           file))
    if not os.path.exists('./plots_'+Dname):
        os.makedirs('./plots_'+Dname)
    
    os.system('mv difference_plots/ relative_error_plots figures/ plots_'+Dname)##Move png files to a dedicated folder...
    #os.system('mv *.png plots_'+Dname)##Move png files to a dedicated folder...
    #shutil.move('*.png','plots_'+Dname) ##Move png files to a dedicated folder...
    return 0
    
                    
file_id=int(sys.argv[1])
folder='/home/angelh/Documents/DataScienceRelated/Teraki/code_tests/test_Airbus/'
#folder='/home/angel/Airbus_Data/Airbus_DataFiles/'
filenamelist=sorted(glob.glob(folder+'*.csv'),key=os.path.getsize,reverse=True)

filenamelist=[filenamelist[file_id]] 
print filenamelist

##Short Test just one sensor
#relevant = ['ELEV.R']
#block=     [10] 
#optimal_framesizes=[400]


#####################
#####################
relevant = ['BLATACC', 'BNACC', 'BPTCHACC', 'BROLLACC', 'BYAWACC', 'BPTCHR', 'BROLLR', 'BYAWR', 'AOAC', 'SDSLPC', 'ELEV.R', 'ELEV.L']
block=     [15       ,  15    ,  15       ,  15       ,  15      ,  15     ,  15     ,  15    ,   5   ,  10     ,  10     , 10      ] ##I choose 10 for the last two (ELEV.R and ELEV.l) as I saw in the rrmse plots this were the values for smaller rrmse
#Note on framesizes: Taken from the first run without any pre_processing i.w output_updated2/ 
optimal_framesizes=  [1000      , 1000  ,  1000     ,  1000     ,  1000    ,  1000   ,  1000   ,  1000  ,   200 ,  200    ,  400  , 400]
#####################
#####################

##Process cases
#framesizes=[200,500,1000]
#reductions=[0.6,0.8,0.9]   

##Second run...values suggested by Daniel
#framesizes=[100,200,300,400]      
framesizes=[400]      
#reductions=[0.6,0.65,0.7,0.75,0.8]
reductions=[0.5,0.6,0.7]

#short_test
#framesizes=[100]     
#reductions=[0.8]

#Short test
#framesizes=[100]
#reductions=[0.9]

process_cases=[]
for framesize in framesizes:
    for reduction in reductions:
        process_cases.append([framesize,reduction])
#D=[]

for file in filenamelist:
    
    data=read_airbus_sar_data.get_data(file)
    #D.append(DataBank()) #One Databank instance for a single file..in each one 13 sensors.

    D=DataBank() #One Databank instance for a single file..in each one 13 sensors.
    D_sub=DataBank() #One Databank instance for the subsampled sensors...
    D_up=DataBank() #One Databank instance for a upsampled sensors...
    
    ####################### Add sensor data to a DataBank ###############################
    for sensor_id in range(len(relevant)):
        timestamps=data[relevant[sensor_id]][0] ##timestamps are in datetime format
        timestamps_in_miliseconds=[ (time.mktime(dt.timetuple()) + (dt.microsecond / 1000000.0))*1000 for dt in timestamps]
        values=data[relevant[sensor_id]][1]
        #Convert values to a list of lists
        values_array=[[value] for value in values]
        value_names=[[relevant[sensor_id]]]
        
        ##### 1) Add raw data
        #S = D.add_sensor_data(values_array,timestamps, sensor_id, relevant[sensor_id], "Airbus Data Set",value_names=value_names,quantity=relevant[sensor_id])
        S = D.add_sensor_data(values_array,timestamps_in_miliseconds, sensor_id, relevant[sensor_id], "Airbus Data Set",value_names=value_names)
        
        ##### 2) Add subsampled data...Subsampling taken from Daniel script
        import test_subs as sub
        subs_values, subs_tss = sub.sub_sampling(values_array,timestamps_in_miliseconds) ##The default factor in this function is 2
        S2 = D_sub.add_sensor_data(subs_values, subs_tss, sensor_id, relevant[sensor_id], "Airbus Data Set",value_names=value_names)
        
        ##### 3) The D_up Databank instance is going to be used to add processed sensor data after the up sampling...but it need some raw data to add too...which for deviation calculatations should be the original data (i.e the raw data without any subsampling)
        S3=D_up.add_sensor_data(values_array,timestamps_in_miliseconds, sensor_id, relevant[sensor_id], "Airbus Data Set",value_names=value_names)
        ##
        
        ##Print here raw_data
        #Raw data info
        #raw_data=D.get_sensor_raw_data(sensor_id)
        #tss=raw_data['tss']
        #count=raw_data['count']
        #max=raw_data['max_v']
        #min=raw_data['min_v']
        
    #######################################################################################
        
        
    ###########Process
    pre_process = {
        "multiple": [100000]
    }
    post_process = {
        "divide": [100000.0]
    }
    #block=5 ##As suggested by Daniel on slack Di 26. Apr 17:08:03 CEST 2016. for ELEVR you should study other settings
    #pre_process['min_delta'] = [80]
    
    
    ##Process Data for each sensor in sensor list
    for sensor in D.get_sensor_list():
        index=sensor['index']
        sensor_id=sensor['sensor_id']
        sensor_name=sensor['sensor_name']
        
        #print '****Processing: sensor_id=', sensor_id, ' ,block:',block[index], ' ,index:', index, 'sensor_name: ', sensor_name
        
        #process_id=0 #The process_id external variable is dangerous! Below the process_id variable is extracted...
        for item in process_cases:        
            #framesize = item[0]
            framesize=optimal_framesizes[sensor_id]
            reduction = item[1]
            print '  '
            print '**** Processing: sensor_id=', sensor_id, ',index:', index, ' ,sensor_name: ', sensor_name
            print '**** block: ',block[index], ' ,framesize: ',framesize, ' ,reduction: ',reduction
            
            
            ##############       Process Raw Data (Data bank instance: D.) 
            D.sensor_data_process(sensor_id, framesize, reduction,'cii_dct_v011', pre_process,post_process,block[index])
            print ' process calls: DataBank <D>',len(D.get_sensor_process_info(sensor_id))
            # print ' len(D.get_sensor_info(sensor_id)[processed_data]', len(D.get_sensor_info(sensor_id)['processed_data'])
            
            
            ###############      Process subsampled data (Data_Bank instance: D_sub)
            raw_data=D_sub.get_sensor_raw_data(sensor_id)
            npoints=raw_data['count']
            if npoints<=framesize:
                framesize=250
            D_sub.sensor_data_process(sensor_id, framesize, reduction,'cii_dct_v011', pre_process,post_process,block[index])
            print ' process calls: DataBank <D_sub>',len(D_sub.get_sensor_process_info(sensor_id))
            
            process_id=len(D_sub.get_sensor_process_info(sensor_id))-1 ##The proces id wil be the same for all the DataBank instances!
            reduce_info=D_sub.get_sensor_process_info(sensor_id, process_id)['reduce_info']
            
            ################    Add upsampled data to a dedicated DataBank instance: D_up
            ##Add up sampled data to a dedicated DataBank...
            recon_data_sub = D_sub.get_sensor_recon_data(sensor_id, process_id)
            ups_values, ups_tss = sub.up_sampling(recon_data_sub['values'], recon_data_sub['tss'])
            
            # Meta information to be attached to the processed sensor data
            meta={
                'framesize': framesize,
                'reduction': reduction,
                'pre_process': pre_process,
                'post_process': post_process,
                'reduce_info':reduce_info #####Cauttion!!! I am assingning here the reduce info from the subsampled data to the upsampled data...is that correct??
            }
            D_up.add_processed_sensor_data(sensor_id, ups_values, ups_tss,meta=meta)
            
            #print '** File_id ',file_id,'','** Goes in sensor: ',sensor_id,'','** sensor_name: ',sensor_name,
            #' ,process calls ', len(D.get_sensor_process_info(sensor_id)),' process_id: ', process_id
            #print sensor_name, block[index]
            #process_id=process_id+1
            
    ##Save D instance to a file
    name='Processed_data_file_id_'+str(file_id)+'.dbk'
    D.save(name)
    
    name='Processed_data_file_id_'+str(file_id)+'_sub_sampled.dbk'
    D_sub.save(name)
    
    name='Processed_data_file_id_'+str(file_id)+'_up_sampled.dbk'
    D_up.save(name)     
    
    ##Print info of processed data to a csv file...and make plots! :P
    
    ##Without subsampling
    output_file1='summary_'+'file_id_'+str(file_id)+'_output_recon.csv'
    write_result_to_csv_file(D,'',file_id,file,output_file1)
    
    ##With subsampling
    output_file2='summary_'+'file_id_'+str(file_id)+'_output_recon_sub_sampling.csv'
    write_result_to_csv_file(D_sub,'subsampling',file_id,file,output_file2)
    
    ##With sub and up sampling
    output_file2='summary_'+'file_id_'+str(file_id)+'_output_recon_sub_and_up_sampling.csv'
    write_result_to_csv_file(D_up,'sub_and_upsamplig',file_id,file,output_file2)

    ###############################
    file_id=file_id+1
    

