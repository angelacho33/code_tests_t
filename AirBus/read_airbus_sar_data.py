#from tools import tk_databank
import csv
import time, datetime
import matplotlib.pyplot as plt
import matplotlib.style
import matplotlib.dates as md
matplotlib.style.use('ggplot')
import re

def num(s):
    try:
        return int(s)
    except ValueError:
        try:
            return float(s)
        except ValueError:
            return s

def transpose_dict(list_of_dicts):
    """Transform list of dictionaries into a dict of list"""
    keys = list_of_dicts[0].iterkeys()
    return {key: [d[key] for d in list_of_dicts] for key in keys}
    
def get_data(filename):
    data = []
    with open(filename, 'rb') as csvfile:
        header_reader = csv.DictReader(csvfile, delimiter=';')
        for i in range(5): header_reader.next() # skip header lines
        header = header_reader.next()
        header_values = header.values()
        fieldnames = [header_values[0], header_values[1]] + header_values[2]

        # Continue reading now the data
        data_reader = csv.DictReader(csvfile, delimiter=';', fieldnames=fieldnames)
        for i in range(2): data_reader.next() # skip header lines
        for row in data_reader:
            for key in row:
                row[key] = re.sub('\!\d', '', row[key])
                row[key] = num(row[key])
            data.append(row)

    data = transpose_dict(data)

#    print data['ELEV.R']
#    print [item for item in data['ELEV.R'] if item]
#    tmp = [item for item in zip(data['Uptime(sec)'], data['ELEV.R']) if item[1]]
#    print
#    print 

    # remove emty strings from lists and extract acrodingly timestamp for non empty entries
    keys_to_delete = []
    data_cleaned = {}
    for key in data:
        tmp = [item for item in zip(data['Uptime(sec)'], data[key]) if (item[1] or item[1]==0)]   #if item[1] also filters out e.g. [0,0,0]
        if len(tmp) > 0:
            data_cleaned[key] = map(list, zip(*tmp))

    # convert time in ms to datetime
    for key in data_cleaned:
        if data_cleaned[key] and len(data_cleaned[key][0]) > 2:                    # check weather list is empty or not
            #data_cleaned[key][0] = [datetime.datetime.fromtimestamp(float(elm) / 1000.) for elm in data_cleaned[key][0]]
#            time_delta =  #[item - data_cleaned[key][0][0] for item in data_cleaned[key][0]]
            data_cleaned[key][0] = [datetime.datetime.fromtimestamp(float(elm)) for elm in data_cleaned[key][0]]
    del data_cleaned['Uptime(sec)']

    return data_cleaned


def plot(data, keys):
    fig, ax = plt.subplots(3, 4, figsize=(10, 7.5), sharex=True)
    xfmt = md.DateFormatter('%M:%S')
    colors = {'raw':'g', 'red':'r', 'rec':'b', 'sub':'c', 'dev':'y'}
    
    ij = [[0,0], [0,1], [0,2], [0,3], [1,0], [1,1], [1,2], [1,3], [2,0], [2,1], [2,2], [2,3]]
    for l, key in enumerate(keys):
        i, j = ij[l]
        ax[i][j].plot(data[key][0], data[key][1], color=colors['raw'])
        ax[i][j].set_title(key)
        ax[i][j].xaxis.set_major_formatter(xfmt)
        
    for ax in fig.axes:
        plt.sca(ax)
        plt.xticks(rotation=30)
        
    plt.tight_layout()
    plt.show()
    
if __name__ == '__main__':
    #filename = "/home/angelh/Documents/DataScienceRelated/Teraki/code_tests/test_Airbus/Decoded_SAR00074200160209134543.csv" #Largest file
    filename = "/home/angelh/Documents/DataScienceRelated/Teraki/code_tests/test_Airbus/Decoded_SAR00074300160323122709.csv" 
    #filename = "/home/angelh/Documents/DataScienceRelated/Teraki/code_tests/test_Airbus/Decoded_SAR00074200160324071659.csv" #smallest file

    data = get_data(filename)
    
    relevant = ['BLATACC', 'BNACC', 'BPTCHACC', 'BROLLACC', 'BYAWACC', 'BPTCHR', 'BROLLR', 'BYAWR', 'AOAC', 'SDSLPC', 'ELEV.R', 'ELEV.L']
    
    plot(data, relevant)
    
#    D.add_sensor_data(data['BLATACC'][1], data['BLATACC'][0], sensor_id=0)
    
