#from tools import tk_databank
import csv
import time, datetime
import matplotlib.pyplot as plt
import matplotlib.style
import matplotlib.dates as md
matplotlib.style.use('ggplot')
import re
import sys, os
def num(s):
    try:
        return int(s)
    except ValueError:
        try:
            return float(s)
        except ValueError:
            return s

def transpose_dict(list_of_dicts):
    """Transform list of dictionaries into a dict of list"""
    keys = list_of_dicts[0].iterkeys()
    return {key: [d[key] for d in list_of_dicts] for key in keys}
    
def get_data(filename):
    data = []
    with open(filename, 'rb') as csvfile:
        header_reader = csv.DictReader(csvfile, delimiter=';')
        for i in range(5): header_reader.next() # skip header lines
        header = header_reader.next()
        header_values = header.values()
        fieldnames = [header_values[0], header_values[1]] + header_values[2]

        # Continue reading now the data
        data_reader = csv.DictReader(csvfile, delimiter=';', fieldnames=fieldnames)
        for i in range(2): data_reader.next() # skip header lines
        for row in data_reader:
            for key in row:
                row[key] = re.sub('\!\d', '', row[key])
                row[key] = num(row[key])
            data.append(row)

    data = transpose_dict(data)

#    print data['ELEV.R']
#    print [item for item in data['ELEV.R'] if item]
#    tmp = [item for item in zip(data['Uptime(sec)'], data['ELEV.R']) if item[1]]
#    print
#    print 

    # remove emty strings from lists and extract acrodingly timestamp for non empty entries
    keys_to_delete = []
    data_cleaned = {}
    for key in data:
        tmp = [item for item in zip(data['Uptime(sec)'], data[key]) if (item[1] or item[1]==0)]   #if item[1] also filters out e.g. [0,0,0]
        if len(tmp) > 0:
            data_cleaned[key] = map(list, zip(*tmp))

    # convert time in ms to datetime
    for key in data_cleaned:
        if data_cleaned[key] and len(data_cleaned[key][0]) > 2:                    # check weather list is empty or not
            #data_cleaned[key][0] = [datetime.datetime.fromtimestamp(float(elm) / 1000.) for elm in data_cleaned[key][0]]
#            time_delta =  #[item - data_cleaned[key][0][0] for item in data_cleaned[key][0]]
            data_cleaned[key][0] = [datetime.datetime.fromtimestamp(float(elm)) for elm in data_cleaned[key][0]]
    del data_cleaned['Uptime(sec)']

    return data_cleaned


def plot(data, keys,file_id,setnumber):
    fig, ax = plt.subplots(3, 4, figsize=(12, 9), sharex=True)
    xfmt = md.DateFormatter('%M:%S')
    colors = {'raw':'g', 'red':'r', 'rec':'b', 'sub':'c', 'dev':'y'}
    
    ij = [[0,0], [0,1], [0,2], [0,3], [1,0], [1,1], [1,2], [1,3], [2,0], [2,1], [2,2], [2,3]]
    for l, key in enumerate(keys):
        i, j = ij[l]
        
        if data.has_key(key):
            npoints=len(data[key][0])
            if npoints<100:
                ax[i][j].plot(data[key][0], data[key][1], color=colors['raw'],marker='o')
            else:
                ax[i][j].plot(data[key][0], data[key][1], color=colors['raw'])
            ax[i][j].xaxis.set_major_formatter(xfmt)
            ax[i][j].set_title(key+' ,npoints='+str(npoints))
        else:
            ax[i][j].set_title(key)
        
    for ax in fig.axes:
        plt.sca(ax)
        plt.xticks(rotation=30)
        
    plt.tight_layout()
    #plt.show()
    fig.savefig('AirBus_file_id_'+str(file_id)+'_set_'+str(setnumber)+'.png')
    
if __name__ == '__main__':
    
    file_id=int(sys.argv[1])
    if file_id==1:
        filename = "/home/angelh/Documents/DataScienceRelated/Teraki/code_tests/test_Airbus/Decoded_SAR00074200160209134543.csv" #Largest file
        
    if file_id==2:
        filename = "/home/angelh/Documents/DataScienceRelated/Teraki/code_tests/test_Airbus/Decoded_SAR00074300160323122709.csv" 
    
    if file_id==3:
        filename = "/home/angelh/Documents/DataScienceRelated/Teraki/code_tests/test_Airbus/Decoded_SAR00074200160324071659.csv" #smallest file

    data = get_data(filename)
    
    #relevant = ['BLATACC', 'BNACC', 'BPTCHACC', 'BROLLACC', 'BYAWACC', 'BPTCHR', 'BROLLR', 'BYAWR', 'AOAC', 'SDSLPC', 'ELEV.R', 'ELEV.L']

    relevant = ['ROLL','R1GYR','Q1GYR','PTCH','P1GYR','GS','DA','BYAWR','BYAWACC','BROLLR','BROLLACC','BPTCHR']
    plot(data, relevant,file_id=file_id,setnumber=1)
    
    relevant = ['BPTCHACC','BNACC','BLONGACC','BLATACC','ACCVER','ACCLONG','ACCLAT','WS','WD','VELONS','VELOEW','UBNACC']
    plot(data, relevant,file_id=file_id,setnumber=2)
    
    relevant = ['UBLONGACC','UBLATACC','THDG','TAS','SDSLPC','NZ1IRS','NZ1ACCFP','NZ1ACC','NY1IRS','NY1ACCFP','NY1ACC','NX1IRS']
    plot(data, relevant,file_id=file_id,setnumber=3)
    
    relevant = ['NX1ACC','MN','IRS313','IALTROC','GYR5CF','GYR4CF','GYR3CF','GYR2CF','GYR1CF','FPFWS','CAS','AOAC']
    plot(data, relevant,file_id=file_id,setnumber=4)
    
    relevant = ['ALTROC','ACCFWD4','ACCFWD3','ACCFWD2','ACCFWD1','ACCENGR3','ACCENGR2','ACCENGR1','ACCENGL3','ACCENGL2','ACCENGL1','ACCAFT3']
    plot(data, relevant,file_id=file_id,setnumber=5)

    ##CHanged AXCG that was listed at the end here...
    relevant = ['ACCAFT2','ACCAFT1','ACC6CFP','ACC5CFP','ACC4CFP','ACC3CFP','ACC2CFP','ACC1CFP','STAB','SPAC','SP.R7','AXCG']
    plot(data, relevant,file_id=file_id,setnumber=6)
    
    relevant = ['SP.R6','SP.R5','SP.R4','SP.R3','SP.R2','SP.R1','SP.L7','SP.L6','SP.L5','SP.L4','SP.L3','SP.L2']
    plot(data, relevant,file_id=file_id,setnumber=7)
    
    relevant = ['SP.L1','SLAT','SAT','RUDD','RMLGUPL','RMLGDNL','RHGONG','RHFEXT','RALT.1','NLGUPL','NLGFEXT','NLGDNL']
    plot(data, relevant,file_id=file_id,setnumber=8)
    
    relevant = ['N1.2','N1.1','LMLGUPL','LMLGDNL','LHGONG','LHFEXT','IALT','GOGNLG','FRHOBSA','FLHOBSA','FLAP.I','ENGFAIL.2']
    plot(data, relevant,file_id=file_id,setnumber=9)
    
    relevant = ['ENGFAIL.1','ELEV.R','ELEV.L','ALT','AIL.RO','AIL.RI','AIL.LO','AIL.LI','VMOW','VLEW','VFEW']
    plot(data, relevant,file_id=file_id,setnumber=10)
    
    relevant = ['TLAV.2','TLAV.1','STKROLL.FO','STKROLL.CP','STKPTCH.FO','STKPTCH.CP','RUDDTRIM','RUDDPED','NORMALLAW','FLEVF','FLEV3','FLEV2']
    plot(data, relevant,file_id=file_id,setnumber=11)
    
    relevant = ['FLEV1','FLEV0','BFP3','BFP2','BFP1','AP2ENG','AP1ENG','FTFQ.R','FTFQ.L','FTFQ.C','FGW','FFOB']
    plot(data, relevant,file_id=file_id,setnumber=12)
    
    #   D.add_sensor_data(data['BLATACC'][1], data['BLATACC'][0], sensor_id=0)
    
