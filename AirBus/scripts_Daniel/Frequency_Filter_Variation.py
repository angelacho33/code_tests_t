import numpy as np
import scipy.fftpack
import matplotlib.pyplot as plt

def DCT_Filter(data,Reduction):


    #Reduction = 0
    framesize = len(data)
 
    
    data_freq = scipy.fftpack.dct(data,norm='ortho', overwrite_x=False)
    
    data_freq_normed = [i/sum(np.abs(data_freq)) for i in np.abs(data_freq)]
    data_freq_sort = sorted(np.abs(data_freq),reverse=True)
    index_freq_sort = np.argsort(data_freq)
    index_freq_sort = index_freq_sort.tolist()
    index_freq_sort = index_freq_sort[::-1]
    data_freq_sort = data_freq_sort[::-1]

    reduced_length = int(framesize*(1-Reduction))
   
    n = 0
    filtered = np.zeros(len(data_freq))
    #Reduced_length = len(data)*Reduction
    print "This is the reduced length",reduced_length
    while n != reduced_length:
        new_loop = 0
        for i in range(len(data_freq)):
		   print "This is the data",abs(data_freq[i])
		   print "This is the maximal data",data_freq_sort[-n]
		   print "This is the index",n
		   if (abs(data_freq[i])==data_freq_sort[-n]) & (new_loop == 0):
			   #print data_freq[i]
			   #data_freq_max = data_freq[i]
			   #print "This is the new data max",data_freq_max
			   filtered[i]=data_freq[i]
			   n = n + 1
			   new_loop = 1 
	   
    arr = np.array(filtered)
    
    print "This is the filtered",data_freq
    print "This is the filtered",filtered
	   
    #data_rec = np.dot(psi_I,filtered)
    data_rec = scipy.fftpack.idct(filtered,norm='ortho', overwrite_x=False)
    
    #plt.plot(data_freq)
    #plt.plot(filtered)
    #plt.show()
    
    #plt.plot(data_freq)
    #plt.plot(filtered)
    #plt.show()


    return (data_rec)
    
#def FFT_Filter(data,Reduction):
#    framesize = len(data)
#    ls = range(len(data)) # data contains the function
#    reduced_length = int(framesize*(1-Reduction))
#    
#    freq = np.fft.fftfreq(len(data))
#    fft = np.fft.fft(data)
#    #x = freq[:len(data)/2]
#    
#    
#    fft_sort = sorted(abs(fft),reverse=True)
#    
#    #plt.plot(fft)
#    #plt.plot(fft_sort)
#    #plt.show()
#    
#    index_fft_sort = [i[0] for i in sorted(enumerate(fft), key=lambda x:x[1])]
#    
#    print "This is the length",reduced_length
#    #for i in range(reduced_length + 1, len(data)):
#    max_freq = fft[0]
#    fft_clean = []
#    k = 0
#    #while k != reduced_length:
#    #    for k in range(len(data)):
##	       if fft[k] > max_freq:
##                max_freq = fft[k]
##                fft_clean.append([k,fft[k]])
##                print fft_clean
#                #k = k + 1
#    for i in range(reduced_length + 1, len(data)):
       # if x[i] > 0.005: # cut off all frequencies higher than 0.005
#        fft[i] = 0.0
#        #fft[len(data)/2 + i] = 0.0
#    fft_filtered = [x for (y,x) in sorted(zip(index_fft_sort,fft_sort))]
#    #plt.plot(fft)
#    #plt.plot(fft_filtered)
#    #plt.show()
#    inverse = np.fft.ifft(fft)
#    inverse_filtered = np.fft.ifft(fft_filtered)
#    
#    return inverse