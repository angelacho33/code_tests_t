import os, sys, time
from tk_databank import DataBank
from tk_plot import Plot
import numpy as np
import glob
import csv
import read_airbus_sar_data


folder='/Users/danielrichart/Documents/Projects/Research/Data/Airbus/'
filenamelist=sorted(glob.glob(folder+'*.csv'),key=os.path.getsize,reverse=True)

#filenamelist=[filenamelist[0]]#Test just the largest file

relevant = ['BLATACC', 'BNACC', 'BPTCHACC', 'BROLLACC', 'BYAWACC', 'BPTCHR', 'BROLLR', 'BYAWR', 'AOAC', 'SDSLPC', 'ELEV.R', 'ELEV.L']
relevant = ['ELEV.L'] ##Test just one sensor
#relevant = range(12)

##Process cases
framesizes=[400,1000]
reductions=[0.8]   
#Short test
#framesizes=[100,200]
#reductions=[0.9]

process_cases=[]
for framesize in framesizes:
    for reduction in reductions:
        process_cases.append([framesize,reduction])
D=[]

file_id=0
for file in filenamelist:
    print "This is the file name",file
    
    data=read_airbus_sar_data.get_data(file)
    D.append(DataBank()) #One Databank instance for a single file..in each one 13 sensors.
    
    #Add sensor data to a DataBank
    for sensor_id in range(len(relevant)):
        timestamps=data[relevant[sensor_id]][0]
        #timestamps_seconds_plus_microseconds=[ time.mktime(dt.timetuple()) + (dt.microsecond / 1000000.0) for dt in timestamps]
        values=data[relevant[sensor_id]][1]
        #Convert values to a list of lists
        values_array=[[value] for value in values]
        value_names=[[relevant[sensor_id]]]
        
        #Add data to a Databank()
        S = D[file_id].add_sensor_data(values_array,timestamps, sensor_id, relevant[sensor_id], 
                                       "Airbus Data Set",value_names=value_names,quantity=relevant[sensor_id])
        #S = D[file_id].add_sensor_data(values_array,timestamps_seconds_plus_microseconds, sensor_id, relevant[sensor_id], 
        #"Airbus Data Set",value_names=value_names,quantity=relevant[sensor_id])
	   
      
        
        #Raw data info
        raw_data=D[file_id].get_sensor_raw_data(sensor_id)
        tss=raw_data['tss']
        count=raw_data['count']
        max=raw_data['max_v']
        min=raw_data['min_v']
        
        ##Print here raw_data
        
        
    ###########Process
    pre_process = {
        "multiple": [1000]
    }
    post_process = {
        "divide": [1000.0]
    }
    pre_process['min_delta'] = [80]
    block = 20
    
    output_file='summary_'+'file_id_'+str(file_id)+'_output_recon.csv'
    
    with open(output_file, 'w',0) as f_recon:
        #Output file
        writer_recon = csv.writer(f_recon,quoting=csv.QUOTE_NONNUMERIC)
        writer_recon.writerow(('file_id','npoints','sensor_id', 'sensor_name','value_name','process_id' ,'framesize','reduction', 
                               'tss_recon_min','tss_recon_max', 'values_recon_min','values_recon_max', 'values_recon.mean', 
                               'zlib_ratio', 'count_red','red_info_ratio',
                               'dev_max','dev_mean','dev_min',
                               'relerr','mape','wpe','rrmse','mae',
                               'file_name') )
    
        
        process_id=0
        for item in process_cases:        
            framesize = item[0]
            reduction = item[1]
		  
            print "This is the actual framesize",framesize
            print "This is the actual reduction",reduction
            
            for sensor in D[file_id].get_sensor_list():
                from tk_plot import Plot
                P = Plot(D[file_id])
                index=sensor['index']
                dof=sensor['dof']
                sensor_id=sensor['sensor_id']
                sensor_name=sensor['sensor_name']
                
                #Raw data info
                raw_data=D[file_id].get_sensor_raw_data(sensor_id)
                tss=np.asarray(raw_data['tss'])
                values=np.asarray(raw_data['values'])
                npoints=raw_data['count']
                if npoints<framesize:
                    break
                    
                ##Process Data
                D[file_id].sensor_data_process(sensor_id, framesize, reduction,'cii_dct_v011', pre_process,post_process,block)
                print D[file_id].get_sensor_process_info(sensor_id, process_id)['reduce_info']
                
                #Plots
                #P=Plot(D[file_id])
                P.plot(sensor_id,process_id,deviation='difference',dt_format='%M:%S') 
            
                #Recon data info
                #recon_data=D[file_id].get_sensor_recon_data(sensor_id,process_id)
                #tss_recon=np.asarray(recon_data['tss'])
                #values_recon=np.asarray(recon_data['values'])
                print "Compression ratio",D[file_id].get_zip_reduction_ratio(sensor_id,framesize)
			 
                #data_raw = D[file_id].get_sensor_raw_data(sensor_id)['values']
                #data_rec = D[file_id].get_sensor_recon_data(sensor_id,process_id)['values']
			 
                #P.plot(sensor_id,process_id,deviation='difference')
			 
                print D[file_id].get_deviations(sensor_id,process_id)
			 
                
                
			 
                #pre_process = {
                #    "multiple": [1000000, 1000000],
                #    #"integer": [1, 1]
                #}
                #post_process = {
                #    "divide": [1000000.0, 1000000.0]
                #}
                #D[file_id].sensor_data_process(sensor_id, framesize, reduction,'cii_dct_v011', pre_process,post_process,block)
                print D[file_id].get_sensor_process_info(sensor_id, process_id)['reduce_info']
                #print D[file_id].get_gps_deviations(sensor_id, 0)['mean_distance']
			 
                import test_subs as sub

                subs_values, subs_tss = sub.sub_sampling(values_array,timestamps)
                S = D[file_id].add_sensor_data(subs_values, subs_tss, sensor_id+100, "gps", "bmw_dataset_pos")
                D[file_id].sensor_data_process(sensor_id+100, framesize, reduction, 'cii_dct_v011', pre_process, post_process,block)
                recon_data = D[file_id].get_sensor_recon_data(sensor_id+100, process_id)
                ups_values, ups_tss = sub.up_sampling(recon_data['values'], recon_data['tss'])

                D[file_id].add_processed_sensor_data(sensor_id, ups_values, ups_tss)
                print D[file_id].get_sensor_process_info(sensor_id, process_id + 1 )['reduce_info']
                #print D[file_id].get_gps_deviations(8, 1)['mean_distance']
			 
			 
                
                #data_raw = [x[0] for x in data_raw]
                #data_rec = [x[0] for x in data_rec]
			 
                #print data_raw
                #print data_rec
			 
                #import matplotlib.pyplot as plt
                #plt.plot(data_raw)
                #plt.plot(data_rec)
                #plt.show()
			 
                #print "This is the compression ratio",zlib_ratio
			 
                #P.plot(sensor_id,process_id,deviation='difference')
			 
                P.plot(sensor_id,process_id + 1,deviation='difference')
                
                #Reduce info
                reduce_info=D[file_id].get_sensor_process_info(sensor_id, process_id + 1)['reduce_info']
                count_red=reduce_info['count_red']
                red_info_ratio=reduce_info['ratio']
                
                #Deviation_info
                deviations=D[file_id].get_deviations(sensor_id,process_id)
                print D[file_id].get_deviations(sensor_id,process_id)
                
                for i in range(dof):
                    value_name=sensor['value_names'][i]
                    values_comp=values_recon.T[i]
                    dev_max=deviations['E'][i]['max']
                    dev_mean=deviations['E'][i]['mean']
                    dev_min=deviations['E'][i]['min']
                    
                    relerr=deviations['relerr'][i]
                    mape=deviations['MAPE%'][i]
                    wpe=deviations['WAPE%'][i]
                    rrmse=deviations['RRMSE%'][i]
                    mae=deviations['MAE'][i]
                    
                    #Write output
                    writer_recon.writerow((file_id,npoints,sensor_id,sensor_name, value_name,process_id ,framesize,reduction, 
                                   tss_recon.min(),tss_recon.max(), float("%.3f"%values_comp.min()), 
                                           float("%.3f"%values_comp.max()), float("%.3f"%values_comp.mean()), 
                                           float("%.3f"%zlib_ratio), count_red,float("%.3f"%red_info_ratio), 
                                           float("%.3f"%dev_max),float("%.3f"%dev_mean),float("%.3f"%dev_min),
                                           relerr,mape,wpe,rrmse,mae,
                                           file))
            
            process_id=process_id+1
            print '** File_id ',file_id,'','** Goes in sensor: ',sensor_id,'','** sensor_name: ',sensor_name,'',' process_id: ',process_id
    
    file_id=file_id+1
    

