"""
Project Teraki Data Process APIs
Author: qingzhou

it provides the APIs for raw_data file, tka data file, and other files.
"""

import os, sys, time
from tk_gdfr import get_ts_value_from_file

def sub_sampling(values, tss, factor=2):
    subs_values = []
    subs_tss = []
    for i in range(len(values)):
        if (i % factor) == 0:
            subs_values.append(values[i])
            subs_tss.append(tss[i])
    return subs_values, subs_tss

def up_sampling(values, tss, factor=2):
    ups_tss = tss_up_sampling(tss, factor)
    ups_di_list = []
    for di in range(len(values[0])):
        value_di = [x[di] for x in values]
        ups_v_di = value_up_sampling(value_di, factor)
        ups_di_list.append(ups_v_di)
    ups_values = []
    for i in range(len(ups_di_list[0])):
        v = [x[i] for x in ups_di_list]
        ups_values.append(v)
    return ups_values, ups_tss

def tss_up_sampling(tss, factor=2):
    result = []
    for i in range(len(tss)):
        if (i+1) < len(tss):
            interval = 1.0 * (tss[i+1] - tss[i]) / factor
        else:
            interval = 0
        for j in range(factor):
            result.append(tss[i] + int(j*interval))
    return result

def value_up_sampling(value, factor=2):
    result = []
    for i in range(len(value)):
        if (i+1) < len(value):
            interval = 1.0 * (value[i+1] - value[i]) / factor
        else:
            interval = 0
        for j in range(factor):
            result.append(value[i] + j*interval)
    return result

def test3():
    from tk_databank import DataBank
    D = DataBank()    
    sensor_data = get_ts_value_from_file("5f2e2dda-79d8-4454-a0ba-6cb767256861_2.csv", ',', 0, 2, 1, [3,4])
    S = D.add_sensor_data(sensor_data['pos']['values'], 
        sensor_data['pos']['tss'], 8, "gps", "bmw_dataset_pos")
    pre_process = {
        "multiple": [1000000, 1000000],
        "integer": [1, 1]
    }
    post_process = {
        "divide": [1000000.0, 1000000.0]
    }
    D.sensor_data_process(8, 500, 0.9, 'cii_dct_v011', pre_process, post_process)
    print D.get_sensor_process_info(8, 0)['reduce_info']['ratio']
    print D.get_gps_deviations(8, 0)['mean_distance']

    subs_values, subs_tss = sub_sampling(sensor_data['pos']['values'], sensor_data['pos']['tss'])
    S = D.add_sensor_data(subs_values, subs_tss, 9, "gps", "bmw_dataset_pos")
    D.sensor_data_process(9, 500, 0.8, 'cii_dct_v011', pre_process, post_process)
    recon_data = D.get_sensor_recon_data(9, 0)
    ups_values, ups_tss = up_sampling(recon_data['values'], recon_data['tss'])

    D.add_processed_sensor_data(8, ups_values, ups_tss)
    print D.get_sensor_process_info(9, 0)['reduce_info']['ratio']
    print D.get_gps_deviations(8, 1)['mean_distance']


if __name__ == '__main__':
    test3()
