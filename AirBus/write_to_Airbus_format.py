from airbus_db import *

##For information here is the list I used to add AirBus data to a databank...this list is not used below
#all_sensors = ['ROLL','R1GYR','Q1GYR','PTCH','P1GYR','GS','DA','BYAWR','BYAWACC','BROLLR','BROLLACC','BPTCHR'
#               ,'BPTCHACC','BNACC','BLONGACC','BLATACC','ACCVER','ACCLONG','ACCLAT','WS','WD','VELONS','VELOEW','UBNACC'
#               ,'UBLONGACC','UBLATACC','THDG','TAS','SDSLPC','NZ1IRS','NZ1ACCFP','NZ1ACC','NY1IRS','NY1ACCFP','NY1ACC','NX1IRS'
#               ,'NX1ACC','MN','IRS313','IALTROC','GYR5CF','GYR4CF','GYR3CF','GYR2CF','GYR1CF','FPFWS','CAS','AOAC'
#               ,'ALTROC','ACCFWD4','ACCFWD3','ACCFWD2','ACCFWD1'
#               ,'ACCENGR3','ACCENGR2','ACCENGR1','ACCENGL3','ACCENGL2','ACCENGL1'
#               ,'ACCAFT3','ACCAFT2','ACCAFT1','ACC6CFP','ACC5CFP','ACC4CFP','ACC3CFP','ACC2CFP','ACC1CFP','STAB','SPAC','SP.R7'
#               ,'SP.R6','SP.R5','SP.R4','SP.R3','SP.R2','SP.R1','SP.L7','SP.L6','SP.L5','SP.L4','SP.L3','SP.L2'
#               ,'SP.L1','SLAT','SAT','RUDD'
#               ,'RMLGUPL','RMLGDNL','RHGONG','RHFEXT','RALT.1','NLGUPL','NLGFEXT','NLGDNL'
#               ,'N1.2','N1.1','LMLGUPL'
#               ,'LMLGDNL','LHGONG','LHFEXT'
#               ,'IALT'
#               ,'GOGNLG','FRHOBSA','FLHOBSA','FLAP.I','ENGFAIL.2','ENGFAIL.1'
#               ,'ELEV.R','ELEV.L','ALT','AIL.RO','AIL.RI','AIL.LO','AIL.LI'
#               ,'VMOW','VLEW','VFEW'
#               ,'TLAV.2','TLAV.1'
#               ,'STKROLL.FO','STKROLL.CP'
#               ,'STKPTCH.FO','STKPTCH.CP'
#               ,'RUDDTRIM','RUDDPED','NORMALLAW','FLEVF','FLEV3','FLEV2'
#               ,'FLEV1','FLEV0','BFP3','BFP2','BFP1','AP2ENG','AP1ENG'
#               ,'FTFQ.R','FTFQ.L','FTFQ.C','FGW','FFOB','AXCG']

##########Load databank file
D = AirBusDataBank()
dbk_file = 'output_file_id_0.dbk'
D.load(dbk_file)

#####Qiing example:
#sensor_amount = 12
#sensor_process_list = [[0, None],[1, 4],[2, 3],[3, 5],[4, 2],[5, 4],[6, 2],[7, 5],[8, 0],[9, 4],[10, 1],[11, 3]]
##################

sensor_amount=len(D.get_sensor_list())
print "sensor_ammount: ", sensor_amount

############### List needed to pass to write_airbus_data_file function below:
sensor_process_list=[] 

##List for for information only...contains the names of the sensors in the produced DataBank...
sensor_names=[] 

for sensor in D.get_sensor_list():
    sensor_id=sensor['sensor_id']
    sensor_names.append(sensor['sensor_name']) ##Array for printing information only
            
    raw_data=D.get_sensor_raw_data(sensor_id)
    npoints=raw_data['count']
    
    if npoints<=400:
        ##We decided not to process sensors with less than 400 points...so write the raw data for those sensors
        #The next  just write the raw data for each sensor
        sensor_process_list.append([sensor_id, None])
    else:
        ##Note: In the last run of the dbk file I process each sensor
        ##only once with a set of "optimal parameters" therefore process_id=0 for all of them
        process_id=0
        sensor_process_list.append([sensor_id, process_id])

    #if sensor_id==1:
    #    process_id=0
    #sensor_process_list.append[sensor_id, process_id]
##############################################################################

##sensor_names is the list of sensors in the databank
print sensor_names 
print sensor_process_list

#######Write output file....
#output_file = 'Decoded_SAR00074200160209134543_raw_from_DataBank.csv'
output_file = 'Decoded_SAR00074200160209134543_reconstructed_7_digits.csv'

##Copied from Decoded_SAR00074200160209134543.csv
ts_para = {'start_time': '13:45:08'} 

#Note: Headlined copied from file:Decoded_SAR00074200160209134543.csv
headlines = [
    "Channel:;#07 - SOMF-SAR-F (GPT DB P/N SFI53S0123S0285)",
    "Trigger:;4200 High loads on horizontal tail plane",
    ";ACID;DATEDAY;DATEMON;DATEYR;UTCHR;UTCMIN;UTCSEC;ACMS_FP;FLT;FROMTO;$FLTLEGCNT;$ACMSPN;$AIDBPN;$AIRLDBPN;C_ACTYP;C_ENG;FMASSUNIT;MSN;ZFW;ZFWCG",
    "Unit:;;day;month;year;h;min;s;;;;;;;;;;;No Unit;lb;%",
"Snapshot:;.F-WWJL!1;9!1;2!1;16!1;13!1;45!1;43!1;71!1;          !2;LFBO LFBO!1;65!1;SFI52S0122S0285!1;SFI53S0123S0285!1;SFI57S0142S0185!1;3!1;0!1;1!1;5013!1;330693.38!1;28!1",
    "Triggered: 2016-02-09 13:45:43, ACMS Flight Phase 7.1",
    "Uptime(sec);UTC;ROLL;R1GYR;Q1GYR;PTCH;P1GYR;GS;DA;BYAWR;BYAWACC;BROLLR;BROLLACC;BPTCHR;BPTCHACC;BNACC;BLONGACC;BLATACC;ACCVER;ACCLONG;ACCLAT;WS;WD;VELONS;VELOEW;UBNACC;UBLONGACC;UBLATACC;THDG;TAS;SDSLPC;NZ1IRS;NZ1ACCFP;NZ1ACC;NY1IRS;NY1ACCFP;NY1ACC;NX1IRS;NX1ACC;MN;IRS313;IALTROC;GYR5CF;GYR4CF;GYR3CF;GYR2CF;GYR1CF;FPFWS;CAS;AOAC;ALTROC;ACCFWD4;ACCFWD3;ACCFWD2;ACCFWD1;ACCENGR3;ACCENGR2;ACCENGR1;ACCENGL3;ACCENGL2;ACCENGL1;ACCAFT3;ACCAFT2;ACCAFT1;ACC6CFP;ACC5CFP;ACC4CFP;ACC3CFP;ACC2CFP;ACC1CFP;STAB;SPAC;SP.R7;SP.R6;SP.R5;SP.R4;SP.R3;SP.R2;SP.R1;SP.L7;SP.L6;SP.L5;SP.L4;SP.L3;SP.L2;SP.L1;SLAT;SAT;RUDD;RMLGUPL;RMLGDNL;RHGONG;RHFEXT;RALT.1;NLGUPL;NLGFEXT;NLGDNL;N1.2;N1.1;LMLGUPL;LMLGDNL;LHGONG;LHFEXT;IALT;GOGNLG;FRHOBSA;FLHOBSA;FLAP.I;ENGFAIL.2;ENGFAIL.1;ELEV.R;ELEV.L;ALT;AIL.RO;AIL.RI;AIL.LO;AIL.LI;VMOW;VLEW;VFEW;TLAV.2;TLAV.1;STKROLL.FO;STKROLL.CP;STKPTCH.FO;STKPTCH.CP;RUDDTRIM;RUDDPED;NORMALLAW;FLEVF;FLEV3;FLEV2;FLEV1;FLEV0;BFP3;BFP2;BFP1;AP2ENG;AP1ENG;FTFQ.R;FTFQ.L;FTFQ.C;FGW;FFOB;AXCG",
    "Unit:;;deg;No Unit;No Unit;deg;No Unit;kt;deg;deg/s;deg/s2;deg/s;deg/s2;deg/s;deg/s2;g;g;g;g;g;g;kt;deg;kt;kt;g;g;g;deg;kt;deg;No Unit;No Unit;g;No Unit;No Unit;g;No Unit;g;No Unit;deg;ft/min;deg/s;deg/s;deg/s;deg/s;deg/s;No Unit;kt;deg;ft/min;g;g;g;g;g;g;g;g;g;g;g;g;g;g;g;g;g;g;g;deg;mbar;deg;deg;deg;deg;deg;deg;deg;deg;deg;deg;deg;deg;deg;deg;deg;deg C;deg;;;;;ft;;;;%;%;;;;;ft;;deg;deg;deg;;;deg;deg;ft;deg;deg;deg;deg;;;;deg;deg;deg;deg;deg;deg;deg;deg;;;;;;;;;;;;kg;kg;kg;kg;kg;%MAC",
    "Tolerance:;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0"]

##Write airbus data_file

D.write_airbus_data_file(output_file, sensor_amount, sensor_process_list, ts_para, headlines,digits=7)
#Note: I see in the original file that the number of digits depend on the sensor..but most of them have 7 digits...








