#First test using the Teraki software
import matplotlib
matplotlib.use('Agg') ### Force matplotlib to not use any Xwindows backend

import os, sys, time
from tk_databank import DataBank
from tk_plot import Plot
import numpy as np
import gc
import tk_gdfr 

import  glob
import csv
import string

import matplotlib.pyplot as plt 
#scenario_input=int(sys.argv[1]) ##0:highway_probes, 1:innerCity ,2:rural 3:InnercityCrossing
#scenario=['highway_probes','innerCity','rural','InnercityCrossing']

##Input Files
folder='/home/angelh/Documents/DataScienceRelated/Teraki/Files/Audi_Phase_two/'
#folder='/home/angel/Files/Audi_Phase_two/'
filenamelist = sorted(glob.glob(folder+'*.csv'),key=os.path.getsize,reverse=False)
print filenamelist
print ' '

##Process cases
#framesizes=[500]
#reductions=[0.8,0.9]
#blocks=np.arange(5,30,5)

#SHort test
framesizes=[1000]
reductions=[0.8]
blocks=[15]


process_cases=[]
for framesize in framesizes:
    for reduction in reductions:
        process_cases.append([framesize,reduction])
        
output_file='summary_Audi_output_recon.csv'

D = DataBank() ##One Databank instance for different sensors...


file_id=0 
for file in filenamelist:
    #Extract the sensor_id from the name of the file..
    print 'file name:', file 
    file_strip1=string.strip(file,chars=folder+'Sensor')
    sensor_id=int(string.strip(file_strip1,chars='.csv'))
    
    #if sensor_id==630: ##Skip sensor 630 which is the largest file...
    #    continue
    ##*********************** Missing sensors:    281, 283!, 161, 163.....  Why??'*********************************************
    ##***********************Note: They are not missing...I started from the lightest file and
    ##                         jobs are still running at the server since 2 days ago........ 
    ##*********************************************
    print 'sensor_id: ', sensor_id
    
    ##Read the file
    number_of_points=10000
    sensor_data1 = tk_gdfr.get_ts_value_from_file(file, ',', 'pos', 'pos', 0, [0])
    len_data=len(sensor_data1['pos']['values'])
    tss=np.arange(0,len_data)*1000 ##Fake time stamps in ms assuming a sampling rate of 1 Hz
    
    ##Add data to a DataFrame...
    S1 = D.add_sensor_data(sensor_data1['pos']['values'][:number_of_points],tss[:number_of_points], sensor_id, "sensor_"+str(sensor_id), "Audi_dataset")
    
    file_id+=1
    
    print file_id, file, sensor_id
    
################################################################################################################
with open(output_file, 'w',0) as f_recon:
    #Output file
    writer_recon = csv.writer(f_recon,quoting=csv.QUOTE_NONNUMERIC)
    writer_recon.writerow(('file_id','npoints','sensor_id', 'sensor_name','value_name','process_id' ,'framesize','reduction', 'block', 
                           'tss_recon_min','tss_recon_max', 'values_recon_min','values_recon_max', 'values_recon.mean', 
                           'zlib_ratio', 'count_red','red_info_ratio',
                           'dev_max','dev_mean','dev_min',
                           'relerr','mape','smape','wpe','wape','rrmse','mae',
                           'file_name') )
    
    for sensor in D.get_sensor_list():
        index=sensor['index']
        dof=sensor['dof']
        sensor_id=sensor['sensor_id']
        sensor_name=sensor['sensor_name']
        
        file_id=index
        file_name=filenamelist[index]
        
        #Raw data info
        raw_data=D.get_sensor_raw_data(sensor_id)
        tss=np.asarray(raw_data['tss'])
        values=np.asarray(raw_data['values'])
        npoints=raw_data['count']
        print 'sensor_id:', sensor_id,' , npoints ', npoints
        if npoints<framesize:
            continue

        pre_process={
            #"multiple": [1],
            #'integer': [1]
        }
        post_process={
            #"divide": [1.]
        }

        ##Process Data    
        for item in process_cases:        
            framesize = item[0]
            reduction = item[1]
            
            for block in blocks:
                
                print 'Processing:', file_id, 'Goes in sensor: ', sensor_id, ' ,framesize:',framesize, ' ,reduction:', reduction, ' ,block:', block
                D.sensor_data_process(sensor_id, framesize, reduction,'cii_dct_v011', pre_process,post_process,block)
                
                process_id=len(D.get_sensor_info(sensor_id)['processed_data'])-1
                
                #Recon data info
                recon_data=D.get_sensor_recon_data(sensor_id,process_id)
                tss_recon=np.asarray(recon_data['tss'])
                values_recon=np.asarray(recon_data['values'])
                zlib_ratio=D.get_zip_reduction_ratio(sensor_id,framesize)
                
                #Reduce info
                reduce_info=D.get_sensor_process_info(sensor_id, process_id)['reduce_info']
                count_red=reduce_info['count_red']
                red_info_ratio=reduce_info['ratio']
                
                #Deviation_info
                deviations=D.get_deviations(sensor_id,process_id)
                
                for i in range(dof):
                    value_name=sensor['value_names'][i]
                    values_comp=values_recon.T[i]
                    dev_max=deviations['E'][i]['max']
                    dev_mean=deviations['E'][i]['mean']
                    dev_min=deviations['E'][i]['min']
                    
                    relerr=deviations['relerr'][i]
                    mae=deviations['MAE'][i]
                    mape=deviations['MAPE%'][i]
                    smape=deviations['SMAPE%'][i]
                    rrmse=deviations['RRMSE%'][i]
                    wape=deviations['WAPE%'][i]
                    wpe=deviations['WPE%'][i]
                    
                    #Plots
                    #suptitle='sensor '+str(sensor_id)+', '+sensor_name+', '+'framesize: '+str(framesize)+', '+'reduction: '+str(reduction)+', '+'block: '+str(block)
                    #suptitle='sid:'+str(sensor_id)+' ,'+sensor_name+' ,pid'+str(process_id)+' ,f'+str(framesize)+' ,corered'+str(round(reduction*100,2))+' ,red'+str(round(red_info_ratio*100,2))+'\n rrmse:  ('+rrmses_string+')'+' ,multiple:'+str(multiple)+' ,mindelta'+str(min_delta)+' ,delta'+str(delta)
                    suptitle='sid:'+str(sensor_id)+' ,'+sensor_name+' ,pid'+str(process_id)+' ,f'+str(framesize)+' ,corered'+str(round(reduction*100,2))+' ,red'+str(round(red_info_ratio*100,2))+'rrmse:'+str(rrmse)
                    P=Plot(D)
                    P.plot(sensor_id,process_id,deviation='difference',save_png=True,suptitle=suptitle) 
                    #clear matplotlib memory
                    plt.close('all')
                    gc.collect()
                

                    
                    #Write output
                    writer_recon.writerow((file_id,npoints,sensor_id,sensor_name, value_name,process_id ,framesize,reduction, block, 
                                           tss_recon.min(),tss_recon.max(), float("%.3f"%values_comp.min()), float("%.3f"%values_comp.max()), 
                                           float("%.3f"%values_comp.mean()), 
                                           float("%.3f"%zlib_ratio), count_red,float("%.3f"%red_info_ratio), 
                                           dev_max,dev_mean,dev_min,
                                           relerr,mape,smape,wpe,wape,rrmse,mae,
                                           file))
                    
                    #process_id=process_id+1
                    #print '** Goes in file: ',file_id,''
                    
name='output_file_id_'+str(file_id)+'.dbk'
D.save(name)

        
            
        
