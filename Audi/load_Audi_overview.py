#First test using the Teraki software
import matplotlib
matplotlib.use('Agg') ### Force matplotlib to not use any Xwindows backend

import os, sys, time
from tk_databank import DataBank
from tk_plot import Plot
import numpy as np
import gc
import tk_gdfr 

import  glob
import csv
import string

import matplotlib.pyplot as plt 
#scenario_input=int(sys.argv[1]) ##0:highway_probes, 1:innerCity ,2:rural 3:InnercityCrossing
#scenario=['highway_probes','innerCity','rural','InnercityCrossing']

##Input Files
folder='/home/angelh/Documents/DataScienceRelated/Teraki/Files/Audi_Phase_two/'
#folder='/home/angel/Files/Audi_Phase_two/'
filenamelist = sorted(glob.glob(folder+'*.csv'),key=os.path.getsize,reverse=False)
print filenamelist
print ' '

##Process cases
framesizes=[500]
reductions=[0.8,0.9]
blocks=np.arange(5,30,5)

#SHort test
#framesizes=[500]
#reductions=[0.8]
#blocks=[20]


process_cases=[]
for framesize in framesizes:
    for reduction in reductions:
        process_cases.append([framesize,reduction])
        
output_file='summary_Audi_output_recon.csv'

D = DataBank() ##One Databank instance for different sensors...


file_id=0 
for file in filenamelist:
    #Extract the sensor_id from the name of the file..
    print 'file name:', file 
    file_strip1=string.strip(file,chars=folder+'Sensor')
    sensor_id=int(string.strip(file_strip1,chars='.csv'))
    
    #if sensor_id==630: ##Skip sensor 630 which is the largest file...
    #    continue
    ##*********************** Missing sensors:    281, 283!, 161, 163.....  Why??'*********************************************
    ##***********************Note: They are not missing...I started from the lightest file and
    ##                         jobs are still running at the server since 2 days ago........ 
    ##*********************************************
    print 'sensor_id: ', sensor_id
    
    ##Read the file
    sensor_data1 = tk_gdfr.get_ts_value_from_file(file, ',', 'pos', 'pos', 0, [0])
    len_data=len(sensor_data1['pos']['values'])
    tss=np.arange(0,len_data)*1000 ##Fake time stamps in ms assuming a sampling rate of 1 Hz
    
    ##Add data to a DataFrame...
    number_of_points=10000
    S1 = D.add_sensor_data(sensor_data1['pos']['values'][:number_of_points],tss[:number_of_points], sensor_id, "sensor_"+str(sensor_id), "Audi_dataset")
    
    file_id=file_id+1    
    
    for sensor in D.get_sensor_list():
        index=sensor['index']
        dof=sensor['dof']
        sensor_id=sensor['sensor_id']
        sensor_name=sensor['sensor_name']
        
        P=Plot(D)
        P.plot_raw_inspection(sensor_id, 'png')
        
name='output_file_id_'+str(file_id)+'.dbk'
D.save(name)

        
            
        
