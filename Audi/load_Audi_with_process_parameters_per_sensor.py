#First test using the Teraki software
import matplotlib
matplotlib.use('Agg') ### Force matplotlib to not use any Xwindows backend

import os, sys, time
from tk_databank import DataBank
from tk_plot import Plot
import numpy as np
import gc
import tk_gdfr 

import  glob
import csv
import string


import matplotlib.pyplot as plt 

def set_pre_process(multiple=1,min_delta=0,delta=1e15,tss_differences=0):
    #Defines the pre and post process parameters.
    #output: pre_process and post_process_dictionaries
    
    print "len(multiple)",len(multiple)
    print "len(min_delta)",len(min_delta)
    print "len(delta)",len(delta)
    pre_process = {
        "multiple": multiple,
        #'integer': [1],
        "min_delta":min_delta,
        #"ts_exp_threshold": [tss_differences-500,tss_differences+500],
    }
    ##Exception detection for each dof as specified in the input array delta
    pre_process["value_exceptions"]=[]
    for delta_dof in delta:
        pre_process["value_exceptions"].append(
            [
                {
                    "e_name": "red_peak_points",
                    "e_id":  4,
                    "v_types": ["red"],
                    "condition": "(abs(delta_v)>"+str(delta_dof)+")",
                    "para": "delta_v"
                },
                {
                    "e_name": "rest_peak_points",
                    "e_id":  5,
                    "v_types": ["rest"],
                    "condition": "(abs(delta_v)>"+str(delta_dof)+")",
                    "para": "str(delta_v)+'_'+str(ts)+'_'+str(v)"
                }
            ]
        )

    post_process = {
        "divide": [float(multiple[0])]
    }

    return pre_process,post_process

def set_process_parameters():
    default_delta=1e15 ##As suggested by qing set to a very high value not to have exception detections
    block=15
    #                       'sensor_name:[framesize,reduction ,multiple, min_delta        ,delta              , block] 
    all_sensors_parameters = {"sensor_1":[1000  ,0.80       ,[1]       ,[0]            ,[default_delta],      block],
                              "sensor_2":[1000  ,0.90       ,[1]       ,[0]            ,[default_delta],      block],
                              "sensor_3":[1000  ,0.80       ,[1]       ,[0]            ,[default_delta],      block],
                              "sensor_4":[1000  ,0.90       ,[1]       ,[0]            ,[default_delta],      block],
                              "sensor_5":[1000  ,0.80       ,[1]       ,[0]            ,[default_delta],      block],
                              "sensor_6":[1000  ,0.80       ,[1]       ,[50]            ,[1000],      block],
                              "sensor_88":[1000  ,0.60       ,[1]       ,[3]            ,[default_delta],      block],
                              "sensor_91":[1000  ,0.80       ,[1]       ,[0]            ,[default_delta],      block],
                              "sensor_159":[1000  ,0.95       ,[1]       ,[0]           ,[1],                block],
                              "sensor_161":[1000  ,0.75       ,[1]       ,[0]           ,[100],                block],
                              "sensor_163":[1000  ,0.75       ,[1]       ,[0]           ,[5],                  block],
                              "sensor_281":[2000  ,0.70       ,[1000]    ,[50000]        ,[1000000],               block],
                              "sensor_283":[1000  ,0.85       ,[1000]    ,[2000]          ,[2000],              block],
                              "sensor_289":[2000  ,0.80       ,[1]       ,[2]            ,[0.5],      block],
                              "sensor_298":[1000  ,0.95       ,[1]       ,[1]            ,[0.5],      block],
                              "sensor_299":[1000  ,0.80       ,[1]       ,[2]            ,[30],      block],
                              "sensor_343":[1000  ,0.50       ,[1]       ,[1]            ,[1],      block],
                              "sensor_547":[1000  ,0.95       ,[1]       ,[0]            ,[0.5],      block],
                              "sensor_630":[1000  ,0.90       ,[1]       ,[0]            ,[10000],      block],
                              "sensor_660":[1000  ,0.30       ,[1000]    ,[500]          ,[1000],      block],
                              "sensor_670":[1000  ,0.70       ,[1]       ,[2]            ,[default_delta],      block],
                              "sensor_686":[1000  ,0.95       ,[1]       ,[0]            ,[0.8],      block],
                              "sensor_687":[1000  ,0.50       ,[1]       ,[0]            ,[1],      block],
                              "sensor_767":[1000  ,0.85       ,[1]       ,[10]           ,[50],      block],
                              "sensor_768":[1000  ,0.80       ,[1]       ,[2]            ,[2],      block],
                              "sensor_769":[1000  ,0.80       ,[1]       ,[0]            ,[default_delta],      block],
                              "sensor_835":[2000  ,0.95       ,[1]       ,[0]            ,[10],      block]
                          }
    
    return all_sensors_parameters
#scenario_input=int(sys.argv[1]) ##0:highway_probes, 1:innerCity ,2:rural 3:InnercityCrossing
#scenario=['highway_probes','innerCity','rural','InnercityCrossing']

##Input Files
#folder='/home/angelh/Documents/DataScienceRelated/Teraki/Files/Audi_Phase_two/'
folder='/home/angel/Files/Audi_Phase_two/'
filenamelist = sorted(glob.glob(folder+'*.csv'),key=os.path.getsize,reverse=False)
print filenamelist
print ' '

##Process cases
#framesizes=[500]
#reductions=[0.8,0.9]
#blocks=np.arange(5,30,5)

#SHort test
framesizes=[1000]
reductions=[0.8]
blocks=[15]


process_cases=[]
for framesize in framesizes:
    for reduction in reductions:
        process_cases.append([framesize,reduction])
        
output_file='summary_Audi_output_recon.csv'

D = DataBank() ##One Databank instance for different sensors...


file_id=0 
for file in filenamelist:
    #Extract the sensor_id from the name of the file..
    print 'file name:', file 
    file_strip1=string.strip(file,chars=folder+'Sensor')
    sensor_id=int(string.strip(file_strip1,chars='.csv'))
    
    #if sensor_id==630: ##Skip sensor 630 which is the largest file...
    #    continue
    ##*********************** Missing sensors:    281, 283!, 161, 163.....  Why??'*********************************************
    ##***********************Note: They are not missing...I started from the lightest file and
    ##                         jobs are still running at the server since 2 days ago........ 
    ##*********************************************
    print 'sensor_id: ', sensor_id
    
    ##Read the file
    number_of_points=10000
    sensor_data1 = tk_gdfr.get_ts_value_from_file(file, ',', 'pos', 'pos', 0, [0])
    len_data=len(sensor_data1['pos']['values'])
    tss=np.arange(0,len_data)*1000 ##Fake time stamps in ms assuming a sampling rate of 1 Hz
    
    ##Add data to a DataFrame...
    S1 = D.add_sensor_data(sensor_data1['pos']['values'][:number_of_points],tss[:number_of_points], sensor_id, "sensor_"+str(sensor_id), "Audi_dataset")
    
    file_id+=1
    
    print file_id, file, sensor_id
    
################################################################################################################
with open(output_file, 'w',0) as f_recon:
    #Output file
    writer_recon = csv.writer(f_recon,quoting=csv.QUOTE_NONNUMERIC)
    writer_recon.writerow(('file_id','npoints','sensor_id', 'sensor_name','value_name','process_id' ,'framesize','reduction',
                           'multiple','min_delta','delta(excp.)', 
                           'tss_recon_min','tss_recon_max',
                           'raw_min_value','values_recon_min',
                           'raw_max_value','values_recon_max',
                           'raw_amplitude',
                           'values_recon.mean', 
                           'zlib_ratio', 'count_red','red_info_ratio',
                           'dev_max','dev_mean','dev_min',
                           'relerr','mape','smape',
                           'wpe','wape','rrmse',
                           'relative_to_the_max_error','mae'
                           #,'file_name'
                    ))
    
    for sensor in D.get_sensor_list():
        index=sensor['index']
        dof=sensor['dof']
        sensor_id=sensor['sensor_id']
        sensor_name=sensor['sensor_name']
        
        file_id=index
        file_name=filenamelist[index]
        
        #Raw data info
        raw_data=D.get_sensor_raw_data(sensor_id)
        tss=np.asarray(raw_data['tss'])
        values=np.asarray(raw_data['values'])
        npoints=raw_data['count']
        print 'sensor_id:', sensor_id,' , npoints ', npoints
        if npoints<framesize:
            continue

        all_sensors_parameters=set_process_parameters()
        
        framesize=all_sensors_parameters[sensor_name][0]
        reduction=all_sensors_parameters[sensor_name][1]
        multiple=all_sensors_parameters[sensor_name][2]
        min_delta=all_sensors_parameters[sensor_name][3]
        delta=all_sensors_parameters[sensor_name][4]
        block=all_sensors_parameters[sensor_name][5]
        
        tss_differences_min=0
        
        pre_process,post_process=set_pre_process(multiple=multiple,min_delta=min_delta,delta=delta,tss_differences=tss_differences_min)
        
        if sensor_id in [88,298,343,660,670]:
            pre_process['integer']=[1]
       
        print '***********'
        print "Processing sensor:", sensor_id, sensor_name
        print "framesize: ",framesize
        print "reduction: ",reduction
        print "pre_process: ",pre_process
        print "post_process: ",post_process
        print "block: ",block
        print '...'
        
        #print 'Processing:', file_id, 'Goes in sensor: ', sensor_id, ' ,framesize:',framesize, ' ,reduction:', reduction, ' ,block:', block
        
        process_id=D.sensor_data_process(sensor_id, framesize, reduction,'cii_dct_v011', pre_process,post_process,block,raw_tss=True)
        
        #process_id=len(D.get_sensor_info(sensor_id)['processed_data'])-1
        
        #Recon data info
        recon_data=D.get_sensor_recon_data(sensor_id,process_id)
        tss_recon=np.asarray(recon_data['tss'])
        values_recon=np.asarray(recon_data['values'])
        zlib_ratio=D.get_zip_reduction_ratio(sensor_id,framesize)
        
        #Reduce info
        reduce_info=D.get_sensor_process_info(sensor_id, process_id)['reduce_info']
        count_red=reduce_info['count_red']
        red_info_ratio=reduce_info['ratio']
        
        #Deviation_info
        deviations=D.get_deviations(sensor_id,process_id)
        
        rrmses=[]
        rrmses_string=""
        for i in range(dof):
            
            max=raw_data['max_v'][i]
            min=raw_data['min_v'][i]
            
            value_name=sensor['value_names'][i]
            values_comp=values_recon.T[i]
            dev_max=deviations['E'][i]['max']
            dev_mean=deviations['E'][i]['mean']
            dev_min=deviations['E'][i]['min']
            
            ##A relative error measurement they want to see
            amplitude=np.abs(max-min)
            if np.abs(dev_min)>np.abs(dev_max):
                relative_error=np.abs(dev_min/amplitude)*100 
            else:
                relative_error=np.abs(dev_max/amplitude)*100 
            ###
                
            relerr=deviations['relerr'][i]
            mae=deviations['MAE'][i]
            mape=deviations['MAPE%'][i]
            smape=deviations['SMAPE%'][i]
            rrmse=deviations['RRMSE%'][i]
            wape=deviations['WAPE%'][i]
            wpe=deviations['WPE%'][i]
            
            rrmses.append(rrmse)
            rrmses_string=rrmses_string+str(round(rrmse,1))+"%"
            if i<dof-1:
                rrmses_string=rrmses_string+","
            else:
                rrmses_string=rrmses_string
                
            #Plots
            #suptitle='sensor '+str(sensor_id)+', '+sensor_name+', '+'framesize: '+str(framesize)+', '+'reduction: '+str(reduction)+', '+'block: '+str(block)
            suptitle='sid:'+str(sensor_id)+' ,'+sensor_name+' ,pid'+str(process_id)+' ,f'+str(framesize)+' ,corered'+str(round(reduction*100,2))+' ,red'+str(round(red_info_ratio*100,2))+'\n rrmse:  ('+rrmses_string+')'+' ,multiple:'+str(multiple)+' ,mindelta'+str(min_delta)+' ,delta'+str(delta)
            #suptitle='sid:'+str(sensor_id)+' ,'+sensor_name+' ,pid'+str(process_id)+' ,f'+str(framesize)+' ,corered'+str(round(reduction*100,2))+' ,red'+str(round(red_info_ratio*100,2))+'rrmse:'+str(rrmse)
            P=Plot(D)
            P.plot(sensor_id,process_id,deviation='difference',save_png=True,suptitle=suptitle) 
            #clear matplotlib memory
            plt.close('all')
            gc.collect()
            
            
            #Write output
            writer_recon.writerow((file_id,npoints,sensor_id,sensor_name, value_name,process_id ,framesize,reduction, 
                                   multiple[i], min_delta[i],delta[i],
                                   tss_recon.min(),tss_recon.max(),
                                   min,float("%.3f"%values_comp.min()),
                                   max, float("%.3f"%values_comp.max()),
                                   amplitude,
                                   float("%.3f"%values_comp.mean()), 
                                   float("%.3f"%zlib_ratio), count_red,float("%.3f"%red_info_ratio), 
                                   dev_max,dev_mean,dev_min,
                                   relerr,mape,smape,
                                   wpe,wape,rrmse,
                                   relative_error,mae
                                   #,filename
                               ))   
            
            #process_id=process_id+1
            #print '** Goes in file: ',file_id,''
            
    name='output_file_id_'+'all'+'.dbk'
    D.save(name)

        
            
        
