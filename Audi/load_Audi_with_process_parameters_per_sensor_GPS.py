#First test using the Teraki software
#import matplotlib
#matplotlib.use('Agg') ### Force matplotlib to not use any Xwindows backend

import os, sys, time
from tk_databank import DataBank
from tk_plot import Plot
import numpy as np
import gc
import tk_gdfr 

import  glob
import csv
import string

import tk_err as tk_err
import tk_gdfr_New as tk_gdfr_New ##Script that Daniel sent me...uses one function from there that I think Xao wrote to plot produce the google maps

import matplotlib.pyplot as plt 

def set_pre_process(multiple=1,min_delta=0,delta=1e15,tss_differences=0):
    #Defines the pre and post process parameters.
    #output: pre_process and post_process_dictionaries
    
    print "len(multiple)",len(multiple)
    print "len(min_delta)",len(min_delta)
    print "len(delta)",len(delta)
    pre_process = {
        "multiple": multiple,
        #'integer': [1],
        "min_delta":min_delta,
        #"ts_exp_threshold": [tss_differences-500,tss_differences+500],
    }
    ##Exception detection for each dof as specified in the input array delta
    pre_process["value_exceptions"]=[]
    for delta_dof in delta:
        pre_process["value_exceptions"].append(
            [
                {
                    "e_name": "red_peak_points",
                    "e_id":  4,
                    "v_types": ["red"],
                    "condition": "(abs(delta_v)>"+str(delta_dof)+")",
                    "para": "delta_v"
                },
                {
                    "e_name": "rest_peak_points",
                    "e_id":  5,
                    "v_types": ["rest"],
                    "condition": "(abs(delta_v)>"+str(delta_dof)+")",
                    "para": "str(delta_v)+'_'+str(ts)+'_'+str(v)"
                }
            ]
        )

    post_process = {
        "divide": multiple
    }

    return pre_process,post_process

###########################################################################
#Pre process parameters for GPS in the new processing
distance_pre_process = {
    "multiple": [1000],
    "integer": [1],
    "value_exceptions": [
        [
            {
                "e_name": "red_peak_points",
                "e_id":  4,
                "v_types": ["red"],
                "condition": "(abs(delta_v)>5000)",
                "para": "delta_v"
            },
            {
                "e_name": "rest_peak_points",
                "e_id":  5,
                "v_types": ["rest"],
                "condition": "(abs(delta_v)>5000)",
                "para": "str(delta_v)+'_'+str(ts)+'_'+str(v)"
            }
        ] 
    ]
}
distance_post_process = {
    #    "range_min": [0],
    "divide": [1000.0]
}
###########################################################################

def set_process_parameters():
    default_delta=1e15 ##As suggested by qing set to a very high value not to have exception detections
    block=15
    ########################NOTE!: sensor_0 is the GPS data Daniel sent me in the new format
    #                       'sensor_name:[framesize,reduction ,multiple, min_delta        ,delta              , block]
    all_sensors_parameters = {"GPS_raw":[800  ,0.80       ,[1,1]     ,[0,0]          ,[default_delta,default_delta],      block],
                              "GPS_cleaned":[800  ,0.80       ,[1,1]     ,[0,0]          ,[default_delta,default_delta],      block],
                              "sensor_1":[1000  ,0.80       ,[1]       ,[0]            ,[default_delta],      block],
                              "sensor_2":[1000  ,0.90       ,[1]       ,[0]            ,[default_delta],      block],
                              "sensor_3":[1000  ,0.80       ,[1]       ,[0]            ,[default_delta],      block],
                              "sensor_4":[1000  ,0.90       ,[1]       ,[0]            ,[default_delta],      block],
                              "sensor_5":[1000  ,0.80       ,[1]       ,[0]            ,[default_delta],      block],
                              "sensor_6":[2000  ,0.90       ,[1]       ,[0]            ,[2000],      block],
                              "sensor_88":[1000  ,0.60       ,[1]       ,[2]            ,[default_delta],      block],
                              "sensor_91":[1000  ,0.80       ,[1]       ,[0]            ,[default_delta],      block],
                              "sensor_159":[1000  ,0.95       ,[1]       ,[0]           ,[1],                block],
                              "sensor_161":[1000  ,0.75       ,[1]       ,[0]           ,[100],                block],
                              "sensor_163":[1000  ,0.75       ,[1]       ,[0]           ,[5],                  block],
                              "sensor_281":[2000  ,0.85       ,[1]       ,[10]          ,[2000],               block],
                              "sensor_283":[1000  ,0.90       ,[1]       ,[1]           ,[5],                 block],
                              "sensor_289":[1000  ,0.90       ,[1]       ,[1]            ,[1],      block],
                              "sensor_298":[1000  ,0.60       ,[1]       ,[1]            ,[2],      block],
                              "sensor_299":[1000  ,0.80       ,[1]       ,[2]            ,[30],      block],
                              "sensor_343":[1000  ,0.60        ,[1]       ,[1]          ,[1],      block],
                              "sensor_547":[1000  ,0.95       ,[1]       ,[0]            ,[1],      block],
                              "sensor_630":[1000  ,0.90       ,[1]       ,[0]            ,[10000],      block],
                              "sensor_660":[1000  ,0.10       ,[1]       ,[1]            ,[2],      block],
                              "sensor_670":[1000  ,0.90       ,[1]       ,[0]            ,[5],      block],
                              "sensor_686":[1000  ,0.95       ,[1]       ,[0]            ,[0.8],      block],
                              "sensor_687":[1000  ,0.70       ,[1]       ,[0]            ,[2],      block],
                              "sensor_767":[1000  ,0.85       ,[1]       ,[10]           ,[50],      block],
                              "sensor_768":[1000  ,0.80       ,[1]       ,[1]            ,[3],      block],
                              "sensor_769":[1000  ,0.80       ,[1]       ,[0]            ,[default_delta],      block],
                              "sensor_835":[2000  ,0.95       ,[1]       ,[0]            ,[10],      block]
                          }
    
    return all_sensors_parameters
#scenario_input=int(sys.argv[1]) ##0:highway_probes, 1:innerCity ,2:rural 3:InnercityCrossing
#scenario=['highway_probes','innerCity','rural','InnercityCrossing']

##Input Files
folder='/home/angelh/Documents/DataScienceRelated/Teraki/Files/Audi_Phase_two/'
#folder='/home/angel/Files/Audi_Phase_two/'
#filenamelist = sorted(glob.glob(folder+'*.csv'),key=os.path.getsize,reverse=False)
filenamelist=[folder+'Audi_Sensor_LatLon_New_Conv.csv']
print filenamelist
print ' '

##Process cases
#framesizes=[500]
#reductions=[0.8,0.9]
#blocks=np.arange(5,30,5)

#SHort test
framesizes=[1000]
reductions=[0.8]
blocks=[15]


process_cases=[]
for framesize in framesizes:
    for reduction in reductions:
        process_cases.append([framesize,reduction])
        
output_file='summary_Audi_output_recon.csv'

D = DataBank() ##One Databank instance for different sensors...


file_id=0 
for file in filenamelist:
    #Extract the sensor_id from the name of the file..
    print 'file name:', file 
    #file_strip1=string.strip(file,chars=folder+'Sensor')
    #sensor_id=int(string.strip(file_strip1,chars='.csv'))
    sensor_id=1 ##Cannot use 0 for GPS since internally there is a sensor_id=sensor_id*100
    
    #if sensor_id==630: ##Skip sensor 630 which is the largest file...
    #    continue
    ##*********************** Missing sensors:    281, 283!, 161, 163.....  Why??'*********************************************
    ##***********************Note: They are not missing...I started from the lightest file and
    ##                         jobs are still running at the server since 2 days ago........ 
    ##*********************************************
    print 'sensor_id: ', sensor_id
    
    ##Read the file
    #number_of_points=10000
    number_of_points=1000
    #sensor_data1 = tk_gdfr.get_ts_value_from_file(file, ',', 'pos', 'pos', 0, [0])
    sensor_data1 = tk_gdfr.get_ts_value_from_file(file, ';', 'pos', 'pos', 0, [5,4])
    len_data=len(sensor_data1['pos']['values'])
    tss=np.arange(0,len_data)*1000 ##Fake time stamps in ms assuming a sampling rate of 1 Hz
    
    ##Add data to a DataFrame...
    sensor_name='GPS_raw'
    value_names=['Longitude','Latitude']
    #sensor_name="sensor_"+str(sensor_id)
    gps=sensor_data1['pos']['values'][:number_of_points]
    timestamps_gps=tss[:number_of_points]
    
    S1 = D.add_sensor_data(gps,timestamps_gps, sensor_id, sensor_name, "Audi_dataset",value_names=value_names)
    
    print 'GPS_raw data first 10 points:',sensor_data1['pos']['values'][:10] 
    
    #######################################################################
    cleaned_gps_coordinates=[]
    cleaned_timestamps_gps=[]
    for i in range(len(gps)):
    #for i in range(10): #test
        longitude=gps[i][0]
        latitude=gps[i][1]
        timestamp=timestamps_gps[i]
        if i+1<len(gps):
            difference_longitude=gps[i+1][0]-gps[i][0]
            print difference_longitude
        if (latitude!=0 or longitude!=0) and (difference_longitude!=0): ##Getting rid of zero entries and entries with equal lon, lat
            #if (latitude!=0 or longitude!=0):
            cleaned_gps_coordinates.append([longitude,latitude])
            cleaned_timestamps_gps.append(timestamp)
        #print 'cleaned gps data: ', [timestamp,longitude,latitude]
    
    value_names=['Longitude','Latitude']
    #print timestamps_gps[50]-timestamps_gps[0]
    sensor_id_GPS=sensor_id+1 ##After all the sensors were added above here add a new sensor with both GPS coordinates
    sensor_name='GPS_cleaned'
    #D.add_sensor_data(gps_coordinates[:number_of_points],timestamps_gps[:number_of_points], sensor_id, 'GPS', 'GPS',value_names=value_names)
    print 'len(cleaned_gps_coordinates): ',len(cleaned_gps_coordinates)
    print 'first cleaned gps_coordinates: ',cleaned_gps_coordinates[:10]
    print 'len(cleaned gps_coordinates[:number_of_points]): ',len(cleaned_gps_coordinates[:number_of_points])
    D.add_sensor_data(cleaned_gps_coordinates[:number_of_points],cleaned_timestamps_gps[:number_of_points], sensor_id_GPS, sensor_name, 'Audi Dataset',value_names=value_names)
        
    ########################################################################
    file_id+=1
    
    print file_id, file, sensor_id
    print '************************************************************************************'
    print '*********** There are ', len(D.get_sensor_list()), ' sensors in DataBank ***********'
    print '************************************************************************************'

################################################################################################################
with open(output_file, 'w',0) as f_recon:
    #Output file
    writer_recon = csv.writer(f_recon,quoting=csv.QUOTE_NONNUMERIC)
    writer_recon.writerow(('file_id','npoints','sensor_id', 'sensor_name','value_name','process_id' ,'framesize','reduction',
                           'multiple','min_delta','delta(excp.)', 
                           'tss_recon_min','tss_recon_max',
                           'raw_min_value','values_recon_min',
                           'raw_max_value','values_recon_max',
                           'raw_amplitude',
                           'values_recon.mean', 
                           'zlib_ratio', 'count_red','red_info_ratio',
                           'dev_max','dev_mean','dev_min',
                           'relerr','mape','smape',
                           'wpe','wape','rrmse',
                           'relative_to_the_max_error','mae'
                           #,'file_name'
                    ))
    
    for sensor in D.get_sensor_list():
        index=sensor['index']
        dof=sensor['dof']
        sensor_id=sensor['sensor_id']
        sensor_name=sensor['sensor_name']
        
        #if sensor_name!='GPS_raw':
        if sensor_name not in ['GPS_raw','GPS_cleaned']:
            continue
        file_id=index
        #file_name=filenamelist[index]
        
        
        #Raw data info
        raw_data=D.get_sensor_raw_data(sensor_id)
        tss=np.asarray(raw_data['tss'])
        values=np.asarray(raw_data['values'])
        npoints=raw_data['count']
        print 'sensor_id:', sensor_id,' ,sensor_name: ',sensor_name,' , npoints ', npoints
        
        all_sensors_parameters=set_process_parameters()
        
        framesize=all_sensors_parameters[sensor_name][0]
        reduction=all_sensors_parameters[sensor_name][1]
        multiple=all_sensors_parameters[sensor_name][2]
        min_delta=all_sensors_parameters[sensor_name][3]
        delta=all_sensors_parameters[sensor_name][4]
        block=all_sensors_parameters[sensor_name][5]
        
        tss_differences_min=0
        if npoints<framesize:
            continue
        
        #if sensor_name=='GPS_raw':
        #if sensor_name=='GPS_cleaned':
        if sensor_name in ['GPS_raw','GPS_cleaned']:
            #reduction=0.3
            #framesize=npoints-1
            print 'setting pre_process parameters for GPS...'
            pre_process=distance_pre_process
            post_process=distance_post_process
        else:
            pre_process,post_process=set_pre_process(multiple=multiple,min_delta=min_delta,delta=delta,tss_differences=tss_differences_min)
        
        print '***********'
        print "Processing sensor:", sensor_id, sensor_name
        print "framesize: ",framesize
        print "reduction: ",reduction
        print "pre_process: ",pre_process
        print "post_process: ",post_process
        print "block: ",block
        print '...'
        
        #print 'Processing:', file_id, 'Goes in sensor: ', sensor_id, ' ,framesize:',framesize, ' ,reduction:', reduction, ' ,block:', block
        #if sensor_name=='GPS_raw':
        if sensor_name in ['GPS_raw','GPS_cleaned']:
            process_id=D.gps_sensor_data_process(sensor_id, framesize, reduction, 'cii_dct_v011', distance_pre_process, distance_post_process, block=15, gps_sync_rate=10, heading_digits=1, gps_digits=5)
        else:
            process_id=D.sensor_data_process(sensor_id, framesize, reduction,'cii_dct_v011', pre_process,post_process,block,raw_tss=True)
        #process_id=len(D.get_sensor_info(sensor_id)['processed_data'])-1
        
        #Recon data info
        recon_data=D.get_sensor_recon_data(sensor_id,process_id)
        tss_recon=np.asarray(recon_data['tss'])
        values_recon=np.asarray(recon_data['values'])
        zlib_ratio=D.get_zip_reduction_ratio(sensor_id,framesize)
        
        #Reduce info
        reduce_info=D.get_sensor_process_info(sensor_id, process_id)['reduce_info']
        #if sensor_name=='GPS_raw':
        if sensor_name in ['GPS_raw','GPS_cleaned']:
            count_red=reduce_info['len_red'] ##For the new GPS processing Daniel used this...
            gps_deviation = D.get_gps_deviations(sensor_id, process_id)
            print "edge distance : " + str(gps_deviation['pe_deviations'])
            print "point distance : " + str(gps_deviation['pp_deviations'])
            print "wgs point distance : " + str(gps_deviation['wgs84_pp_deviations'])
            #print gps_deviation['pe_distances']
        else:
            count_red=reduce_info['count_red']
        red_info_ratio=reduce_info['ratio']
        print '********************************************************************************************************'
        print '********************************************************************************************************'
        print "**********  core_reduction: ",reduction, ' , effective reduction',red_info_ratio,'**********************'
        print '********************************************************************************************************'
        print '********************************************************************************************************'
        #Deviation_info
        deviations=D.get_deviations(sensor_id,process_id)
        
        rrmses=[]
        rrmses_string=""
        for i in range(dof):
            
            max=raw_data['max_v'][i]
            min=raw_data['min_v'][i]
            
            value_name=sensor['value_names'][i]
            values_comp=values_recon.T[i]
            dev_max=deviations['E'][i]['max']
            dev_mean=deviations['E'][i]['mean']
            dev_min=deviations['E'][i]['min']
            
            ##A relative error measurement they want to see
            amplitude=np.abs(max-min)
            if np.abs(dev_min)>np.abs(dev_max):
                relative_error=np.abs(dev_min/amplitude)*100 
            else:
                relative_error=np.abs(dev_max/amplitude)*100 
            ###
                
            relerr=deviations['relerr'][i]
            mae=deviations['MAE'][i]
            mape=deviations['MAPE%'][i]
            smape=deviations['SMAPE%'][i]
            rrmse=deviations['RRMSE%'][i]
            wape=deviations['WAPE%'][i]
            wpe=deviations['WPE%'][i]
            
            rrmses.append(rrmse)
            rrmses_string=rrmses_string+str(round(rrmse,1))+"%"
            if i<dof-1:
                rrmses_string=rrmses_string+","
            else:
                rrmses_string=rrmses_string
                
                        
            #Write output
            writer_recon.writerow((file_id,npoints,sensor_id,sensor_name, value_name,process_id ,framesize,reduction, 
                                   multiple[i], min_delta[i],delta[i],
                                   tss_recon.min(),tss_recon.max(),
                                   min,float("%.3f"%values_comp.min()),
                                   max, float("%.3f"%values_comp.max()),
                                   amplitude,
                                   float("%.3f"%values_comp.mean()), 
                                   float("%.3f"%zlib_ratio), count_red,float("%.3f"%red_info_ratio), 
                                   dev_max,dev_mean,dev_min,
                                   relerr,mape,smape,
                                   wpe,wape,rrmse,
                                   relative_error,mae
                                   #,filename
                               ))   
            
            #........end dof loop
        #Plots
        #suptitle='sensor '+str(sensor_id)+', '+sensor_name+', '+'framesize: '+str(framesize)+', '+'reduction: '+str(reduction)+', '+'block: '+str(block)
        suptitle='sid:'+str(sensor_id)+' ,'+sensor_name+' ,pid'+str(process_id)+' ,f'+str(framesize)+' ,corered'+str(round(reduction*100,2))+' ,red'+str(round(red_info_ratio*100,2))+'\n rrmse:  ('+rrmses_string+')'+' ,multiple:'+str(multiple)+' ,mindelta'+str(min_delta)+' ,delta'+str(delta)
        #suptitle='sid:'+str(sensor_id)+' ,'+sensor_name+' ,pid'+str(process_id)+' ,f'+str(framesize)+' ,corered'+str(round(reduction*100,2))+' ,red'+str(round(red_info_ratio*100,2))+'rrmse:'+str(rrmse)
        P=Plot(D)
        P.plot(sensor_id,process_id,deviation='difference',dt_format='%H:%M:%S',save_png=True,suptitle=suptitle) 
        
        
        vsid_distance = sensor_id*100 + 1
        print 'NAME VIRTUAL SENSOR FOR DISTANCE:', D.get_sensor_info(vsid_distance)['sensor_name']
        print 'VALUE NAMES VIRTUAL SENSOR FOR DISTANCE:', D.get_sensor_info(vsid_distance)['value_names']
        P.plot(vsid_distance,process_id,deviation='difference',dt_format='%H:%M:%S',save_png=True) 
        
        #vsid_gps = sensor_id*100
        #P.plot(vsid_gps,process_id,deviation='difference',dt_format='%H:%M:%S',save_png=True) 
        
        #clear matplotlib memory
        plt.close('all')
        gc.collect()
        
        #process_id=process_id+1
        #print '** Goes in file: ',file_id,''
        
        #......end loop over sensors to process    
    name='output_file_id_'+'all'+'.dbk'
    D.save(name)

#################################################################################
#################################################################################
##After the processing is done....plot maps and box plots
################################Maps!#################################################
#sensor_id=1 ##I assigned sensor_id=1 to the GPS raw data...sensor_id=2 is the GPS_cleaned data
sensor_id=2 ##I assigned sensor_id=1 to the GPS raw data...sensor_id=2 is the GPS_cleaned data
raw_data = D.get_sensor_raw_data(sensor_id)
recon_data = D.get_sensor_recon_data(sensor_id,0)
#print recon_data['values']

##Trimming the raw data according to recon data...taken from tk_databank (calculate deviations)
recon_data_len = len(recon_data['values'])
tss_raw   = raw_data['tss'][:recon_data_len]
tss_recon = recon_data['tss']

component_raw_lon   = list( zip(*raw_data['values'][:recon_data_len])[0] )
component_recon_lon = list( zip(*recon_data['values'])[0] )
print 'first raw longitude values:',component_raw_lon[:10]
print 'first recon longitude values:',component_recon_lon[:10]
component_recon_interpol_lon = tk_err.interpolate(tss_raw, tss_recon, component_recon_lon)

#print component_recon_interpol_lon[0]
component_raw_lat   = list( zip(*raw_data['values'][:recon_data_len])[1] )
component_recon_lat = list( zip(*recon_data['values'])[1] )
component_recon_interpol_lat = tk_err.interpolate(tss_raw, tss_recon, component_recon_lat)
print 'first raw latitude values:',component_raw_lat[:10]
print 'first recon latitute values:',component_recon_lat[:10]

#print component_recon_interpol_lat[0]
###############

##Need to convert to a list of lists to be used in the functions later    
component_raw_lat=[[x] for x in component_raw_lat]
component_recon_lat=[[x] for x in component_recon_lat]

component_raw_lon=[[x] for x in component_raw_lon]
component_recon_lon=[[x] for x in component_recon_lon]

#print  component_recon_lat, len(component_recon_lat)
#print  len(component_recon_interpol_lat), component_recon_interpol_lat

#print  len(component_recon_interpol_lon), component_recon_interpol_lon
#print  len(component_recon_lon), component_recon_lon

### maps with gps deviations
from tk_gmd_gps_point import maps
import random
#sensor_data = get_ts_value_from_file("daimler.csv", ' ', 2, 3, 1, [4])
#gpsdata = combin_list(sensor_data['GPS_Lat']['values'],sensor_data['GPS_Long']['values'])
#print(gpsdata[0][0])
gpsdata = tk_gdfr_New.combin_list(component_raw_lat,component_raw_lon)
gpsdata_rec = tk_gdfr_New.combin_list(component_recon_lat,component_recon_lon)

#print "This is the GPS data",gpsdata
#print(gpsdata[0][0])
mymap = maps(gpsdata[0][0], gpsdata[0][1], 16)
path = gpsdata
path_rec = gpsdata_rec
v = []
for i in range(len(gpsdata)):
    #v.append(diff_speed[i]*150/max(diff_speed))
    v.append(gps_deviation['pe_distances'][i])
    #if i<10:
    #    print round(gps_deviation['pe_distances'][i],2)
print 'first_pe_distances:',gps_deviation['pe_distances'][:10]
mymap.addpath(path)
mymap.addcolorpath(path_rec,v)
for i in range(len(component_raw_lat)):
    mymap.addpoint(gpsdata[i][0], gpsdata[i][1],title=str(i))
    mymap.addpoint(gpsdata_rec[i][0], gpsdata_rec[i][1], color = '#00FF00',title=str(i)+','+str(gps_deviation['pe_distances'][i]))
if not os.path.exists('./maps_gps'):
    os.makedirs('./maps_gps')
mymap.draw('maps_gps/'+'sensor_id_'+str(sensor_id)+'_tk_googlemaps_gps.html')

#################################################################################
##########       Box plot of pe and pp distances ################################

fig1, ax1 = plt.subplots(2,1)
plt.subplots_adjust(left=0.125,right=0.9,bottom=0.1,top=0.9,wspace=0.2,hspace=0.9)
meanpointprops = dict(marker='D', markerfacecolor='w')
ax1[0].boxplot(gps_deviation['pe_distances'], showmeans=True, meanprops=meanpointprops , labels=['pe_distances'],whis=[2.5,97.5],vert=False,widths=0.9)
ax1[0].set_xlabel('point to edge distances (m)')
#ax1[0].set_title('PE Distances, Eff. Red: '+str(round(reduce_info_ratio_gps,3)))
ax1[1].boxplot(gps_deviation['wgs84_pp_distances'], showmeans=True, meanprops=meanpointprops , labels=['pe_distances'],whis=[2.5,97.5],vert=False,widths=0.9)
ax1[1].set_xlabel('point to point distances (m)')


if not os.path.exists('./individual_box_plots'):
    os.makedirs('./individual_box_plots')
fig1.savefig('individual_box_plots/GPS_sensor_'+str(sensor_id)+'_box_plot.png')
         
        


