"""
Project Teraki: Ayyeka HTML Image Output Funtion
Author: xucao

it accept command line parameter: filename
print png image in one html file
"""


"""
for testing, the following commands can type in ternimal :
        python Ayyeka.py
"""


import os, sys, time
import csv
from tk_databank import DataBank
from tk_plot import Plot
from tk_plot import gen_html_grid
import glob
import datetime 

def str_to_int_float(string): 
    try:
        return int(string)
    except ValueError:
        pass
    try:
        return float(string)
    except ValueError:
        return string


def get_single_sensor_data(filename,sensor_pos =3):
    result = {}
    with open(filename) as f:
        reader = csv.reader(f)
        for i in range(0,5):
            next(reader)
        key =  "Raw Velocity"

        for line in reader:
            try:
                ts = datetime.datetime.strptime(line[0], "%Y/%m/%d %H:%M:%S")
                value = [str_to_int_float(line[sensor_pos])]
                if key not in result.keys():
                    result[key] = {}
                    result[key]['mt'] = key
                    result[key]['tss'] = []
                    result[key]['values'] = []
                result[key]['tss'].append(ts)
                result[key]['values'].append(value)

            except:
                pass
    return result

def process():
    filename = "Mi-Hadera_New_2__Raw_Velocity(ms).csv"

    D = DataBank()
    sensor_data = get_single_sensor_data(filename)
    sensor_name = "Raw Velocity"
    output_name = 'Ayyeka_'+sensor_name
    D.add_sensor_data(sensor_data[sensor_name]['values'],sensor_data[sensor_name]['tss'], 0, output_name, sensor_name)
        

    Pl = Plot(D)
    Pl.plot_raw_inspection(0,'png')
    
    outputfile = 'Ayyeka_data.html'

    gen_html_grid(out_filename = outputfile)


if __name__ == '__main__':
    process()

