import matplotlib
matplotlib.use('Agg') ### Force matplotlib to not use any Xwindows backend.

import glob
import os

from ayyeka_db import AyyekaDataBank
from tools import tk_plot as tkplt
from tools.tk_plot import gen_html_grid

import numpy as np

def create_raw_overview():
    D = AyyekaDataBank()
    
    filenames = glob.glob('data/*.csv')
    for i, filename in enumerate(filenames):
        print 'Readning file:', i, filename
        D.read_sensor_file(filename, sensor_id=i)

    P = tkplt.Plot(D)
    for sensor_id in range( len(D.get_sensor_list()) ):
        print 'Creating plot for sensor_id:', sensor_id
        P.plot_raw_inspection(sensor_id, 'png')

    tkplt.gen_html_grid(out_filename='ayyeka_overview.html', title='Overview of Ayyeka data', n_cols=1, img_width=1000)

#raw_interval=900000 ##IN ms...
def set_pre_process(multiple,min_delta,delta,tss_differences_mean):
    pre_process = {
        "multiple": [multiple],
        #'integer': [1],
        "min_delta":[min_delta],
        #"ts_exp_threshold": [tss_differences_mean-500,tss_differences_mean+500],
        "value_exceptions": [
            [
                {
                    "e_name": "red_peak_points",
                    "e_id":  4,
                    "v_types": ["red"],
                    "condition": "(abs(delta_v)>"+str(delta)+")",
                    "para": "delta_v"
                },
                {
                    "e_name": "rest_peak_points",
                    "e_id":  5,
                    "v_types": ["rest"],
                    "condition": "(abs(delta_v)>"+str(delta)+")",
                    "para": "str(delta_v)+'_'+str(ts)+'_'+str(v)"
                }
            ]
        ]
    }
    post_process = {
        "divide": [float(multiple)]
    }
    return pre_process, post_process

def process():
    D = AyyekaDataBank()
    folder='/home/angelh/Documents/DataScienceRelated/Teraki/Ayyeka/'
    filenames = sorted(glob.glob(folder+'data/*.csv'),key=os.path.getsize,reverse=False) ##False to start with the lightest
    for i, filename in enumerate(filenames):
        print 'Reading file:', i, filename
        D.read_sensor_file(filename, sensor_id=i)
    
    raw_data=D.get_sensor_raw_data(sensor_id=1) ##sensor 2 is WaterLevel!
    tss=np.asarray(raw_data['tss'])
    tss_differences=np.diff(tss)
    tss_differences_mean=tss_differences.mean()
    
    print 'first time stamps:',tss[:20]
    print 'first time stamp diferences',tss_differences[:20]
    print 'time stamp differences mean:', tss_differences_mean
    
    # Write here code for processing...
    ############## From here down added by AngelH
    #P=Plot(D)
    P = tkplt.Plot(D)
    #P.print_sensor_summary()
    
    
    
    block=15
    default_delta=1e15
    #                                     :[framesize  ,reduction ,multiple, min_delta ,delta] 
    all_sensors_parameters = {"pH"               :[2000  ,0.90       ,1       ,0.            ,1.  ],
                              "Water Level"      :[2000  ,0.90       ,1       ,0.0           ,2.  ],
                              "Pressure"         :[2000  ,0.80       ,1       ,0.1           ,1.  ],
                              "Level"            :[2000  ,0.90       ,1       ,0.            ,2.  ],
                              "Raw Velocity"     :[2000  ,0.70       ,1       ,0.05          ,0.1 ],
                              "Dissolved Oxygen" :[2000  ,0.85       ,1       ,0.            ,2.  ],
                          }                              

    ##Used in test4 
    #all_sensors_parameters = {"pH"              :[800  ,0.90      ,1       ,0          ,1],
    #                          "Water Level"     :[800  ,0.9       ,1       ,0          ,2],
    #                          "Pressure"        :[800  ,0.80      ,1       ,0.1        ,1],
    #                          "Level"           :[800  ,0.85      ,1       ,0          ,2],
    #                          "Raw Velocity"    :[800  ,0.8       ,1       ,0        ,0.1],
    #                          "Dissolved Oxygen":[800  ,0.85      ,1       ,0          ,2],
                                
    
    for sensor in D.get_sensor_list():
        index=sensor['index']
        dof=sensor['dof']
        sensor_id=sensor['sensor_id']
        sensor_name=sensor['sensor_name']
        
        #if sensor_name!='Pressure':
        #if sensor_name!='pH':
        #if sensor_name!='Raw Velocity':
        #if sensor_name!='Level':
        #if sensor_name!='Water Level':
            #if sensor_name!='Raw Velocity' or sensor_name!='Water Level':
        #    continue
            
            
        framesize=all_sensors_parameters[sensor_name][0]
        reduction=all_sensors_parameters[sensor_name][1]
        multiple=all_sensors_parameters[sensor_name][2]
        min_delta=all_sensors_parameters[sensor_name][3]
        delta=all_sensors_parameters[sensor_name][4]
        
        pre_process,post_process=set_pre_process(multiple=multiple,min_delta=min_delta,delta=delta,tss_differences_mean=tss_differences_mean)
        
        process_id=D.sensor_data_process(sensor_id, framesize, reduction,'cii_dct_v011', pre_process=pre_process, post_process=post_process,block=block)
        print sensor_name, process_id
        
        
        reduce_info=D.get_sensor_process_info(sensor_id, process_id)['reduce_info']
        red_info_ratio=reduce_info['ratio']
        
        deviations=D.get_deviations(sensor_id,process_id)
        rrmse=deviations['RRMSE%'][0]
        
        #suptitle='sid_'+str(sensor_id)+'_'+sensor_name+'_pid'+str(process_id)+'_f'+str(framesize)+'_corered'+str(round(reduction*100,2))+'_red'+str(round(red_info_ratio*100,2))+'_rrmse'+str(round(rrmse,2))+'_mindelta'+str(round(min_delta,2))+'_delta'+str(round(delta,2))
        #suptitle='sid_'+str(sensor_id)+'_'+sensor_name+'_pid'+str(process_id)+'_f'+str(framesize)+'_corered'+str(round(reduction*100,2))+'_red'+str(round(red_info_ratio*100,2))+'_rrmse'+str(round(rrmse,2))+'_mindelta'+str(round(min_delta,2))+'_delta'+str(round(delta,2))+'_block'+str(block)
        suptitle=sensor_name+' ,reduction:'+str(int(red_info_ratio*100))+'% ,rrmse:'+str(round(rrmse,1))+'% ,sid:'+str(sensor_id)
        
        
        #P.plot(sensor_id,process_id,deviation='difference',dt_format='%H:%M:%S',save_png=True,suptitle=suptitle) 
        P.plot(sensor_id,process_id,deviation='difference',dt_format='%Y-%m-%d %H',save_png=True,suptitle=suptitle) 
        
        #D.write_sensor_data_files(sensor_id)
        
    name='output_file.dbk'
    D.save(name)
if __name__ == '__main__':
#    create_raw_overview()
    process()
