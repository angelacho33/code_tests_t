import time, datetime
import csv, glob

from tools.tk_databank import DataBank
from tools.tk_plot import Plot
from tools.tk_plot import gen_html_grid
from tools.tk_mvsd import SensorData
from tools.tk_mvsd import str_to_int_float

class AyyekaDataBank(DataBank):
    """Class for DataBank in Ayyeka project
    It inherits all the attributes and methods from DataBank, 
    and include following Attributes and Methods for AirBus:

    Public Methods:

    Private Methods:

        convert_timestamp_to_str(): convert timestamp to string
    """

    def __init__(self, config_para=None):
        DataBank.__init__(self, config_para)
        """ Add new attributes here if it is necessary"""
        return

    def read_sensor_file(self, path, sensor_id=None, sensor_name=None, sensor_type=None, value_names=None, quantity=None, unit=None, sort=False, clean=False):
        """ 
        API for read data from a file

        Args: 
            path:  filename.
            sensnor_id:  sensor id.
            sensor_name: sensor name.
            sensor_type: sensor type.
            value_names: the value names for plot
            quantity: the quantity for plot
            unit: the unit of the value
            sort: Flag to sort the data when load the dataset.
            clean: Flag to clean the duplicated data when load the dataset

        Returns: 
            SensorData Instance
        """
        if self.get_sensor_info(sensor_id) is not None:
            print("Duplicated sensor id : " + str(sensor_id))
            return None
        SD = SensorData()

        with open(path) as f:
            reader = csv.reader(f, delimiter=',')
        
            site_name = next(reader)
            stream_name = next(reader)
            time_zone = next(reader)
            next(reader)
            header = next(reader)
            stream_name = stream_name[0].split(':')[1].strip()
            sensor_name = stream_name

            tss = []
            values = []
            for line in reader:
                ts = datetime.datetime.strptime(line[1]+' '+line[2], '%d/%m/%Y %H:%M:%S')
                ts = int(time.mktime(ts.timetuple()) * 1000 + ts.microsecond / 1000)
            
                value = str_to_int_float(line[3])
                tss.append(ts)
                values.append([value])

        ####################################
        #number_of_points=1000
        #tss=tss[:number_of_points]
        #values=values[:number_of_points]
        #####################################
        raw_data = {}
        raw_data['dof'] = len(values[0])
        raw_data['count'] = len(values)
        raw_data['values'] = [x for x in values]
        if tss is None:
            raw_data['tss'] = self.get_default_tss(len(values))
        else:
            raw_data['tss'] = [ts for ts in tss]
        SD = SensorData()
        SD._raw_data = raw_data
        if sort:
            SD.sort_raw_data()
        if clean:
            SD.clean_duplicates()
        SD.raw_data_process()

        sensor_info = {}
        sensor_info['index'] = len(self._sensors)
        sensor_info['path'] = path
        sensor_info['sensor_id'] = sensor_id
        sensor_info['sensor_name'] = sensor_name
        sensor_info['sensor_type'] = sensor_type
        sensor_info['dof'] = raw_data['dof']
        sensor_info['sensor_data'] = SD
        sensor_info['processed_data'] = []
        if value_names is None:
            sensor_info['value_names'] = [('value_' + str(x)) for x in range(sensor_info['dof'])]
        else:
            sensor_info['value_names'] = value_names
        if quantity is None:
            sensor_info['quantity'] = [''] * sensor_info['dof']
        else:
            sensor_info['quantity'] = quantity
        if unit is None:
            sensor_info['unit'] = [''] * sensor_info['dof']
        else:
            sensor_info['unit'] = unit
        self._sensors.append(sensor_info)
        return sensor_info['sensor_data']

def test1():
    D = AyyekaDataBank()

    filenames = glob.glob('data/*.csv')
    for i, filename in enumerate(filenames):
        print 'Readning file:', i, filename
        D.read_sensor_file(filename, sensor_id=i)

    print D.get_sensor_list()

    
if __name__ == '__main__':
    test1()
