from tk_databank import * 
from tk_plot import Plot
import numpy as np
import glob
import csv
import sys
import datetime,time


D = DataBank()
dbk_file = 'output_file.dbk'
D.load(dbk_file)


sensor_amount=len(D.get_sensor_list())
print "sensor_ammount: ", sensor_amount


process_id=0
for sensor in D.get_sensor_list():
    sensor_id=sensor['sensor_id']
    sensor_name=sensor['sensor_name']
    
    #if sensor_id!=0:
    #    continue
            
    if sensor_name=='Raw Velocity':
        SiteName='Mi-Hadera New 2'
        StreamName='Raw Velocity'
        Unit='(m/s)'
        
    if sensor_name=='Water Level':
        SiteName='Mei hadera 3 Replacment'
        StreamName='Water Level'
        Unit='(cm)'
        
    if sensor_name=='pH':
        SiteName='???? ???? SN-0716010052'
        StreamName='pH'
        Unit='(pH)'
        
    if sensor_name=='Level':
        SiteName='Ein Afek-Glisha Kvish Orki  SN-05160100039'
        StreamName='Level'
        Unit='(cm)'
        
    if sensor_name=='Dissolved Oxygen':
        SiteName='Jordan Gesher'
        StreamName='Dissolved Oxygen'
        Unit='(mg/L)'
        
    if sensor_name=='Pressure':
        SiteName='MeiHod2'
        StreamName='Pressure'
        Unit='(bar)'
        
    if sensor_name=='Pressure' and sensor_id==6:
        SiteName='MeiHod1'
        StreamName='Pressure'
        Unit='(bar)'
        
    output_file='reconstructed_'+SiteName+' '+sensor_name+'.csv'
    with open(output_file, 'w',0) as f_recon:
        #Output file
        #writer_recon = csv.writer(f_recon,quoting=csv.QUOTE_NONNUMERIC)
        writer_recon = csv.writer(f_recon)
        
        writer_recon.writerow(("Site Name: "+SiteName,None,None,None))
        writer_recon.writerow(("Stream Name: "+StreamName,None,None,None))
        writer_recon.writerow(("Time Zone: UTC",None,None,None))
        writer_recon.writerow([])
        writer_recon.writerow(("Datetime","Date","Time",SiteName+' / '+StreamName+Unit))
        
        
        ##Raw_data
        raw_data=D.get_sensor_raw_data(sensor_id)
        tss=np.asarray(raw_data['tss'])
        values_raw=np.asarray(raw_data['values'])
        npoints=raw_data['count']
        
        
        #print "npoints", npoints
        #print "values[:10]=",values_raw[:10]
        #print "***********************"
        
        #Recon data
        recon_data=D.get_sensor_recon_data(sensor_id,process_id)
        tss_recon=np.asarray(recon_data['tss'])
        values_recon=np.asarray(recon_data['values'])
        npoints_recon=recon_data['count']
        
        print sensor_id, sensor_name, npoints, 2000, npoints_recon
        #print "npoints", npoints
        #print "values_recon[:10]=",values_recon[:10]
        #print "tss_recon[:10]=",tss_recon[:10]
        timestamps=[datetime.datetime.fromtimestamp(float(elm)) for elm in tss_recon/1000.]
        
        timestamps_datetime=[timestamp.strftime("%m/%d/%Y %H:%M:%S") for timestamp in timestamps]
        timestamps_date=[timestamp.strftime("%d/%m/%Y") for timestamp in timestamps]
        timestamps_time=[timestamp.strftime("%H:%M:%S") for timestamp in timestamps]
        
        #print "timestamps:",str(timestamps[:10])
        #print  "date:", timestamps[:10]
        for i in range(0,len(values_recon)):
            #writer_recon.writerow((timestamps_datetime[i] ,timestamps_date[i],timestamps_time[i],str(values_raw[i][0])))
            if sensor_name in ['Water Level','Level']:
                writer_recon.writerow((  timestamps_datetime[i] ,timestamps_date[i],timestamps_time[i],round(values_recon[i][0],1)  ))
            else:
                writer_recon.writerow((  timestamps_datetime[i] ,timestamps_date[i],timestamps_time[i],round(values_recon[i][0],3)  ))

            
    #D.write_sensor_data_files(sensor_id)
    
    
    
    


