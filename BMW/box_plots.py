import numpy as np
import pandas as pd
import matplotlib 
matplotlib.rcParams.update({'font.size': 25})
#import matplotlib.style
#matplotlib.style.use('ggplot')
import matplotlib.pyplot as plt
import sys
import os
#%pylab

#file=str(sys.argv[1]) ##summary csv file
scenario=int(sys.argv[1])
scenarios=['Highway','InnerCity','Rural']
scenarios_file_name=['highway_probes','innerCity','rural']
scenarios_file2_name=['highway','innercity','rural']

file='summary_'+scenarios_file_name[scenario]+'_output_recon.csv'
file2='After_smoothing_'+scenarios_file2_name[scenario]+'_output_recon.csv'

sensors=['gps','heading','speed']
coordinates=['lat','lon']

name1='box_plot_'+scenarios[scenario]+'_max_deviations.png'
name2='box_plot_'+scenarios[scenario]+'_mean_deviations.png'
name3='box_plot_'+scenarios[scenario]+'_effective_reduction.png'

df1=pd.read_csv(file,sep=',')
df2=pd.read_csv(file2,sep=',')

##GPS dataframs using the after smothing file which should have smaller deviations
df_gps=df2[df2['sensor_name']=='gps']
df_lat=df_gps[df_gps['value_name']=='lat']
df_lon=df_gps[df_gps['value_name']=='lon']

df_heading=df1[df1['sensor_name']=='heading']
df_speed=df1[df1['sensor_name']=='speed']

#######################################################################################
##Max deviations
fig1, ax1 = plt.subplots(2, 2,figsize=(30,15))
plt.subplots_adjust(left=0.125,right=0.9,bottom=0.1,top=0.9,wspace=0.2,hspace=0.4)

lat_mean=df_lat.dev_max.values.mean()
lon_mean=df_lon.dev_max.values.mean()
heading_mean=df_heading.dev_max.values.mean()
speed_mean=df_speed.dev_max.values.mean()

meanpointprops = dict(marker='D', markerfacecolor='w')
ax1[0][0].boxplot(df_lat.dev_max.values, showmeans=True, meanprops=meanpointprops , labels=[''],whis=[2.5,97.5],vert=False,widths=0.9)
ax1[0][0].set_xlabel('lat. max deviation')
ax1[0][0].set_xscale('log')
ax1[0][0].set_title('lat max. deviation,'+' mean: '+str(round(lat_mean,3)))

ax1[1][0].boxplot(df_lon.dev_max.values, showmeans=True, meanprops=meanpointprops , labels=[''],whis=[2.5,97.5],vert=False,widths=0.9)
ax1[1][0].set_xlabel('lon. max deviation')
ax1[1][0].set_xscale('log')
ax1[1][0].set_title('lon. max deviation,'+' mean: '+str(round(lon_mean,3)))

ax1[0][1].boxplot(df_heading.dev_max.values, showmeans=True, meanprops=meanpointprops , labels=[''],whis=[2.5,97.5],vert=False,widths=0.9)
ax1[0][1].set_xlabel('heading max deviation')
ax1[0][1].set_title('heading max deviation,'+' mean: '+str(round(heading_mean,3)))

ax1[1][1].boxplot(df_speed.dev_max.values, showmeans=True, meanprops=meanpointprops , labels=[''],whis=[2.5,97.5],vert=False,widths=0.9)
ax1[1][1].set_xlabel('speed max deviation')
ax1[1][1].set_title('speed max deviation,'+' mean: '+str(round(speed_mean,2)))

if scenario==0:
    fig1.suptitle(scenarios[scenario]+' scenario,323/404 sessions, Max. deviations , 80% reduction, Framesize=500', fontsize=40)
if scenario==1:
    fig1.suptitle(scenarios[scenario]+' scenario,201/237 sessions, Max. deviations , 80% reduction, Framesize=500', fontsize=40)
if scenario==2:
    fig1.suptitle(scenarios[scenario]+' scenario,114/140 sessions, Max. deviations , 80% reduction, Framesize=500', fontsize=40)

if not os.path.exists('./box_plots'):
    os.makedirs('./box_plots')
plt.savefig('box_plots/'+name1)

#######################################################################################
##Mean deviations
fig2, ax2 = plt.subplots(2, 2,figsize=(30,15))
plt.subplots_adjust(left=0.125,right=0.9,bottom=0.1,top=0.9,wspace=0.2,hspace=0.4)

lat_mean=df_lat.dev_mean.values.mean()*1e6
lon_mean=df_lon.dev_mean.values.mean()*1e6
heading_mean=df_heading.dev_mean.values.mean()
speed_mean=df_speed.dev_mean.values.mean()

ax2[0][0].boxplot(df_lat.dev_mean.values*1e4, showmeans=True, meanprops=meanpointprops , labels=[''],whis=[2.5,97.5],vert=False,widths=0.9)
ax2[0][0].set_xlabel('lat. mean deviation x$10^{-4}$')
#ax2[0][0].set_xscale('log')
ax2[0][0].set_title('lat. mean deviation,'+' mean: '+str(round(lat_mean,3))+'x$10^{-6}$')

ax2[1][0].boxplot(df_lon.dev_mean.values*1e4, showmeans=True, meanprops=meanpointprops , labels=[''],whis=[2.5,97.5],vert=False,widths=0.9)
ax2[1][0].set_xlabel('lon. mean deviation x$10^{-4}$ ')
#ax2[1][0].set_xscale('log')
ax2[1][0].set_title('lon mean deviation,'+' mean: '+str(round(lon_mean,3))+'x$10^{-6}$')

ax2[0][1].boxplot(df_heading.dev_mean.values, showmeans=True, meanprops=meanpointprops , labels=[''],whis=[2.5,97.5],vert=False,widths=0.9)
ax2[0][1].set_xlabel('heading mean deviation')
ax2[0][1].set_title('heading mean deviation,'+' mean: '+str(round(heading_mean,3)))

ax2[1][1].boxplot(df_speed.dev_mean.values, showmeans=True, meanprops=meanpointprops , labels=[''],whis=[2.5,97.5],vert=False,widths=0.9)
ax2[1][1].set_xlabel('speed mean deviation')
ax2[1][1].set_title('speed mean deviation,'+' mean: '+str(round(speed_mean,3)))


if scenario==0:
    fig2.suptitle(scenarios[scenario]+' scenario, 323/404 sessions, Mean deviations , 80% reduction,framesize=500', fontsize=40)
if scenario==1:
    fig2.suptitle(scenarios[scenario]+' scenario,201/237 sessions, Mean deviations , 80% reduction, Framesize=500', fontsize=40)
if scenario==2:
    fig2.suptitle(scenarios[scenario]+' scenario,114/140 sessions, Mean deviations , 80% reduction, Framesize=500', fontsize=40)

plt.savefig('box_plots/'+name2)
#######################################################################################
##Effective reduction
fig3, ax3 = plt.subplots(2, 2,figsize=(30,15))
plt.subplots_adjust(left=0.125,right=0.9,bottom=0.1,top=0.9,wspace=0.2,hspace=0.4)

lat_mean=df_lat.red_info_ratio.values.mean()
lon_mean=df_lon.red_info_ratio.values.mean()
heading_mean=df_heading.red_info_ratio.values.mean()
speed_mean=df_speed.red_info_ratio.values.mean()

meanpointprops = dict(marker='D', markerfacecolor='w',markersize=15)
ax3[0][0].boxplot(df_lat.red_info_ratio.values, showmeans=True, meanprops=meanpointprops , labels=[''],whis=[2.5,97.5],vert=False,widths=0.9)
ax3[0][0].set_xlabel('lat. Effective_reduction')
ax3[0][0].set_title('lat. Effective_reduction,'+' mean: '+str(round(lat_mean,3)))

ax3[1][0].boxplot(df_lon.red_info_ratio.values, showmeans=True, meanprops=meanpointprops , labels=[''],whis=[2.5,97.5],vert=False,widths=0.9)
ax3[1][0].set_xlabel('lon. Effective_reduction')
ax3[1][0].set_title('lon. Effective_reduction,'+' mean: '+str(round(lon_mean,3)))

ax3[0][1].boxplot(df_heading.red_info_ratio.values, showmeans=True, meanprops=meanpointprops , labels=[''],whis=[2.5,97.5],vert=False,widths=0.9)
ax3[0][1].set_xlabel('Heading Effective_reduction')
ax3[0][1].set_title('Heading Effective_reduction,'+' mean: '+str(round(heading_mean,3)))

ax3[1][1].boxplot(df_speed.red_info_ratio.values, showmeans=True, meanprops=meanpointprops , labels=[''],whis=[2.5,97.5],vert=False,widths=0.9)
ax3[1][1].set_xlabel('Speed Effective_reduction')
ax3[1][1].set_title('Speed Effective_reduction,'+' mean: '+str(round(speed_mean,3)))

if scenario==0:
    fig3.suptitle(scenarios[scenario]+' scenario, 323/404 sessions, 80% reduction,framesize=500', fontsize=40)
if scenario==1:
    fig3.suptitle(scenarios[scenario]+' scenario,201/237 sessions, 80% reduction, Framesize=500', fontsize=40)
if scenario==2:
    fig3.suptitle(scenarios[scenario]+' scenario,114/140 sessions, 80% reduction, Framesize=500', fontsize=40)

plt.savefig('box_plots/'+name3)


