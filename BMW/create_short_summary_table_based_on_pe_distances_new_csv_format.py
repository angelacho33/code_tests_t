import numpy as np
import pandas as pd
import matplotlib 
matplotlib.rcParams.update({'font.size': 25})
#import matplotlib.style
#matplotlib.style.use('ggplot')
import matplotlib.pyplot as plt
import sys

scenario=int(sys.argv[1])

folder='/home/angelh/Documents/DataScienceRelated/Teraki/BMW_output/fourteenth_run/'
scenarios=['Highway','InnerCity','Rural']
scenarios_file1_name=['highway_probes','innerCity','rural']
scenarios_file2_name=['highway','innercity','rural']

file1=folder+scenarios_file2_name[scenario]+'/summary_'+scenarios_file1_name[scenario]+'_output_recon.csv'
file2=folder+scenarios_file2_name[scenario]+'/After_smoothing_with_percentiles_as_columns_'+scenarios_file2_name[scenario]+'_output_recon.csv'

df_summary=pd.read_csv(file1,sep=',')
df_after=pd.read_csv(file2,sep=',')
#df_after['file_name']=df_summary['file_name'] ##does not work...I need data frames with the same size...

##Selecting the rows with an unique file_id. Since the pe column is the same for lat and lon I choose lat
df_after=df_after[(df_after['sensor_name']=='gps') & (df_after['value_name']=='lat')]

df_pe_distances=df_after[['file_id','npoints','max_pe','upper_ninetysevenfive_percentile','file_name']]

#print 'There are ', len(df_pe_max),'/',len(df_pe_distances), ' files with pe distances>',pe_distance_limit,'m' 

##Match the file_ids in After smothing and file names in summary csv file
file_names=[]
for file_id in df_pe_distances.file_id.values:
    df=df_summary[(df_summary['file_id']==file_id) & (df_summary['sensor_name']=='speed')]##chose speed to have a single entry to compare file ids with names
    file_names.append(df.file_name.values[0])
    #print df.file_name.values

##Add a new column with file names to a new dataframw
df_pe_distances['file_name'] = pd.Series(file_names, index=df_pe_distances.index)

##Sort according to max_pe column values
df_pe_distances=df_pe_distances.sort_values(['max_pe'],ascending=False)

df_pe_distances.to_csv('combined_short_summary_'+scenarios_file2_name[scenario]+'.csv',sep=',')

