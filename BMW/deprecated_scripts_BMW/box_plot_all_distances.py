import numpy as np
import pandas as pd
import matplotlib 
import cPickle as pickle
matplotlib.rcParams.update({'font.size': 20})
#import matplotlib.style
#matplotlib.style.use('ggplot')
import matplotlib.pyplot as plt
import sys
#%pylab

#file=str(sys.argv[1]) ##summary csv file
scenario=int(sys.argv[1])
scenarios=['highway','innercity','rural']

folder='/home/angelh/Documents/DataScienceRelated/Teraki/BMW_output/tenth_run/'
file=folder+scenarios[scenario]+'/all_distances.pickle'

#Load the picke file
f = open(file, "r")     
all_pe_distances = pickle.load(f)                                                                                                                                                                             
all_pp_distances = pickle.load(f)                                                                                                                                                                       
all_wgs84_pp_distances = pickle.load(f)
f.close()

##########Note: all_pe_distances[i] are all the distances for the file_id=i...each one with the corresponding number of reconstructed data points for that file

#the arrays in the picle files has to be reshaped to have a huge array with all the distances of all the files...:S
pe_distances=[]
pp_distances=[]
wgs84_pp_distances=[]
for i in range(0,len(all_pe_distances)):
    for j in range(0,len(all_pe_distances[i])):
        pe_distances.append(all_pe_distances[i][j])
        pp_distances.append(all_pp_distances[i][j])
        wgs84_pp_distances.append(all_wgs84_pp_distances[i][j])
        
pe_distances_array=np.asarray(pe_distances)                            
pp_distances_array=np.asarray(pp_distances)                   
wgs84_pp_distances_array=np.asarray(wgs84_pp_distances)                            



fig1, ax1 = plt.subplots(1,figsize=(30,15))
plt.subplots_adjust(left=0.125,right=0.9,bottom=0.1,top=0.9,wspace=0.2,hspace=0.4)

meanpointprops = dict(marker='D', markerfacecolor='w')
ax1.boxplot((pe_distances_array,pp_distances_array,wgs84_pp_distances_array), showmeans=True, meanprops=meanpointprops , labels=['pe_distances','pp_distances','wgs84_pp_distances'],whis=[2.5,97.5],vert=False,widths=0.9)

if scenario==0:
    fig1.suptitle(scenarios[scenario]+' scenario, 323/404 sessions, 80% reduction,framesize=500', fontsize=40)
if scenario==1:
    fig1.suptitle(scenarios[scenario]+' scenario, 201/237 sessions, 80% reduction, Framesize=500', fontsize=40)
if scenario==2:
    fig1.suptitle(scenarios[scenario]+' scenario, 114/140 sessions, 80% reduction, Framesize=500', fontsize=40)

#name1='box_plot_'+scenarios[scenario]+'_pe_pp_wgs84_pp_distances.png'
name1='box_plot_'+scenarios[scenario]+'_pe_pp_wgs84_pp_distances_zoom_300m.png'
ax1.set_xlim(0,300)
ax1.set_xticks(range(0,300,20))
plt.savefig(name1)
#plt.show()

