import numpy as np
import pandas as pd
import matplotlib 
matplotlib.rcParams.update({'font.size': 25})
#import matplotlib.style
#matplotlib.style.use('ggplot')
import matplotlib.pyplot as plt
import sys

scenario=int(sys.argv[1])

#folder='/home/angelh/Documents/DataScienceRelated/Teraki/BMW_output/twelfth_run/'
folder='/home/angelh/Documents/DataScienceRelated/Teraki/BMW_output/thirtenth_run/'
scenarios=['Highway','InnerCity','Rural']
scenarios_file1_name=['highway_probes','innerCity','rural']
scenarios_file2_name=['highway','innercity','rural']

file1=folder+scenarios_file2_name[scenario]+'/summary_'+scenarios_file1_name[scenario]+'_output_recon.csv'
file2=folder+scenarios_file2_name[scenario]+'/After_smoothing_'+scenarios_file2_name[scenario]+'_output_recon.csv'

#print file1
#print file2

df_summary=pd.read_csv(file1,sep=',')
df_after=pd.read_csv(file2,sep=',')
#df_after['file_name']=df_summary['file_name'] ##does not work...I need data frames with the same size...

df_pe_distances=df_after[df_after['value_name']=='pe_distance'] ##chosing the pe_column
#print df_pe_distances

pe_distance_limit=20000 ##20 km....! jaja
#df_pe_max=df_pe_distances[df_pe_distances['values_recon_max']<pe_distance_limit] ##Used this wiht the 'ugly after smoothing file'
df_pe_max=df_pe_distances[df_pe_distances['dev_max']<=pe_distance_limit] ##This with the new after smotting file where the gps deviations are actualy in the dev_ columns

#print 'There are ', len(df_pe_max),'/',len(df_pe_distances), ' files with pe distances<',pe_distance_limit,'m' 
print 'There are ', len(df_pe_max),'/',len(df_pe_distances), ' files with pe distances>',pe_distance_limit,'m' 

##New data framne with selected columns.
df_pe_max_reduced=df_pe_max[['file_id','npoints','value_name','dev_max','file_name']]

##Match the file_ids in After smothing and file names in summary csv file
file_names=[]
for file_id in df_pe_max_reduced.file_id.values:
    df=df_summary[(df_summary['file_id']==file_id) & (df_summary['sensor_name']=='speed')]##chose speed to have a single entry to compare file ids with names
    file_names.append(df.file_name.values[0])
    #print df.file_name.values

##Add a new column with file names to a new dataframw
df_pe_max_reduced['file_name'] = pd.Series(file_names, index=df_pe_max_reduced.index)

#print file_names, len(file_names) , len(df_pe_max_reduced)
    
#print len(df['file_name'])
#print file_name

df_pe_max_reduced.to_csv('combined_short_summary_'+scenarios_file2_name[scenario]+'.csv',sep=',')
#df_pe_max_reduced.to_csv('output_innercity.csv',sep=',')
#df_pe_max_reduced.to_csv('output_rural.csv',sep=',')
