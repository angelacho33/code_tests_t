from tk_databank import * 
from tk_plot import Plot
import tk_err
import numpy as np
import glob
import csv
import read_airbus_sar_data
import sys
import matplotlib
#matplotlib.rcParams.update({'font.size': 20})
import matplotlib.pyplot as plt 
import os
import gc ##For the memory leak?
import tk_gdfr_New as tk_gdfr_New ##Script that Daniel sent me...uses one function from there.
    
scenario_input=int(sys.argv[1]) ##0:highway_probes, 1:innerCity ,2:rural 3:InnercityCrossing

scenarios        = ['highway','innercity','rural']
#number_dbk_files = [324      ,202        ,115]
number_dbk_files = [1      ,1        ,1]
#number_dbk_files = [5      ,5        ,5]

#file_name=str(sys.argv[1])

all_pe_distances=[]
all_headings_differences=[]
all_speeds_differences=[]

reduce_info_gps=[]
reduce_info_heading=[]
reduce_info_speed=[]

max_pe_distance=20

file_id=0
files_with_small_pe_distances=0
for i in np.arange(0,number_dbk_files[scenario_input],1):
    #file='output_file_id_'+str(i)+'.dbk'
    file='post_processed_output_file_id_'+str(i)+'.dbk'
    if not os.path.exists(file):
        continue
    E=DataBank()
    E.load(file)
    
    ##Calculate differences for heading and speed
    differences=[]
    for sensor_id in [1,2]:
        process_id=0
        raw_data    = E.get_sensor_raw_data(sensor_id)
        recon_data  = E.get_sensor_recon_data(sensor_id, process_id)
        ##Trimming of raw data..taken from tk_databank calculate deviations...
        recon_data_len = len(recon_data['values'])
        tss_raw   = raw_data['tss'][:recon_data_len]
        tss_recon = recon_data['tss']
        component_raw   = list( zip(*raw_data['values'][:recon_data_len])[0] )
        component_recon = list( zip(*recon_data['values'])[0] )
        component_recon_interpol = tk_err.interpolate(tss_raw, tss_recon, component_recon)
        e_v      = tk_err.e(component_raw, component_recon_interpol)     # error (pointwise difference)
        ###
        ##Array with differences to be used later
        differences.append(tk_err.e(component_raw, component_recon_interpol))     # error (pointwise difference)
        #print e_v
        #print e_v.max()
    
    differences=np.asarray(differences)
    
    
    ####get gps deviations of the post procesed data
    process_id=1 #1 contains the after smoothing data
    sensor_id=0
    gps_deviation1 = E.get_gps_deviations(sensor_id, process_id)
    print 'pe_deviations: ', gps_deviation1['pe_deviations']
    print 'pp_deviations: ', gps_deviation1['pp_deviations']
    print 'wgs84_pp_deviations: ',gps_deviation1['wgs84_pp_deviations']
    pe_distances1=np.asarray(gps_deviation1['pe_distances'])
    pp_distances1=np.asarray(gps_deviation1['pp_distances'])
    wgs84_pp_distances1=np.asarray(gps_deviation1['wgs84_pp_distances'])
    
    ##Create huge array with all distances, headings and speeds
    if gps_deviation1['pe_deviations']['max_distance']<=max_pe_distance:
        all_pe_distances.append(pe_distances1)
        all_headings_differences.append(differences[0])
        all_speeds_differences.append(differences[1])
        files_with_small_pe_distances=files_with_small_pe_distances+1
        print 'There are '+str(files_with_small_pe_distances)+' files with pe distances <='+str(max_pe_distance)
    print 'file_id', file_id
    
    
    reduce_info_ratio_gps=E.get_sensor_process_info(0,1)['reduce_info']['ratio']
    reduce_info_ratio_heading=E.get_sensor_process_info(1,0)['reduce_info']['ratio']
    reduce_info_ratio_speed=E.get_sensor_process_info(2,0)['reduce_info']['ratio']

    reduce_info_gps.append(reduce_info_ratio_gps)
    reduce_info_heading.append(reduce_info_ratio_heading)
    reduce_info_speed.append(reduce_info_ratio_speed)
    
    ########## Plots
    #P=Plot(E)
    #P.plot(0,1,deviation='difference',dt_format='%Y-%m-%d %H:%M:%S',save_png=True,suptitle='File_id_'+str(file_id))
    #P.plot(1,0,deviation='difference',dt_format='%Y-%m-%d %H:%M:%S',save_png=True,suptitle='File_id_'+str(file_id))
    #P.plot(2,0,deviation='difference',dt_format='%Y-%m-%d %H:%M:%S',save_png=True,suptitle='File_id_'+str(file_id))
    
    #P.plot_rec_inspection(0, 1, output='png', fft_yscale='log', dt_format='%Y-%m-%d %H:%M:%S')
    #P.plot_rec_inspection(1, 0, output='png', fft_yscale='log', dt_format='%Y-%m-%d %H:%M:%S')
    #P.plot_rec_inspection(2, 0, output='png', fft_yscale='log', dt_format='%Y-%m-%d %H:%M:%S')
    
    ####################################Box plots###############################
    #P.plot_boxplot_values(,,)
    #fig1, ax1 = plt.subplots(3,1)
    #plt.subplots_adjust(left=0.125,right=0.9,bottom=0.1,top=0.9,wspace=0.2,hspace=0.9)
    #meanpointprops = dict(marker='D', markerfacecolor='w')
    #ax1[0].boxplot(pe_distances1, showmeans=True, meanprops=meanpointprops , labels=['pe_distances'],whis=[2.5,97.5],vert=False,widths=0.9)
    #ax1[0].set_xlabel('point to edge distances (m)')
    #ax1[0].set_title('PE Distances, Eff. Red: '+str(round(reduce_info_ratio_gps,3)))
    
    #ax1[1].boxplot(differences[0], showmeans=True, meanprops=meanpointprops , labels=['heading'],whis=[2.5,97.5],vert=False,widths=0.9)
    #ax1[1].set_xlabel('Heading differences')
    #ax1[1].set_title('Heading, Eff. Red: '+str(round(reduce_info_ratio_heading,3)))
   # 
    #ax1[2].boxplot(differences[1], showmeans=True, meanprops=meanpointprops , labels=['speed'],whis=[2.5,97.5],vert=False,widths=0.9)
    #ax1[2].set_xlabel('Speed differences')
    #ax1[2].set_title('Speed, Eff. Red: '+str(round(reduce_info_ratio_speed,3)))
    
    #fig1.suptitle('File_id:'+str(file_id))
    
    #if not os.path.exists('./individual_box_plots'):
    #    os.makedirs('./individual_box_plots')
    #fig1.savefig('individual_box_plots/File_'+str(file_id)+'_box_plot.png')
    ###########################################################################
    
    #####################################Maps###################################
    raw_data = E.get_sensor_raw_data(0)
    recon_data = E.get_sensor_recon_data(0,1)
    #print recon_data['values']
    
    ##Trimming the raw data according to recon data...taken from tk_databank (calculate deviations)
    recon_data_len = len(recon_data['values'])
    tss_raw   = raw_data['tss'][:recon_data_len]
    tss_recon = recon_data['tss']
    
    component_raw_lat   = list( zip(*raw_data['values'][:recon_data_len])[0] )
    component_recon_lat = list( zip(*recon_data['values'])[0] )
    component_recon_interpol_lat = tk_err.interpolate(tss_raw, tss_recon, component_recon_lat)
    
    component_raw_lon   = list( zip(*raw_data['values'][:recon_data_len])[1] )
    component_recon_lon = list( zip(*recon_data['values'])[1] )
    component_recon_interpol_lon = tk_err.interpolate(tss_raw, tss_recon, component_recon_lon)
    ###############
    
    ##Need to convert to a list of lists to be used in the functions later    
    component_raw_lat=[[x] for x in component_raw_lat]
    component_recon_lat=[[x] for x in component_recon_lat]
    
    component_raw_lon=[[x] for x in component_raw_lon]
    component_recon_lon=[[x] for x in component_recon_lon]
    
    #print  component_recon_lat, len(component_recon_lat)
    #print  len(component_recon_interpol_lat), component_recon_interpol_lat
    
    #print  len(component_recon_interpol_lon), component_recon_interpol_lon
    #print  len(component_recon_lon), component_recon_lon
    
    ### maps with gps deviations
    #from tk_gmd_gps_point import maps
    #import random
    #sensor_data = get_ts_value_from_file("daimler.csv", ' ', 2, 3, 1, [4])
    #gpsdata = combin_list(sensor_data['GPS_Lat']['values'],sensor_data['GPS_Long']['values'])
    #print(gpsdata[0][0])
    #gpsdata = tk_gdfr_New.combin_list(component_raw_lat,component_raw_lon)
    #gpsdata_rec = tk_gdfr_New.combin_list(component_recon_lat,component_recon_lon)
    #print "This is the GPS data",gpsdata
    #print(gpsdata[0][0])
    #mymap = maps(gpsdata[0][0], gpsdata[0][1], 16)
    #path = gpsdata
    #path_rec = gpsdata_rec
    #v = []
    #for i in range(len(gpsdata)):
    #    #v.append(diff_speed[i]*150/max(diff_speed))
    #       v.append(gps_deviation1['pe_distances'][i])
    #mymap.addpath(path)
    #mymap.addcolorpath(path_rec,v)
    #for i in range(len(component_raw_lat)):
    #    mymap.addpoint(gpsdata[i][0], gpsdata[i][1])
    #    mymap.addpoint(gpsdata_rec[i][0], gpsdata_rec[i][1], color = '#00FF00')
    #if not os.path.exists('./maps_gps'):
    #    os.makedirs('./maps_gps')
    #mymap.draw('maps_gps/'+'File_id_'+str(file_id)+'_tk_googlemaps_gps.html')
    
    #print max(v)
    #print (v)
    ###
    
    ###maps with speed deviations
    from tk_gmd_speed_point import maps
    import random
    
    #sensor_data = get_ts_value_from_file("daimler.csv", ' ', 2, 3, 1, [4])
    #gpsdata = combin_list(sensor_data['GPS_Lat']['values'],sensor_data['GPS_Long']['values'])
    #print(gpsdata[0][0])
    gpsdata = tk_gdfr_New.combin_list(component_raw_lat,component_raw_lon)
    gpsdata_rec = tk_gdfr_New.combin_list(component_recon_lat,component_recon_lon)
    #print "This is the GPS data",gpsdata
    #print(gpsdata[0][0])
    mymap = maps(gpsdata[0][0], gpsdata[0][1], 16)
    path = gpsdata
    path_rec = gpsdata_rec
    v = []
    for i in range(len(gpsdata)):
        #v.append(diff_speed[i]*150/max(diff_speed))
	   v.append(differences[1][i])
    mymap.addpath(path)
    mymap.addcolorpath(path_rec,v)
    for i in range(len(component_raw_lat)):
        mymap.addpoint(gpsdata[i][0], gpsdata[i][1])
        mymap.addpoint(gpsdata_rec[i][0], gpsdata_rec[i][1], color = '#00FF00')
    if not os.path.exists('./maps_speed'):
        os.makedirs('./maps_speed')
    mymap.draw('maps_speed/'+'File_id_'+str(file_id)+'_tk_googlemaps_speed.html')
    #mymap.draw('./tk_googlemaps_speed.html')
    
    print 'speed_differences: ', differences[1][:10]
    
    #############################################################################
    #P.plot_raw_inspection(sensor_id, output='screen', fft_yscale='log', dt_format='%Y-%m-%d %H:%M:%S')
    #P.plot_rec_inspection(sensor_id, process_id, output='screen', fft_yscale='log', dt_format='%Y-%m-%d %H:%M:%S')
    #
    #P.show()
    #fig1.clf()
    #plt.close('all')
    file_id=file_id+1
    gc.collect()


#####################################################################################################
#####################################################################################################
#####################################################################################################
##Once the loop over all files are finished...create box plot with all distances, headings and speeds
##
#print all_pe_distances
#print all_headings_differences
#print all_speeds_differences

#pe_distances=[]
#speeds=[]
#headings=[]
##I have an array of arrays for the box plot I need a huge single array with aaaaaalll entries
#for i in range(0,len(all_pe_distances)):
#    for j in range(0,len(all_pe_distances[i])):
#        pe_distances.append(all_pe_distances[i][j])
#        headings.append(all_headings_differences[i][j])
#        speeds.append(all_speeds_differences[i][j])
#        
#pe_distances_array=np.asarray(pe_distances)                            
#headings_array=np.asarray(headings)                   
#speeds_array=np.asarray(speeds)                            ##

#fig1, ax1 = plt.subplots(3,1,figsize=(30,15))
#plt.subplots_adjust(left=0.125,right=0.9,bottom=0.1,top=0.9,wspace=0.2,hspace=0.6)#

#meanpointprops = dict(marker='D', markerfacecolor='w')
#ax1[0].boxplot(pe_distances_array, showmeans=True, meanprops=meanpointprops , labels=['pe_distances'],whis=[2.5,97.5],vert=False,widths=0.9)
#ax1[0].set_xlabel('point to edge distances (m)')
#ax1[0].set_title('#Files: '+str(files_with_small_pe_distances)+' ,Mean Effective reduction: '+ str(round(np.asarray(reduce_info_gps).mean(),3)))###

#ax1[1].boxplot(headings_array, showmeans=True, meanprops=meanpointprops , labels=['headings'],whis=[2.5,97.5],vert=False,widths=0.9)
#ax1[1].set_xlabel('Heading differences')
#ax1[1].set_title('#Files: '+str(files_with_small_pe_distances)+' ,Mean Effective reduction: '+ str(round(np.asarray(reduce_info_heading).mean(),3)))#

#ax1[2].boxplot(speeds_array, showmeans=True, meanprops=meanpointprops , labels=['speeds'],whis=[2.5,97.5],vert=False,widths=0.9)
#ax1[2].set_xlabel('Speed differences')
#ax1[2].set_title('#Files: '+str(files_with_small_pe_distances)+' ,Mean Effective reduction: '+ str(round(np.asarray(reduce_info_speed).mean(),3)))

#if scenario_input==0:
    #fig1.suptitle('Heading scenario, 323/404 sessions, 80% reduction,framesize=500', fontsize=40)
    #fig1.suptitle('Highway scenario, 80% reduction,framesize=500, Files with max pe_distance<='+str(max_pe_distance)+'m', fontsize=40)
#    fig1.suptitle('Highway scenario, 80% reduction,framesize=500', fontsize=40)
#if scenario_input==1:
    #fig1.suptitle('Innercity scenario, 201/237 sessions, 80% reduction, Framesize=500', fontsize=40)
    #fig1.suptitle('Innercity scenario, 80% reduction,framesize=500, Files with max pe_distance<='+str(max_pe_distance)+'m', fontsize=40)
#    fig1.suptitle('Innercity scenario, 80% reduction,framesize=500', fontsize=40)
#if scenario_input==2:
    #fig1.suptitle('Rural scenario, 114/140 sessions, 80% reduction, Framesize=500', fontsize=40)
    #fig1.suptitle('Rural scenario, 80% reduction,framesize=500, Files with max pe_distance<='+str(max_pe_distance)+'m', fontsize=40)
#    fig1.suptitle('Rural scenario, 80% reduction,framesize=500', fontsize=40)

#name1='box_plots_'+scenarios[scenario_input]+'_all_differences.png'
#name1='box_plots_'+scenarios[scenario_input]+'_all_differences_20m.png'
#if not os.path.exists('./box_plots'):
#    os.makedirs('./box_plots')#

#plt.savefig('box_plots/'+name1)
