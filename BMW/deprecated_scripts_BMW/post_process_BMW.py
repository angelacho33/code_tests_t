##Script that goes through produced dbk files for BMW (with a file_id in the name)
##and produces a new table after smoothing and calculating gps distances.
##Note: I set the number of files here explcitly in a variable since I have not
##managed to order the dbk files by name using sorted and glob...:S

import cPickle as pickle
from tk_databank import * 
#from tk_plot import Plot
import numpy as np
import glob
import csv
import sys
import glob
scenario_input=int(sys.argv[1]) ##0:highway_probes, 1:innerCity ,2:rural 3:InnercityCrossing
scenarios=['highway','innercity','rural']
#folder='/home/angelh/Documents/DataScienceRelated/Teraki/BMW_output/tenth_run/'
#folder='/home/angel/BMW_output_updated_ALL_updated_parameters_80_percent/'

#filenames=sorted(glob.glob(folder+scenarios[scenario_input]+'/dbk_files/*.dbk'))
#filenames=sorted(glob.glob(folder+scenarios[scenario_input]+'/*.dbk'))
#filenames=[filenames[0]] #Just the first file
#filenames=filenames[:2] #Just the first files

#print filenames
#file=filenames[0]


output_file='After_smoothing_'+scenarios[scenario_input]+'_output_recon.csv'
with open(output_file, 'w',0) as f_recon:
    #Output file
    writer_recon = csv.writer(f_recon,quoting=csv.QUOTE_NONNUMERIC)
    writer_recon.writerow(('file_id','npoints','sensor_id', 'sensor_name','value_name','process_id' ,'framesize','reduction', 
                           'tss_recon_min','tss_recon_max', 'values_recon_min','values_recon_max', 'values_recon.mean', 
                           'zlib_ratio', 'count_red','red_info_ratio',
                           'dev_max','dev_mean','dev_min',
                           'relerr','mape','smape','wpe','wape','rrmse','mae',
                           'file_name') )
    
    
    #all_pe_distances=[]
    #all_pp_distances=[]
    #all_wgs84_pp_distances=[]
    
    if scenario_input==0:
        window_lenght=[19] 
        n_files=324
    if scenario_input==1:
        window_lenght=[19]
        n_files=202
    if scenario_input==2:
        window_lenght=[19]
        n_files=115

    file_id=0
    #n_files=1
    for i in np.arange(0,n_files,1):
    #for file in filenames:
        #file=folder+scenarios[scenario_input]+'/output_file_id_'+str(i)+'.dbk'
        #file=scenarios[scenario_input]+'/output_file_id_'+str(i)+'.dbk'
        file='output_file_id_'+str(i)+'.dbk'
        #if scenario_input==1 and i==72: ##File 72 for innercity was not produced in an erliers test...keine anung
        #    file_id=file_id+1
        #    continue
        print file
        E=DataBank()
        E.load(file)
        if len(E.get_sensor_process_info(0))==0: ##Check dbk files with actual processed data...some files does not include this since are smaller than framesize
            file_id=file_id+1
            continue
        #E.get_sensor_list()
        
        sensor=E.get_sensor_list()[0]########### Sensor 0 is the GPS!!
        index=sensor['index']
        dof=sensor['dof']
        #sensor_id=0  
        sensor_id=sensor['sensor_id']
        sensor_name=sensor['sensor_name']
        
        #E.get_sensor_raw_data(sensor_id)
        #SD=E.get_sensor_object(sensor_id,process_id)
        #raw_data=SD.get_raw_data()
        #recon_data=SD.get_raw_data()
        
        ##Raw data
        raw_data=E.get_sensor_raw_data(sensor_id)
        tss=np.asarray(raw_data['tss'])
        values=np.asarray(raw_data['values'])
        npoints=raw_data['count']
        #if npoints<framesize:
        #    break
        
        ############Process info
        print 'Len process info ', len(E.get_sensor_process_info(sensor_id))
        print 'Framesize', E.get_sensor_process_info(sensor_id)[0]['framesize'], 'Reduction', E.get_sensor_process_info(sensor_id)[0]['reduction']
        framesize=E.get_sensor_process_info(sensor_id)[0]['framesize']
        reduction=E.get_sensor_process_info(sensor_id)[0]['reduction']
        process_id=0  ########### Atm only one process in the dbk files
        print 'sensor_id: ' , sensor_id, ',sensor_name:', sensor_name, ',process_id:', process_id
        
        ############  Deviations without smoothing  ##############
        gps_deviation0 = E.get_gps_deviations(sensor_id, process_id)
        #print gps_deviation0['pe_deviations']
        #print gps_deviation0['pp_deviations']
        #print gps_deviation0['wgs84_pp_deviations']
        pe_distances0=np.asarray(gps_deviation0['pe_distances'])
        pp_distances0=np.asarray(gps_deviation0['pp_distances'])
        pp_distances0=np.asarray(gps_deviation0['wgs84_pp_distances'])
        
        print 'max= ',pe_distances0.max()
        ###########    Applying smoothing           ##############
        post_process_para = {
            "method": "savgol_filter",
            "window_length": window_lenght[0], ##33 for innercity?
            "polyorder": 2
        }
        SD_post=E.sensor_data_post_process(sensor_id, process_id, post_process_para)
        #############################Caution!!! The post process data would be now in process_id+1
        print 'Len process info ', len(E.get_sensor_process_info(sensor_id))
        process_id=1
        
        ############  Deviations with smoothing  ##############
        gps_deviation1 = E.get_gps_deviations(sensor_id, process_id)
        print 'pe_deviations: ', gps_deviation1['pe_deviations']
        print 'pp_deviations: ', gps_deviation1['pp_deviations']
        print 'wgs84_pp_deviations: ',gps_deviation1['wgs84_pp_deviations']
        pe_distances1=np.asarray(gps_deviation1['pe_distances'])
        pp_distances1=np.asarray(gps_deviation1['pp_distances'])
        wgs84_pp_distances1=np.asarray(gps_deviation1['wgs84_pp_distances'])
        
        #all_pe_distances.append(pe_distances1)
        #all_pp_distances.append(pp_distances1)
        #all_wgs84_pp_distances.append(wgs84_pp_distances1)
        
        #print 'len(all_pe_distances)=', len(all_pe_distances)
        
        min_pe_distance=pe_distances1.min()
        max_pe_distance=pe_distances1.max()
        mean_pe_distance=pe_distances1.mean()
        
        min_pp_distance=pp_distances1.min()
        max_pp_distance=pp_distances1.max()
        mean_pp_distance=pp_distances1.mean()

        min_wgs84_pp_distance=wgs84_pp_distances1.min()
        max_wgs84_pp_distance=wgs84_pp_distances1.max()
        mean_wgs84_pp_distance=wgs84_pp_distances1.mean()
        
        
        print 'max pe_distance= ',pe_distances1.max()
        
        #print all_pe_distances
        ################################################################################
        recon_data=E.get_sensor_recon_data(sensor_id,process_id)
        tss_recon=np.asarray(recon_data['tss'])
        values_recon=np.asarray(recon_data['values'])
        zlib_ratio=E.get_zip_reduction_ratio(sensor_id,framesize)
        
        #Reduce info
        reduce_info=E.get_sensor_process_info(sensor_id, process_id)['reduce_info']
        count_red=reduce_info['count_red']
        red_info_ratio=reduce_info['ratio']
        
        #Deviation_info
        deviations=E.get_deviations(sensor_id,process_id)
        
        for i in range(dof):
            value_name=sensor['value_names'][i]
            values_comp=values_recon.T[i]
            dev_max=deviations['E'][i]['max']
            dev_mean=deviations['E'][i]['mean']
            dev_min=deviations['E'][i]['min']
            
            relerr=deviations['relerr'][i]
            mae=deviations['MAE'][i]
            mape=deviations['MAPE%'][i]
            smape=deviations['SMAPE%'][i]
            rrmse=deviations['RRMSE%'][i]
            wape=deviations['WAPE%'][i]
            wpe=deviations['WPE%'][i]
            
            
            
            #Write output
            writer_recon.writerow((file_id,npoints,sensor_id,sensor_name, value_name,process_id ,framesize,reduction, 
                                   tss_recon.min(),tss_recon.max(), float("%.3f"%values_comp.min()), float("%.3f"%values_comp.max()), float("%.3f"%values_comp.mean()), 
                                   float("%.3f"%zlib_ratio), count_red,float("%.3f"%red_info_ratio), 
                                   dev_max,dev_mean,dev_min,
                                   relerr,mape,smape,wpe,wape,rrmse,mae,
                                   file))
            
            ##Plot
            #P=Plot(E)
            #P.plot(sensor_id,process_id,deviation='difference',dt_format='%M:%S',save_png=True)
            #P.plot(sensor_id,process_id+1,deviation='difference',dt_format='%M:%S',save_png=True)
            #P.plot_raw_inspection(sensor_id, output='screen', fft_yscale='log', dt_format='%Y-%m-%d %H:%M:%S')
            #P.plot_rec_inspection(sensor_id, process_id, output='screen', fft_yscale='log', dt_format='%Y-%m-%d %H:%M:%S')
            #P.plot_rec_inspection(sensor_id, process_id, output='png', fft_yscale='log', dt_format='%Y-%m-%d %H:%M:%S')
            #P.show()
            
        value_name='pe_distance'
        writer_recon.writerow((file_id,npoints,sensor_id,sensor_name, value_name,process_id ,framesize,reduction,
                               tss_recon.min(),tss_recon.max(), 'NULL' , 'NULL' , 'NULL',
                               'NULL','NULL','NULL',
                               max_pe_distance,mean_pe_distance,min_pe_distance,
                               'NULL','NULL','NULL','NULL','NULL','NULL','NULL',
                               file))

        value_name='pp_distance'
        writer_recon.writerow((file_id,npoints,sensor_id,sensor_name, value_name,process_id ,framesize,reduction,
                               tss_recon.min(),tss_recon.max(), 'NULL' , 'NULL' , 'NULL',
                               'NULL','NULL','NULL',
                               max_pp_distance,mean_pp_distance,min_pp_distance,
                               'NULL','NULL','NULL','NULL','NULL','NULL','NULL',
                               file))
                                
        value_name='wgs84_pp_distance'
        writer_recon.writerow((file_id,npoints,sensor_id,sensor_name, value_name,process_id ,framesize,reduction,
                               tss_recon.min(),tss_recon.max(), 'NULL' , 'NULL' , 'NULL',
                               'NULL','NULL','NULL',
                               max_wgs84_pp_distance,mean_wgs84_pp_distance,min_wgs84_pp_distance,
                               'NULL','NULL','NULL','NULL','NULL','NULL','NULL',
                               file))
        
        print '** Goes in file: ',file_id,''
        name='post_processed_output_file_id_'+str(file_id)+'.dbk'
        E.save(name)
        file_id=file_id+1
##Save huge array of distances to a cpicle object:
#with open("all_distances.pickle", "wb") as f:
    #pickle.dump((all_pe_distances,all_pp_distances,all_wgs84_pp_distances),f)
#    pickle.dump(all_pe_distances,f)
#    pickle.dump(all_pp_distances,f)
#    pickle.dump(all_wgs84_pp_distances,f)

##Load:
#f = open("all_distances.pickle", "r")
#all_pe_distances = pickle.load(f)
#all_pp_distances = pickle.load(f)
#all_wgs84_pe_distances = pickle.load(f)

#######################################
##Example to dum and load several files:
#a = [1,2]
#b = [3,4]
#with open("tmp.pickle", "wb") as f:
#    pickle.dump((a,b), f)

#with open("tmp.pickle", "rb") as f:
#    a,b = pickle.load(f) 

