#First test using the Teraki software
import os, sys, time
from tk_databank import DataBank
#from tk_plot import Plot
import numpy as np

import tk_gdfr 

import  glob
import csv

##Output files


#filenamelist = glob.glob("/home/angel/Files/BMW/probes/highway_probes/*.csv")
#filenamelist = glob.glob("/home/angelh/Documents/DataScienceRelated/Teraki/Files/BMW/probes/highway_probes/*.csv")
#filenamelist = glob.glob("/home/angelh/Documents/DataScienceRelated/Teraki/Files/BMW/probes/rural/*.csv")
filenamelist = glob.glob("/home/angelh/Documents/DataScienceRelated/Teraki/Files/BMW/probes/innerCity/*.csv")

#filenamelist = filenamelist[:100]
#filenamelist = filenamelist[:7] ##Files...#AngelH April 15th Number of files suggested by Daniel
#filenamelist = filenamelist[:10] ##Files...#AngelH April 15th
#filenamelist=['/home/angelh/Documents/DataScienceRelated/Teraki/Files/BMW/probes/highway_probes/2eef208-63c2-4a9f-8098-28d8207d1bb_1.csv']##1M file
#filenamelist=['/home/angelh/Documents/DataScienceRelated/Teraki/Files/BMW/probes/highway_probes/f88520-2efa-46e6-8fc0-35c353f84922_1.csv']##24k file
#print filenamelist

with open('summary_innercity_output_raw.csv', 'w',0) as f_raw:
    writer_raw = csv.writer(f_raw,quoting=csv.QUOTE_NONNUMERIC)
    writer_raw.writerow(('file_id','sensor_id','index', 'sensor_name','value_name','npoints', 'tss.min()','tss.max()','value_min','value_max', file))
    sensor_id=0 ##One sensor id per file?...not really..each file has 3 sensors: pos, speed and heading...:S
    #D = DataBank()
    file_id=0
    for file in filenamelist:
        D = DataBank()
        #Load Pos Data
        value_names=['lat','lon']
        sensor_data1 = tk_gdfr.get_ts_value_from_file(file, ',', 0, 2, 1, [3,4])
        S1 = D.add_sensor_data(sensor_data1['pos']['values'],
                              sensor_data1['pos']['tss'], 0, "gps", "bmw_dataset_pos",value_names=value_names)
        
        #Load Heading Data
        sensor_data2 = tk_gdfr.get_ts_value_from_file(file, ',', 0, 2, 1, [5])
        S2 = D.add_sensor_data(sensor_data2['pos']['values'],
                              sensor_data2['pos']['tss'], 1, "heading", "bmw_dataset_heading")

        #Load Speed Data
        sensor_data3 = tk_gdfr.get_ts_value_from_file(file, ',', 0, 2, 1, [6])
        S3 = D.add_sensor_data(sensor_data3['pos']['values'], 
                              sensor_data3['pos']['tss'], 2, "speed", "bmw_dataset_speed")
        
        
        #Raw data info
        for sensor in D.get_sensor_list():
            index=sensor['index']
            dof=sensor['dof']
            sensor_id=sensor['sensor_id']
            sensor_name=sensor['sensor_name']
            
            raw_data=sensor['sensor_data'].get_raw_data()
            npoints=raw_data['count']
            tss=np.asarray(raw_data['tss'])
            
            for i in range(dof):
                value_name=sensor['value_names'][i]
                value_min=raw_data['min_v'][i]
                value_max=raw_data['max_v'][i]
                writer_raw.writerow((file_id,sensor_id,index, sensor_name,value_name,npoints, tss.min(),tss.max(),float("%.3f"%value_min),float("%.3f"%value_max), file))
                
        file_id=file_id+1
        
#f_raw.close()
#f_recon.close()        
#Print summary
#P.print_summary_sensors()
#Print deviation for sensor_id=1
#P.print_deviations_as_table(sensor_id=9)
#Add subplots for all deviations= difference, ratio and rel. error 
#P.plot(sensor_id=9,process_id=0,deviation='all')    
