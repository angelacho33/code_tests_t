#First test using the Teraki software
import os, sys, time
from tk_databank import DataBank
#from tk_plot import Plot
import numpy as np

import tk_gdfr 

import  glob
import csv

##Output files


#filenamelist = glob.glob("/home/angel/Files/BMW/probes/highway_probes/*.csv")
#filenamelist = glob.glob("/home/angelh/Documents/DataScienceRelated/Teraki/Files/BMW/probes/highway_probes/*.csv")

filenamelist = sorted(glob.glob('/home/angel/Files/BMW/probes/innerCity/*.csv'),key=os.path.getsize,reverse=True)
#filenamelist = filenamelist[:100]
#filenamelist = filenamelist[:5] ##Files...#AngelH April 15th Number of files suggested by Daniel
filenamelist = filenamelist[:4] ##Files...#AngelH April 15th
#filenamelist=['/home/angel/Files/BMW/probes/InnercityCrossing/897607a-74f3-41c1-a375-3a042f424c2_1.csv']##1M file
#filenamelist=['/home/angel/Files/BMW/probes/InnercityCrossing/72bb66e2-5c10-4a84-bb85-2c052718583_1.csv']##8k file
#filenamelist=['/home/angel/Files/BMW/probes/innerCity/71a56a4e-648d-4f34-b7af-310b13d4c27_1.csv']##1M file
#print filenamelist

with open('summary_innercity_speed_output_recon.csv', 'w',0) as f_recon:
    #Output file
    writer_recon = csv.writer(f_recon,quoting=csv.QUOTE_NONNUMERIC)
    writer_recon.writerow( ('sensor_id', 'process_id' ,'framesize','reduction', 'tss_recon.min()','tss_recon.max()', 'values_recon.min()','values_recon.max()', 'values_recon.mean()', 'zlib_ratio', 'count_red','red_info_ratio','dev_max','dev_mean','dev_min','relerr','mape','wpe','rrmse') )
    ##
    sensor_id=0 ##One sensor id per file?...not really..each file has 3 sensors: pos, speed and heading...:S
    D = DataBank()    
    for file in filenamelist:    
        #Load Speed Data
        sensor_data = tk_gdfr.get_ts_value_from_file(file, ',', 0, 2, 1, [6])
        S = D.add_sensor_data(sensor_data['pos']['values'], 
                              sensor_data['pos']['tss'], sensor_id, "speed (Innercity)", "bmw_dataset_speed")
        
        index=D.get_sensor_info(sensor_id)['index']
        
        #Raw data info
        raw_data=D.get_sensor_raw_data(sensor_id)
        npoints=raw_data['count']
        tss=np.asarray(raw_data['tss'])
        values=np.asarray(raw_data['values'])
        #writer_raw.writerow((sensor_id, npoints, tss.min(),tss.max(), values.min(),values.max(),values.mean(), file))
        
        ####Process files
        ##Pre and post process
        ####Daniel Version of pre and pos
        #pre_process = {
        #    "multiple": [1]
        #}
        #post_process = {
        #    "treshhold": [5], ## 8
        #    "maximum": [300], ##
        #}
        ##
        #########Quing version of pre and pos with Daniel Parameters.
        pre_process = {
            "multiple": [1000],
            "value_exceptions": [
                [
                    {
                        "e_name": "red_peak_points",
                        "e_id":  2,
                        "v_types": ["red"],
                        "condition": "(abs(delta_v)>4000)",
                        "para": "v"
                    },
                    {
                        "e_name": "rest_peak_points",
                        "e_id":  3,
                        "v_types": ["rest"],
                        "condition": "(abs(delta_v)>4000)",
                        "para": "v"
                    }
                ]
            ]
        }
        post_process = {
            "divide": [1000.0]
        }
        ################################################
        
        ##Process cases
        #framesizes=[100,200,300,400,500,600,700,800,900,1000]
        #reductions=[0.6,0.7,0.8,0.9]
        
        #framesizes=[100,300,500,800,1000] #AngelH April 15th
        #reductions=[0.6,0.7,0.8,0.9]      #AngelH Aptil 15th
        
        framesizes=[100,500,1000] #AngelH April 15th                                                                                                                                                               
        reductions=[0.6,0.8,0.9]      #AngelH Aptil 15th 
        
        #Short test
        #framesizes=[100,200]
        #reductions=[0.9]
        
        process_cases=[]
        for framesize in framesizes:
            for reduction in reductions:
                process_cases.append([framesize,reduction])
    
        process_id=0
                
        for item in process_cases:        
            framesize = item[0]
            reduction = item[1]
            #A,red_points,zlib_ratio=D.sensor_data_process(sensor_id, framesize, reduction,'cii_dct_v011', pre_process, post_process,10)
            D.sensor_data_process(sensor_id, framesize, reduction,'cii_dct_v011', pre_process, post_process,10)
            #zlib_ratio=0 ##AngelH: Update to the real value
            zlib_ratio=D.get_zip_reduction_ratio(sensor_id,framesize)#New function
            ##Plots
            #P=Plot(D)
            #P.plot(sensor_id,process_id,deviation='difference')    
            #P.plot(sensor_id,process_id,deviation='all')    
            
            #Recon data info
            recon_data=D.get_sensor_recon_data(sensor_id,process_id)
            tss_recon=np.asarray(recon_data['tss'])
            values_recon=np.asarray(recon_data['values'])
            #framesize=recon_data['framesize']
            #reduction=recon_data['reduction']
            
            #Reduce info
            reduce_info=D.get_sensor_process_info(sensor_id, process_id)['reduce_info']
            count_red=reduce_info['count_red']
            red_info_ratio=reduce_info['ratio']
            
            #Deviation_info
            deviations=D.get_deviations(sensor_id,process_id)
            
            dev_max=deviations['E'][0]['max']
            dev_mean=deviations['E'][0]['mean']
            dev_min=deviations['E'][0]['min']
            
            relerr=deviations['relerr'][0]
            mape=deviations['MAPE%'][0]
            wpe=deviations['WAPE%'][0]
            rrmse=deviations['RRMSE%'][0]
            
            ##Print out files
            #D.write_sensor_data_files(sensor_id)
            #Trying to get a single file for reconstructed data I get an error...oh well...
            #recondata_filename = 'recon_data_'+str(sensor_id)+'_'+str(process_id)+'_'+str(framesize)+'_'+str(reduction)+'.csv'
            #D.write_sensor_file(index, recondata_filename, 'recon_data')
            
            writer_recon.writerow((sensor_id, process_id ,framesize,reduction, tss_recon.min(),tss_recon.max(), 
                                   float("%.3f"%values_recon.min()), float("%.3f"%values_recon.max()), float("%.3f"%values_recon.mean()), float("%.3f"%zlib_ratio), count_red,float("%.3f"%red_info_ratio), 
                                   float("%.3f"%dev_max),float("%.3f"%dev_mean),float("%.3f"%dev_min),float("%.3f"%relerr),float("%.3f"%mape),float("%.3f"%wpe),float("%.3f"%rrmse)))
            
            #print 'raw: ', sensor_id, npoints, tss.min(),tss.max(), values.min(),values.max(),values.mean(), file 
            #print 'recon: ', sensor_id, process_id ,framesize,reduction, tss_recon.min(),tss_recon.max(), values_recon.min(),values_recon.max(), values_recon.mean(), zlib_ratio, red_points, relerr,mape,wpe,rrmse 
        
            process_id=process_id+1
    
        sensor_id=sensor_id+1
        
#f_raw.close()
#f_recon.close()
        
#Print summary
#P.print_summary_sensors()
#Print deviation for sensor_id=1
#P.print_deviations_as_table(sensor_id=9)
#Add subplots for all deviations= difference, ratio and rel. error 
#P.plot(sensor_id=9,process_id=0,deviation='all')    
