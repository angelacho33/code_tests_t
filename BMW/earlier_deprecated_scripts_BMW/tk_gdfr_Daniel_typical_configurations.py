from tk_databank import DataBank
from tk_plot import Plot
file = "./probes/rural/a0c70f7-5c91-49ce-982b-696a714554df_1.csv"
sensor_data = get_ts_value_from_file(file, ',', 0, 2, 1, [3,4])
D = DataBank()
S = D.add_sensor_data(sensor_data['pos']['values'],
                      sensor_data['pos']['tss'], 8, "gps", "bmw_dataset_pos")
pre_process = {
    "multiple": [100000, 100000]
}
post_process = {
    "divide": [100000.0, 100000.0]
}
D.sensor_data_process(8, 800, 0.95, 'cii_dct_v011', pre_process, post_process, 5) ##5 block 
#print D.get_sensor_raw_data(8)
#print D.get_sensor_red_data(8, 0)
#D.write_sensor_data_files(8)

sensor_data = get_ts_value_from_file(file, ',', 0, 2, 1, [5])
S = D.add_sensor_data(sensor_data['pos']['values'],
                      sensor_data['pos']['tss'], 9, "heading", "bmw_dataset_pos")
pre_process = {
    "multiple": [1000],
    "value_exceptions": [
        [
            {
                "e_name": "modulus",
                "e_id":  1,
                "v_types": ["red", "rest"],
                "condition": "(abs(delta_v)>180000)",
                "para":  "(delta_v/abs(delta_v)*360000)"
            },
            {
                "e_name": "red_peak_points",
                "e_id":  2,
                "v_types": ["red"],
                "condition": "((abs(delta_v)>20000) and (abs(delta_v)<270000))",
                "para": "v"
            },
            {
                "e_name": "rest_peak_points",
                "e_id":  3,
                "v_types": ["rest"],
                "condition": "((abs(delta_v)>20000) and (abs(delta_v)<270000))",
                "para": "v"
            }
        ]
    ]
}
post_process = {
    "divide": [1000.0]
}
#D.sensor_data_process(9, 200, 0.8, 'cii_dct_v011', pre_process, post_process, 15)
D.sensor_data_process(9, 500, 0.93, 'cii_dct_v011', pre_process, post_process, 10)
D.write_sensor_data_files(9)    
sensor_data = get_ts_value_from_file(file, ',', 0, 2, 1, [6])
S = D.add_sensor_data(sensor_data['pos']['values'],
                      sensor_data['pos']['tss'], 10, "speed", "bmw_dataset_pos")
pre_process = {
    "multiple": [1000],
    "value_exceptions": [
        [
            {
                "e_name": "red_peak_points",
                "e_id":  2,
                "v_types": ["red"],
                "condition": "(abs(delta_v)>4000)",
                "para": "v"
            },
            {
                "e_name": "rest_peak_points",
                "e_id":  3,
                "v_types": ["rest"],
                "condition": "(abs(delta_v)>4000)",
                "para": "v"
            }
        ]
    ]
}
post_process = {
    "divide": [1000.0]
}
#D.sensor_data_process(10, 200, 0.8, 'cii_dct_v011', pre_process, post_process, 5)
D.sensor_data_process(10, 600, 0.8, 'cii_dct_v011', pre_process, post_process, 10)
D.write_sensor_data_files(10)

P = Plot(D)

#P.plot(0,process_id=0)
P.plot(8,process_id=0,deviation='difference')
P.plot(9,process_id=0,deviation='difference')
P.plot(10,process_id=0,deviation='difference')
