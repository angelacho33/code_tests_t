"""
Project Teraki Data Process APIs
Author: qingzhou

it provides the APIs for raw_data file, tka data file, and other files.
"""

import os, sys, time, datetime
import tk_series
import pandas as pd

def str_to_int_float(string): 
    try:
        return int(string)
    except ValueError:
        pass
    try:
        return float(string)
    except ValueError:
        return string

def get_ts_value_from_file(filename, sep=';', pos_sid=0, pos_mt=2, pos_ts=0, pos_value=[1]):
    result = {}
    f = open(filename, 'r')
    for line in f:
        try:
            text = line.rstrip()
            items = text.split(sep)
            key = items[pos_mt]
            ts = str_to_int_float(items[pos_ts])
            value = [str_to_int_float(items[x]) for x in pos_value]
            if key not in result.keys():
                result[key] = {}
                result[key]['sid'] = items[pos_sid]
                result[key]['mt'] = items[pos_mt]
                result[key]['tss'] = []
                result[key]['values'] = []
            result[key]['tss'].append(ts)
            result[key]['values'].append(value)
        except:
            pass
    f.close()
    return result


#--------------------------------------------------------------------
def test1():
    from tk_databank import DataBank
    from tk_plot import Plot
    #file = "./probes/rural/a0c70f7-5c91-49ce-982b-696a714554df_1.csv"
    file = "../../../Files/BMW/probes/rural/a0c70f7-5c91-49ce-982b-696a714554df_1.csv"
    #sensor_data = get_ts_value_from_file(file, ',', 0, 2, 1, [3,4])
    D = DataBank()
    
    #S = D.add_sensor_data(sensor_data['pos']['values'], 
    #    sensor_data['pos']['tss'], 0, "gps", "bmw_dataset_pos")
    #pre_process = {
    #    "multiple": [100000, 100000]
    #}
    #post_process = {
    #    "treshhold": [0.5],
    #    "divide": [100000.0, 100000.0]
    #}
    #D.sensor_data_process(0, 500, 0.9, 'aii_v010', pre_process, post_process)#cii_dct_v010
    
    
    #P = Plot(D)
    
    #P.plot(0,process_id=0)
    #P.plot(0,process_id=0)
    ##print D.get_sensor_list()
    ##print D.get_sensor_data(0, 'raw_data')
    ##print D.get_sensor_data(0, 'red_data')
    ##print D.get_sensor_data(0, 'recon_data')
    #D.write_sensor_data_files(8)
    ##D.write_sensor_file(0, 'bmw_gps_raw.csv', 'raw_data')
    ##D.write_sensor_file(0, 'bmw_gps_red.csv', 'red_data')
    ##D.write_sensor_file(0, 'bmw_gps_rec.csv', 'recon_data')
    ##D.write_sensor_file(0, 'bmw_gps_delta.csv', 'red_delta')
    
    sensor_data = get_ts_value_from_file(file, ',', 0, 2, 1, [5]) ## AngelH: change to [6] to get the speed. 
    S = D.add_sensor_data(sensor_data['pos']['values'], 
        sensor_data['pos']['tss'], 0, "heading", "bmw_dataset_heading")
    pre_process = {
        "multiple": [1000],
        "max_offset":[[200000, -360000]],
        "min_offset":[[-200000, 360000]]
    }
    post_process = {
        "treshhold": [30],
        "maximum": [340],
        "modulus": [360000],
        "divide": [1000.0]
    }
    [A,red_points,zlib_ratio] = D.sensor_data_process(0, 500, 0.93, 'cii_dct_v011', pre_process, post_process,10)
    D.write_sensor_data_files(0)
    
    print red_points ##AngelH: Values to be printed in a separate file 
    print zlib_ratio ##AngelH: Values to be printed in a separate file.
    #D.write_sensor_file(1, 'bmw_heading_red.csv', 'red_data')
    #D.write_sensor_file(1, 'bmw_heading_rec.csv', 'recon_data')
    #D.write_sensor_file(1, 'bmw_heading_delta.csv', 'red_delta')
    
    
    #P = Plot(D)
    
    #P.plot(0,process_id=0)
    #P.plot(0,process_id=0)
    
    #sensor_data = get_ts_value_from_file(file, ',', 0, 2, 1, [6])
    #S = D.add_sensor_data(sensor_data['pos']['values'], 
    #    sensor_data['pos']['tss'], 1, "speed", "bmw_dataset_pos")
    #pre_process = {
    #    "multiple": [1]
    #}
    #post_process = {
    #    "treshhold": [3], ## AngelH change to 5 or even 8 miles/hour 
#	   "maximum": [300], ##
        
#    }
#    D.sensor_data_process(1, 500, 0.9, 'cii_dct_v011', pre_process, post_process, 10)
#    D.write_sensor_data_files(1)


    #D.write_sensor_file(0, 'bmw_heading_raw.csv', 'raw_data')
    #D.write_sensor_file(0, 'bmw_heading_red.csv', 'red_data')
    #D.write_sensor_file(0, 'bmw_heading_rec.csv', 'recon_data')
    #D.write_sensor_file(0, 'bmw_heading_delta.csv', 'red_delta')
    #D.write_sensor_data_files(10)
    
    
    #D.write_sensor_file(2, 'bmw_speed_raw.csv', 'raw_data')
    #D.write_sensor_file(2, 'bmw_speed_red.csv', 'red_data')
    #D.write_sensor_file(2, 'bmw_speed_rec.csv', 'recon_data')
    #D.write_sensor_file(2, 'bmw_speed_delta.csv', 'red_delta')
    
    P = Plot(D)
    
    #P.plot(0,process_id=0)
    #P.plot(0,process_id=0,deviation='difference')
    P.plot(0,process_id=0,deviation='all')
    #P.plot(1)
    #P.plot(2)
    
    #P.plot_difference_histogram(0,process_id=0)
    
    #P.plot_difference_histogram(0)
    
    

if __name__ == '__main__':
    test1()
