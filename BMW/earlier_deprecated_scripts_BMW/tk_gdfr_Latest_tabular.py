"""
Project Teraki Data Process APIs
Author: qingzhou

it provides the APIs for raw_data file, tka data file, and other files.
"""
import os, sys, time, datetime
import tk_series
import pandas as pd

def str_to_int_float(string): 
    try:
        return int(string)
    except ValueError:
        pass
    try:
        return float(string)
    except ValueError:
        return string
        
def get_ts_value_from_file(filename, sep=';', pos_sid=0, pos_mt=2, pos_ts=0, pos_value=[1]):
    result = {}
    f = open(filename, 'r')
    for line in f:
        try:
            text = line.rstrip()
            items = text.split(sep)
            key = items[pos_mt]
            ts = str_to_int_float(items[pos_ts])
            value = [str_to_int_float(items[x]) for x in pos_value]
            if key not in result.keys():
                result[key] = {}
                result[key]['sid'] = items[pos_sid]
                result[key]['mt'] = items[pos_mt]
                result[key]['tss'] = []
                result[key]['values'] = []
            result[key]['tss'].append(ts)
            result[key]['values'].append(value)
        except:
            pass
    f.close()
    return result
#--------------------------------------------------------------------
def test1():
    from tk_databank import DataBank
    from tk_plot import Plot
    import  glob
    filenamelist = glob.glob("/home/angelh/Documents/DataScienceRelated/Teraki/Files/BMW/probes/highway_probes/*.csv")
    file=filenamelist[1]
    print file
    D = DataBank()
    
    #########################################################################################################
    #Heading (Works but Daniel said there is work to be done still)
    #sensor_data = get_ts_value_from_file(file, ',', 0, 2, 1, [5]) 
    #S = D.add_sensor_data(sensor_data['pos']['values'], 
    #    sensor_data['pos']['tss'], 0, "heading", "bmw_dataset_heading")
    #pre_process = {
    #    "multiple": [1000],
    #    "max_offset":[[200000, -360000]],
    #    "min_offset":[[-200000, 360000]]
    #}
    #post_process = {
    #    "treshhold": [30],
    #    "maximum": [340],
    #    "modulus": [360000],
    #    "divide": [1000.0]    
    #}
    ##############################################################################################################
    #Position ##Error message: Keyerror
    #sensor_data = get_ts_value_from_file(file, ',', 0, 2, 1, [3,4])
    #S = D.add_sensor_data(sensor_data['pos']['values'], 
    #                      sensor_data['pos']['tss'], 0, "gps", "bmw_dataset_pos")
    #pre_process = {
    #    "multiple": [100000, 100000]
    #}
    #post_process = {
    #    "treshhold": [0.1],
    #    "divide": [100000.0, 100000.0]
    #}
    ##############################################################################################################
    #Speed (works)
    sensor_data = get_ts_value_from_file(file, ',', 0, 2, 1, [6])
    S = D.add_sensor_data(sensor_data['pos']['values'], 
                          sensor_data['pos']['tss'], 0, "speed", "bmw_dataset_speed")
    pre_process = {
        "multiple": [1]
    }
    post_process = {
        "treshhold": [5], ## AngelH change to 5 or even 8 miles/hour 
        "maximum": [300], ##
    }
    ##############################################################################################################
    #Process and print
    [A,red_points,zlib_ratio] = D.sensor_data_process(0, 500, 0.93, 'cii_dct_v011', pre_process, post_process,10)
    D.write_sensor_data_files(0)
    print red_points 
    print zlib_ratio 
    ###############################################################################################################
    
    D.get_sensor_process_info(0)
    D.get_deviations(0, 0)

    
    P = Plot(D)
    P.plot(0,process_id=0,deviation='all')
    

if __name__ == '__main__':
    test1()
