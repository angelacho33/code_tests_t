import numpy as np
import pandas as pd
import matplotlib 
matplotlib.rcParams.update({'font.size': 25})
#import matplotlib.style
#matplotlib.style.use('ggplot')
import matplotlib.pyplot as plt
import sys
import os
#%pylab


#file=str(sys.argv[1]) ##summary csv file
#scenario=int(sys.argv[1])
#scenarios=['Highway','InnerCity','Rural']
#scenarios_file_name=['highway_probes','innerCity','rural']

#folder= str(sys.argv[1])
folder='/home/angelh/Documents/DataScienceRelated/Teraki/BMW_output/fourteenth_run/'
scenarios=['highway','innercity','rural']
scenario=0


file1=folder+scenarios[0]+'/After_smoothing_with_percentiles_as_columns_'+scenarios[0]+'_output_recon.csv'
file2=folder+scenarios[1]+'/After_smoothing_with_percentiles_as_columns_'+scenarios[1]+'_output_recon.csv'
file3=folder+scenarios[2]+'/After_smoothing_with_percentiles_as_columns_'+scenarios[2]+'_output_recon.csv'

all_files=[file1,file2,file3]

sensors=['gps','heading','speed']
coordinates=['lat','lon']

#name1='All_scenarios_97_5_deviations.png'
#name2=scenarios[scenario]+'_mean_deviations.png'
name3='All_scenarios_effective_reduction.png'
name4='All_scenarios_zlib_ratio.png'

df1=pd.read_csv(file1,sep=',')
df2=pd.read_csv(file2,sep=',')
df3=pd.read_csv(file3,sep=',')

df1['scenario']='highway'
df2['scenario']='innercity'
df3['scenario']='rural'

df = pd.concat((pd.read_csv(f) for f in all_files),keys=['highway', 'innercity', 'rural']) 
#df.ix['highway']
#df.ix['innercity']
#df.ix['rural']


##GPS dataframs using the after smothing file which should have smaller deviations
df_gps=df[df['sensor_name']=='gps']
df_lat=df_gps[df_gps['value_name']=='lat']
df_lon=df_gps[df_gps['value_name']=='lon']

df_heading=df[df['sensor_name']=='heading']
df_speed=df[df['sensor_name']=='speed']

legend_fontsize=20

max_pe_distance=20

#######################################################################################
##Effective reduction
fig3, ax3 = plt.subplots(2, 2,figsize=(30,15))
plt.subplots_adjust(left=0.125,right=0.9,bottom=0.1,top=0.9,wspace=0.2,hspace=0.4)

intervals_lat=[0,max_pe_distance,3000]
df_lat_upper_ninetysevenfive_percentile_intervals=[]
df_lat_upper_ninetysevenfive_percentile_red_info_ratio_values=[]
labels=[]
lat_means=[]
for i in range(0,len(intervals_lat)-1,1):
    #print i,i+1
    low_value=intervals_lat[i]
    high_value=intervals_lat[i+1]
    df_lat_upper_ninetysevenfive_percentile_intervals.append(df_lat[(df_lat.upper_ninetysevenfive_percentile>=low_value) & (df_lat.upper_ninetysevenfive_percentile<=high_value)])
    df_lat_upper_ninetysevenfive_percentile_red_info_ratio_values.append(df_lat_upper_ninetysevenfive_percentile_intervals[i].red_info_ratio.values)
    mean=round(df_lat_upper_ninetysevenfive_percentile_intervals[i].red_info_ratio.values.mean(),3)
    lat_means.append(mean)
    
labels=['pe_distance(97.5%)<'+str(max_pe_distance)+'m, Mean='+str(lat_means[0]), 'pe_distance(97.5%)>'+str(max_pe_distance)+'m ,Mean='+str(lat_means[1])]

ax3[0][0].hist(df_lat_upper_ninetysevenfive_percentile_red_info_ratio_values,bins=50,label=labels, stacked=True)
ax3[0][0].set_xlabel('Lat. Effective Reduction')
ax3[0][0].set_ylabel('# sessions')
ax3[0][0].set_title('lat')
ax3[0][0].legend(loc='best',fontsize=legend_fontsize)

##############
intervals_lon=[0,max_pe_distance,3000]
df_lon_upper_ninetysevenfive_percentile_intervals=[]
df_lon_upper_ninetysevenfive_percentile_red_info_ratio_values=[]
labels=[]
lon_means=[]
for i in range(0,len(intervals_lon)-1,1):
    #print i,i+1
    low_value=intervals_lon[i]
    high_value=intervals_lon[i+1]
    df_lon_upper_ninetysevenfive_percentile_intervals.append(df_lon[(df_lon.upper_ninetysevenfive_percentile>=low_value) & (df_lon.upper_ninetysevenfive_percentile<=high_value)])
    df_lon_upper_ninetysevenfive_percentile_red_info_ratio_values.append(df_lon_upper_ninetysevenfive_percentile_intervals[i].red_info_ratio.values)
    mean=round(df_lon_upper_ninetysevenfive_percentile_intervals[i].red_info_ratio.values.mean(),3)
    lon_means.append(mean)
    
labels=['pe_distance(97.5%)<'+str(max_pe_distance)+'m, Mean='+str(lon_means[0]), 'pe_distance(97.5%)>'+str(max_pe_distance)+'m ,Mean='+str(lon_means[1])]

ax3[1][0].hist(df_lon_upper_ninetysevenfive_percentile_red_info_ratio_values,bins=50,label=labels, stacked=True)
ax3[1][0].set_xlabel('Lon. Effective Reduction')
ax3[1][0].set_ylabel('# sessions')
ax3[1][0].set_title('lon')
ax3[1][0].legend(loc='best',fontsize=legend_fontsize)
#####
##############
#heading
heading_max=10
intervals_heading=[0,heading_max,3000]
df_heading_upper_ninetysevenfive_percentile_intervals=[]
df_heading_upper_ninetysevenfive_percentile_red_info_ratio_values=[]
labels=[]
heading_means=[]
for i in range(0,len(intervals_heading)-1,1):
    #print i,i+1
    low_value=intervals_heading[i]
    high_value=intervals_heading[i+1]
    df_heading_upper_ninetysevenfive_percentile_intervals.append(df_heading[(df_heading.upper_ninetysevenfive_percentile>=low_value) & (df_heading.upper_ninetysevenfive_percentile<=high_value)])
    df_heading_upper_ninetysevenfive_percentile_red_info_ratio_values.append(df_heading_upper_ninetysevenfive_percentile_intervals[i].red_info_ratio.values)
    mean=round(df_heading_upper_ninetysevenfive_percentile_intervals[i].red_info_ratio.values.mean(),3)
    heading_means.append(mean)
    
labels=['Heading diff.(97.5%)<'+str(heading_max)+', Mean='+str(heading_means[0]), 'Heading diff.(97.5%)>'+str(heading_max)+' ,Mean='+str(heading_means[1])]

ax3[0][1].hist(df_heading_upper_ninetysevenfive_percentile_red_info_ratio_values,bins=50,label=labels, stacked=True)
ax3[0][1].set_xlabel('Heading. Effective Reduction')
ax3[0][1].set_ylabel('# sessions')
ax3[0][1].set_title('heading')
ax3[0][1].legend(loc='best',fontsize=legend_fontsize)
##############
#speed
speed_max=5
intervals_speed=[0,speed_max,3000]
df_speed_upper_ninetysevenfive_percentile_intervals=[]
df_speed_upper_ninetysevenfive_percentile_red_info_ratio_values=[]
labels=[]
speed_means=[]
for i in range(0,len(intervals_speed)-1,1):
    #print i,i+1
    low_value=intervals_speed[i]
    high_value=intervals_speed[i+1]
    df_speed_upper_ninetysevenfive_percentile_intervals.append(df_speed[(df_speed.upper_ninetysevenfive_percentile>=low_value) & (df_speed.upper_ninetysevenfive_percentile<=high_value)])
    df_speed_upper_ninetysevenfive_percentile_red_info_ratio_values.append(df_speed_upper_ninetysevenfive_percentile_intervals[i].red_info_ratio.values)
    mean=round(df_speed_upper_ninetysevenfive_percentile_intervals[i].red_info_ratio.values.mean(),3)
    speed_means.append(mean)
    
labels=['Speed diff.(97.5%)<'+str(speed_max)+', Mean='+str(speed_means[0]), 'Speed diff.(97.5%)>'+str(speed_max)+' ,Mean='+str(speed_means[1])]

ax3[1][1].hist(df_speed_upper_ninetysevenfive_percentile_red_info_ratio_values,bins=50,label=labels, stacked=True)
ax3[1][1].set_xlabel('Speed. Effective Reduction')
ax3[1][1].set_ylabel('# sessions')
ax3[1][1].set_title('speed')
ax3[1][1].legend(loc='best',fontsize=legend_fontsize)


fig3.suptitle('Effective reduction, All scenarios',fontsize=25)
fig3.savefig(name3)


######################################################################################################################
######################################################################################################################
#######################################################################################
##zlib_ratio reduction
fig4, ax4 = plt.subplots(2, 2,figsize=(30,15))
plt.subplots_adjust(left=0.125,right=0.9,bottom=0.1,top=0.9,wspace=0.2,hspace=0.4)

intervals_lat=[0,max_pe_distance,3000]
df_lat_upper_ninetysevenfive_percentile_intervals=[]
df_lat_upper_ninetysevenfive_percentile_zlib_ratio_values=[]
labels=[]
lat_means=[]
for i in range(0,len(intervals_lat)-1,1):
    #print i,i+1
    low_value=intervals_lat[i]
    high_value=intervals_lat[i+1]
    df_lat_upper_ninetysevenfive_percentile_intervals.append(df_lat[(df_lat.upper_ninetysevenfive_percentile>=low_value) & (df_lat.upper_ninetysevenfive_percentile<=high_value)])
    df_lat_upper_ninetysevenfive_percentile_zlib_ratio_values.append(df_lat_upper_ninetysevenfive_percentile_intervals[i].zlib_ratio.values)
    mean=round(df_lat_upper_ninetysevenfive_percentile_intervals[i].zlib_ratio.values.mean(),3)
    lat_means.append(mean)
    
labels=['pe_distance(97.5%)<'+str(max_pe_distance)+'m, Mean='+str(lat_means[0]), 'pe_distance(97.5%)>'+str(max_pe_distance)+'m ,Mean='+str(lat_means[1])]

ax4[0][0].hist(df_lat_upper_ninetysevenfive_percentile_zlib_ratio_values,bins=50,label=labels, stacked=True)
ax4[0][0].set_xlabel('Lat. zlib ratio')
ax4[0][0].set_ylabel('# sessions')
ax4[0][0].set_title('lat')
ax4[0][0].legend(loc='best',fontsize=legend_fontsize)

##############
intervals_lon=[0,max_pe_distance,3000]
df_lon_upper_ninetysevenfive_percentile_intervals=[]
df_lon_upper_ninetysevenfive_percentile_zlib_ratio_values=[]
labels=[]
lon_means=[]
for i in range(0,len(intervals_lon)-1,1):
    #print i,i+1
    low_value=intervals_lon[i]
    high_value=intervals_lon[i+1]
    df_lon_upper_ninetysevenfive_percentile_intervals.append(df_lon[(df_lon.upper_ninetysevenfive_percentile>=low_value) & (df_lon.upper_ninetysevenfive_percentile<=high_value)])
    df_lon_upper_ninetysevenfive_percentile_zlib_ratio_values.append(df_lon_upper_ninetysevenfive_percentile_intervals[i].zlib_ratio.values)
    mean=round(df_lon_upper_ninetysevenfive_percentile_intervals[i].zlib_ratio.values.mean(),3)
    lon_means.append(mean)
    
labels=['pe_distance(97.5%)<'+str(max_pe_distance)+'m, Mean='+str(lon_means[0]), 'pe_distance(97.5%)>'+str(max_pe_distance)+'m ,Mean='+str(lon_means[1])]

ax4[1][0].hist(df_lon_upper_ninetysevenfive_percentile_zlib_ratio_values,bins=50,label=labels, stacked=True)
ax4[1][0].set_xlabel('Lon. zlib ratio')
ax4[1][0].set_ylabel('# sessions')
ax4[1][0].set_title('lon')
ax4[1][0].legend(loc='best',fontsize=legend_fontsize)
#####
##############
#heading
heading_max=10
intervals_heading=[0,heading_max,3000]
df_heading_upper_ninetysevenfive_percentile_intervals=[]
df_heading_upper_ninetysevenfive_percentile_zlib_ratio_values=[]
labels=[]
heading_means=[]
for i in range(0,len(intervals_heading)-1,1):
    #print i,i+1
    low_value=intervals_heading[i]
    high_value=intervals_heading[i+1]
    df_heading_upper_ninetysevenfive_percentile_intervals.append(df_heading[(df_heading.upper_ninetysevenfive_percentile>=low_value) & (df_heading.upper_ninetysevenfive_percentile<=high_value)])
    df_heading_upper_ninetysevenfive_percentile_zlib_ratio_values.append(df_heading_upper_ninetysevenfive_percentile_intervals[i].zlib_ratio.values)
    mean=round(df_heading_upper_ninetysevenfive_percentile_intervals[i].zlib_ratio.values.mean(),3)
    heading_means.append(mean)
    
labels=['Heading diff.(97.5%)<'+str(heading_max)+', Mean='+str(heading_means[0]), 'Heading diff.(97.5%)>'+str(heading_max)+' ,Mean='+str(heading_means[1])]

ax4[0][1].hist(df_heading_upper_ninetysevenfive_percentile_zlib_ratio_values,bins=50,label=labels, stacked=True)
ax4[0][1].set_xlabel('Heading zlib ratio')
ax4[0][1].set_ylabel('# sessions')
ax4[0][1].set_title('heading')
ax4[0][1].legend(loc='best',fontsize=legend_fontsize)
##############
#speed
speed_max=5
intervals_speed=[0,speed_max,3000]
df_speed_upper_ninetysevenfive_percentile_intervals=[]
df_speed_upper_ninetysevenfive_percentile_zlib_ratio_values=[]
labels=[]
speed_means=[]
for i in range(0,len(intervals_speed)-1,1):
    #print i,i+1
    low_value=intervals_speed[i]
    high_value=intervals_speed[i+1]
    df_speed_upper_ninetysevenfive_percentile_intervals.append(df_speed[(df_speed.upper_ninetysevenfive_percentile>=low_value) & (df_speed.upper_ninetysevenfive_percentile<=high_value)])
    df_speed_upper_ninetysevenfive_percentile_zlib_ratio_values.append(df_speed_upper_ninetysevenfive_percentile_intervals[i].zlib_ratio.values)
    mean=round(df_speed_upper_ninetysevenfive_percentile_intervals[i].zlib_ratio.values.mean(),3)
    speed_means.append(mean)
    
labels=['Speed diff.(97.5%)<'+str(speed_max)+', Mean='+str(speed_means[0]), 'Speed diff.(97.5%)>'+str(speed_max)+' ,Mean='+str(speed_means[1])]

ax4[1][1].hist(df_speed_upper_ninetysevenfive_percentile_zlib_ratio_values,bins=50,label=labels, stacked=True)
ax4[1][1].set_xlabel('Speed  zlib ratio')
ax4[1][1].set_ylabel('# sessions')
ax4[1][1].set_title('speed')
ax4[1][1].legend(loc='best',fontsize=legend_fontsize)

fig4.suptitle('Zlib ratio, All scenarios',fontsize=25)
fig4.savefig(name4)


