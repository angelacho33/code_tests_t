import numpy as np
import pandas as pd
import matplotlib 
matplotlib.rcParams.update({'font.size': 25})
#import matplotlib.style
#matplotlib.style.use('ggplot')
import matplotlib.pyplot as plt
import sys
import os
#%pylab


#file=str(sys.argv[1]) ##summary csv file
scenario=int(sys.argv[1])
scenarios=['Highway','InnerCity','Rural']
scenarios_file_name=['highway_probes','innerCity','rural']
scenarios_file2_name=['highway','innercity','rural']

file='summary_'+scenarios_file_name[scenario]+'_output_recon.csv'
file2='After_smoothing_'+scenarios_file2_name[scenario]+'_output_recon.csv'


sensors=['gps','heading','speed']
coordinates=['lat','lon']

name1=scenarios[scenario]+'_max_deviations.png'
name2=scenarios[scenario]+'_mean_deviations.png'
name3=scenarios[scenario]+'_effective_reduction.png'

df1=pd.read_csv(file,sep=',')
df2=pd.read_csv(file2,sep=',')

##GPS dataframs using the after smothing file which should have smaller deviations
df_gps=df2[df2['sensor_name']=='gps']
df_lat=df_gps[df_gps['value_name']=='lat']
df_lon=df_gps[df_gps['value_name']=='lon']

df_heading=df1[df1['sensor_name']=='heading']
df_speed=df1[df1['sensor_name']=='speed']

legend_fontsize=20
#######################################################################################
#######################################################################################
#######################################################################################
##Max deviations
fig1, ax1 = plt.subplots(2, 2,figsize=(30,15))
plt.subplots_adjust(left=0.125,right=0.9,bottom=0.1,top=0.9,wspace=0.2,hspace=0.4)

lat_mean=df_lat.dev_max.values.mean()
lon_mean=df_lon.dev_max.values.mean()
heading_mean=df_heading.dev_max.values.mean()
speed_mean=df_speed.dev_max.values.mean()

#######################################################################
intervals_lat=np.linspace(0,round(df_lat.rrmse.max()),4)
df_lat_rrmse_intervals=[]
df_lat_rrmse_dev_max_values=[]
labels=[]
lat_means=[]
for i in range(0,len(intervals_lat)-1,1):
    #print i,i+1
    low_value=intervals_lat[i]
    high_value=intervals_lat[i+1]
    df_lat_rrmse_intervals.append(df_lat[(df_lat.rrmse>=low_value) & (df_lat.rrmse<=high_value)])
    df_lat_rrmse_dev_max_values.append(df_lat_rrmse_intervals[i].dev_max.values*1e2) ###Here I multiply all the deviations times 100!!
    mean=round(df_lat_rrmse_intervals[i].dev_max.values.mean(),3)
    lat_means.append(mean)
    labels.append('rrmse=['+str(round(low_value,3))+'%,'+str(round(high_value,3))+'%]'+',Mean='+str(mean))
ax1[0][0].hist(df_lat_rrmse_dev_max_values,bins=50,label=labels, stacked=True)    
########################################################################    
#ax1[0][0].hist(df_lat.dev_max.values*1e2,bins=50,label='Mean='+str(round(lat_mean,3)))
ax1[0][0].set_xlabel('max deviation x$10^{-2}$')
ax1[0][0].set_ylabel('# sessions')
#ax1[0][0].set_yscale('log')
ax1[0][0].set_title('lat')
ax1[0][0].legend(loc='best',fontsize=legend_fontsize)

#######################################################################
intervals_lon=np.linspace(0,df_lon.rrmse.max(),4)
df_lon_rrmse_intervals=[]
df_lon_rrmse_dev_max_values=[]
labels=[]
lon_means=[]
for i in range(0,len(intervals_lon)-1,1):
    #print i,i+1
    low_value=intervals_lon[i]
    high_value=intervals_lon[i+1]
    df_lon_rrmse_intervals.append(df_lon[(df_lon.rrmse>=low_value) & (df_lon.rrmse<=high_value)])
    df_lon_rrmse_dev_max_values.append(df_lon_rrmse_intervals[i].dev_max.values*1e2) ###Here I multiply all the deviations times 100!!
    mean=round(df_lon_rrmse_intervals[i].dev_max.values.mean(),3)
    lon_means.append(mean)
    labels.append('rrmse=['+str(round(low_value,2))+'%,'+str(round(high_value,2))+'%]'+',Mean='+str(mean))
ax1[1][0].hist(df_lon_rrmse_dev_max_values,bins=50,label=labels, stacked=True)
########################################################################
#ax1[1][0].hist(df_lon.dev_max.values*1e2,bins=50,label='Mean='+str(round(lon_mean,3)))
ax1[1][0].set_xlabel('max. deviation x$10^{-2}$')
ax1[1][0].set_ylabel('# sessions')
ax1[1][0].set_title('lon')
ax1[1][0].legend(loc='best',fontsize=legend_fontsize)

#######################################################################
intervals_heading=np.linspace(0,np.ceil(df_heading.rrmse.max()),4)
df_heading_rrmse_intervals=[]
df_heading_rrmse_dev_max_values=[]
labels=[]
heading_means=[]
for i in range(0,len(intervals_heading)-1,1):
    #print i,i+1
    low_value=intervals_heading[i]
    high_value=intervals_heading[i+1]
    df_heading_rrmse_intervals.append(df_heading[(df_heading.rrmse>=low_value) & (df_heading.rrmse<=high_value)])
    df_heading_rrmse_dev_max_values.append(df_heading_rrmse_intervals[i].dev_max.values)
    mean=round(df_heading_rrmse_intervals[i].dev_max.values.mean(),3)
    heading_means.append(mean)
    labels.append('rrmse=['+str(round(low_value,2))+'%,'+str(round(high_value,2))+'%]'+',Mean='+str(mean))
ax1[0][1].hist(df_heading_rrmse_dev_max_values,bins=50,label=labels, stacked=True)
########################################################################
#ax1[0][1].hist(df_heading.dev_max.values,bins=50,label='Mean='+str(round(heading_mean,3)))
ax1[0][1].set_xlabel('max. deviation')
ax1[0][1].set_ylabel('# sessions')
ax1[0][1].set_title('heading')
ax1[0][1].legend(loc='best',fontsize=legend_fontsize)


#######################################################################
intervals_speed=np.linspace(0,round(df_speed.rrmse.max()),4)
df_speed_rrmse_intervals=[]
df_speed_rrmse_dev_max_values=[]
labels=[]
speed_means=[]
for i in range(0,len(intervals_speed)-1,1):
    #print i,i+1
    low_value=intervals_speed[i]
    high_value=intervals_speed[i+1]
    df_speed_rrmse_intervals.append(df_speed[(df_speed.rrmse>=low_value) & (df_speed.rrmse<=high_value)])
    df_speed_rrmse_dev_max_values.append(df_speed_rrmse_intervals[i].dev_max.values) ###Here I multiply all the deviations times 100!!
    mean=round(df_speed_rrmse_intervals[i].dev_max.values.mean(),3)
    speed_means.append(mean)
    labels.append('rrmse=['+str(round(low_value,2))+'%,'+str(round(high_value,2))+'%]'+',Mean='+str(mean))
ax1[1][1].hist(df_speed_rrmse_dev_max_values,bins=50,label=labels, stacked=True)
########################################################################
#ax1[1][1].hist(df_speed.dev_max.values,bins=50,label='Mean='+str(round(speed_mean,3)))
ax1[1][1].set_xlabel('max. deviation')
ax1[1][1].set_ylabel('# sessions')
ax1[1][1].set_title('speed')
ax1[1][1].legend(loc='best',fontsize=legend_fontsize)

if scenario==0:
    fig1.suptitle(scenarios[scenario]+' scenario,323/404 sessions, Max. deviations, 80% reduction, Framesize=500', fontsize=40)
if scenario==1:
    fig1.suptitle(scenarios[scenario]+' scenario,201/237 sessions, Max. deviations, 80% reduction, Framesize=500', fontsize=40)
if scenario==2:
    fig1.suptitle(scenarios[scenario]+' scenario,114/140 sessions, Max. deviations, 80% reduction, Framesize=500', fontsize=40)

if not os.path.exists('./histograms_with_rrmse'):
    os.makedirs('./histograms_with_rrmse')

plt.savefig('histograms_with_rrmse/'+name1)
#plt.savefig(name1)
#plt.savefig("Hists_Highway_max_deviations.png")

#######################################################################################
#######################################################################################
#######################################################################################
##Mean deviations
fig2, ax2 = plt.subplots(2, 2,figsize=(30,15))
plt.subplots_adjust(left=0.125,right=0.9,bottom=0.1,top=0.9,wspace=0.2,hspace=0.4)

lat_mean=df_lat.dev_mean.values.mean()*1e4
lon_mean=df_lon.dev_mean.values.mean()*1e4
heading_mean=df_heading.dev_mean.values.mean()
speed_mean=df_speed.dev_mean.values.mean()

#######################################################################
intervals_lat=np.linspace(0,round(df_lat.rrmse.max()),4)
df_lat_rrmse_intervals=[]
df_lat_rrmse_dev_mean_values=[]
labels=[]
lat_means=[]
for i in range(0,len(intervals_lat)-1,1):
    #print i,i+1
    low_value=intervals_lat[i]
    high_value=intervals_lat[i+1]
    df_lat_rrmse_intervals.append(df_lat[(df_lat.rrmse>=low_value) & (df_lat.rrmse<=high_value)])
    df_lat_rrmse_dev_mean_values.append(df_lat_rrmse_intervals[i].dev_mean.values*1e4) ###Here I multiply all the deviations times 1e4!!
    mean=round(df_lat_rrmse_intervals[i].dev_mean.values.mean()*1e4,3)
    lat_means.append(mean)
    labels.append('rrmse=['+str(round(low_value,2))+'%,'+str(round(high_value,2))+'%]'+',Mean='+str(mean)+'x$10^{-4}$')
ax2[0][0].hist(df_lat_rrmse_dev_mean_values,bins=50,label=labels, stacked=True)
########################################################################
#ax2[0][0].hist(df_lat.dev_mean.values*1e4,bins=50,label='Mean='+str(round(lat_mean,3))+'x$10^{-4}$')
ax2[0][0].set_xlabel('mean deviation x$10^{-4}$')
ax2[0][0].set_ylabel('# sessions')
ax2[0][0].set_title('lat')
ax2[0][0].legend(loc='best',fontsize=legend_fontsize)

#######################################################################
#intervals_lon=np.linspace(0,round(df_lon.rrmse.max()),4)
df_lon_rrmse_intervals=[]
df_lon_rrmse_dev_mean_values=[]
labels=[]
lon_means=[]
for i in range(0,len(intervals_lon)-1,1):
    #print i,i+1
    low_value=intervals_lon[i]
    high_value=intervals_lon[i+1]
    df_lon_rrmse_intervals.append(df_lon[(df_lon.rrmse>=low_value) & (df_lon.rrmse<=high_value)])
    df_lon_rrmse_dev_mean_values.append(df_lon_rrmse_intervals[i].dev_mean.values*1e4) ###Here I multiply all the deviations times 1e4!!
    mean=round(df_lon_rrmse_intervals[i].dev_mean.values.mean()*1e4,3)
    lon_means.append(mean)
    labels.append('rrmse=['+str(round(low_value,2))+'%,'+str(round(high_value,2))+'%]'+',Mean='+str(mean)+'x$10^{-4}$')
ax2[1][0].hist(df_lon_rrmse_dev_mean_values,bins=50,label=labels, stacked=True)
########################################################################
#ax2[1][0].hist(df_lon.dev_mean.values*1e4,bins=50,label='Mean='+str(round(lon_mean,3))+'x$10^{-4}$')
ax2[1][0].set_xlabel('mean. deviation x$10^{-4}$')
ax2[1][0].set_ylabel('# sessions')
ax2[1][0].set_title('lon')
ax2[1][0].legend(loc='best',fontsize=legend_fontsize)

#######################################################################
#intervals_heading=np.linspace(0,round(df_heading.rrmse.max()),4)
df_heading_rrmse_intervals=[]
df_heading_rrmse_dev_mean_values=[]
labels=[]
heading_means=[]
for i in range(0,len(intervals_heading)-1,1):
    #print i,i+1
    low_value=intervals_heading[i]
    high_value=intervals_heading[i+1]
    df_heading_rrmse_intervals.append(df_heading[(df_heading.rrmse>=low_value) & (df_heading.rrmse<=high_value)])
    df_heading_rrmse_dev_mean_values.append(df_heading_rrmse_intervals[i].dev_mean.values)
    mean=round(df_heading_rrmse_intervals[i].dev_mean.values.mean(),3)
    heading_means.append(mean)
    labels.append('rrmse=['+str(round(low_value,2))+'%,'+str(round(high_value,2))+'%]'+',Mean='+str(mean))
ax2[0][1].hist(df_heading_rrmse_dev_mean_values,bins=50,label=labels, stacked=True)
########################################################################
#ax2[0][1].hist(df_heading.dev_mean.values,bins=50,label='Mean='+str(round(heading_mean,3)))
ax2[0][1].set_xlabel('mean. deviation')
ax2[0][1].set_ylabel('# sessions')
ax2[0][1].set_title('heading')
ax2[0][1].legend(loc='best',fontsize=legend_fontsize)

#######################################################################
intervals_speed=np.linspace(0,round(df_speed.rrmse.max()),4)
df_speed_rrmse_intervals=[]
df_speed_rrmse_dev_mean_values=[]
labels=[]
speed_means=[]
for i in range(0,len(intervals_speed)-1,1):
    #print i,i+1
    low_value=intervals_speed[i]
    high_value=intervals_speed[i+1]
    df_speed_rrmse_intervals.append(df_speed[(df_speed.rrmse>=low_value) & (df_speed.rrmse<=high_value)])
    df_speed_rrmse_dev_mean_values.append(df_speed_rrmse_intervals[i].dev_mean.values) ###Here I multiply all the deviations times 1e4!!
    mean=round(df_speed_rrmse_intervals[i].dev_mean.values.mean(),3)
    speed_means.append(mean)
    labels.append('rrmse=['+str(round(low_value,2))+'%,'+str(round(high_value,2))+'%]'+',Mean='+str(mean))
ax2[1][1].hist(df_speed_rrmse_dev_mean_values,bins=50,label=labels, stacked=True)
########################################################################
#ax2[1][1].hist(df_speed.dev_mean.values,bins=50,label='Mean='+str(round(speed_mean,3)))
ax2[1][1].set_xlabel('mean. deviation')
ax2[1][1].set_ylabel('# sessions')
ax2[1][1].set_title('speed')
ax2[1][1].legend(loc='best',fontsize=legend_fontsize)

if scenario==0:
    fig2.suptitle(scenarios[scenario]+' scenario, 323/404 sessions, Mean deviations , 80% reduction,framesize=500', fontsize=40)
if scenario==1:
    fig2.suptitle(scenarios[scenario]+' scenario,201/237 sessions, Mean deviations , 80% reduction, Framesize=500', fontsize=40)
if scenario==2:
    fig2.suptitle(scenarios[scenario]+' scenario,114/140 sessions, Mean deviations , 80% reduction, Framesize=500', fontsize=40)

plt.savefig('histograms_with_rrmse/'+name2)
#plt.savefig(name2)
#######################################################################################
#######################################################################################
#######################################################################################
#######################################################################################
##Effective reduction
fig3, ax3 = plt.subplots(2, 2,figsize=(30,15))
plt.subplots_adjust(left=0.125,right=0.9,bottom=0.1,top=0.9,wspace=0.2,hspace=0.4)

lat_mean=df_lat.red_info_ratio.values.mean()
lon_mean=df_lon.red_info_ratio.values.mean()
heading_mean=df_heading.red_info_ratio.values.mean()
speed_mean=df_speed.red_info_ratio.values.mean()

#######################################################################
intervals_lat=np.linspace(0,round(df_lat.rrmse.max()),4)
df_lat_rrmse_intervals=[]
df_lat_rrmse_red_info_ratio_values=[]
labels=[]
lat_means=[]
for i in range(0,len(intervals_lat)-1,1):
    #print i,i+1
    low_value=intervals_lat[i]
    high_value=intervals_lat[i+1]
    df_lat_rrmse_intervals.append(df_lat[(df_lat.rrmse>=low_value) & (df_lat.rrmse<=high_value)])
    df_lat_rrmse_red_info_ratio_values.append(df_lat_rrmse_intervals[i].red_info_ratio.values)
    mean=round(df_lat_rrmse_intervals[i].red_info_ratio.values.mean(),3)
    lat_means.append(mean)
    labels.append('rrmse=['+str(round(low_value,2))+'%,'+str(round(high_value,2))+'%]'+',Mean='+str(mean))
ax3[0][0].hist(df_lat_rrmse_red_info_ratio_values,bins=50,label=labels, stacked=True)
########################################################################
#ax3[0][0].hist(df_lat.red_info_ratio.values,bins=50,label='Mean='+str(round(lat_mean,3)))
ax3[0][0].set_xlabel('Lat. Effective Reduction')
ax3[0][0].set_ylabel('# sessions')
ax3[0][0].set_title('lat')
ax3[0][0].legend(loc='best',fontsize=legend_fontsize)

#######################################################################
#intervals_lon=np.linspace(0,round(df_lon.rrmse.max()),4)
df_lon_rrmse_intervals=[]
df_lon_rrmse_red_info_ratio_values=[]
labels=[]
lon_means=[]
for i in range(0,len(intervals_lon)-1,1):
    #print i,i+1
    low_value=intervals_lon[i]
    high_value=intervals_lon[i+1]
    df_lon_rrmse_intervals.append(df_lon[(df_lon.rrmse>=low_value) & (df_lon.rrmse<=high_value)])
    df_lon_rrmse_red_info_ratio_values.append(df_lon_rrmse_intervals[i].red_info_ratio.values)
    mean=round(df_lon_rrmse_intervals[i].red_info_ratio.values.mean(),3)
    lon_means.append(mean)
    labels.append('rrmse=['+str(round(low_value,2))+'%,'+str(round(high_value,2))+'%]'+',Mean='+str(mean))
ax3[1][0].hist(df_lon_rrmse_red_info_ratio_values,bins=50,label=labels, stacked=True)
########################################################################
#ax3[1][0].hist(df_lon.red_info_ratio.values,bins=50,label='Mean='+str(round(lon_mean,3)))
ax3[1][0].set_xlabel('Lon. Effective Reduction')
ax3[1][0].set_ylabel('# sessions')
ax3[1][0].set_title('lon')
ax3[1][0].legend(loc='best',fontsize=legend_fontsize)


#######################################################################
#intervals_heading=np.linspace(0,round(df_heading.rrmse.max()),4)
df_heading_rrmse_intervals=[]
df_heading_rrmse_red_info_ratio_values=[]
labels=[]
heading_means=[]
for i in range(0,len(intervals_heading)-1,1):
    #print i,i+1
    low_value=intervals_heading[i]
    high_value=intervals_heading[i+1]
    df_heading_rrmse_intervals.append(df_heading[(df_heading.rrmse>=low_value) & (df_heading.rrmse<=high_value)])
    df_heading_rrmse_red_info_ratio_values.append(df_heading_rrmse_intervals[i].red_info_ratio.values)
    mean=round(df_heading_rrmse_intervals[i].red_info_ratio.values.mean(),3)
    heading_means.append(mean)
    labels.append('rrmse=['+str(round(low_value,2))+'%,'+str(round(high_value,2))+'%]'+',Mean='+str(mean))
ax3[0][1].hist(df_heading_rrmse_red_info_ratio_values,bins=50,label=labels, stacked=True)
########################################################################
#ax3[0][1].hist(df_heading.red_info_ratio.values,bins=50,label='Mean='+str(round(heading_mean,3)))
ax3[0][1].set_xlabel('Heading Effective Reduction')
ax3[0][1].set_ylabel('# sessions')
ax3[0][1].set_title('heading')
ax3[0][1].legend(loc='best',fontsize=legend_fontsize)

#######################################################################
intervals_speed=np.linspace(0,round(df_speed.rrmse.max()),4)
df_speed_rrmse_intervals=[]
df_speed_rrmse_red_info_ratio_values=[]
labels=[]
speed_means=[]
for i in range(0,len(intervals_speed)-1,1):
    #print i,i+1
    low_value=intervals_speed[i]
    high_value=intervals_speed[i+1]
    df_speed_rrmse_intervals.append(df_speed[(df_speed.rrmse>=low_value) & (df_speed.rrmse<=high_value)])
    df_speed_rrmse_red_info_ratio_values.append(df_speed_rrmse_intervals[i].red_info_ratio.values)
    mean=round(df_speed_rrmse_intervals[i].red_info_ratio.values.mean(),3)
    speed_means.append(mean)
    labels.append('rrmse=['+str(round(low_value,2))+'%,'+str(round(high_value,2))+'%]'+',Mean='+str(mean))
ax3[1][1].hist(df_speed_rrmse_red_info_ratio_values,bins=50,label=labels, stacked=True)
########################################################################
#ax3[1][1].hist(df_speed.red_info_ratio.values,bins=50,label='Mean='+str(round(speed_mean,3)))
ax3[1][1].set_xlabel('Speed Effective Reduction')
ax3[1][1].set_ylabel('# sessions')
ax3[1][1].set_title('speed')
ax3[1][1].legend(loc='best',fontsize=legend_fontsize)

if scenario==0:
    fig3.suptitle(scenarios[scenario]+' scenario, 323/404 sessions, 80% reduction,framesize=500', fontsize=40)
if scenario==1:
    fig3.suptitle(scenarios[scenario]+' scenario,201/237 sessions, 80% reduction, Framesize=500', fontsize=40)
if scenario==2:
    fig3.suptitle(scenarios[scenario]+' scenario,114/140 sessions, 80% reduction, Framesize=500', fontsize=40)
plt.savefig('histograms_with_rrmse/'+name3)
#plt.savefig(name3)
