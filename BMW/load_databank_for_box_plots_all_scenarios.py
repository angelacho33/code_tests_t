from tk_databank import * 
from tk_plot import Plot
import tk_err
import numpy as np
import glob
import csv
#import read_airbus_sar_data
import sys
import matplotlib
matplotlib.rcParams.update({'font.size': 25})
import matplotlib.pyplot as plt 
import os

#scenario_input=int(sys.argv[1]) ##0:highway_probes, 1:innerCity ,2:rural 3:InnercityCrossing

scenarios        = ['highway','innercity','rural']
number_dbk_files = [324      ,202        ,115]
#number_dbk_files = [1      ,1        ,1]
#number_dbk_files = [5      ,5        ,5]

#file_name=str(sys.argv[1])

all_pe_distances=[]
all_wgs84_pp_distances=[]
all_headings_differences=[]
all_speeds_differences=[]


all_pe_distances_larger=[]
all_wgs84_pp_distances_larger=[]
all_headings_differences_larger=[]
all_speeds_differences_larger=[]

reduce_info_gps=[]
reduce_info_heading=[]
reduce_info_speed=[]

max_pe_distance=20


files_with_small_pe_distances=0
files_with_small_pe_distances_larger=0

#folder='/home/angelh/Documents/DataScienceRelated/Teraki/BMW_output/thirtenth_run/'
folder='/home/angel/BMW_output_super_updated_ALL_GPS_50_percent/'

for scenario_input in [0,1,2]:
    file_id=0
    scenario=scenarios[scenario_input]
    for i in np.arange(0,number_dbk_files[scenario_input],1):
        #file='output_file_id_'+str(i)+'.dbk'
        #file='post_processed_output_file_id_'+str(i)+'.dbk'
        file=folder+scenario+'/post_processed_output_file_id_'+str(i)+'.dbk'
        if not os.path.exists(file):
            continue
        E=DataBank()
        E.load(file)
    
        ##Calculate differences for heading and speed
        differences=[]
        for sensor_id in [1,2]:
            process_id=0
            raw_data    = E.get_sensor_raw_data(sensor_id)
            recon_data  = E.get_sensor_recon_data(sensor_id, process_id)
            recon_data_len = len(recon_data['values'])
            tss_raw   = raw_data['tss'][:recon_data_len]
            tss_recon = recon_data['tss']
            component_raw   = list( zip(*raw_data['values'][:recon_data_len])[0] )
            component_recon = list( zip(*recon_data['values'])[0] )
            component_recon_interpol = tk_err.interpolate(tss_raw, tss_recon, component_recon)
            e_v      = tk_err.e(component_raw, component_recon_interpol)     # error (pointwise difference)
            differences.append(tk_err.e(component_raw, component_recon_interpol))     # error (pointwise difference)
            #print e_v
            #print e_v.max()
    
        differences=np.asarray(differences)
        
        ####get gps deviations of the post procesed data
        process_id=1 #1 contains the after smoothing data
        sensor_id=0
        gps_deviation1 = E.get_gps_deviations(sensor_id, process_id)
        print 'pe_deviations: ', gps_deviation1['pe_deviations']
        print 'pp_deviations: ', gps_deviation1['pp_deviations']
        print 'wgs84_pp_deviations: ',gps_deviation1['wgs84_pp_deviations']
        pe_distances1=np.asarray(gps_deviation1['pe_distances'])
        pp_distances1=np.asarray(gps_deviation1['pp_distances'])
        wgs84_pp_distances1=np.asarray(gps_deviation1['wgs84_pp_distances'])
    
    
        ##Create huge array with all distances, headings and speeds
        #if (gps_deviation1['pe_deviations']['max_distance']<=max_pe_distance) and (differences[0].max()<50) and (differences[0].min()>-50):
        #if (gps_deviation1['pe_deviations']['max_distance']>=max_pe_distance):
        if (gps_deviation1['pe_deviations']['max_distance']<=max_pe_distance):
            all_pe_distances.append(pe_distances1)
            all_wgs84_pp_distances.append(wgs84_pp_distances1)
            all_headings_differences.append(differences[0])
            all_speeds_differences.append(differences[1])
            files_with_small_pe_distances=files_with_small_pe_distances+1
            print 'There are '+str(files_with_small_pe_distances)+' files with pe distances <='+str(max_pe_distance)
            
        if (gps_deviation1['pe_deviations']['max_distance']>=max_pe_distance):
            all_pe_distances_larger.append(pe_distances1)
            all_wgs84_pp_distances_larger.append(wgs84_pp_distances1)
            all_headings_differences_larger.append(differences[0])
            all_speeds_differences_larger.append(differences[1])
            files_with_small_pe_distances_larger=files_with_small_pe_distances_larger+1
            print 'There are '+str(files_with_small_pe_distances_larger)+' files with pe distances >='+str(max_pe_distance)
    
    
        reduce_info_ratio_gps=E.get_sensor_process_info(0,1)['reduce_info']['ratio']
        reduce_info_ratio_heading=E.get_sensor_process_info(1,0)['reduce_info']['ratio']
        reduce_info_ratio_speed=E.get_sensor_process_info(2,0)['reduce_info']['ratio']
        
        reduce_info_gps.append(reduce_info_ratio_gps)
        reduce_info_heading.append(reduce_info_ratio_heading)
        reduce_info_speed.append(reduce_info_ratio_speed)
        
        ########## Plots
        #P=Plot(E)
        #P.plot(sensor_id,process_id,deviation='difference',dt_format='%M:%S')
        #P.plot_raw_inspection(sensor_id, output='screen', fft_yscale='log', dt_format='%Y-%m-%d %H:%M:%S')
        #P.plot_rec_inspection(sensor_id, process_id, output='screen', fft_yscale='log', dt_format='%Y-%m-%d %H:%M:%S')
        #P.plot_rec_inspection(sensor_id, process_id, output='png', fft_yscale='log', dt_format='%Y-%m-%d %H:%M:%S')
        #P.show()
    
        print 'file_id', file_id, 'scenario:', scenario_input
        file_id=file_id+1
        
#####################################################################################################
#####################################################################################################
#####################################################################################################
##Once the loop over all files are finished...create box plot with all distances, headings and speeds
##
#print all_pe_distances
#print all_headings_differences
#print all_speeds_differences

pe_distances=[]
wgs84_pp_distances=[]
speeds=[]
headings=[]
##I have an array of arrays for the box plot I need a huge single array with aaaaaalll entries
for i in range(0,len(all_pe_distances)):
    for j in range(0,len(all_pe_distances[i])):
        pe_distances.append(all_pe_distances[i][j])
        wgs84_pp_distances.append(all_wgs84_pp_distances[i][j])
        headings.append(all_headings_differences[i][j])
        speeds.append(all_speeds_differences[i][j])
        
pe_distances_array=np.asarray(pe_distances) 
wgs84_pp_distances_array=np.asarray(wgs84_pp_distances)                 
headings_array=np.asarray(headings)                   
speeds_array=np.asarray(speeds)                            

fig1, ax1 = plt.subplots(3,1,figsize=(35,25))
plt.subplots_adjust(left=0.125,right=0.9,bottom=0.1,top=0.9,wspace=0.2,hspace=0.6)

meanpointprops = dict(marker='D', markerfacecolor='w')
ax1[0].boxplot(pe_distances_array, showmeans=True, meanprops=meanpointprops , labels=['pe_distances'],whis=[2.5,97.5],vert=False,widths=0.9)
ax1[0].set_xlabel('point to edge distances (m)')
ax1[0].set_title('#Files: '+str(files_with_small_pe_distances)+' ,Mean Effective reduction: '+ str(round(np.asarray(reduce_info_gps).mean()*100,3))+'%')
ax1[0].set_xticks(np.arange(0,21,1))
#ax1[1].boxplot(wgs84_pp_distances_array, showmeans=True, meanprops=meanpointprops , labels=['pe_distances'],whis=[2.5,97.5],vert=False,widths=0.9)
#ax1[1].set_xlabel('point to point (wgs84) distances (m)')
#ax1[1].set_title('#Files: '+str(files_with_small_pe_distances)+' ,Mean Effective reduction: '+ str(round(np.asarray(reduce_info_gps*100).mean(),3)))###

ax1[1].boxplot(headings_array, showmeans=True, meanprops=meanpointprops , labels=['headings'],whis=[2.5,97.5],vert=False,widths=0.9)
ax1[1].set_xlabel('Heading differences')
ax1[1].set_title('#Files: '+str(files_with_small_pe_distances)+' ,Mean Effective reduction: '+ str(round(np.asarray(reduce_info_heading).mean()*100,3))+'%')
ax1[1].set_xticks(np.arange(-60,80,5))

ax1[2].boxplot(speeds_array, showmeans=True, meanprops=meanpointprops , labels=['speeds'],whis=[2.5,97.5],vert=False,widths=0.9)
ax1[2].set_xlabel('Speed differences')
ax1[2].set_title('#Files: '+str(files_with_small_pe_distances)+' ,Mean Effective reduction: '+ str(round(np.asarray(reduce_info_speed).mean()*100,3))+'%')

fig1.suptitle('All scenarios, framesize=500', fontsize=35)

name1='box_plots_all_scenarios_all_differences_pe_distances_smaller_than_'+str(max_pe_distance)+'m.png'
#name1='box_plots_all_scenarios_all_differences_pe_distances_larger_than_'+str(max_pe_distance)+'m.png'
#if not os.path.exists('./box_plots'):
#    os.makedirs('./box_plots')
#plt.savefig('box_plots/'+name1)
plt.savefig(name1)
###################################################################################################################################################
###################################################################################################################################################
###################################################################################################################################################

pe_distances=[]
wgs84_pp_distances=[]
speeds=[]
headings=[]
##I have an array of arrays for the box plot I need a huge single array with aaaaaalll entries                                                                                                                      
for i in range(0,len(all_pe_distances_larger)):
    for j in range(0,len(all_pe_distances_larger[i])):
        pe_distances.append(all_pe_distances_larger[i][j])
        wgs84_pp_distances.append(all_wgs84_pp_distances_larger[i][j])
        headings.append(all_headings_differences_larger[i][j])
        speeds.append(all_speeds_differences_larger[i][j])

pe_distances_array=np.asarray(pe_distances)
wgs84_pp_distances_array=np.asarray(wgs84_pp_distances)
headings_array=np.asarray(headings)
speeds_array=np.asarray(speeds)

fig1, ax1 = plt.subplots(3,1,figsize=(35,25))
plt.subplots_adjust(left=0.125,right=0.9,bottom=0.1,top=0.9,wspace=0.2,hspace=0.6)

meanpointprops = dict(marker='D', markerfacecolor='w')
ax1[0].boxplot(pe_distances_array, showmeans=True, meanprops=meanpointprops , labels=['pe_distances'],whis=[2.5,97.5],vert=False,widths=0.9)
ax1[0].set_xlabel('point to edge distances (m)')
ax1[0].set_title('#Files: '+str(files_with_small_pe_distances_larger)+' ,Mean Effective reduction: '+ str(round(np.asarray(reduce_info_gps).mean()*100,3))+'%')

#ax1[1].boxplot(wgs84_pp_distances_array, showmeans=True, meanprops=meanpointprops , labels=['pe_distances'],whis=[2.5,97.5],vert=False,widths=0.9) 
#ax1[1].set_xlabel('point to point (wgs84) distances (m)')                                                                                     
#ax1[1].set_title('#Files: '+str(files_with_small_pe_distances)+' ,Mean Effective reduction: '+ str(round(np.asarray(reduce_info_gps*100).mean(),3)))##
ax1[1].boxplot(headings_array, showmeans=True, meanprops=meanpointprops , labels=['headings'],whis=[2.5,97.5],vert=False,widths=0.9)
ax1[1].set_xlabel('Heading differences')
ax1[1].set_title('#Files: '+str(files_with_small_pe_distances_larger)+' ,Mean Effective reduction: '+ str(round(np.asarray(reduce_info_heading).mean()*100,3))+'%')

ax1[2].boxplot(speeds_array, showmeans=True, meanprops=meanpointprops , labels=['speeds'],whis=[2.5,97.5],vert=False,widths=0.9)
ax1[2].set_xlabel('Speed differences')
ax1[2].set_title('#Files: '+str(files_with_small_pe_distances_larger)+' ,Mean Effective reduction: '+ str(round(np.asarray(reduce_info_speed).mean()*100,3))+'%')

fig1.suptitle('All scenarios, framesize=500', fontsize=35)

name1='box_plots_all_scenarios_all_differences_pe_distances_larger_than_'+str(max_pe_distance)+'m.png'                                                                                                             
#if not os.path.exists('./box_plots'):                                                                                                                                                                             
#    os.makedirs('./box_plots')                                                                                                                                                                                   
#plt.savefig('box_plots/'+name1)                                                                                                                                                                                    
plt.savefig(name1)
