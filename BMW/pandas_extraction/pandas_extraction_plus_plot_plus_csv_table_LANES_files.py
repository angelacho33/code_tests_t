#!/usr/bin/env python

import matplotlib
matplotlib.rcParams.update({'font.size': 30})
matplotlib.rcParams['agg.path.chunksize'] = 10000

import matplotlib.pyplot as plt
import matplotlib.dates as md
import csv
import sys
import pandas as pd
import glob
import os
import numpy as np
import gc
from tk_plot import ts_to_dt
#scenario_input=0
scenario_input=int(sys.argv[1]) ##0:city ##1 overland
scenario=['city','overland']

folder='/home/angelh/Documents/DataScienceRelated/Teraki/Files/BMW_UC3/2016_05_19_Sample_02_Original_3/'
filenamelist_lanes = sorted(glob.glob(folder+scenario[scenario_input]+'/*lanes.csv'),key=os.path.getsize,reverse=True)

#filenamelist_lanes = [filenamelist_lanes[11]] #JUst the firt file

dt_format='%Y-%m-%d %H:%M'
xfmt = md.DateFormatter(dt_format)

file_id=0
#file=filenamelist_lanes[0]
output_file='summary_raw_scenario_'+scenario[scenario_input]+'.csv'

with open(output_file, 'w',0) as f_recon:
    writer_recon = csv.writer(f_recon,quoting=csv.QUOTE_NONNUMERIC)
    writer_recon.writerow(('file_id','npoints','sensor_name','npoints_sensor','file_name'))
    for file in filenamelist_lanes:
        
        dfa=pd.read_csv(file,sep=', ',header=0)
        
        ##Replace nulls witn NaN
        dfa=dfa.replace('null',np.nan,regex=True)
        #print '**************************'
        print 'file_id:', file_id, 'file_name:', file
        
        #print dfa.ix[58] #Has a value (of heading not distance)
        
        ##Extraxct arrays tss and values for not null values
        df_key=[]
        tss_key=[]
        values_key=[]
        
        ##The keys are (otput of dfa.keys())
        #key=dfa.keys()[1] ##This is the second column key:heading
        #key=dfa.keys()[3] ##This is tthir column nextLeft_y[m]
        i=0
        for key in dfa.keys():
            if not 'type' in key: ##The type values are strings!: DASHED, SOLID, ETC...how to plot them?
                # print '*******key*********', key
                ##Another dataframe without the null valus...
                df_key.append(dfa[dfa[key].notnull()])
                
                ##time stamps and transform to datetime objects
                tss_key.append(df_key[i]['ts [ms]'].astype(float).values) ##For some reason pandas read numbers as strings...:S##Caution!!In this conversion 3.4=3,9999999??
                tss_raw_dt = [ts_to_dt(elm, dt_format=dt_format) for elm in tss_key[i]] 
                
                ##values
                values_key.append(df_key[i][key].astype(float).values)##For some reason pandas read numbers as strings...:S
                if 'heading' in key:
                    key='heading'
            
                #Make plots!
                fig1, ax1 = plt.subplots(1,1,figsize=(15,10))
                
                
                if df_key[i].empty:
                    #print 'The DataFrame is empty!'
                    tss_raw_dt=np.arange(1,11,1)
                    values_key[i]=np.zeros(10)
                    fig1.suptitle('File_id:'+str(file_id)+' ,'+key+', All the values are null!!', fontsize=30)
                else:
                    fig1.suptitle('File_id:'+str(file_id)+' ,'+key + ' ,Number of points: '
                                  +str(len(tss_key[i]))+ ', Total points in the file:'+str(len(dfa)), fontsize=20)
                    
                ax1.plot(tss_raw_dt,values_key[i],label='raw',marker='o',markersize=10,color='g',lw=2)
                ax1.set_xlabel('Time')
                ax1.set_ylabel(key,fontsize=20)
                ax1.xaxis.set_major_formatter(xfmt)
                plt.xticks( rotation=25 )
                if not os.path.exists('./Plots'):
                    os.makedirs('./Plots')
                fig1.savefig('Plots/Plot_file_id_'+str(file_id)+'_'+key+'.png')
                plt.close('all')
                gc.collect()
                if df_key[i].empty:
                    writer_recon.writerow((file_id,len(dfa),key,'null',file))
                else:
                    writer_recon.writerow((file_id,len(dfa),key,str(len(tss_key[i])),file))
                i=i+1
        
        file_id=file_id+1
    
    
    
        
        




