#!/usr/bin/env python
import matplotlib
matplotlib.rcParams.update({'font.size': 15})
matplotlib.rcParams['agg.path.chunksize'] = 10000
import matplotlib.pyplot as plt
import matplotlib.dates as md
import time,datetime
import csv
import sys
import pandas as pd
import glob
import os
import numpy as np
import gc
#from tk_plot import ts_to_dt
##For interactive plots...
#%pylab 

def ts_to_dt(ts, dt_format):
    s = ts / 1000.0
    dt = datetime.datetime.fromtimestamp(s)
    return dt

#scenario_input=0
scenario_input=int(sys.argv[1]) ##0:city ##1 overland
scenario=['city','overland']

folder='/home/angelh/Documents/DataScienceRelated/Teraki/Files/BMW_UC3/2016_05_19_Sample_02_Original_3/'
filenamelist_lanes = sorted(glob.glob(folder+scenario[scenario_input]+'/*lanes.csv'),key=os.path.getsize,reverse=True)

#filenamelist_lanes = [filenamelist_lanes[11]] 
#filenamelist_lanes=[filenamelist_lanes[11]] ##11 file has several null columns at all times...
                                                

        
file_id=0
output_file='summary_raw_scenario_'+scenario[scenario_input]+'_line_types.csv'

with open(output_file, 'w',0) as f_recon:
    writer_recon = csv.writer(f_recon,quoting=csv.QUOTE_NONNUMERIC)
    writer_recon.writerow(('file_id','npoints','sensor_name','npoints_sensor','file_name'))
    for file in filenamelist_lanes:
        file_name=file.replace(folder+scenario[scenario_input]+'/','')
        dfa=pd.read_csv(file,sep=', ',header=0)
        #dfa=dfa.head(10) ##Fisrt 10 lines of the DataFrame for test
        
        ##Replace nulls witn NaN
        #dfa=dfa.replace('null',np.nan,regex=True)
        #print '**************************'
        print 'file_id:', file_id, 'file_name:', file
        
        #print dfa.ix[58] #Has a value (of heading not distance)
        
        ##The keys are (otput of dfa.keys())
        #key=dfa.keys()[1] ##This is the second column key:heading
        #key=dfa.keys()[3] ##This is tthir column nextLeft_y[m]
        
        ##############################################################################
        ##These are the keys (columns) of a given data fil:
        #:Index([u'ts [ms]', u'lane_heading []', u'nextLeft_type', u'nextLeft_y[m]',
        #u'left_type', u'left_y[m]', u'right_type', u'right_y',
        #u'nextRight_type', u'nextRight_y [m]'],
        
        ##############################################################################
        
        ######################           Test! #################################
        #dfa[dfa['nextLeft_type']=='DASHED'][['ts [ms]','nextLeft_y[m]']].values
        #dfa[dfa['nextLeft_type']=='CURBSIDE'][['ts [ms]','nextLeft_y[m]']].values
        #dfa[dfa['nextLeft_type']=='DOUBLE_LINE'][['ts [ms]','nextLeft_y[m]']].values
        
        #dfa[dfa['left_type']=='SOLID'][['ts [ms]','left_y[m]']].values
        #dfa[dfa['left_type']=='DASHED'][['ts [ms]','left_y[m]']].values
        #dfa[dfa['left_type']=='CURBSIDE'][['ts [ms]','left_y[m]']].values
        #dfa[dfa['left_type']=='DOUBLE_LINE'][['ts [ms]','left_y[m]']].values

        #dfa[dfa['right_type']=='SOLID'][['ts [ms]','right_y[m]']].values
        #dfa[dfa['right_type']=='DASHED'][['ts [ms]','right_y[m]']].values
        #dfa[dfa['right_type']=='CURBSIDE'][['ts [ms]','right_y[m]']].values
        #dfa[dfa['right_type']=='DOUBLE_LINE'][['ts [ms]','right_y[m]']].values

        #dfa[dfa['nextRight_type']=='SOLID'][['ts [ms]','nextRight_y [m]']].values
        #dfa[dfa['nextRight_type']=='DASHED'][['ts [ms]','nextRight_y [m]']].values
        #dfa[dfa['nextRight_type']=='CURBSIDE'][['ts [ms]','nextRight_y [m]']].values
        #dfa[dfa['nextRight_type']=='DOUBLE_LINE'][['ts [ms]','nextRight_y [m]']].values
        #####################################################################################
        
        
        ##Extracting distance info from databank according to line type
        #keys=['nextLeft_type','left_type','right_type','nextRight_type']
        #distances=['nextLeft_y[m]','left_y[m]','right_y[m]','nextRight_y [m]']
        
        dict={
            'nextLeft_type':'nextLeft_y[m]',
            'left_type':'left_y[m]',
            'right_type':'right_y',
            'nextRight_type':'nextRight_y [m]'
        }
        
        #Test
        #lane_type='nextLeft_type'
        #line_style='SOLID'
        #lane_type='nextLeft_type'
        #line_style='DASHED'
        #lane_type='left_type'
        #line_style='DASHED'
        
        dict_line_style={
            'DASHED':['--','blue'],
            'SOLID':['-','red'],
            'CURBSIDE':[':','green'],
            'DOUBLE_LINE':['-','black']
        }
        
        for lane_type in dict.keys():
            i=0
            fig1, ax1 = plt.subplots(2,1,figsize=(15,10),sharex=True)
            plt.subplots_adjust(left=0.125,right=0.9,bottom=0.1,top=0.82,wspace=0.0,hspace=0.05)
            #for line_style in ['DASHED', 'SOLID', 'CURBSIDE','DOUBLE_LINE']:
            for line_style in dict_line_style.keys():
                #print 'lane_type: ', lane_type, ' ,line_style:', line_style
                
                #DataFrame with a given lane style
                dfa_temp=dfa[dfa[lane_type]==line_style]
                #dt_format='%Y-%m-%d %H:%M'
                dt_format='%H:%M'
                
                if dfa_temp.empty==False:
                    ##Get rid of null values...which in principle is not needed since 
                    ##in the selection of dfa_temp already the null values are not selected....
                    dfa_lane=dfa_temp[dfa_temp[dict[lane_type]].notnull()] 
                    
                    tss=dfa_lane['ts [ms]'].values ##time stamps
                    distances=dfa_lane[dict[lane_type]].astype(float).values ##distances
                    
                    
                    title='File_id: '+str(file_id)+' ,File_name: '+file_name+' ,'+dict[lane_type]
                    label=line_style
                else:
                    i=i+1
                    #print 'The Dataframe is empty...', i
                    #print '******************', 'i: ' , i 
                    if i==4:
                        #print 'ALL ENTRIES ARE NULL!!'
                        tss=dfa['ts [ms]'].values ##All the time stamps in the file...
                        tss_raw_dt = [ts_to_dt(elm, dt_format=dt_format) for elm in tss] 
                        #print tss_raw_dt[:10]
                        n=len(tss)
                        distances=np.zeros(n)
                        title='File_id: '+str(file_id)+' ,File_name: '+file_name+' ,'+dict[lane_type]
                        label='All enties are null!!...setting to 0'
                        ax1[0].plot(tss_raw_dt,distances,label=label)
                        ax1[0].set_ylabel(dict[lane_type],fontsize=20)
                        #xfmt = md.DateFormatter(dt_format)
                        #ax1.xaxis.set_major_formatter(xfmt)
                        ax1[0].legend(loc='best',title=lane_type,fontsize=20)
                        #ax1[0].set_xlabel('Time')
                    continue
                #Transform time stamps to datetime objects
                tss_raw_dt = [ts_to_dt(elm, dt_format=dt_format) for elm in tss] 
                
                tss_all=dfa['ts [ms]'].values ##time stamps
                #print len(tss_all), #tss_all[1]-tss_all[0]
                tss_differences=np.ediff1d(tss_all,to_begin=0)/1000. ##/1000 to put in secinds!!
                #print np.mean(tss_differences)
                tss_raw_dt_all = [ts_to_dt(elm, dt_format=dt_format) for elm in tss_all] 
                ax1[1].plot(tss_raw_dt_all,tss_differences, color='#004c00') ##Dark green
                
                
                npoints=len(tss_raw_dt)
                #ax1.plot(tss_raw_dt,distances,label=label+' ,npoints='+str(npoints),marker='o',markersize=10,linewidth=3,linestyle=dict_line_style[line_style])
                if(npoints<100):
                    marker='o'
                    markersize=10
                else:
                    marker=''
                    markersize=0
                    
                ax1[0].plot(tss_raw_dt,distances,label=label+'\nnpoints='+str(npoints)
                         ,linewidth=4,linestyle=dict_line_style[line_style][0]
                         ,color=dict_line_style[line_style][1]
                         ,marker=marker,markersize=markersize
                )
                
                #ax1.plot(tss,distances,label=lebel,marker='o',markersize=10,color='g',lw=2)
                ax1[0].set_ylabel(dict[lane_type],fontsize=20)
                xfmt = md.DateFormatter(dt_format)
                ax1[0].xaxis.set_major_formatter(xfmt)
                #ax1[0].legend(loc='best',title=lane_type,fontsize=20)
                ax1[0].legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                              ncol=4, mode="expand", borderaxespad=0.,title=lane_type)
                
                ax1[1].xaxis.set_major_formatter(xfmt)
                ax1[1].set_ylabel('Timestamps differences (s)',fontsize=20)
                ax1[1].set_yscale('log')
                ax1[1].set_xlabel('Time')
                
                
                
                
            ##Attempt to use the same time scale for all the plots...
            #tss_raw_dt = [elm.minute for elm in tss_raw_dt] 
            #plt.xticks(np.arange(min(tss_raw_dt),max))
            plt.xticks( rotation=25)
            fig1.suptitle(title, fontsize=20)
            if not os.path.exists('./Plots_with_lane_types'):
                os.makedirs('./Plots_with_lane_types')
            fig1.savefig('Plots_with_lane_types/Plot_file_id_'+str(file_id)+'_'+dict[lane_type]+'.png')
            #plt.show()
            plt.close('all')
            gc.collect()
            
        file_id=file_id+1
        
        
        
        
        
        
    
    
    
        
        




