##Script that goes through produced dbk files for BMW (with a file_id in the name)
##and produces a new table after smoothing and calculating gps distances as additional columns!!!
##Note: I set the number of files here explcitly in a variable since I have not
##managed to order the dbk files by name using sorted and glob...

import cPickle as pickle
from tk_databank import * 
from tk_plot import Plot
import numpy as np
import glob
import csv
import sys
import glob
import gc ##For the memory leak?
import matplotlib
#matplotlib.rcParams.update({'font.size': 30})
import tk_err
import os

import matplotlib.pyplot as plt
matplotlib.style.use('ggplot')

#####Function to extract percentile information from a horizontal box plot
def get_percentiles_from_box_plots(bp):
    percentiles = []
    for i in range(len(bp['boxes'])):
        percentiles.append((bp['caps'][2*i].get_xdata()[0], ##This is the lower percentile
                            bp['boxes'][i].get_xdata()[0],
                            bp['medians'][i].get_xdata()[0],
                            bp['boxes'][i].get_xdata()[2],
                            bp['caps'][2*i + 1].get_xdata()[0], ##This is the upper percentile
                            (bp['fliers'][i].get_ydata(),
                             bp['fliers'][i].get_xdata())))
    return percentiles
#######################################################################

scenario_input=int(sys.argv[1]) ##0:highway_probes, 1:innerCity ,2:rural 3:InnercityCrossing
scenario=['highway_probes','innerCity','rural','InnercityCrossing']
scenarios=['highway','innercity','rural']

#output_file='After_smoothing_with_pe_distances_as_columns_'+scenarios[scenario_input]+'_output_recon.csv'
output_file='After_smoothing_with_percentiles_as_columns_'+scenarios[scenario_input]+'_output_recon.csv'

#I need an array of percentiles..one entry per file
all_ninety_seven_five_percentiles=[]
all_max_pe_distances=[]
#
with open(output_file, 'w',0) as f_recon:
    #Output file
    writer_recon = csv.writer(f_recon,quoting=csv.QUOTE_NONNUMERIC)
    writer_recon.writerow(('file_id','npoints','sensor_id', 'sensor_name','value_name','process_id' ,'framesize','reduction', 
                           'tss_recon_min','tss_recon_max', 'values_recon_min','values_recon_max', 'values_recon.mean', 
                           'zlib_ratio', 'count_red','red_info_ratio',
                           'dev_max','dev_mean','dev_min',
                           'relerr','mape','smape','wpe','wape','rrmse','mae','max_pe','lower_ninetysevenfive_percentile','upper_ninetysevenfive_percentile',
                           'file_name') )
    
    ##Set window lenghts for the Savgol filter
    if scenario_input==0:
        window_lenght=[19] 
        n_files=324
    if scenario_input==1:
        window_lenght=[19]
        n_files=202
    if scenario_input==2:
        window_lenght=[19]
        n_files=115

    #n_files=1 #Test for one file
    for file_id in np.arange(0,n_files,1):
        file='output_file_id_'+str(file_id)+'.dbk'
        E=DataBank()
        E.load(file)
        if len(E.get_sensor_process_info(0))==0: ##Check dbk files with actual processed data...some files does not include this since are smaller than framesize
            continue
        
        ####1 Figure per file
        #fig1, ax1 = plt.subplots(1, 1,figsize=(30,15))
        #fig1, ax1 = plt.subplots(3,1,figsize=(30,15))
        
        #matplotlib.rcParams.update({'font.size': 25})
        fig1, ax1 = plt.subplots(3,1)
        plt.subplots_adjust(left=0.125,right=0.9,bottom=0.1,top=0.9,wspace=0.2,hspace=0.9)
        #plt.subplots_adjust(left=0.125,right=0.9,bottom=0.1,top=0.9,wspace=0.2,hspace=0.4)
        
        for sensor in E.get_sensor_list(): ##Loop over sensors in the dbk file
            
            index=sensor['index']
            dof=sensor['dof']
            sensor_id=sensor['sensor_id']
            sensor_name=sensor['sensor_name']
            
            process_id=0 #Contains the not processed data for gps
            framesize=E.get_sensor_process_info(sensor_id,process_id)['framesize']
            reduction=E.get_sensor_process_info(sensor_id,process_id)['reduction']
            #print framesize, reduction
            
    
            #print 'Len process info before post processing',sensor_name, len(E.get_sensor_process_info(sensor_id))
            print 'Len process info before post processing for *** ',sensor_name,':', len(E.get_sensor_process_info(sensor_id))
            
            ###########    Applying smoothing for GPS       ##############
            post_process_para = {
                "method": "savgol_filter",
                "window_length": window_lenght[0], ##33 for innercity?
                "polyorder": 2
            }
            if sensor_name=='gps':
                SD_post=E.sensor_data_post_process(sensor_id, process_id, post_process_para)
                process_id=1 #After post processing the process id=1 for gps!!!  
                print 'Len process info after post processing for *** ',sensor_name,':', len(E.get_sensor_process_info(sensor_id))
            
                ############  Deviations with smoothing  ##############
                gps_deviation1 = E.get_gps_deviations(sensor_id, process_id)
                print 'pe_deviations: ', gps_deviation1['pe_deviations']
                print 'pp_deviations: ', gps_deviation1['pp_deviations']
                print 'wgs84_pp_deviations: ',gps_deviation1['wgs84_pp_deviations']
                pe_distances1=np.asarray(gps_deviation1['pe_distances'])
                pp_distances1=np.asarray(gps_deviation1['pp_distances'])
                wgs84_pp_distances1=np.asarray(gps_deviation1['wgs84_pp_distances'])
                
                #all_pe_distances.append(pe_distances1)
                #all_pp_distances.append(pp_distances1)
                #all_wgs84_pp_distances.append(wgs84_pp_distances1)
                
                #print 'len(all_pe_distances)=', len(all_pe_distances)
                
                min_pe_distance=pe_distances1.min()
                max_pe_distance=pe_distances1.max()
                all_max_pe_distances.append(max_pe_distance) ##One max pe distance per file
                mean_pe_distance=pe_distances1.mean()
                
                min_pp_distance=pp_distances1.min()
                max_pp_distance=pp_distances1.max()
                mean_pp_distance=pp_distances1.mean()
                
                min_wgs84_pp_distance=wgs84_pp_distances1.min()
                max_wgs84_pp_distance=wgs84_pp_distances1.max()
                mean_wgs84_pp_distance=wgs84_pp_distances1.mean()
            
           
            
            #all_ninety_seven_five_percentiles.append(upper_ninety_seven_five_percentile)
            
            ##################################
            ##Plots
            P=Plot(E)
            P.plot(0,1,deviation='difference',dt_format='%Y-%m-%d %H:%M:%S',save_png=True,suptitle='File_id_'+str(file_id))
            P.plot(1,0,deviation='difference',dt_format='%Y-%m-%d %H:%M:%S',save_png=True,suptitle='File_id_'+str(file_id))
            P.plot(2,0,deviation='difference',dt_format='%Y-%m-%d %H:%M:%S',save_png=True,suptitle='File_id_'+str(file_id))
            ###############################################################################################################
            
            #Reduce info
            reduce_info=E.get_sensor_process_info(sensor_id, process_id)['reduce_info']
            count_red=reduce_info['count_red']
            red_info_ratio=reduce_info['ratio']
            zlib_ratio=E.get_zip_reduction_ratio(sensor_id,framesize)
            
            #Deviation_info
            deviations=E.get_deviations(sensor_id,process_id)
            
            #######Taken from tk_databank (calculate deviations)
            raw_data    = E.get_sensor_raw_data(sensor_id)
            recon_data  = E.get_sensor_recon_data(sensor_id, process_id)
            recon_data_len = len(recon_data['values'])
            ##############################
            
            npoints=raw_data['count']
            #if npoints<framesize:
            #    break
            
            #Recon data info
            #recon_data=D.get_sensor_recon_data(sensor_id,process_id)
            #tss_recon=np.asarray(recon_data['tss'])
            #values_recon=np.asarray(recon_data['values'])
            
            for i in range(dof):
             
                ##Trim raw according to recon data...and calculate difference...Taken from tk_databank (calculate deviations)
                tss_raw   = raw_data['tss'][:recon_data_len]
                tss_recon = recon_data['tss']
                component_raw   = list( zip(*raw_data['values'][:recon_data_len])[i] )
                component_recon = list( zip(*recon_data['values'])[i] )
                component_recon_interpol = tk_err.interpolate(tss_raw, tss_recon, component_recon)
                e_v      = tk_err.e(component_raw, component_recon_interpol)     # error (pointwise difference)
                
                ##############################
                #box plots 
                meanpointprops = dict(marker='D', markerfacecolor='w')
                if sensor_name=='gps':
                    bp=ax1[0].boxplot(pe_distances1, showmeans=True, meanprops=meanpointprops , 
                                      labels=['pe_distance'],whis=[2.5,97.5],vert=False,widths=0.9,sym='b+')
                    ax1[0].set_xlabel('point to edge distances (m)')
                    #ax1[0].set_title('File_id: '+str(file_id)+'  ,sensor_name: '+sensor_name
                    #              +'  ,process_id: '+str(process_id)+'  ,reduction: '+str(reduction)+'  ,framesize:'+str(framesize))
                    ax1[0].set_title('PE Distances, Eff. Red: '+str(round(red_info_ratio,3)))
                    
                if sensor_name=='heading':
                    bp=ax1[1].boxplot(e_v, showmeans=True, meanprops=meanpointprops , labels=['heading'],whis=[2.5,97.5],vert=False,widths=0.9,sym='b+')
                    ax1[1].set_xlabel('Heading differences')
                    ax1[1].set_title('Heading, Eff. Red: '+str(round(red_info_ratio,3)))
                
                if sensor_name=='speed':
                    bp=ax1[2].boxplot(e_v, showmeans=True, meanprops=meanpointprops , labels=['speed'],whis=[2.5,97.5],vert=False,widths=0.9,sym='b+')
                    ax1[2].set_xlabel('Speed differences')
                    ax1[2].set_title('Speed, Eff. Red: '+str(round(red_info_ratio,3)))
                
                ##Extract info from box plot:
                percentiles=get_percentiles_from_box_plots(bp)
                lower_ninety_seven_five_percentile=percentiles[0][0]
                upper_ninety_seven_five_percentile=percentiles[0][4]
                
                #print 'max pe_distance= ',pe_distances1.max(), 'for file_id:', file_id
                print 'sensor_name: ',sensor_name,' ,97.5% percentile=',upper_ninety_seven_five_percentile, 'for file_id:', file_id
                
                
                ##########################################
                value_name=sensor['value_names'][i]
                #values_comp=values_recon.T[i]
                values_comp=np.asarray(component_recon_interpol)
                dev_max=deviations['E'][i]['max']
                dev_mean=deviations['E'][i]['mean']
                dev_min=deviations['E'][i]['min']
                
                relerr=deviations['relerr'][i]
                mae=deviations['MAE'][i]
                mape=deviations['MAPE%'][i]
                smape=deviations['SMAPE%'][i]
                rrmse=deviations['RRMSE%'][i]
                wape=deviations['WAPE%'][i]
                wpe=deviations['WPE%'][i]
                                
                max_pe=all_max_pe_distances[file_id]
                #ninetysevenfive_pe=all_ninety_seven_five_percentiles[file_id]
                tss_recon=np.asarray(tss_recon)
                
                #Write output
                writer_recon.writerow((file_id,npoints,sensor_id,sensor_name, value_name,process_id ,framesize,reduction, 
                                       tss_recon.min(),tss_recon.max(), float("%.3f"%values_comp.min()), float("%.3f"%values_comp.max()), 
                                       float("%.3f"%values_comp.mean()), 
                                       float("%.3f"%zlib_ratio), count_red,float("%.3f"%red_info_ratio), 
                                       dev_max,dev_mean,dev_min,
                                       relerr,mape,smape,wpe,wape,rrmse,mae,max_pe,lower_ninety_seven_five_percentile,upper_ninety_seven_five_percentile,
                                       file))
            
            ##Box plot title and png.
            fig1.suptitle('File_id: '+str(file_id))
            
            if not os.path.exists('./individual_box_plots2'):
                os.makedirs('./individual_box_plots2')

            fig1.savefig('individual_box_plots2/File_id_'+str(file_id)+'.png')
            #fig1.savefig('individual_box_plots2/File_id_'+str(file_id)+'_sensor_id_'+str(sensor_id)+'_sensor_name_'+sensor_name
            #             +'_process_id_'+str(process_id)+'_reduction_'+str(reduction)+'.png')

        #process_id=process_id+1
        print '** Goes in file: ',file_id,''
        ##
        name='post_processed_output_file_id_'+str(file_id)+'.dbk'
        E.save(name)
        
        ##Clean everything
        fig1.clf()
        plt.close('all')
        #file_id=file_id+1
        gc.collect()
            
