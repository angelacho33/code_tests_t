"""
Project Teraki Data Process APIs
Author: qingzhou

it provides the APIs for raw_data file, tka data file, and other files.
"""

import os, sys, time

def str_to_int_float(string): 
    try:
        return int(string)
    except ValueError:
        pass
    try:
        return float(string)
    except ValueError:
        return string

def get_ts_value_from_file(filename, sep=';', pos_sid=0, pos_mt=2, pos_ts=0, pos_value=[1]):
    result = {}
    if isinstance(pos_sid,int):
        read_sid = True
    elif isinstance(pos_sid, basestring):
        read_sid = False
        sid = pos_sid
    else:
        return result
    if isinstance(pos_mt,int):
        read_mt = True
    elif isinstance(pos_mt, basestring):
        read_mt = False
        key = pos_mt
    else:
        return result
    f = open(filename, 'r')
    for line in f:
        try:
            text = line.rstrip()
            items = text.split(sep)
            if read_sid:
                sid = items[pos_sid]
            if read_mt:
                key = items[pos_mt]
            ts = str_to_int_float(items[pos_ts])
            value = [str_to_int_float(items[x]) for x in pos_value]
            if key not in result.keys():
                result[key] = {}
                result[key]['sid'] = sid
                result[key]['mt'] = key
                result[key]['tss'] = []
                result[key]['values'] = []
            result[key]['tss'].append(ts)
            result[key]['values'].append(value)
        except:
            pass
    f.close()
    return result

def get_format_data_string(sep, order, ts, value, sid=0, mt=2):
	texts = []
	for item in order:
		if item == 'sid':
			texts.append(str(sid))
		elif item == 'mt':
			texts.append(str(mt))
		elif item == 'ts':
			texts.append(str(ts))
		elif item == 'values':
			texts.append(sep.join([str(x) for x in value]))
		elif item == 'value':
			texts.append(value)
	result = sep.join(texts)
	return result

def write_ts_value_to_file(filename, sep=';', order=[], tss=[], values=[], sid=0, mt=2):
    if len(tss) > len(values):
    	count = len(values)
    else:
        count = len(tss)
    f = open(filename, 'w')
    for i in range(count):
        try:
        	text = get_format_data_string(sep, order, tss[i], values[i], sid, mt) + '\n'
        	f.write(text)
        except:
            pass
    f.close()
    return count

def write_bmv_data_file(filename, tss, values, sid, mt):
	sep = ','
	order = ['sid', 'ts', 'mt', 'values']
	write_ts_value_to_file(filename, sep, order, tss, values, sid, mt)
	
def combin_list(list1,list2):
	result = []
	i = 0
	while i<len(list1):
		result.append(list1[i]+list2[i])
		i +=1
	#print(result)
	return result

#--------------------------------------------------------------------
def test1():
    from tk_databank import DataBank
    sensor_data = get_ts_value_from_file("bmw1.csv", ',', 0, 2, 1, [3,4])
    D = DataBank()
    S = D.add_sensor_data(sensor_data['pos']['values'], 
        sensor_data['pos']['tss'], 8, "gps", "bmw_dataset_pos")
    pre_process = {
        "multiple": [100000, 100000]
    }
    post_process = {
        "divide": [100000.0, 100000.0]
    }
    D.sensor_data_process(8, 500, 0.9, 'cii_dct_v011', pre_process, post_process, 5)
    print "*********** MIN_VALUES ***************"
    print D.get_sensor_raw_data(8)['min_v']
    print "*********** MAX_VALUES ***************"
    print D.get_sensor_raw_data(8)['max_v']
    print "********** REDUCATION INFO ***********"
    print D.get_sensor_process_info(8, 0)['reduce_info']
    print "********** COMPRESS, REDUCATION RATIO ***********"
    print D.get_zip_reduction_ratio(8, 500)    
    print "********** End of SENSOR 8 ***********"
    #print D.get_sensor_red_data(8, 0)
    #D.write_sensor_data_files(8)

    sensor_data = get_ts_value_from_file("bmw1.csv", ',', 0, 2, 1, [3, 4, 5, 6])
    S = D.add_sensor_data(sensor_data['pos']['values'], 
        sensor_data['pos']['tss'], 9, "heading", "bmw_dataset_pos")
    pre_process = {
        "multiple": [100000, 100000, 1000, 1000],
        "value_exceptions": [
            [],
            [],
            [
                {
                    "e_name": "modulus",
                    "e_id":  1,
                    "v_types": ["red", "rest"],
                    "condition": "(abs(delta_v)>180000)",
                    "para":  "(delta_v/abs(delta_v)*360000)"
                },
                {
                    "e_name": "red_peak_points",
                    "e_id":  2,
                    "v_types": ["red"],
                    "condition": "((abs(delta_v)>30000) and (abs(delta_v)<330000))",
                    "para": "v"
                },
                {
                    "e_name": "rest_peak_points",
                    "e_id":  3,
                    "v_types": ["rest"],
                    "condition": "((abs(delta_v)>30000) and (abs(delta_v)<330000))",
                    "para": "v"
                }
            ], 
            [
                {
                    "e_name": "red_peak_points",
                    "e_id":  2,
                    "v_types": ["red"],
                    "condition": "(abs(delta_v)>8000)",
                    "para": "v"
                },
                {
                    "e_name": "rest_peak_points",
                    "e_id":  3,
                    "v_types": ["rest"],
                    "condition": "(abs(delta_v)>8000)",
                    "para": "v"
                }
            ] 
        ]
    }
    post_process = {
        "divide": [100000.0, 100000.0, 1000.0, 1000.0]
    }
    D.sensor_data_process(9, 200, 0.8, 'cii_dct_v011', pre_process, post_process, 5)
    D.sensor_data_process(9, 500, 0.9, 'cii_dct_v011', pre_process, post_process, 5)
    D.write_sensor_data_files(9)
    recon_data = D.get_sensor_recon_data(9, 0)
    write_bmv_data_file("bmw1_500_09.csv", recon_data['tss'], recon_data['values'], sensor_data['pos']['sid'], sensor_data['pos']['mt'])
    print "*********** MIN_VALUES ***************"
    print D.get_sensor_raw_data(9)['min_v']
    print "*********** MAX_VALUES ***************"
    print D.get_sensor_raw_data(9)['max_v']
    print "********** REDUCATION INFO ***********"
    print D.get_sensor_process_info(9, 0)['reduce_info']
    print D.get_sensor_process_info(9, 1)['reduce_info']
    print "********** COMPRESS, REDUCATION RATIO ***********"
    print D.get_zip_reduction_ratio(9, 200)    
    print D.get_zip_reduction_ratio(9, 500)    
    print "********** End of SENSOR 9 ***********"

    sensor_data = get_ts_value_from_file("bmw1.csv", ',', 0, 2, 1, [6])
    S = D.add_sensor_data(sensor_data['pos']['values'], 
        sensor_data['pos']['tss'], 10, "speed", "bmw_dataset_pos")
    pre_process = {
        "multiple": [1000],
        "value_exceptions": [
            [
                {
                    "e_name": "red_peak_points",
                    "e_id":  2,
                    "v_types": ["red"],
                    "condition": "(abs(delta_v)>8000)",
                    "para": "v"
                },
                {
                    "e_name": "rest_peak_points",
                    "e_id":  3,
                    "v_types": ["rest"],
                    "condition": "(abs(delta_v)>8000)",
                    "para": "v"
                }
            ] 
        ]
    }
    post_process = {
        "divide": [1000.0]
    }
    D.sensor_data_process(10, 200, 0.8, 'cii_dct_v011', pre_process, post_process, 5)
    D.sensor_data_process(10, 500, 0.9, 'cii_dct_v011', pre_process, post_process, 5)
    D.write_sensor_data_files(10)
    print "*********** MIN_VALUES ***************"
    print D.get_sensor_raw_data(10)['min_v']
    print "*********** MAX_VALUES ***************"
    print D.get_sensor_raw_data(10)['max_v']
    print "********** REDUCATION INFO ***********"
    print D.get_sensor_process_info(10, 0)['reduce_info']
    print D.get_sensor_process_info(10, 1)['reduce_info']
    print "********** COMPRESS, REDUCATION RATIO ***********"
    print D.get_zip_reduction_ratio(10, 200)    
    print D.get_zip_reduction_ratio(10, 500)    
    print "********** End of SENSOR 10 ***********"

def test2():
    from tk_databank import DataBank
    D = DataBank()
    sensor_data = get_ts_value_from_file("bmw1.csv", ',', 0, 2, 1, [5])
    S = D.add_sensor_data(sensor_data['pos']['values'], 
    sensor_data['pos']['tss'], 10, "speed", "bmw_dataset_pos")
    pre_process = {
        "multiple": [1000],
        "value_exceptions": [
            [
                {
                    "e_name": "red_peak_points",
                    "e_id":  4,
                    "v_types": ["red"],
                    "condition": "(abs(delta_v)>30000)",
                    "para": "delta_v"
                },
                {
                    "e_name": "rest_peak_points",
                    "e_id":  5,
                    "v_types": ["rest"],
                    "condition": "(abs(delta_v)>30000)",
                    "para": "str(delta_v)+'_'+str(ts)+'_'+str(v)"
                }
            ] 
        ]
    }
    post_process = {
        "divide": [1000.0]
    }
    D.sensor_data_process(10, 200, 0.8, 'cii_dct_v011', pre_process, post_process, 5)
    print D.get_sensor_raw_data(10)['dof']
    print "*"*100
    print D.get_sensor_red_data(10, 0)['dof']
    print "+"*100
    print D.get_sensor_recon_data(10, 0)['dof']

    D.save("test.dbk")

    E = DataBank()
    E.load("test.dbk")
    print("E"*50)
    print E.get_sensor_raw_data(10)['dof']
    print E.get_sensor_object(10)
    print E.get_sensor_object(10, 0)

def test3():
    from tk_databank import DataBank
    from tk_plot import Plot
    from linear import LinearInterpolation
    from scipy.signal import savgol_filter
    import matplotlib.pyplot as plt
    import numpy as np
    
    import tk_plot
    file = "./probes/rural/3d236daa-2ed5-4a6a-b656-44a941947acd_1.csv"
    file = "./probes/innerCity/2bdc5673-67b9-4a15-82e8-6bdd24675e11_1.csv"
    D = DataBank()
    P = Plot(D)
    
    sensor_data = get_ts_value_from_file(file, ',', 0, 2, 1, [3,4])
    S = D.add_sensor_data(sensor_data['pos']['values'], 
        sensor_data['pos']['tss'], 8, "gps", "bmw_dataset_pos")
    pre_process = {
        "multiple": [1000000, 1000000]
    }
    post_process = {
        "divide": [1000000.0, 1000000.0]
    }
    #pre_process['min_delta'] = [20, 20]
    D.sensor_data_process(8, 500, 0.75, 'cii_dct_v011', pre_process, post_process, 10)
    
    post_process_para = {
        "method": "savgol_filter",
        "window_length": 13,
        "polyorder": 2
    }
    #33 optimal city
    D.sensor_data_post_process(8,0, post_process_para)
    
    print "This is the reduction info",D.get_sensor_process_info(8, 1)['reduce_info']
    print "This is the zip compression",D.get_zip_reduction_ratio(8, 500)    
    
    P.plot(8,process_id=0,deviation='difference')

    P.plot(8,process_id=1,deviation='difference')
    #pre_process['integer'] = [1, 1]
    #D.sensor_data_process(8, 500, 0.8, 'cii_dct_v011', pre_process, post_process, 5)
    #D.write_sensor_data_files(8)

    
    #D.sensor_data_process(8, 500, 0.8, 'cii_dct_v011', pre_process, post_process, 5)
    #D.write_sensor_data_files(8)

    recon_data = get_ts_value_from_file("recon_data_8_0.csv", ';', '8', 'test', 0, [1,2])
    D.add_processed_sensor_data(8, recon_data['test']['values'], recon_data['test']['tss'])
    
    
    print "This is the list",D.get_sensor_process_info(8, 1)['reduce_info']['thelist']
    
    the_list = sorted(D.get_sensor_process_info(8, 1)['reduce_info']['thelist'])
    
    diff_list = [0]
    
    for i in range(len(the_list)-1):
        diff_list.append(the_list[i+1]-the_list[i])
	   
    fig = plt.figure()
    ax = fig.add_subplot(111)
    tk_plot.plot_hist_of_values(ax,diff_list)
    plt.show()
	   
    

    print "*" * 50
    print "gps distance devition process 0"
    gps_deviation = D.get_gps_deviations(8, 1)
    print "edge distance : " + str(gps_deviation['pe_deviations'])
    print "point distance : " + str(gps_deviation['pp_deviations'])
    print "wgs point distance : " + str(gps_deviation['wgs84_pp_deviations'])
    max_index = gps_deviation['pe_deviations']['max_d_index']
    
    
    #print deviation_pe_sort
    

    
    #print D.get_sensor_process_info(8, 2)
    #D.get_deviations(8, 2)
    #D.get_sensor_recon_data(8, 2)

    
    #D.sensor_data_post_process(8,1, post_process_para)
    #print "*" * 50
    #print "gps distance devition process 4"
    #gps_deviation = D.get_gps_deviations(8, 0)
    #print "edge distance : " + str(gps_deviation['pe_deviations'])
    #print "point distance : " + str(gps_deviation['pp_deviations'])
    #print "wgs point distance : " + str(gps_deviation['wgs84_pp_deviations'])
    #print gps_deviation['pe_distances'][3]
    #print gps_deviation['pp_distances'][3]
    #print gps_deviation['wgs84_pp_distances'][3]
    
    
    ##print gps_deviation['pe_distances']
    ##print gps_deviation['pp_distances']
    ##print gps_deviation['wgs84_pp_distances'][3]
    #max_dev = 0
    #max_index = 0
    #for i in range(len(gps_deviation['pe_distances'])):
    #    if gps_deviation['pe_distances'][i] > max_dev:
    #        max_dev = gps_deviation['pe_distances'][i]
    #        max_index = i
		  
		   
    
    data_raw_tss = D.get_sensor_raw_data(8)['tss']
    data_raw = D.get_sensor_raw_data(8)['values']
    data_rec = D.get_sensor_recon_data(8,1)['values']
    data_red = D.get_sensor_red_data(8,0)['values']
    
    
    #print T
    print "This is the max deviation", gps_deviation['pe_distances'][max_index]
    
    print "This is the max position raw",data_raw[max_index]
    print "This is the max position rec",data_rec[max_index]
    
    from vincenty import vincenty
    
    print "This is the calculated distance",vincenty((data_raw[max_index][0],data_raw[max_index][1]), (data_rec[max_index][0],data_rec[max_index][1]))
    
    #from vincenty import vincenty
    #for i in range(len(data_raw_x_clean)):
    #    data_raw_clean.append([data_raw_x_clean[i],data_raw_y_clean[i]])
    #    data_rec_clean.append([data_rec_x_clean[i],data_rec_y_clean[i]])
    #    diff_vicenty.append(vincenty((data_raw_x_clean[i],data_raw_y_clean[i]), (data_rec_x_clean[i],data_rec_y_clean[i]))/2*1000)
    
    
    
    #print "These are the timestamps",data_raw_tss
    
    #data_raw_tss = [x[0] for x in data_raw_tss]
    data_raw_x = [x[0] for x in data_raw]
    data_raw_y = [x[1] for x in data_raw]
    data_rec_x = [x[0] for x in data_rec]
    data_rec_y = [x[1] for x in data_rec]
    data_red_x = [x[0] for x in data_red]
    data_red_y = [x[1] for x in data_red]
    
    data_raw_x_clean = []
    data_raw_y_clean = []
    data_rec_x_clean = []
    data_rec_y_clean = []
    for i in range(len(data_rec_x)):
        data_raw_x_clean.append(data_raw_x[i])
        data_raw_y_clean.append(data_raw_y[i])
        data_rec_x_clean.append(data_rec_x[i])
        data_rec_y_clean.append(data_rec_y[i])
    
    data_raw_clean = []
    data_rec_clean = []
    
    for i in range(len(data_rec_x)):
        data_raw_clean.append([data_raw_x[i],data_raw_y[i]])
        data_rec_clean.append([data_rec_x[i],data_rec_y[i]])
		  
    max_pos_x = data_raw_x[max_index]
    max_pos_y = data_raw_y[max_index]
    
    # Sort the indices
    deviation_pe = gps_deviation['pe_distances']
    
    #print deviation_pe
    
    deviation_pe_sort = sorted(gps_deviation['pe_distances'],reverse=True)
    index_deviation_pe_sort = np.argsort(deviation_pe)
    index_deviation_pe_sort = index_deviation_pe_sort.tolist()
    index_deviation_pe_sort = index_deviation_pe_sort[::-1]
    #index_deviation_pe_sort = index_deviation_pe_sort.reverse()
    #index_deviation_pe_sort =  sorted(range(len(deviation_pe)), key=lambda k: deviation_pe[k])
    
    print index_deviation_pe_sort[0]
    
    max_deviations = 10
    
    max_dev_index_list = []
    max_pos_raw = []
    max_pos_rec = []
    
    for i in range(max_deviations):
        max_dev_index_list.append(index_deviation_pe_sort[i])
        max_pos_raw.append(data_raw_clean[index_deviation_pe_sort[i]])
        max_pos_rec.append(data_rec_clean[index_deviation_pe_sort[i]])
    #print index_deviation_pe_sort
    
    print "These are the max positions raw",max_pos_raw
    print "These are the max positions rec",max_pos_rec
    
    
    epsilon = 0.00001
    framesize = 500
    
    
    
	   
	   
    [index_rdp,data_rdp_x,data_rdp_y,reduction_rdp] = D.get_rdp_data(data_raw_x, data_raw_y, framesize, epsilon)
    
    
    data_range = range(0,len(data_rec_x))#np.linspace(1, len(data_raw_clean), num=len(data_raw_clean), endpoint=True)
    
    table_rdp_x = LinearInterpolation(
        x_index=index_rdp,
        values=data_rdp_x,
        extrapolate=True)
	
    data_int_rdp_clean_x = []
    #print "This is the range",data_range
    for i in data_range:
        data_int_rdp_clean_x.append(table_rdp_x(i))
	   
    table_rdp_y = LinearInterpolation(
        x_index=index_rdp,
        values=data_rdp_y,
        extrapolate=True)
	
    data_int_rdp_clean_y = []
    #print "This is the range",data_range
    for i in data_range:
        data_int_rdp_clean_y.append(table_rdp_y(i))
	   
	   
    #print len(data_raw_clean)
    #print len(data_int_rdp_clean)
	   
    data_rdp_int_clean = []
    for k in range(len(data_rec_x)):
        data_rdp_int_clean.append([data_int_rdp_clean_x[k],data_int_rdp_clean_y[k]])
	   
    #print "This is the raw data", D.get_sensor_raw_data(8)
    #print "This is the raw data",data_rdp_int_clean
    
    gps_RDP_deviation = D.calculate_gps_deviations(data_raw_clean, data_rdp_int_clean)
    
    
    fig = plt.figure()
    ax = fig.add_subplot(111)
    tk_plot.plot_boxplot_residuals(ax, gps_deviation['pe_distances'], np.zeros(len(gps_deviation['pe_distances'])))
    #tk_plot.plot_hist_of_values(ax, diff_speed)
    plt.show()
    
    fig = plt.figure()
    ax = fig.add_subplot(111)
    tk_plot.plot_boxplot_residuals(ax, gps_RDP_deviation['pe_distances'], np.zeros(len(gps_deviation['pe_distances'])))
    #tk_plot.plot_hist_of_values(ax, diff_speed)
    plt.show()
    
    fig = plt.figure()
    ax = fig.add_subplot(111)
    tk_plot.plot_boxplot_residuals(ax, gps_deviation['wgs84_pp_distances'], np.zeros(len(gps_deviation['wgs84_pp_distances'])))
    #tk_plot.plot_hist_of_values(ax, diff_speed)
    plt.show()
    
    fig = plt.figure()
    ax = fig.add_subplot(111)
    tk_plot.plot_boxplot_residuals(ax, gps_RDP_deviation['wgs84_pp_distances'], np.zeros(len(gps_RDP_deviation['wgs84_pp_distances'])))
    #tk_plot.plot_hist_of_values(ax, diff_speed)
    plt.show()
    
    

    fig = plt.figure()
    ax = fig.add_subplot(111)
    tk_plot.plot_hist_of_values(ax,gps_deviation['pe_distances'])
    plt.show()
    
    fig = plt.figure()
    ax = fig.add_subplot(111)
    tk_plot.plot_boxplot_residuals(ax, gps_deviation['wgs84_pp_deviations'], np.zeros(len(gps_deviation['pe_distances'])))
    #tk_plot.plot_hist_of_values(ax, diff_speed)
    plt.show()
    
    
    from tk_databank_rdp import data_m_n_convert
    
    #print "This is the GPS data",data_raw_x_clean
    
    # Get the GPS data here
    
    data_raw_x_clean_vis = data_m_n_convert([data_raw_x_clean])
    data_raw_y_clean_vis = data_m_n_convert([data_raw_y_clean])
    data_rec_x_clean_vis = data_m_n_convert([data_rec_x_clean])
    data_rec_y_clean_vis = data_m_n_convert([data_rec_y_clean])
    
    
    from tk_gmd_gps_point import maps
    import random
    #sensor_data = get_ts_value_from_file("daimler.csv", ' ', 2, 3, 1, [4])
    #gpsdata = combin_list(sensor_data['GPS_Lat']['values'],sensor_data['GPS_Long']['values'])
    #print(gpsdata[0][0])
    gpsdata = combin_list(data_raw_x_clean_vis,data_raw_y_clean_vis)
    gpsdata_rec = combin_list(data_rec_x_clean_vis,data_rec_y_clean_vis)
    #print "This is the GPS data",gpsdata
    #print(gpsdata[0][0])
    mymap = maps(gpsdata[0][0], gpsdata[0][1], 16)
    path = gpsdata
    path_rec = gpsdata_rec
    v = []
    for i in range(len(gpsdata)):
        #v.append(diff_speed[i]*150/max(diff_speed))
	   v.append(gps_deviation['pe_distances'][i])
    mymap.addpath(path)
    mymap.addcolorpath(path_rec,v)
    for i in range(len(data_raw_x_clean_vis)):
        mymap.addpoint(gpsdata[i][0], gpsdata[i][1])
        mymap.addpoint(gpsdata_rec[i][0], gpsdata_rec[i][1], color = '#00FF00')
    mymap.draw('./tk_googlemaps_gps.html')
    
    
    plt.plot(v)
    plt.show()
    
    
    sensor_data = get_ts_value_from_file(file, ',', 0, 2, 1, [6])
    S = D.add_sensor_data(sensor_data['pos']['values'], 
    sensor_data['pos']['tss'], 9, "speed", "bmw_dataset_pos")
    pre_process = {
        "multiple": [1000],
        "value_exceptions": [
            [
                {
                    "e_name": "red_peak_points",
                    "e_id":  4,
                    "v_types": ["red"],
                    "condition": "(abs(delta_v)>2000)",
                    "para": "delta_v"
                },
                {
                    "e_name": "rest_peak_points",
                    "e_id":  5,
                    "v_types": ["rest"],
                    "condition": "(abs(delta_v)>2000)",
                    "para": "str(delta_v)+'_'+str(ts)+'_'+str(v)"
                }
            ] 
        ]
    }
    pre_process['integer'] = [1]
    #pre_process['min_delta'] = [400]
    post_process = {
        "divide": [1000.0]
    }
    framesize = 500
    D.sensor_data_process(9, framesize , 0.8, 'cii_dct_v011', pre_process, post_process, 15)
    D.write_sensor_data_files(9)
    print "This is the ratio",D.get_sensor_process_info(9, 0)['reduce_info']
    
    print "This is the zip compression",D.get_zip_reduction_ratio(9, 500)
    
    P.plot(9,process_id=0,deviation='difference')
    
    data_raw_tss = D.get_sensor_raw_data(9)['tss']
    data_raw = D.get_sensor_raw_data(9)['values']
    data_rec = D.get_sensor_recon_data(9,0)['values']
    data_red = D.get_sensor_red_data(9,0)['values']
    
    #print "These are the timestamps",data_raw_tss
    
    #data_raw_tss = [x[0] for x in data_raw_tss]
    data_raw_speed_x = [x[0] for x in data_raw]
    #data_raw_y = [x[1] for x in data_raw]
    data_rec_speed_x = [x[0] for x in data_rec]
    #data_rec_y = [x[1] for x in data_rec]
    data_red_speed_x = [x[0] for x in data_red]
    #data_red_y = [x[1] for x in data_red]
    
    #print "This is the length of the raw data",len(data_raw)
    #print "This is the length of the reduced data",len(data_red)

    data_raw_speed_x_clean = []
    data_rec_speed_x_clean = []
    diff_speed = []
    
    for i in range(len(data_rec_x)):
        data_raw_speed_x_clean.append(data_raw_speed_x[i])
        #data_raw_y_clean.append(data_raw_y[i])
        data_rec_speed_x_clean.append(data_rec_speed_x[i])
        diff_speed.append(abs(data_raw_speed_x[i]-data_rec_speed_x[i]))
    
    
    
    
    
    from tk_gmd_speed_point import maps
    import random
    
    #sensor_data = get_ts_value_from_file("daimler.csv", ' ', 2, 3, 1, [4])
    #gpsdata = combin_list(sensor_data['GPS_Lat']['values'],sensor_data['GPS_Long']['values'])
    #print(gpsdata[0][0])
    gpsdata = combin_list(data_raw_x_clean_vis,data_raw_y_clean_vis)
    gpsdata_rec = combin_list(data_rec_x_clean_vis,data_rec_y_clean_vis)
    #print "This is the GPS data",gpsdata
    #print(gpsdata[0][0])
    mymap = maps(gpsdata[0][0], gpsdata[0][1], 16)
    path = gpsdata
    path_rec = gpsdata_rec
    v = []
    for i in range(len(gpsdata)):
        #v.append(diff_speed[i]*150/max(diff_speed))
	   v.append(diff_speed[i])
    mymap.addpath(path)
    mymap.addcolorpath(path_rec,v)
    for i in range(len(data_raw_x_clean_vis)):
        mymap.addpoint(gpsdata[i][0], gpsdata[i][1])
        mymap.addpoint(gpsdata_rec[i][0], gpsdata_rec[i][1], color = '#00FF00')
    mymap.draw('./tk_googlemaps_speed.html')
    
    
    plt.plot(v)
    plt.show()
    
    #framesize = 50
   
    #Sparsity = 0.1
    
    
    #filtered_x = savgol_filter(data_rec_x_clean, 19, 2)
    #filtered_y = savgol_filter(data_rec_y_clean, 19, 2)
    ##plt.plot(filtered_x)
    ##plt.plot(data_raw_x_clean)
    ##plt.show()
    
    #data_rec_x_clean = filtered_x
    #data_rec_y_clean = filtered_y
    
    #import GPS_Trafo
    #[diff_tk,diff_tk_2] = GPS_Trafo.GPS_Transformation(data_raw_tss,data_raw_x_clean,data_raw_y_clean,data_rec_x_clean,data_rec_y_clean,data_int_rdp_clean_x,data_int_rdp_clean_y,data_red_x,data_red_y,max_pos_x,max_pos_y,max_pos_raw,max_pos_rec,epsilon,Sparsity,framesize)
    

    #print "*" * 50
    #print "gps distance devition process 1"
    #gps_deviation = D.get_gps_deviations(8, 1)
    #print "edge distance : " + str(gps_deviation['pe_deviations'])
    #print "point distance : " + str(gps_deviation['pp_deviations'])
    #print "wgs point distance : " + str(gps_deviation['wgs84_pp_deviations'])
    #print gps_deviation['pe_distances'][3]
    #print gps_deviation['pp_distances'][3]
    #print gps_deviation['wgs84_pp_distances'][3]

    #print "*" * 50
    #print "gps distance devition process 2"
    #gps_deviation = D.get_gps_deviations(8, 2)
    #print "edge distance : " + str(gps_deviation['pe_deviations'])
    #print "point distance : " + str(gps_deviation['pp_deviations'])
    #print "wgs point distance : " + str(gps_deviation['wgs84_pp_deviations'])
    #print gps_deviation['pe_distances'][3]
    #print gps_deviation['pp_distances'][3]
    #print gps_deviation['wgs84_pp_distances'][3]
    
    
    #P.plot(8,process_id=0,deviation='difference')
    
    #data_raw_tss = D.get_sensor_raw_data(8)['tss']
    #data_raw = D.get_sensor_raw_data(8)['values']
    #data_rec = D.get_sensor_recon_data(8,0)['values']
    #data_red = D.get_sensor_red_data(8,0)['values']
    
    ##print "These are the timestamps",data_raw_tss
    
    ##data_raw_tss = [x[0] for x in data_raw_tss]
    #data_raw_x = [x[0] for x in data_raw]
    #data_raw_y = [x[1] for x in data_raw]
    #data_rec_x = [x[0] for x in data_rec]
    #data_rec_y = [x[1] for x in data_rec]
    #data_red_x = [x[0] for x in data_red]
    #data_red_y = [x[1] for x in data_red]
    
    ##print "This is the length of the raw data",len(data_raw)
    ##print "This is the length of the reduced data",len(data_red)

    #data_raw_x_clean = []
    #data_raw_y_clean = []
    #data_rec_x_clean = []
    #data_rec_y_clean = []
    #for i in range(len(data_rec_x)):
    #    data_raw_x_clean.append(data_raw_x[i])
    #    data_raw_y_clean.append(data_raw_y[i])
    #    data_rec_x_clean.append(data_rec_x[i])
    #    data_rec_y_clean.append(data_rec_y[i])
	   
    #epsilon = 0.00001 # Accuracy in degrees of RDP
	   
	   
    ##[index_rdp,data_rdp_x,data_rdp_y,reduction_rdp] = D.get_rdp_data(data_raw_x, data_raw_y, framesize, epsilon)
    
    ##print "This is the RDP reduction",reduction_rdp
    
	   
    #from scipy.signal import savgol_filter
    
    
    ##print "Data RDP",data_rdp_x
    ##print "Index RDP",index_rdp
    
    ##plt.plot(index_rdp,data_rdp_x)
    ##plt.plot(data_raw_x)
    ##plt.show()
    
    
    #data_raw_clean = []
    #data_rec_clean = []
    #data_rdp_clean = []
    #data_rdp_raw_clean = []
    #diff_vicenty = []
    
    #from vincenty import vincenty
    #for i in range(len(data_raw_x_clean)):
    #    data_raw_clean.append([data_raw_x_clean[i],data_raw_y_clean[i]])
    #    data_rec_clean.append([data_rec_x_clean[i],data_rec_y_clean[i]])
    #    diff_vicenty.append(vincenty((data_raw_x_clean[i],data_raw_y_clean[i]), (data_rec_x_clean[i],data_rec_y_clean[i]))/2*1000)
	   
	   
    #Epsilon = epsilon
    #Sparsity = 0.1
    #Sample_size = 500
	   
    #import GPS_Trafo
    #[diff_tk,diff_tk_2] = GPS_Trafo.GPS_Transformation(data_raw_tss,data_raw_x_clean,data_raw_y_clean,data_rec_x_clean,data_rec_y_clean,data_rdp_x_clean,data_rdp_y_clean,data_red_x,data_red_y,max_pos_x,max_pos_y,Epsilon,Sparsity,Sample_size)
    
	   
	   
    #D.add_processed_sensor_data(8, recon_data['test']['values'], recon_data['test']['tss'])

    #print "*" * 50
    #print "gps distance devition process 0"
    #gps_deviation = D.get_gps_deviations(8, 0)
    #print "edge distance : " + str(gps_deviation['pe_deviations'])
    #print "point distance : " + str(gps_deviation['pp_deviations'])
    #print "wgs point distance : " + str(gps_deviation['wgs84_pp_deviations'])
    #print gps_deviation['pe_distances'][3]
    #print gps_deviation['pp_distances'][3]
    #print gps_deviation['wgs84_pp_distances'][3]
	   
    #for i in range(len(data_rdp_x)):
    #   data_rdp_clean.append([data_rdp_x[i],data_rdp_y[i]])
    #    data_rdp_raw_clean.append([data_raw_x_clean[index_rdp[i]],data_raw_y_clean[index_rdp[i]]])
	   
	   
    #print "These are the deviations",diff_vicenty
	   
    
    #print "These are the deviations",D.calculate_gps_deviations(data_raw_clean, data_rec_clean)
    
    #Deviations = D.calculate_gps_deviations(data_raw_clean, data_rec_clean)['distances']
    
    #data_range = range(0,len(data_raw_clean))#np.linspace(1, len(data_raw_clean), num=len(data_raw_clean), endpoint=True)
    
    #table_rdp_x = LinearInterpolation(
    #    x_index=index_rdp,
    #    values=data_rdp_x,
    #    extrapolate=True)
	
    #data_int_rdp_clean_x = []
    ##print "This is the range",data_range
    #for i in data_range:
    #    data_int_rdp_clean_x.append(table_rdp_x(i))
	   
    #table_rdp_y = LinearInterpolation(
    #    x_index=index_rdp,
    #    values=data_rdp_y,
    #    extrapolate=True)
	
    #data_int_rdp_clean_y = []
    ##print "This is the range",data_range
    #for i in data_range:
    #    data_int_rdp_clean_y.append(table_rdp_y(i))
	   
	   
    #print len(data_raw_clean)
    #print len(data_int_rdp_clean)
	   
    #data_rdp_int_clean = []
    #for k in range(len(data_raw_clean)):
    #    data_rdp_int_clean.append([data_int_rdp_clean_x[k],data_int_rdp_clean_y[k]])

    #diff_vicenty_rdp = []
    #for i in range(len(data_raw_x_clean)):
    #    diff_vicenty_rdp.append(vincenty((data_raw_x_clean[i],data_raw_y_clean[i]), (data_int_rdp_clean_x[i],data_int_rdp_clean_y[i]))/2*1000)

    #print D.get_sensor_process_info(8, 2)
    #D.get_deviations(8, 0)
    #D.get_sensor_recon_data(8, 0)

if __name__ == '__main__':
    test3()
