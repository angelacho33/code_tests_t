import math
import os, sys, time
import csv
###########################################################
## Google map python api --> color track
## 
############################################################

class maps:

    def __init__(self, centerLat, centerLng, zoom ):
        self.center = (float(centerLat),float(centerLng))
        self.zoom = int(zoom)
        self.grids = None
        self.paths = []
        self.points = []
        self.radpoints = []
        self.gridsetting = None
        self.coloricon = 'http://chart.apis.google.com/chart?cht=mm&chs=12x16&chco=FFFFFF,XXXXXX,000000&ext=.png'

    def addpath(self,path,color = '#0000FF'):
        path.append(color)
        self.paths.append(path)

    def addpoint(self, lat, lng, color = '#FF0000', icon=None, title="n/a"):
        self.points.append((lat,lng,color[1:],icon,title))

    def addcolorpath(self,path,parameter):
        val = 10
        for i in range(1,len(parameter)):
            if parameter[i-1] == parameter[i] :
                path_temp = [path[i-1],path[i]]
                #print(path_temp)
                if parameter[i] <= 1:
                    self.addpath(path_temp,"#AAFF00") 
                elif parameter[i] > 1 and parameter[i] <= 2 :
                    self.addpath(path_temp,"#D4FF00")
                elif parameter[i] > 2 and parameter[i] <= 4 :
                    self.addpath(path_temp,"#FFFF00") 
                elif parameter[i] > 4 and parameter[i] <= 6 :
                    self.addpath(path_temp,"#FFD400")
                elif parameter[i] > 6 and parameter[i] <= 8 :
                    self.addpath(path_temp,"#FFAA00")
                elif parameter[i] > 8 and parameter[i] <= 10 :
                    self.addpath(path_temp,"#FF8000")
                elif parameter[i] > 10 and parameter[i] <= 12 :
                    self.addpath(path_temp,"#FF5500")
                elif parameter[i] > 12 :
                    self.addpath(path_temp,"#FF2A00")
                    #print("3")
            else:
                mid = (parameter[i-1]+parameter[i])/2;
                path_mid_lat = (path[i-1][0]+path[i][0])/2
                path_mid_long = (path[i-1][1]+path[i][1])/2
                if parameter[i-1] < parameter[i] :
                    path_temp_0 = [path[i-1],(path_mid_lat,path_mid_long)]
                    path_temp_1 = [(path_mid_lat,path_mid_long),path[i]]
                elif parameter[i-1] > parameter[i] :
                    path_temp_1 = [path[i-1],(path_mid_lat,path_mid_long)]
                    path_temp_0 = [(path_mid_lat,path_mid_long),path[i]]


                if mid <= 2:
                    self.addpath(path_temp_0,"#abff00") 
                    self.addpath(path_temp_1,"#bfe600")
                elif mid > 2 and mid <=  6:
                    self.addpath(path_temp_0,"#FFFF00") 
                    self.addpath(path_temp_1,"#FFD400")
                    #print("1")
                elif mid > 6 and mid <= 10 :
                    self.addpath(path_temp_0,"#FFAA00")
                    self.addpath(path_temp_1,"#FF8000")
                    #print("2")
                elif mid > 10 :
                    self.addpath(path_temp_0,"#FF5500")
                    self.addpath(path_temp_1,"#FF2A00")
                    #print("3")
    
    #create the html file which inlcude one google map and all points and paths
    def draw(self, htmlfile):
        f = open(htmlfile,'w')
        ##Now we required an api key...:S
        f.write('<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAk0RSkujqhJeR9Y7tAkJfMmGCPGMsJxqw&callback=initMap" type="text/javascript"></script>\n')
        f.write('<html>\n')
        f.write('<head>\n')
        f.write('<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />\n')
        f.write('<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>\n')
        f.write('<title>Google Maps - pygmaps </title>\n')
        f.write('<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>\n')
        f.write('<script type="text/javascript">\n')
        f.write('\tfunction initialize() {\n')
        self.drawmap(f)
        self.drawpoints(f)
        self.drawpaths(f,self.paths)
        f.write('\t}\n')
        f.write('</script>\n')
        f.write('</head>\n')
        f.write('<body style="margin:0px; padding:0px;" onload="initialize()">\n')
        f.write('\t<div id="map_canvas" style="width: 100%; height: 100%;"></div>\n')
        f.write('</body>\n')
        f.write('</html>\n')        
        f.close()

    def drawpoints(self,f):
        for point in  self.points:
            self.drawpoint(f,point[0],point[1],point[2],point[3],point[4])


    def drawpaths(self, f, paths):
        for path in paths:
            #print path
            self.drawPolyline(f,path[:-1], strokeColor = path[-1])

    #############################################
    # # # # # # Low level Map Drawing # # # # # # 
    #############################################
    def drawmap(self, f):
        f.write('\t\tvar centerlatlng = new google.maps.LatLng(%f, %f);\n' % (self.center[0],self.center[1]))
        f.write('\t\tvar myOptions = {\n')
        f.write('\t\t\tzoom: %d,\n' % (self.zoom))
        f.write('\t\t\tcenter: centerlatlng,\n')
        f.write('\t\t\tmapTypeId: google.maps.MapTypeId.ROADMAP\n')
        f.write('\t\t};\n')
        f.write('\t\tvar map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);\n')
        f.write('\n')

    def drawpoint(self,f,lat,lon,color,icon,title):
        f.write('\t\tvar latlng = new google.maps.LatLng(%f, %f);\n'%(lat,lon))
        f.write('\t\tvar img = new google.maps.MarkerImage(\'%s\');\n' % (self.coloricon.replace('XXXXXX',color)))
        f.write('\t\tvar marker = new google.maps.Marker({\n')
        f.write('\t\ttitle: "%s",\n' % title)
        f.write('\t\ticon: img,\n')
        if icon:
            f.write('\t\ticon: "%s",\n' % icon)
        f.write('\t\tposition: latlng\n')
        f.write('\t\t});\n')
        f.write('\t\tmarker.setMap(map);\n')
        f.write('\n')
        
        
    def drawPolyline(self,f,path,\
            clickable = False, \
            geodesic = True,\
            strokeColor = "#FF0000",\
            strokeOpacity = 0.5,\
            strokeWeight = 5
            ):
        f.write('var PolylineCoordinates = [\n')
        for coordinate in path:
            f.write('new google.maps.LatLng(%f, %f),\n' % (coordinate[0],coordinate[1]))
        f.write('];\n')
        f.write('\n')

        f.write('var Path = new google.maps.Polyline({\n')
        f.write('clickable: %s,\n' % (str(clickable).lower()))
        f.write('geodesic: %s,\n' % (str(geodesic).lower()))
        f.write('path: PolylineCoordinates,\n')
        f.write('strokeColor: "%s",\n' %(strokeColor))
        f.write('strokeOpacity: %f,\n' % (strokeOpacity))
        f.write('strokeWeight: %d\n' % (strokeWeight))
        f.write('});\n')
        f.write('\n')
        f.write('Path.setMap(map);\n')
        f.write('\n\n')


def test1():
    mymap = maps(37.428, -122.145, 16)
    mymap.addpoint(37.427, -122.145, "#0000FF")

    path = [(37.429, -122.145),(37.428, -122.145),(37.427, -122.145),(37.427, -122.146),(37.427, -122.147),(37.427, -122.148),(37.427, -122.149)]
    v = [5,20,40,150,150,88,20]
    mymap.addcolorpath(path,v)
    mymap.draw('./tk_googlemaps.html')



if __name__ == "__main__":
    test1()

    ########## CONSTRUCTOR: maps(latitude, longitude, zoom) ##############################
    # DESC:        initialize a map  with latitude and longitude of center point  
    #        and map zoom level "15"
    # PARAMETER1:    latitude (float) latittude of map center point
    # PARAMETER2:    longitude (float) latittude of map center point
    # PARAMETER3:    zoom (int)  map zoom level 0~20
    # RETURN:    the instant of pygmaps
    #========================================================================================
    #mymap = maps(37.428, -122.145, 16)

    ########## FUNCTION:  addpoint(latitude, longitude, [color])#############################
    # DESC:        add a point into a map and dispaly it, color is optional default is red
    # PARAMETER1:    latitude (float) latitude of the point
    # PARAMETER2:    longitude (float) longitude of the point
    # PARAMETER3:    color (string) color of the point showed in map, using HTML color code
    #        HTML COLOR CODE:  http://www.computerhope.com/htmcolor.htm
    #        e.g. red "#FF0000", Blue "#0000FF", Green "#00FF00"
    # RETURN:    no return
    #========================================================================================
    #mymap.addpoint(37.427, -122.145, "#0000FF")

    ########## FUNCTION:  addpath(path,[color])##############################################
    # DESC:        add a path into map, the data struceture of Path is a list of points
    # PARAMETER1:    path (list of coordinates) e.g. [(lat1,lng1),(lat2,lng2),...]
    # PARAMETER2:    color (string) color of the point showed in map, using HTML color code
    #        HTML COLOR CODE:  http://www.computerhope.com/htmcolor.htm
    #        e.g. red "#FF0000", Blue "#0000FF", Green "#00FF00"
    # RETURN:    no return
    #========================================================================================

    #path = [(37.425, -122.145),(37.424, -122.145),(37.423, -122.145),(37.423, -122.146),(37.423, -122.147)]
    #mymap.addpath(path,"#00FF00")

    ########## FUNCTION:  addcolorpath(self,path,parameter,val=10):###########################
    # DESC:        add a color path into map, the data struceture of Path is a list of points
    # PARAMETER1:    path (list of coordinates) e.g. [(lat1,lng1),(lat2,lng2),...]
    # PARAMETER2:    parameter(list of coordinates) e.g [velocity1,velocity2.....]
    # RETURN:    no return
    #========================================================================================
    #path = [(37.429, -122.145),(37.428, -122.145),(37.427, -122.145),(37.427, -122.146),(37.427, -122.147),(37.427, -122.148),(37.427, -122.149)]
    #v = [5,20,40,150,150,88,20]
    #mymap.addcolorpath(path,v)


    ########## FUNCTION:  draw(file)######################################################
    # DESC:        create the html map file (.html)
    # PARAMETER1:    file (string) the map path and file
    # RETURN:    no return, generate html file in specified directory
    #========================================================================================
   # mymap.draw('./tk_googlemaps.html')
