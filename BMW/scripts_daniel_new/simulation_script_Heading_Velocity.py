#First test using the Teraki software
import os, sys, time
from tk_databank import DataBank
from tk_plot import Plot
import numpy as np
import matplotlib.pyplot as plt
from linear import LinearInterpolation

import tk_gdfr 

import  glob
import csv

scenario_input=int(sys.argv[1]) ##0:city ##1 overland
scenario=['city','overland']

##Input Files
folder='/Users/danielrichart/Documents/Projects/Research/Data/BMW/UseCase3/2016-05-19_Sample_02_Original/'

filenamelist_positions = sorted(glob.glob(folder+scenario[scenario_input]+'/*positions.csv'),key=os.path.getsize,reverse=True)
#filenamelist_positions = [filenamelist_positions[0]] #Just one file
#print filenamelist_positions

filenamelist_turnrate = sorted(glob.glob(folder+scenario[scenario_input]+'/*turnrate.csv'),key=os.path.getsize,reverse=True)
filenamelist_velocity = sorted(glob.glob(folder+scenario[scenario_input]+'/*velocity.csv'),key=os.path.getsize,reverse=True)

#filenamelist_velocity = ['/Users/danielrichart/Documents/Projects/Research/Data/BMW/UseCase3/2016-05-19_Sample_02_Original/city/b65922d7-f3cf-43fa-bc24-0b521befa927-velocity.csv']
filenamelist_turnrate = ['/Users/danielrichart/Documents/Projects/Research/Data/BMW/UseCase3/2016-05-19_Sample_02_Original/city/82a8535b-4bb8-4899-9923-04b6d7a39b45-turnrate.csv']


print filenamelist_positions
##Process cases
#framesizes=[100,500,1000]
#reductions=[0.6,0.8,0.9]   
#Short test
#framesizes=[500]
#reductions=[0.8]

#process_cases=[]
#for framesize in framesizes:
#    for reduction in reductions:
#        process_cases.append([framesize,reduction])
        

#########Pre and post process gps
pre_process_positions = {
    "multiple": [1000000, 1000000],
    'integer': [1,1]
}
post_process_positions = {
        "divide": [1000000.0, 1000000.0]
}
#########Pre and post process heading
pre_process_turnrate = {
    "multiple": [1000],
    'integer': [1],
    "value_exceptions": [
        [
            {
                "e_name": "red_peak_points",
                "e_id":  4,
                "v_types": ["red"],
                "condition": "(abs(delta_v)>2000)",
                "para": "delta_v"
            },
            {
                "e_name": "rest_peak_points",
                "e_id":  5,
                "v_types": ["rest"],
                "condition": "(abs(delta_v)>2000)",
                "para": "str(delta_v)+'_'+str(ts)+'_'+str(v)"
            }
        ]
    ]
}
post_process_turnrate = {
    "divide": [1000.0]
}

    
#########Pre and postprocess Speed
pre_process_velocity = {
    #"multiple": [1],
    #'integer': [1],
    "value_exceptions": [
        [
            {
                "e_name": "red_peak_points",
                "e_id":  4,
                "v_types": ["red"],
                "condition": "(abs(delta_v)>10)",
                "para": "delta_v"
            },
            {
                "e_name": "rest_peak_points",
                "e_id":  5,
                "v_types": ["rest"],
                "condition": "(abs(delta_v)>10)",
                "para": "str(delta_v)+'_'+str(ts)+'_'+str(v)"
            }
        ]
    ]
}
post_process_velocity = {
    #"divide": [1.0]
}

##array components::
#             0=gps             ,1=heading            ,2=speed
pre_process= [pre_process_positions   ,pre_process_turnrate  ,pre_process_velocity]
post_process=[post_process_positions  ,post_process_turnrate ,post_process_velocity]
block=       [10                ,15                   ,10]

pre_process= [pre_process_turnrate  ,pre_process_velocity]
post_process=[post_process_turnrate ,post_process_velocity]
block=       [5                   ,10]

#pre_process = [pre_process_velocity]
#post_process = [post_process_velocity]
#block =       [10]


output_file='summary_'+scenario[scenario_input]+'_output_recon.csv'



with open(output_file, 'w',0) as f_recon:
    #Output file
    writer_recon = csv.writer(f_recon,quoting=csv.QUOTE_NONNUMERIC)
    writer_recon.writerow(('file_id','npoints','sensor_id', 'sensor_name','value_name','process_id' ,'framesize','reduction', 
                           'tss_recon_min','tss_recon_max', 'values_recon_min','values_recon_max', 'values_recon.mean', 
                           'zlib_ratio', 'count_red','red_info_ratio',
                           'dev_max','dev_mean','dev_min',
                           'relerr','mape','smape','wpe','wape','rrmse','mae',
                           'file_name') )
    
    
    file_id=0 
    for file in filenamelist_turnrate:    
        D = DataBank()
        #Load GPS Data****************** Sensor 0 ******************************
        value_names=['v']
        #sensor_data1 = tk_gdfr.get_ts_value_from_file(file, ',', 0, 2, 0, [1,2])
        sensor_data1 = tk_gdfr.get_ts_value_from_file(file, ',', 'pos', 'pos', 0, [1])
        #sensor_data1 = tk_gdfr.get_ts_value_from_file(file, ',', '', '', 0, [1,2])
        #S1 = D.add_sensor_data(sensor_data1['']['values'],
        #                       sensor_data1['']['tss'], 0, "position", "bmw_dataset_posision_file",value_names=value_names)
        S1 = D.add_sensor_data(sensor_data1['pos']['values'][1:],
	          sensor_data1['pos']['tss'][1:], 0, "gps", "pos",value_names=value_names)
			
        #print sensor_data1['pos']['values'][1:]
        
        #Load Heading Data ************  Sensor 1 ******************************
        #sensor_data2 = tk_gdfr.get_ts_value_from_file(file, ',', 0, 2, 1, [5])
        #S2 = D.add_sensor_data(sensor_data2['pos']['values'],
        #                       sensor_data2['pos']['tss'], 1, "heading", "bmw_dataset_heading")
        
        #Load Speed Data Sensor 2 *****  Sensor 2 ******************************          
        #sensor_data3 = tk_gdfr.get_ts_value_from_file(file, ',', 0, 2, 1, [6])
        #S3 = D.add_sensor_data(sensor_data3['pos']['values'],
        #                       sensor_data3['pos']['tss'], 2, "speed", "bmw_dataset_speed")
        
        process_id=0
        #for item in process_cases:        
        #    framesize = item[0]
        #    reduction = item[1]

        for sensor in D.get_sensor_list():
            index=sensor['index']
            dof=sensor['dof']
            sensor_id=sensor['sensor_id']
            sensor_name=sensor['sensor_name']
            
            framesize=100
            ##Test gps at lower reduction....
            #if sensor_name=='gps':
            #    reduction=0.5
            #else:
            reduction=0.8
                
            #Raw data info
            raw_data=D.get_sensor_raw_data(sensor_id)
            tss=np.asarray(raw_data['tss'])
            values=np.asarray(raw_data['values'])
            npoints=raw_data['count']
		  
            from tk_plot import plot_hist_of_timestamp_intervals
            tss_plot = plot_hist_of_timestamp_intervals(tss)
            plt.plot(tss_plot)
            plt.show()
		  
            values_full = []
            tss_full = []
            for i in range(len(values)):
                #print "This is the treshhold interval",tss_plot[i]
                if tss_plot[i] < 25:
                    values_full.append(values[i])
                    tss_full.append(tss[i])
                if tss_plot[i] > 25:
                    print "This is the treshhold interval",tss_plot[i]
                    print "This is the rounded data point",round(tss_plot[i]/20)
                    data_range = range(0,int(round(tss_plot[i]/20)))
                    int_start = values[i-1]
                    int_stop = values[i]
                    table = LinearInterpolation(
                        x_index=[0,int(round(tss_plot[i]/20))],
                        values=[int_start,int_stop],
                        extrapolate=True)
                    values_int = []
                    tss_int = []
                    #print "This is the range",data_range
                    n = 0
                    for j in data_range:
                        values_int.append(table(j).tolist())
                        print tss[i-1]
                        tss_int.append(tss[i-1] + n*20)
                        print tss_int[j]
                        n = n + 1
                    
                    print "This is the new interval",values_int
				#for i in range(len(int(tss_plot[i]/20)):
                    values_full.extend(values_int)
                    tss_full.extend(tss_int)
            plt.plot(values_full)
            plt.show()
		  
		  
            from tk_plot import plot_hist_of_timestamp_intervals
            tss_plot_int = plot_hist_of_timestamp_intervals(tss_full)
            plt.plot(tss_plot_int)
            plt.show()
		  
            values_full = [x for x in values_full]
            tss_full = [x for x in tss_full]
            # print(values_full)
            S1 = D.add_sensor_data(values_full,
                tss_full, sensor_id + 1, "gps", "pos",value_names=value_names)
		  
            if npoints<framesize:
                break
                
                ##Process Data
            print 'Goes in file:', file_id, 'Goes in sensor: ', sensor_id
            D.sensor_data_process(sensor_id + 1, framesize, reduction,'cii_dct_v011', pre_process=pre_process[sensor_id], post_process=post_process[sensor_id],block=block[sensor_id])
            print "This is the reduction info",D.get_sensor_process_info(sensor_id + 1, 0)['reduce_info']
            
            #Plots
            P=Plot(D)
            P.plot(sensor_id + 1,process_id,deviation='difference',save_png=True)
		  
            post_process_para = {
                "method": "savgol_filter",
                "window_length": 49,
                 "polyorder": 2
            }
	       #33 optimal city
            D.sensor_data_post_process(sensor_id + 1,process_id, post_process_para)
		  
            P.plot(sensor_id + 1,process_id + 1,deviation='difference',save_png=True)
            
            #Recon data info
            raw_data=D.get_sensor_raw_data(sensor_id + 1)
            recon_data=D.get_sensor_recon_data(sensor_id + 1,process_id + 1)
            tss_recon=np.asarray(recon_data['tss'])
            values_recon=np.asarray(recon_data['values'])
            zlib_ratio=D.get_zip_reduction_ratio(sensor_id,framesize)
            print "This is the zlib ratio",zlib_ratio
            
            #Reduce info
            reduce_info=D.get_sensor_process_info(sensor_id + 1, process_id + 1)['reduce_info']
            count_red=reduce_info['count_red']
            red_info_ratio=reduce_info['ratio']
		  
            import tk_plot
		  
            raw_data = [x for x in raw_data]
            recon_data = [x for x in recon_data]
		  
            fig = plt.figure()
            ax = fig.add_subplot(111)
            tk_plot.plot_boxplot_residuals(ax, raw_data, recon_data)
            #tk_plot.plot_hist_of_values(ax, diff_speed)
            plt.show()
            
            #Deviation_info
            deviations=D.get_deviations(sensor_id + 1,process_id)
            
            for i in range(dof):
                value_name=sensor['value_names'][i]
                values_comp=values_recon.T[i]
                dev_max=deviations['E'][i]['max']
                dev_mean=deviations['E'][i]['mean']
                dev_min=deviations['E'][i]['min']
                
                relerr=deviations['relerr'][i]
                mae=deviations['MAE'][i]
                mape=deviations['MAPE%'][i]
                smape=1#deviations['SMAPE%'][i]
                rrmse=deviations['RRMSE%'][i]
                wape=deviations['WAPE%'][i]
                wpe=deviations['WPE%'][i]
                
                
                
                #Write output
                writer_recon.writerow((file_id,npoints,sensor_id,sensor_name, value_name,process_id ,framesize,reduction, 
                                       tss_recon.min(),tss_recon.max(), float("%.3f"%values_comp.min()), float("%.3f"%values_comp.max()), 
                                       float("%.3f"%values_comp.mean()), 
                                       float("%.3f"%zlib_ratio), count_red,float("%.3f"%red_info_ratio), 
                                       dev_max,dev_mean,dev_min,
                                       relerr,mape,smape,wpe,wape,rrmse,mae,
                                       file))
                
        #process_id=process_id+1
        #print '** Goes in file: ',file_id,''
        ##
        name='output_file_id_'+str(file_id)+'.dbk'
        D.save(name)
        ##
        file_id=file_id+1