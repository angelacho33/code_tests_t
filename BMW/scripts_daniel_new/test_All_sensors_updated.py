#First test using the Teraki software
import os, sys, time
#os.chdir('../')
from tk_databank import DataBank
#from tk_plot import Plot
import numpy as np
from linear import LinearInterpolation
from lat_lon import Latitude, Longitude, LatLon, string2geocoord, string2latlon, GeoVector
import matplotlib.pyplot as plt
from tk_plot import Plot


from tk_gdfr import *

import  glob
import csv

scenario_input=int(sys.argv[1]) ##0:highway_probes, 1:innerCity ,2:rural 3:InnercityCrossing
#scenario=['city','overland']
scenario=['highway_probes','innerCity','rural','InnercityCrossing']
##Input Files
folder='/home/angel/Files/BMW/probes/'
#folder='/Users/danielrichart/Documents/Projects/Research/Data/BMW/UseCase3/2016-05-19_Sample_02_Original/'
#folder='/home/angelh/Documents/DataScienceRelated/Teraki/Files/BMW/probes/'
#filenamelist = sorted(glob.glob(folder+scenario[scenario_input]+'/*positions.csv'),key=os.path.getsize,reverse=True)
filenamelist = sorted(glob.glob(folder+scenario[scenario_input]+'/*.csv'),key=os.path.getsize,reverse=True)
#filenamelist = filenamelist[-1:] #last file by size (smallest file)
#filenamelist = filenamelist[:1]   #first fileS by size (biggest file)
#filenamelist = filenamelist[100:]   #first fileS by size (biggest file)
#filenamelist = [filenamelist[13]] #13: File with speed differene peak!
#filenamelist = [filenamelist[2]] #2 file that Daniel reach 2m presision for GPS he show in Slack.
    #filenamelist = [filenamelist[66]] #13: File with heading difference peak!
#filenamelist = [filenamelist[390]] #390: File with 110 lines
#filenamelist = [filenamelist[243]] #390: File with 1100 lines
#filenamelist = [filenamelist[323]] #390: File with 520 lines

#filenamelist=[folder+scenario[0]+'/2eef208-63c2-4a9f-8098-28d8207d1bb_1.csv']##1M file and intersting file for Daniel
#filenamelist=[folder+scenario[0]+'/f88520-2efa-46e6-8fc0-35c353f84922_1.csv']##16k file
#filenamelist=[filenamelist[100]] ##Just the largest file: 0...the 100th file in size=100
print filenamelist

##Process cases
#framesizes=[100,500,1000]
#reductions=[0.6,0.8,0.9]   
#Short test
framesizes=[500]
reductions=[0.85]

process_cases=[]
for framesize in framesizes:
    for reduction in reductions:
        process_cases.append([framesize,reduction])
        

#########Pre and post process gps
pre_process_gps = {
    "multiple": [1000000, 1000000],
    'integer': [1,1]
}
post_process_gps = {
        "divide": [1000000.0, 1000000.0]
}
#########Pre and post process heading
pre_process_heading = {
    "multiple": [1000],
    'integer': [1],
    "value_exceptions": [
        [
            {
                "e_name": "red_peak_points",
                "e_id":  4,
                "v_types": ["red"],
                "condition": "(abs(delta_v)>10000)",
                "para": "delta_v"
            },
            {
                "e_name": "rest_peak_points",
                "e_id":  5,
                "v_types": ["rest"],
                "condition": "(abs(delta_v)>10000)",
                "para": "str(delta_v)+'_'+str(ts)+'_'+str(v)"
            }
        ]
    ]
}
post_process_heading = {
    "divide": [1000.0]
}

    
#########Pre and postprocess Speed
pre_process_speed = {
    "multiple": [1000],
    'integer': [1],
    "value_exceptions": [
        [
            {
                "e_name": "red_peak_points",
                "e_id":  4,
                "v_types": ["red"],
                "condition": "(abs(delta_v)>3000)",
                "para": "delta_v"
            },
            {
                "e_name": "rest_peak_points",
                "e_id":  5,
                "v_types": ["rest"],
                "condition": "(abs(delta_v)>3000)",
                "para": "str(delta_v)+'_'+str(ts)+'_'+str(v)"
            }
        ]
    ]
}
post_process_speed = {
    "divide": [1000.0]
}

##array components::
#             0=gps             ,1=heading            ,2=speed
pre_process= [pre_process_gps   ,pre_process_heading  ,pre_process_speed]
post_process=[post_process_gps  ,post_process_heading ,post_process_speed]
block=       [10                ,15                   ,10]


pre_process= [pre_process_gps]
post_process=[post_process_gps]
block=       [10              ]


output_file='summary_'+scenario[scenario_input]+'_output_recon.csv'

with open(output_file, 'w',0) as f_recon:
    #Output file
    writer_recon = csv.writer(f_recon,quoting=csv.QUOTE_NONNUMERIC)
    writer_recon.writerow(('file_id','npoints','sensor_id', 'sensor_name','value_name','process_id' ,'framesize','reduction', 
                           'tss_recon_min','tss_recon_max', 'values_recon_min','values_recon_max', 'values_recon.mean', 
                           'zlib_ratio', 'count_red','red_info_ratio',
                           'dev_max','dev_mean','dev_min',
                           'relerr','mape','smape','wpe','wape','rrmse','mae',
                           'file_name') )
    
    
    file_id=0 
    for file in filenamelist:    
        print "This is the current file",file
        D = DataBank()
        P = Plot(D)
	   
	   
        # Old data files
        #file = "./probes/innercity/28f21acc-3c8c-49fc-bf29-4acd40891bf0_1.csv"
        value_names=['lat','lon']
        sensor_data = get_ts_value_from_file(file, ',', 0, 2, 1, [3,4])
        
        S = D.add_sensor_data(sensor_data['pos']['values'], 
                              sensor_data['pos']['tss'], 0, "gps", "bmw_dataset_pos",value_names=value_names)
	   
        #Load GPS Data for UC3
        #value_names=['lat','lon']
        #sensor_data1 = tk_gdfr.get_ts_value_from_file(file, ',', 0, 2, 1, [3,4])
        #sensor_data1 = tk_gdfr.get_ts_value_from_file(file, ',', 'pos', 'pos', 0, [1,2])
        #S1 = D.add_sensor_data(sensor_data1['pos']['values'],
        #                       sensor_data1['pos']['tss'], 0, "gps", "bmw_dataset_pos",value_names=value_names)
        #S1 = D.add_sensor_data(sensor_data1['pos']['values'][1:],
        #      sensor_data1['pos']['tss'][1:], 0, "gps", "pos")
		    
        pre_process = {
            "multiple": [1000000, 1000000]
        }
        post_process = {
            "divide": [1000000.0, 1000000.0]
        }
		    
        #D.sensor_data_process(0, 100, 0.8, 'cii_dct_v011', pre_process, post_process, 5)
		    
        #D.write_sensor_data_files(0)
	   
	  

        #raw_data = tk_gdfr.get_ts_value_from_file("raw_data_0_0.csv", ';', '8', 'pos', 0, [1,2])
        #D.add_processed_sensor_data(0, raw_data['pos']['values'], raw_data['pos']['tss'])
    
        data_raw_tss = D.get_sensor_raw_data(0)['tss']
        data_raw = D.get_sensor_raw_data(0)['values']
    
        data_raw_tss = [x for x in data_raw_tss]
        data_raw = [x for x in data_raw]
   
    
        values = data_raw
        tss = data_raw_tss
    
       # print data_raw[0]
    
        from tk_plot import plot_hist_of_timestamp_intervals
        tss_plot = plot_hist_of_timestamp_intervals(tss)
        #plt.plot(tss_plot)
        #plt.show()
    
        #This is the correction scheme of missing GPS positions.
        orig_sample = 1000
  
        values_x_full = []
        values_y_full = []
        tss_full = []
        for i in range(len(values)-1):
            #print "This is the treshhold interval",tss_plot[i]
            if (tss_plot[i] - 1000 < 100) & (tss_plot[i] > 500):# & (i < 10):# & (abs(tss_plot[i]) > 100):# & (tss_plot[i+1] != 0):
                values_x_full.append(values[i][0])
                values_y_full.append(values[i][1])
                tss_full.append(tss[i])
                #print "This is the timestamps before",tss[i]
                #print "This is the difference before",tss_plot[i]
                #print "This is the difference ratio before",round(tss_plot[i]/float(orig_sample))
                ##print "This is the treshhold interval",tss_plot[i]
            if (tss_plot[i]-1000 > 100):# & (i < 10):
                #print "This is the difference",tss_plot[i]
                #print "This is the difference ratio",round(tss_plot[i]/float(orig_sample))
                #print "This is the treshhold interval above",tss[i]
                #print "This is the difference above",tss_plot[i]
                #print "This is the rounded data point",round(tss_plot[i]/orig_sample)
                data_range = range(0,int(round(tss_plot[i]/float(orig_sample))))
                #print "This is the difference above",data_range
                int_x_start = values[i][0]
                int_x_stop = values[i+1][0]
                int_y_start = values[i][1]
                int_y_stop = values[i+1][1]
                #print "These are the values",int_y_stop
                table_x = LinearInterpolation(
                    x_index=[0,int(round(tss_plot[i]/float(orig_sample)))],
                    values=[int_x_start,int_x_stop],
                    extrapolate=True)
                table_y = LinearInterpolation(
                    x_index=[0,int(round(tss_plot[i]/float(orig_sample)))],
                    values=[int_y_start,int_y_stop],
                    extrapolate=True)
                values_x_int = []
                values_y_int = []
                tss_int = []
                #print "This is the range",data_range
                n = 0
                for j in data_range:
                    values_x_int.append(table_x(j))
                    values_y_int.append(table_y(j))
                    tss_int.append(tss[i] + j*orig_sample)
                    #print "This is the interpolated timestamp",tss_int[n]
                    n = n + 1
                values_x_full.extend(values_x_int)
                values_y_full.extend(values_y_int)
                tss_full.extend(tss_int)
		  
        #plt.plot(values_x_full)
        #plt.show()
        #plt.plot(values_y_full)
        #plt.show()
    
        from tk_plot import plot_hist_of_timestamp_intervals
        tss_plot_int = plot_hist_of_timestamp_intervals(tss_full)
        #plt.plot(tss_plot_int)
        #plt.show()
    
        # Introduce the cleaned data here again
        data_raw_int = []
        for i in range(len(values_x_full)):
            data_raw_int.append([values_x_full[i],values_y_full[i]])
		  
		  
        # Implement the RDP scheme here
        S = D.add_sensor_data(data_raw_int, 
            tss_full, 2, "gps", "bmw_dataset_pos")
    
        #D.sensor_data_process(2, 100, 0.9, 'rdpi_v001', epsilon=0.000005)

        S = D.add_sensor_data(data_raw_int,
              tss_full, 15, "gps", "pos",value_names=value_names)
		    
        pre_process = {
            "multiple": [1000,1000],
    	       "integer": [1,1],
            "value_exceptions": [
                [
                    {
                        "e_name": "red_peak_points",
                        "e_id":  4,
                        "v_types": ["red"],
                        "condition": "(abs(delta_v)>2000)",
                        "para": "delta_v"
                    },
                    {
                        "e_name": "rest_peak_points",
                        "e_id":  5,
                        "v_types": ["rest"],
                        "condition": "(abs(delta_v)>2000)",
                        "para": "str(delta_v)+'_'+str(ts)+'_'+str(v)"
                    }
                ] 
            ]
        }
        post_process = {
            "divide": [1000.0,1000.0]
        }
        D.sensor_data_process(15, 500, 0.8, 'cii_dct_v011', pre_process, post_process, 5)
    
        #print "This is the raw data",data_raw
        #print "This is the filled in data",values_x_full
    
        # Insert here the cleaned data and process the new heading locations
        data_raw = data_raw_int
    
        init1 = LatLon(Latitude(data_raw[0][0]), Longitude(data_raw[0][1]))
        init2 = LatLon(Latitude(data_raw[1][0]), Longitude(data_raw[1][1]))
        heading = init1.heading_initial(init2)
        #dist = init1.distance(init2)
        #init_pos = init1.offset(heading, dist)
        #start_pos = [float(str(init_pos).split(',')[0]),float(str(init_pos).split(',')[1])]
        #print start_pos
        heading_tot = []
        heading_red = []
        dist_tot = []
        pos_rec = []
        pos_orig = []
        n = 0
        for i in range(len(data_raw)-1):
            pos1 = LatLon(Latitude(data_raw[i][0]), Longitude(data_raw[i][1])) # Try instantiating Latitude and Longitude objects in call
            pos2 = LatLon(Latitude(data_raw[i+1][0]), Longitude(data_raw[i+1][1])) # Try instantiating Latitude and Longitude objects in call
            heading = pos1.heading_initial(pos2) # Initial heading to Honolulu on WGS84 ellipsoid
            pos_orig.append(pos1)
            #print "This is the first solution for the end",heading
            dist = pos1.distance(pos2) # FAI distance is 1774.77188181 km
    	   # Implement the delta transmission scheme for the heading for the heading
            #if i > 1:
                #print i
                #print len(heading_tot)
            #if (heading - heading_tot[-1] == 0):
            #heading_tot.append(heading) 
            if (i > 1):
    	       if (heading - heading_tot[-1] == 0):
    	           heading_red.append(heading)
            heading_tot.append(heading)
            n = n + 1
            dist_tot.append(dist)
            pos_rec_tmp = pos1.offset(heading, dist) # Reconstruct lat/lon for Honolulu based on offset from Palmyra
            pos_rec.append(pos_rec_tmp)

        #print "This is the reduction in the heading",len(heading_red)/float(len(heading_tot))
	   
        
	   
        #plt.plot(heading_tot)
        #plt.plot(dist_tot)
        #plt.show()
    

        #print "*" * 50
    
        gps_steps = D.get_gps_steps(15, 0)
        #gps_deviation = D.get_gps_deviations(15, 0)
        #print "edge distance : " + str(gps_steps['pe_deviations'])
        #print "point distance : " + str(gps_steps['pp_deviations'])
        #print "wgs point distance : " + str(gps_steps['wgs84_pp_deviations'])
        #max_index = gps_deviation['pe_deviations']['max_d_index']
    
    
   
    
        #fig = plt.figure()
        #ax = fig.add_subplot(111)
        #tk_plot.plot_boxplot_residuals(ax, gps_steps['wgs84_pp_distances'], np.zeros(len(gps_steps['wgs84_pp_distances'])))
        #plt.show()
    
        gps_steps['wgs84_pp_distances'] = [[int(x*100)/100.0] for x in gps_steps['wgs84_pp_distances']]
    
        #print "These are the steps",gps_steps['wgs84_pp_distances']
    
    
    
        # Process the step to step distance data (velocity in m/s as the sampling time is s)
        S = D.add_sensor_data(gps_steps['wgs84_pp_distances'], 
            tss_full, 9, "gps", "bmw_dataset_pos")
        pre_process = {
            "multiple": [1000],
    	   "integer": [1],
            "value_exceptions": [
                [
                    {
                        "e_name": "red_peak_points",
                        "e_id":  4,
                        "v_types": ["red"],
                        "condition": "(abs(delta_v)>2000)",
                        "para": "delta_v"
                    },
                    {
                        "e_name": "rest_peak_points",
                        "e_id":  5,
                        "v_types": ["rest"],
                        "condition": "(abs(delta_v)>2000)",
                        "para": "str(delta_v)+'_'+str(ts)+'_'+str(v)"
                    }
                ] 
            ]
        }
        post_process = {
            "divide": [1000.0]
        }
        D.sensor_data_process(9, 500, 0.8, 'cii_dct_v011', pre_process, post_process, 15)
        #print "This is the reduction info",D.get_sensor_process_info(9, 0)['reduce_info']
    
    
        raw_gps_data_size = D.get_sensor_process_info(15,0)['reduce_info']['len_raw']
        red_heading_data_size = len(heading_red)
        red_distance_data_size = D.get_sensor_process_info(9,0)['reduce_info']['len_red']
        reduce_ratio = 1.0 * (red_heading_data_size + red_distance_data_size) / raw_gps_data_size
        #print("This is the overall reduction",reduce_ratio, raw_gps_data_size, red_heading_data_size, red_distance_data_size)
   
        #P.plot(9,process_id=0,deviation='difference')
        #D.write_sensor_data_files(9)
    
        data_raw_dist = D.get_sensor_raw_data(9)['values']
        data_rec_dist = D.get_sensor_recon_data(9,0)['values']
        data_raw_dist = [x[0] for x in data_raw_dist]
        data_rec_dist = [x[0] for x in data_rec_dist]
    
        #print data_rec_dist
    
        #plt.plot(gps_steps['wgs84_pp_distances'])
        #plt.plot(dist_tot)
        #plt.xlabel("These are the steps")
        #plt.show()
        #print "This is the new distances calculated",data_raw_dist
        pos_rec = []
        pos_tk_rec = []
        pos_orig_format = []
        pos_rec_format = [data_raw[0]]
        pos_tk_rec_format = [data_raw[0]]
        for i in range(len(data_rec_dist)-1):
            #pos1 = LatLon(Latitude(data_raw[i][0]), Longitude(data_raw[i][1])) # Try instantiating Latitude and Longitude objects in call
            #pos2 = LatLon(Latitude(data_raw[i+1][0]), Longitude(data_raw[i+1][1])) # Try instantiating Latitude and Longitude objects in call
            #heading = pos1.heading_initial(pos2) # Initial heading to Honolulu on WGS84 ellipsoid
            #print "This is the first solution for the end",heading
            #dist = pos1.distance(pos2, ellipse = 'sphere') # FAI distance is 1774.77188181 km
            #heading_tot.append(heading) 
            #dist_tot.append(dist*1000)
            #data_rec_dist[i] = LatLon(Latitude(data_rec_dist[i][0]), Longitude(data_rec_dist[i][1])) 
            pos_rec_tmp = pos_orig[i].offset(heading_tot[i], data_raw_dist[i]/1000) 
            pos_tk_rec_tmp = pos_orig[i].offset(heading_tot[i], data_rec_dist[i]/1000) 
            pos_rec.append(pos_rec_tmp)
            pos_tk_rec.append(pos_tk_rec_tmp)
            #print "These are the original reconstructed positions",pos_rec_tmp.to_string('D')
            #print "These are the Teraki reconstructed positions",pos_tk_rec_tmp.to_string('D')
            pos_orig_format.append([float(str(pos_orig[i]).split(',')[0]),float(str(pos_orig[i]).split(',')[1])])
            pos_rec_format.append([float(str(pos_rec[i]).split(',')[0]),float(str(pos_rec[i]).split(',')[1])])
            pos_tk_rec_format.append([float(str(pos_tk_rec[i]).split(',')[0]),float(str(pos_tk_rec[i]).split(',')[1])])
            #print "These are the original positions",[float(str(pos_orig[i]).split(',')[0]),float(str(pos_orig[i]).split(',')[1])]
            #print "These are the original reconstructed positions",[float(str(pos_rec[i]).split(',')[0]),float(str(pos_rec[i]).split(',')[1])]
            #print "These are the Teraki reconstructed positions",[float(str(pos_tk_rec[i]).split(',')[0]),float(str(pos_tk_rec[i]).split(',')[1])]
		  
        sensor_values_clean = []
        sensor_tss_clean = []
    
        for i in range(len(pos_tk_rec_format)):
            sensor_values_clean.append(data_raw_int[i])
            sensor_tss_clean.append(tss_full[i])
        #print len(sensor_values_clean)
        #print len(pos_tk_rec_format)
        # Include the recovered data into databank
        S = D.add_sensor_data(sensor_values_clean, 
            sensor_tss_clean, 1, "gps", "bmw_dataset_pos")
    
        #D.add_processed_sensor_data(1, pos_tk_rec_format, sensor_tss_clean)# Recon data from sensor_id 8
    
        #gps_Recovered_deviation = D.get_gps_deviations(1, 0)
    
        
        #Load Heading Data                                                                                                                                                                                      
        #sensor_data2 = tk_gdfr.get_ts_value_from_file(file, ',', 0, 2, 1, [5])
        #S2 = D.add_sensor_data(sensor_data2['pos']['values'],
        #                       sensor_data2['pos']['tss'], 1, "heading", "bmw_dataset_heading")
        
        ##Load Speed Data                                                                                                                                                                                        
        
	   #sensor_data3 = tk_gdfr.get_ts_value_from_file(file, ',', 0, 2, 1, [6])
        #S3 = D.add_sensor_data(sensor_data3['pos']['values'],
        #                       sensor_data3['pos']['tss'], 2, "speed", "bmw_dataset_speed")
        
        process_id=0
        for item in process_cases:        
            framesize = item[0]
            reduction = 0.9
            #if sensor['sensor_id'] == 14:
            for sensor in D.get_sensor_list():
                if (sensor['sensor_id'] not in [1,2]):# | (sensor['sensor_id'] != 2):
				continue
                #print "This is the sensor",sensor
                index=sensor['index']
                dof=sensor['dof']
                sensor_id=sensor['sensor_id']
                #print "The sensor id",sensor_id
                sensor_name=sensor['sensor_name']
                
                #Raw data info
                raw_data=D.get_sensor_raw_data(sensor_id)
                tss=np.asarray(raw_data['tss'])
                values=np.asarray(raw_data['values'])
                npoints=raw_data['count']
                if npoints<framesize:
                    break
                
                ##Process Data
                #D.sensor_data_process(sensor_id, framesize, reduction,'cii_dct_v011', pre_process=pre_process[sensor_id], post_process=post_process[sensor_id],block=block[sensor_id])
                if (sensor['sensor_id'] == 1):
                    meta = {'reduce_info': {'ratio': reduce_ratio}}
                    print "This happens here",D.add_processed_sensor_data(sensor_id, pos_tk_rec_format, sensor_tss_clean,meta=meta)# Recon data from sensor_id 8
                if (sensor['sensor_id'] == 2):
                    if npoints<100: ##I had to add this since for small npoints the next line will raise a problem
                        continue     
                    print "process id",D.sensor_data_process(sensor_id, 100, reduction , 'rdpi_v001', epsilon=0.00001)
                #P.plot(sensor_id,process_id=0,deviation='difference')
	           #gps_Recovered_deviation = D.get_gps_deviations(1, 0)
                
                ##Plots
                #P=Plot(D)
                #P.plot(sensor_id,process_id,deviation='difference',save_png=True) 
            
                #Recon data info
                #print "This is the current sensor id",sensor['sensor_id']
                recon_data=D.get_sensor_recon_data(sensor_id,process_id)
                tss_recon=np.asarray(recon_data['tss'])
                values_recon=np.asarray(recon_data['values'])
                zlib_ratio=D.get_zip_reduction_ratio(sensor_id,framesize)
            
                #Reduce info
                reduce_info=D.get_sensor_process_info(sensor_id, process_id)['reduce_info']
                count_red = red_heading_data_size + red_distance_data_size#reduce_info['count_red']
                red_info_ratio=reduce_ratio #  This is the updated calculation for the reduction
            
                #Deviation_info
                deviations=D.get_deviations(sensor_id,process_id)
                
                for i in range(dof):
                    value_name=sensor['value_names'][i]
                    values_comp=values_recon.T[i]
                    dev_max=deviations['E'][i]['max']
                    dev_mean=deviations['E'][i]['mean']
                    dev_min=deviations['E'][i]['min']
                    
                    relerr=deviations['relerr'][i]
                    mae=deviations['MAE'][i]
                    mape=deviations['MAPE%'][i]
                    smape=deviations['SMAPE%'][i]
                    rrmse=deviations['RRMSE%'][i]
                    wape=deviations['WAPE%'][i]
                    wpe=deviations['WPE%'][i]
                    
                    
                    
                    #Write output
                    writer_recon.writerow((file_id,npoints,sensor_id,sensor_name, value_name,process_id ,framesize,reduction, 
                                   tss_recon.min(),tss_recon.max(), float("%.3f"%values_comp.min()), float("%.3f"%values_comp.max()), float("%.3f"%values_comp.mean()), 
                                   float("%.3f"%zlib_ratio), count_red,float("%.3f"%red_info_ratio), 
                                   dev_max,dev_mean,dev_min,
                                   relerr,mape,smape,wpe,wape,rrmse,mae,
                                   file))
            
            process_id=process_id+1
        print '** Goes in file: ',file_id,''
        ##
        name='output_file_id_'+scenario[scenario_input]+'file_id_'+str(file_id)+'.dbk'
        D.save(name)
        ##
        file_id=file_id+1
            
