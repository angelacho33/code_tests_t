"""
Project Teraki Data Process APIs
Author: qingzhou

it provides the APIs for raw_data file, tka data file, and other files.
"""

import os, sys, time
from tk_mvsd import SensorData

def data_m_n_convert(data):
    """Convert list of list data 
    
    Args:
        list of data in following data format:
        [
            [V00, V01, V02, V03,....V0n],
            [V10, V11, V12, V13,....V0n],
            [V20, V21, V22, V23,....V0n],
            ......
            [Vm0, Vm1, Vm2, Vm3,....Vmn]
        ]
    
    Returns:
        list of data in following data format:
        [
            [V00, V10, V20, V30,....Vm0],
            [V01, V11, V21, V31,....Vm1],
            [V02, V12, V22, V32,....Vm2],
            ......
            [V0n, V1n, V2n, V3n,....Vmn]
        ]
    """
    result = []
    for i in range(len(data[0])):
        value_i = [x[i] for x in data]
        result.append(value_i)
    return result

def transform_lonlat_to_utm(lonlat):
    """Convert lon/lat (in degrees) to x/y in native projection coordinates (meters)
    
    Args:
        lonlat (list of tuples): List of datapoints where each datapoint is a 
            tuple of longitude and latitude (lon, lat).
    
    Returns:
        xy (list of tuples): List of transformed datapoints (x, y) in UTM system.
    """
    import pyproj
    def grid_zone(lon, lat):
        """Find UTM zone and MGRS band for latitude and longitude.
        
        Arg:
            lat (float): latitude in degrees.
            lon (float): longitude in degrees

        Returns:
            (zone, band) (tuple).
        
        Raises:
            Exception ValueError if lon not in [-180..180] or if lat
            is larger then 84 or smaller then -80 (pole).
        
        Note: 
            Polar zone (A, B, Y, Z) not handled.
        """
        if -180.0 > lon or lon > 180.0:
            raise ValueError('Invalid longitude: ' + str(lon))
        zone = int((lon + 180.0)//6.0) + 1
        band = ' '
        if    84 >= lat and lat >= 72: band = 'X'
        elif  72 > lat and lat >= 64:  band = 'W'
        elif  64 > lat and lat >= 56:  band = 'V'
        elif  56 > lat and lat >= 48:  band = 'U'
        elif  48 > lat and lat >= 40:  band = 'T'
        elif  40 > lat and lat >= 32:  band = 'S'
        elif  32 > lat and lat >= 24:  band = 'R'
        elif  24 > lat and lat >= 16:  band = 'Q'
        elif  16 > lat and lat >= 8:   band = 'P'
        elif   8 > lat and lat >= 0:   band = 'N'
        elif   0 > lat and lat >= -8:  band = 'M'
        elif  -8 > lat and lat >= -16: band = 'L'
        elif -16 > lat and lat >= -24: band = 'K'
        elif -24 > lat and lat >= -32: band = 'J'
        elif -32 > lat and lat >= -40: band = 'H'
        elif -40 > lat and lat >= -48: band = 'G'
        elif -48 > lat and lat >= -56: band = 'F'
        elif -56 > lat and lat >= -64: band = 'E'
        elif -64 > lat and lat >= -72: band = 'D'
        elif -72 > lat and lat >= -80: band = 'C'
        else: raise ValueError('latitude out of UTM range: ' + str(lat))
        return (zone, band)

    # Derive zones contained in the list of points
    zones_bands = []
    for lon, lat in lonlat:
        zone, band = grid_zone(lon, lat)
        zones_bands.append([zone, band])    
    unique_sets = [list(x) for x in set(tuple(x) for x in zones_bands)]
    zones = zip(*zones_bands)[0]       # List with same length as datapoints
    
    # create an instance for each of the projections contained in the list of points
    UTMs = {}
    for zone, band in unique_sets:        
        UTMs[zone] = pyproj.Proj("+proj=utm +zone="+str(zone)+" +north +ellps=WGS84 +datum=WGS84 +units=m +no_defs")
    #UTM = pyproj.Proj("+proj=utm +zone="+str(zone)+" +north +ellps=WGS84 +datum=WGS84 +units=m +no_defs")
    #UTM = pyproj.Proj("+init=EPSG:32632") # UTM coords, zone 32U, WGS84 datum

    xy = []
    for (lon, lat), zone in zip(lonlat, zones):
        UTM = UTMs[zone]
        x, y = UTM(lon, lat)
        xy.append((x, y))
    return xy

class DataBank():
    """Class for offline Data Processing
    Args:
        None 

    Returns:
        None
    
    Attributes:
        _sensors: the list of all the sensors in the databank
        _input_files: the parameter for reading the file
        _sensor_data: the dictionary for the reduced data
        _current_sd: the dictionary for the reconstructed data

        get_sensor_list: get the list of sensors in the DataBank
        get_sensor_object: get SensorData Instance
        read_sensor_file: read sensor data from a file to DataBank
        add_sensor_data: add sensor data to DataBank
        write_sensor_file: write sensor data to a file
        process_sensor_data: process sensor data
        read_file(): read data from one file
        read_files(): read data from list of files
        encoding_file(): encoding raw data file to reduced data file
        decoding_file(): decoding reduced data file to reconstructed data file 
        write_file(): write data to one file
        data_process(): process files based on the config parameters

    Examples:
        To use it via command line:
            $ python tk_databank.py

        To use in your own code to read data from file:

        >>> from tk_databank import DataBank
        >>> D = DataBank()
        >>> S = D.read_sensor_file("edison_gyro.csv", 2, "edison", "gyro")

        To Add data to the DataBank:

        >>> values = [[x, x+x, x*x] for x in range(10000)]
        >>> tss = [1700000000 + x*50 for x in range(10000)]
        >>> S = D.add_sensor_data(values, tss, 9, "test_sensor", "random")

        To get the list of sensors in DataBank:

        >>> D.get_sensor_list()

        To process the data:

        >>> D.sensor_data_process(2, 100, 0.7)
        >>> D.sensor_data_process(2, 200, 0.8)
        >>> D.sensor_data_process(2, 500, 0.8)

        To get the dict of sensor_info:

        >>> D.get_sensor_info(2)

        To get the list of process parameters:

        >>> D.get_sensor_process_info(2)
        >>> D.get_sensor_process_info(2, 0)

        To get deviation of each data process:

        >>> D.get_deviations(2, 0)
        >>> D.get_deviations(2, 1)
        >>> D.get_deviations(2, 2)

        To get raw, red and recon data:

        >>> D.get_sensor_raw_data(2)
        >>> D.get_sensor_red_data(2, 0)
        >>> D.get_sensor_recon_data(2, 0)
        >>> D.get_sensor_red_data(2, 1)
        >>> D.get_sensor_recon_data(2, 1)

        To write data into file:

        >>> D.write_sensor_data_files(2)

        To save all data into file:

        >>> D.save("test.dbk")

        To load data from a previous saved file:

        >>> E = DataBank()
        >>> E.load("test.dbk")

    """
    def __init__(self, config_para=None):
        """ 
        initiate DataBank instance

        Args: 
            config_para: configuration for the DataBank

        Returns: 
            None
        """ 
        self._sensors = []
        self._input_files = []
        self._sensor_data = []
        self._current_sd = None
        if config_para is not None:
            self.data_process(config_para)

    def save(self, filename):
        """ 
        API for saving the Data into a file

        Args: 
            filename: the filename of the file created for the data

        Returns: 
            None
        """ 
        import cPickle
        fp = open(filename, 'wb')
        cPickle.dump(self, fp)

    def load(self, filename):
        """ 
        API for loading the Data from a file

        Args: 
            filename: the filename of the file to load the data

        Returns: 
            None
        """ 
        import cPickle
        fp = open(filename, 'rb')
        D = cPickle.load(fp)
        self._sensors = D._sensors
        self._input_files = D._input_files
        self._sensor_data = D._sensor_data
        self._current_sd = D._current_sd

    def get_sensor_list(self):
        """ 
        API for get list of the sensors

        Args: 
            None

        Returns: 
            dict with the list of the sensors
        """ 
        return self._sensors

    def get_sensor_info(self, sensor_id):
        """ 
        API for get sensor_info

        Args: 
            sensor_id: sensor id

        Returns: 
            dict of sensor info, None if sensor_id does not exist
        """ 
        for item in self._sensors:
        	if item['sensor_id'] == sensor_id:
        		return item
        return None

    def get_sensor_process_info(self, sensor_id, process_id=None):
        """ 
        API for get sensor_info

        Args: 
            sensor_id: sensor id

        Returns: 
            dict of sensor info, None if sensor_id does not exist
        """ 
        for item in self._sensors:
            if item['sensor_id'] == sensor_id:
                if process_id is None:
        	        return item['processed_data']
                elif process_id < len(item['processed_data']):
                    return item['processed_data'][process_id]
                else:
                    return item['processed_data']
        return None

    def get_sensor_object(self, sensor_id, process_id=None):
        """ 
        API for get a SensorData Instance

        Args: 
            sensor_id : Sensor ID
            process_id : process id. return Raw Sensor data if process_id is None

        Returns: 
            SensorData Instance
        """ 
        sensor_info = self.get_sensor_info(sensor_id)
        if sensor_info is None:
        	SD = None
        elif process_id is None:
        	SD = sensor_info['sensor_data']
        elif len(sensor_info['processed_data']) <= process_id:
        	SD = None
        else:
            SD = sensor_info['processed_data'][process_id]['sensor_data']          
        return SD

    def load_dataset(self, dataset, sort=False, clean=False):
        """ 
        API for read data from files

        Args: 
            dataset : a list of dict with datafile information
            sort: Flag to sort the data when load the dataset.
            clean: Flag to clean the duplicated data when load the dataset

        Returns: 
            self._sensors: the list of current sensors
        """ 
        for item in dataset:
            sensor_id = item['sensor_id']
            path = item['file']
            sensor_name = None
            sensor_type = None
            value_names = None
            quantity = None
            unit = None
            if 'sensor_name' in item.keys():
                sensor_name = item['sensor_name']
            if 'sensor_type' in item.keys():
                sensnor_type = item['sensor_type']
            if 'value_names' in item.keys():
                if isinstance(item['value_names'], list):
                    value_names = item['value_names']
                else:
                    value_names = [item['value_names']]
            if 'quantity' in item.keys():
                if isinstance(item['quantity'], list):
                    value_names = item['quantity']
                else:
                    value_names = [item['quantity']]
            if 'unit' in item.keys():
                if isinstance(item['unit'], list):
                    value_names = item['unit']
                else:
                    value_names = [item['unit']]
            if 'sort' in item.keys():
                sort = item['sort']
            if 'clean' in item.keys():
                clean = item['clean']
            self.read_sensor_file(path, sensor_id, sensor_name, sensor_type, value_names, quantity, unit, sort, clean)
        return self._sensors

    def sensors_batch_process(self, sensor_ids, process_profiles):
        """ 
        API for process multiple sensors with multiple process profiles

        Args: 
            sensor_ids : a list of sensor_id
            process_profiles: a list of process profile

        Returns: 
            None
        """ 
        for sensor_id in  sensor_ids:
            for item in process_profiles:
                framesize = 500
                reduction = 0.9
                eng_name = 'aii_v010'
                block = 5
                pre_process = {}
                post_process = {}
                epsilon = None
                batch = 0
                if 'framesize' in item.keys():
                    framesize = (int)(item['framesize'])
                if 'reduction' in item.keys():
                    reduction = (float)(item['reduction'])
                if 'eng_name' in item.keys():
                    eng_name = item['eng_name']
                if 'block' in item.keys():
                    block = (int)(item['block'])
                if 'pre_process' in item.keys():
                    pre_process = item['pre_process']
                if 'post_process' in item.keys():
                    post_process = item['post_process']
                if 'epsilon' in item.keys():
                    epsilon = item['epsilon']
                if 'batch' in item.keys():
                	batch = item['batch']
                self.sensor_data_process(sensor_id, framesize, reduction, eng_name, pre_process, post_process, block, epsilon, batch)
            self.write_sensor_data_files(sensor_id)

    def process_dataset(self, dataset, process_profiles):
        """ 
        API for load and process a dataset in multiple process profiles

        Args: 
            dataset : a list of dict with datafile information
            process_profiles: a list of process profile

        Returns: 
            None
        """ 
        self.load_dataset(dataset)
        sensor_ids = [item['sensor_id'] for item in dataset]
        self.sensors_batch_process(sensor_ids, process_profiles)
        return

    def read_sensor_file(self, path, sensor_id=None, sensor_name=None, sensor_type=None, value_names=None, quantity=None, unit=None, sort=False, clean=False):
        """ 
        API for read data from a file

        Args: 
            path:  filename.
            sensnor_id:  sensor id.
            sensor_name: sensor name.
            sensor_type: sensor type.
            value_names: the value names for plot
            quantity: the quantity for plot
            unit: the unit of the value
            sort: Flag to sort the data when load the dataset.
            clean: Flag to clean the duplicated data when load the dataset

        Returns: 
            SensorData Instance
        """
        if self.get_sensor_info(sensor_id) is not None:
        	print("Duplicated sensor id : " + str(sensor_id))
        	return None
        SD = SensorData()
        f = open(path, 'r')
        for line in f:
            text = line.rstrip()
            if len(text) < 1:
                ret = 0
            elif text[0] == '#':
                ret = SD.read_headline(text)
            else:
                ret = SD.read_dataline(text)
            if ret < 0:
                print("Error when reading line " + str(current_line) + " in file " + path)
                f.close()
                exit(1)
        f.close()
        if sort:
            SD.sort_raw_data()
        if clean:
            SD.clean_duplicates()
        SD.raw_data_process()
        sensor_info = {}
        sensor_info['index'] = len(self._sensors)
        sensor_info['path'] = path
        sensor_info['sensor_id'] = sensor_id
        sensor_info['sensor_name'] = sensor_name
        sensor_info['sensor_type'] = sensor_type
        sensor_info['dof'] = SD._raw_data['dof']
        sensor_info['sensor_data'] = SD
        sensor_info['processed_data'] = []
        if value_names is None:
        	sensor_info['value_names'] = [('value_' + str(x)) for x in range(sensor_info['dof'])]
        else:
        	sensor_info['value_names'] = value_names
        if quantity is None:
        	sensor_info['quantity'] = [''] * sensor_info['dof']
        else:
        	sensor_info['quantity'] = quantity
        if unit is None:
            sensor_info['unit'] = [''] * sensor_info['dof']
        else:
            sensor_info['unit'] = unit
        self._sensors.append(sensor_info)
        return sensor_info['sensor_data']

    def add_sensor_data(self, values, tss=[], sensor_id=None, sensor_name=None, sensor_type=None, value_names=None, quantity=None, unit=None, sort=False, clean=False):
        """ 
        API for add data into DataBank class

        Args: 
            values: a list of values with data format: 
                    [ [value_x[, value_y...]], [value_x[, value_y]...]
            tss: a list of timestamps with data format: [ts1, ts2....]
            sensnor_id:  sensor id
            sensor_name: sensor name
            sensor_type: sensor type
            value_names: the value names for plot
            quantity: the quantity for plot
            unit: the unit of the value
            sort: Flag to sort the data when load the dataset.
            clean: Flag to clean the duplicated data when load the dataset

        Returns: 
            SensorData Instance
        """
        if self.get_sensor_info(sensor_id) is not None:
        	print("Duplicated sensor id : " + str(sensor_id))
        	return None
        raw_data = {}
        raw_data['dof'] = len(values[0])
        raw_data['count'] = len(values)
        raw_data['values'] = [x for x in values]
        if tss is None:
            raw_data['tss'] = self.get_default_tss(len(values))
        else:
            raw_data['tss'] = [ts for ts in tss]
        SD = SensorData()
        SD._raw_data = raw_data
        if sort:
            SD.sort_raw_data()
        if clean:
            SD.clean_duplicates()
        SD.raw_data_process()
        sensor_info = {}
        sensor_info['index'] = len(self._sensors)
        sensor_info['path'] = None
        sensor_info['sensor_id'] = sensor_id
        sensor_info['sensor_name'] = sensor_name
        sensor_info['sensor_type'] = sensor_type
        sensor_info['sensor_data'] = SD
        sensor_info['processed_data'] = []
        sensor_info['dof'] = raw_data['dof']
        if value_names is None:
        	sensor_info['value_names'] = [('value_' + str(x)) for x in range(sensor_info['dof'])]
        else:
        	sensor_info['value_names'] = value_names
        if quantity is None:
        	sensor_info['quantity'] = [''] * sensor_info['dof']
        else:
        	sensor_info['quantity'] = quantity
        if unit is None:
            sensor_info['unit'] = [''] * sensor_info['dof']
        else:
            sensor_info['unit'] = unit
        self._sensors.append(sensor_info)
        return sensor_info['sensor_data']

    def get_current_timestamp(self):
        import time, datetime
        """ get current timestamp of the data"""
        timestamp = datetime.datetime.utcnow()
        timestamp = int(time.mktime(timestamp.timetuple()) * 1000 + timestamp.microsecond / 1000)
        return timestamp

    def get_default_tss(self, length):
        ts0 = self.get_current_timestamp() - length*1000
        tss = [(ts0+i*1000) for i in range(length)]
        return tss

    def write_sensor_data_files(self, sensor_id):
        sensor_info = self.get_sensor_info(sensor_id)
        if sensor_info is None:
        	print("Sensor id " + str(sensor_id) + " does not exist!")
        	return None
        SD = sensor_info['sensor_data']
        content = SD.get_raw_content()
        filename = 'raw_data_' + str(sensor_id) + '.csv'
        f = open(filename, 'w')
        f.write(content)
        f.close()

        for item in sensor_info['processed_data']:
            process_id = item['process_id']
            SD = item['sensor_data']

            content = SD.get_red_content()
            filename = 'red_data_' + str(sensor_id) + '_' + str(process_id) + '.csv'
            f = open(filename, 'w')
            f.write(content)
            f.close()

            content = SD.get_recon_content()
            filename = 'recon_data_' + str(sensor_id) + '_' + str(process_id) + '.csv'
            f = open(filename, 'w')
            f.write(content)
            f.close()

            content = SD.get_red_delta_content()
            filename = 'red_delta_' + str(sensor_id) + '_' + str(process_id) + '.csv'
            f = open(filename, 'w')
            f.write(content)
            f.close()
        return

    def get_sensor_raw_data(self, sensor_id):
        """ 
        API for get sensor raw data

        Args: 
            sensor_id: Sensor ID

        Returns: 
            A dict with following information:
            sensnor_id:  sensor id
            sensor_name: sensor name
            sensor_type: sensor type
            dof:  DOF
            tss: a list of timestamps with data format: [ts1, ts2....]
            values: a list of values with data format: 
                    [ [value_x[, value_y...]], [value_x[, value_y]...]
        """
        sensor_info = self.get_sensor_info(sensor_id)
        if sensor_info is None:
        	print("Sensor id " + str(sensor_id) + " does not exist!")
        	return None
        SD = sensor_info['sensor_data']
        content = SD.get_raw_data()
        content['path'] = sensor_info['path']
        content['sensor_id'] = sensor_info['sensor_id']
        content['sensor_name'] = sensor_info['sensor_name']
        content['sensor_type'] = sensor_info['sensor_type']
        content['value_names'] = sensor_info['value_names']
        content['quantity'] = sensor_info['quantity']
        content['unit'] = sensor_info['unit']
        content['series_type'] = 'raw'
        return content

    def get_sensor_red_data(self, sensor_id, process_id):
        """ 
        API for get sensor red data

        Args: 
            sensor_id: Sensor ID
            process_id: the index of processed data

        Returns: 
            A dict with following information:
            sensnor_id:  sensor id
            sensor_name: sensor name
            sensor_type: sensor type
            dof:  DOF
            tss: a list of timestamps with data format: [ts1, ts2....]
            values: a list of values with data format: 
                    [ [value_x[, value_y...]], [value_x[, value_y]...]
        """
        sensor_info = self.get_sensor_info(sensor_id)
        if sensor_info is None:
        	print("Sensor id " + str(sensor_id) + " does not exist!")
        	return None
        if len(sensor_info['processed_data']) <= process_id:
            print("Process id " + str(process_id) + " out of range! ")
            return None
        SD = sensor_info['processed_data'][process_id]['sensor_data']
        content = SD.get_red_data()
        content['path'] = sensor_info['path']
        content['sensor_id'] = sensor_info['sensor_id']
        content['sensor_name'] = sensor_info['sensor_name']
        content['sensor_type'] = sensor_info['sensor_type']
        content['value_names'] = sensor_info['value_names']
        content['quantity'] = sensor_info['quantity']
        content['unit'] = sensor_info['unit']
        content['series_type'] = 'red'
        content['process_id'] = process_id
        content['framesize'] = sensor_info['processed_data'][process_id]['framesize']
        content['reduction'] = sensor_info['processed_data'][process_id]['reduction']
        content['sparsity'] = sensor_info['processed_data'][process_id]['sparsity']
        content['eng_name'] = sensor_info['processed_data'][process_id]['eng_name']
        content['pre_process'] = sensor_info['processed_data'][process_id]['pre_process']
        content['post_process'] = sensor_info['processed_data'][process_id]['post_process']
        return content

    def get_sensor_recon_data(self, sensor_id, process_id):
        """ 
        API for get sensor recon data

        Args: 
            sensor_id: Sensor ID
            process_id: the index of processed data

        Returns: 
            A dict with following information:
            sensnor_id:  sensor id
            sensor_name: sensor name
            sensor_type: sensor type
            dof:  DOF
            tss: a list of timestamps with data format: [ts1, ts2....]
            values: a list of values with data format: 
                    [ [value_x[, value_y...]], [value_x[, value_y]...]
        """
        sensor_info = self.get_sensor_info(sensor_id)
        if sensor_info is None:
        	print("Sensor id " + str(sensor_id) + " does not exist!")
        	return None
        if len(sensor_info['processed_data']) <= process_id:
            print("Process id " + str(process_id) + " out of range! ")
            return None
        SD = sensor_info['processed_data'][process_id]['sensor_data']
        content = SD.get_recon_data()
        content['path'] = sensor_info['path']
        content['sensor_id'] = sensor_info['sensor_id']
        content['sensor_name'] = sensor_info['sensor_name']
        content['sensor_type'] = sensor_info['sensor_type']
        content['value_names'] = sensor_info['value_names']
        content['quantity'] = sensor_info['quantity']
        content['unit'] = sensor_info['unit']
        content['series_type'] = 'recon'
        content['process_id'] = process_id
        content['framesize'] = sensor_info['processed_data'][process_id]['framesize']
        content['reduction'] = sensor_info['processed_data'][process_id]['reduction']
        content['sparsity'] = sensor_info['processed_data'][process_id]['sparsity']
        content['eng_name'] = sensor_info['processed_data'][process_id]['eng_name']
        content['pre_process'] = sensor_info['processed_data'][process_id]['pre_process']
        content['post_process'] = sensor_info['processed_data'][process_id]['post_process']
        return content

    def get_sensor_exp_data(self, sensor_id, process_id, dof_id=None):
        """ 
        API for get sensor exception data

        Args: 
            sensor_id: Sensor ID
            process_id: the index of processed data
            dof_id: the dof_id if there is multiple dof

        Returns: 
            A list of exception data.
            If dof_id is None, the results are structed as following:
            [timestamp, dof_id, exp_value]
            If dof_id is specified, there results are structed as following:
            [timestamp, exp_value]
        """
        result = []
        SD = self.get_sensor_object(sensor_id, process_id)
        if SD is None: 
        	return result
        if SD._exp_data is None:
        	return result
        if dof_id is None:
        	return SD._exp_data
        for item in SD._exp_data:
        	if item[1] == dof_id:
        		result.append([item[0], item[2]])
        return result

    def get_deviations(self, sensor_id=None, process_id=0):
        sensor_info = self.get_sensor_info(sensor_id)
        if sensor_info is None:
            print("Sensor id " + str(sensor_id) + " does not exist!")
            return None
        if len(sensor_info['processed_data']) <= process_id:
            print("Process id " + str(process_id) + " out of range! ")
            return None
        return sensor_info['processed_data'][process_id]['deviations']

    def calculate_deviations(self, sensor_id=None, process_id=0):
        import tk_err as tkerr
        import itertools
        import numpy as np
        sensor_info = self.get_sensor_info(sensor_id)
        if sensor_info is None:
            print("Sensor id " + str(sensor_id) + " does not exist!")
            return None
        if len(sensor_info['processed_data']) <= process_id:
            print("Process id " + str(process_id) + " out of range! ")
            return None

        deviations = {'sensor_id': [], 
            'series_type1': [],
            'series_type2': [],
            'values_component': [],
            'E': [],
            'MAE': [],
            'MAPE%': [],
            'SMAPE%': [],
            'RRMSE%': [],
            'WAPE%' :[],
            'relerr': [],
            'WPE%': [],
            'engine': [],
            'sparsity': [],
            'framesize': [],
            'reduction': []
        }
        sensor_name = sensor_info['sensor_name']
        sensor_type = sensor_info['sensor_type']
        raw_data    = self.get_sensor_raw_data(sensor_id)
        recon_data  = self.get_sensor_recon_data(sensor_id, process_id)
        recon_data_len = len(recon_data['values'])
        for i in range(raw_data['dof']):        # loop over components (coordinates)
            tss_raw   = raw_data['tss'][:recon_data_len]
            tss_recon = recon_data['tss']
            component_raw   = list( zip(*raw_data['values'][:recon_data_len])[i] )
            component_recon = list( zip(*recon_data['values'])[i] )
            component_recon_interpol = tkerr.interpolate(tss_raw, tss_recon, component_recon)
            
            e_v      = tkerr.e(component_raw, component_recon_interpol)     # error (pointwise difference)
            ae_v     = tkerr.ae(component_raw, component_recon_interpol)    # absolute error (pointwise difference)
            mae_v    = tkerr.mae(component_raw, component_recon_interpol)
            mape_v   = tkerr.mape(component_raw, component_recon_interpol)
            smape_v  = tkerr.smape(component_raw, component_recon_interpol)
            wpe_v    = tkerr.wpe(component_raw, component_recon_interpol)
            wape_v   = tkerr.wape(component_raw, component_recon_interpol)
            rrmse_v  = tkerr.rrmse(component_raw, component_recon_interpol)
            relerr_v = tkerr.relerr(component_raw, component_recon_interpol)

            e_hist, e_bin_edges = np.histogram(e_v, bins=100)
            e_hist_norm = e_hist / float(np.sum(e_hist)) * 100

            deviations['sensor_id'].append(sensor_id)
            deviations['series_type1'].append(raw_data['series_type'])
            deviations['series_type2'].append(recon_data['series_type'])
            deviations['values_component'].append(i)
            deviations['E'].append({'hist': e_hist_norm, 'bin_edges': e_bin_edges, 'max': np.max(e_v), 'min': np.min(e_v), 'mean': np.mean(e_v), 'median': np.median(e_v)})
            deviations['MAE'].append(mae_v)
            deviations['MAPE%'].append(mape_v)
            deviations['SMAPE%'].append(smape_v)
            deviations['RRMSE%'].append(rrmse_v)
            deviations['WAPE%'].append(wape_v)
            deviations['relerr'].append(relerr_v)
            deviations['WPE%'].append(wpe_v)
            deviations['engine'].append(sensor_info['processed_data'][process_id]['eng_name'])
            deviations['reduction'].append(sensor_info['processed_data'][process_id]['reduction'])
            deviations['sparsity'].append(sensor_info['processed_data'][process_id]['sparsity'])
            deviations['framesize'].append(sensor_info['processed_data'][process_id]['framesize'])

        return deviations

    def get_gps_deviations(self, sensor_id, process_id=0):
        """ 
        API for get gps deviations

        Args: 
            sensor_id: Sensor ID
            process_id: the index of processed data

        Returns: 
            A dict with following information:
            pe_distances: a list of Point to Edge distances
            pp_distances: a list of Point to Point distances
            wgs84_pp_distances: a list of Point to Point distances by WGS84
            pe_deviations: edge deviations, a dict includes mean, max and index of max
            pp_deviations: point deviations, a dict includes mean, max and index of max
            wgs84_pp_deviations: point deviations by wgs84, a dict includes mean, max and index of max
        """
        sensor_info = self.get_sensor_info(sensor_id)
        if sensor_info is None:
            print("Sensor id " + str(sensor_id) + " does not exist!")
            return None
        if len(sensor_info['processed_data']) <= process_id:
            print("Process id " + str(process_id) + " out of range! ")
            return None
        raw_data    = self.get_sensor_raw_data(sensor_id)
        recon_data  = self.get_sensor_recon_data(sensor_id, process_id)
        return self.calculate_gps_deviations(raw_data['values'], recon_data['values'])
	   
	   
    def get_gps_steps(self, sensor_id, process_id=0):
        """ 
        API for get gps deviations

        Args: 
            sensor_id: Sensor ID
            process_id: the index of processed data

        Returns: 
            A dict with following information:
            pe_distances: a list of Point to Edge distances
            pp_distances: a list of Point to Point distances
            wgs84_pp_distances: a list of Point to Point distances by WGS84
            pe_deviations: edge deviations, a dict includes mean, max and index of max
            pp_deviations: point deviations, a dict includes mean, max and index of max
            wgs84_pp_deviations: point deviations by wgs84, a dict includes mean, max and index of max
        """
        sensor_info = self.get_sensor_info(sensor_id)
        if sensor_info is None:
            print("Sensor id " + str(sensor_id) + " does not exist!")
            return None
        if len(sensor_info['processed_data']) <= process_id:
            print("Process id " + str(process_id) + " out of range! ")
            return None
        raw_data    = self.get_sensor_raw_data(sensor_id)
        #raw_data_future    = self.get_sensor_future_data(sensor_id)
        raw_data_future = [raw_data['values'][0]]
        for i in range(1,len(raw_data['values'])-1):#Important: Use size of reconstructed data as the size of the windows varies
            raw_data_future.append(raw_data['values'][i+1])
            #print "These are the concrete values",raw_data_future[i]
            #print "These are the raw values",raw_data['values'][i]
        #print "These are the values",raw_data_future['values']
        recon_data  = self.get_sensor_recon_data(sensor_id, process_id)
        return self.calculate_gps_deviations(raw_data['values'], raw_data_future)

    def calculate_gps_deviations(self, raw_values, recon_values):
        """Calculate gps distance of raw values and recon_values
        
        Args:
            raw_values: Coordinates of points in lat and long
    
            recon_values: Coordinates of points in lat and long
            
        Returns:
            A dict with following information:
            pe_distances: a list of Point to Edge distances
            pp_distances: a list of Point to Point distances
            wgs84_pp_distances: a list of Point to Point distances by WGS84
            pe_deviations: edge deviations, a dict includes mean, max and index of max
            pp_deviations: point deviations, a dict includes mean, max and index of max
            wgs84_pp_deviations: point deviations by wgs84, a dict includes mean, max and index of max
        """
        if len(raw_values[0]) != 2:
            return None
        if len(recon_values[0]) != 2:
            return None
        raw_xy = transform_lonlat_to_utm(raw_values)
        rec_xy = transform_lonlat_to_utm(recon_values)
        pp_distances, pp_deviations = self.calculate_point_to_point_distances(raw_xy, rec_xy)
        pe_distances, pe_deviations = self.calculate_point_to_edge_distances(raw_xy, rec_xy)
        wgs84_pp_distances, wps84_pp_deviations = self.calculate_distances_WGS84(raw_values, recon_values)
        gps_deviations = {}
        gps_deviations['pp_distances'] = pp_distances
        gps_deviations['pe_distances'] = pe_distances
        gps_deviations['wgs84_pp_distances'] = wgs84_pp_distances
        gps_deviations['pp_deviations'] = pp_deviations
        gps_deviations['pe_deviations'] = pe_deviations
        gps_deviations['wgs84_pp_deviations'] = wps84_pp_deviations
        return gps_deviations

    def calculate_point_to_point_distances(self, coords_ref, coords_measure):
        """Calculate point to point distance of coresponding datapoints.            
        Args:
            coords_ref (list of tuples): Coordinates of points representing the first line.
    
            coords_measure (list of tuples): Coordinates of points representing the 
                sencond line. Must have same length as ref line.
            
        Returns:
            A dict with following information:
            mean_distance:  mean distance
            max_distance: max distance
            max_d_index: index of max distance
            and 
            distances:  a list of distances
        """
        from math import sqrt
        distances = []
        mean_distance = 0
        sum_distance = 0
        max_distance = 0
        max_d_index = 0
        for i in range(len(coords_measure)):
            dx = abs(coords_ref[i][0] - coords_measure[i][0])
            dy = abs(coords_ref[i][1] - coords_measure[i][1])
            dz = sqrt(dx**2 + dy**2)
            distance = dz
            distances.append(distance)
            sum_distance += distance
            if distance > max_distance:
            	max_distance = distance
            	max_d_index = i
        mean_distance = 1.0 * sum_distance / len(coords_measure)
        deviations = {}
        deviations['mean_distance'] = mean_distance
        deviations['max_distance'] = max_distance
        deviations['max_d_index'] = max_d_index
        return distances, deviations

    def calculate_point_to_edge_distances(self, coords_ref, coords_measure):
        """Calculate distance of coresponding datapoints.
    
        Args:
            coords_ref (list of tuples): Coordinates of points representing the line
                from which distances are measured.

            coords_measure (list of tuples): Coordinates of points of which the distance
                is to be calculated
            
        Returns:
            A dict with following information:
            mean_distance:  mean distance
            max_distance: max distance
            max_d_index: index of max distance
            and 
            distances:  a list of distances

        """
        import shapely.geometry as geom

        line = geom.LineString(coords_ref)
        distances = []
        mean_distance = 0
        sum_distance = 0
        max_distance = 0
        max_d_index = 0
        for i in range(len(coords_measure)):
            point = geom.Point(coords_measure[i])
            distance = point.distance(line)
            distances.append(distance)
            sum_distance += distance
            if distance > max_distance:
            	max_distance = distance
            	max_d_index = i
        mean_distance = 1.0 * sum_distance / len(coords_measure)
        deviations = {}
        deviations['mean_distance'] = mean_distance
        deviations['max_distance'] = max_distance
        deviations['max_d_index'] = max_d_index
        return distances, deviations

    def calculate_distances_WGS84(self, lonlat1, lonlat2):
        """Calculate point to point distance based on WGS84
    
        Args:
            lonlat1 (list of tuples): List of datapoints where each datapoint is a 
                tuple of longitude and latitude (lon, lat).
    
            lonlat2 (list of tuples): List of datapoints where each datapoint is a 
                tuple of longitude and latitude (lon, lat).

        Returns:
            A dict with following information:
            mean_distance:  mean distance
            max_distance: max distance
            max_d_index: index of max distance
            and 
            distances:  a list of distances
        """
        import pyproj
        geod = pyproj.Geod(ellps='WGS84')
        distances = []
        mean_distance = 0
        sum_distance = 0
        max_distance = 0
        max_d_index = 0
        for i in range(len(lonlat2)):
            dx, dy, dz = geod.inv(lonlat1[i][0], lonlat1[i][1], lonlat2[i][0], lonlat2[i][1])
            distance = dz
            distances.append(distance)
            sum_distance += distance
            if distance > max_distance:
            	max_distance = distance
            	max_d_index = i
        mean_distance = 1.0 * sum_distance / len(lonlat2)
        deviations = {}
        deviations['mean_distance'] = mean_distance
        deviations['max_distance'] = max_distance
        deviations['max_d_index'] = max_d_index
        return distances, deviations

    def get_zip_reduction_ratio(self, sensor_id, framesize):
    	import struct, zlib, binascii
    	raw_data_len = 0
    	com_data_len = 0
    	raw_data = self.get_sensor_raw_data(sensor_id)['values']
        frames = len(raw_data)/framesize
        for di in range(len(raw_data[0])):
            all_data = [x[di] for x in raw_data]
            for i in range(frames):
                start = i*framesize
                end = (i+1)*framesize
                data = all_data[start:end]
                try:
                    buffer_tmp=struct.pack('f'*len(data), *data)
                except:
                    buffer_tmp=struct.pack('i'*len(data), *data)
                compressed = zlib.compress(buffer_tmp,1)
                raw_data_len += sys.getsizeof(binascii.hexlify(buffer_tmp))
                com_data_len += sys.getsizeof(binascii.hexlify(compressed))
            if end < len(all_data):
                data = all_data[end:]
                try:
                    buffer_tmp=struct.pack('f'*len(data), *data)
                except:
                    buffer_tmp=struct.pack('i'*len(data), *data)
                compressed = zlib.compress(buffer_tmp,1)
                raw_data_len += sys.getsizeof(binascii.hexlify(buffer_tmp))
                com_data_len += sys.getsizeof(binascii.hexlify(compressed))
        ratio = 1.0 - 1.0*com_data_len/raw_data_len
        return ratio


    def get_sensor_data(self, index, target='raw_data'):
        """ 
        API for get sensor data

        Args: 
            index: index of the SensorData
            target: datatype, raw_data, red_data or recon_data

        Returns: 
            A dict with following information:
            sensnor_id:  sensor id
            sensor_name: sensor name
            sensor_type: sensor type
            dof:  DOF
            tss: a list of timestamps with data format: [ts1, ts2....]
            values: a list of values with data format: 
                    [ [value_x[, value_y...]], [value_x[, value_y]...]
        """
        print "************************** WARNING *****************************"
        print "get_sensor_data(index, datatype) is replaced by functions: "
        print ">>> get_sensor_raw_data(sensor_id)"
        print ">>> get_sensor_red_data(sensor_id, process_id)"
        print ">>> get_sensor_recon_data(sensor_id, process_id)"
        print "*************************END OF WARNING *************************"
        SD = self._sensors[index]['sensor_data']
        if target == 'raw_data':
            content = SD.get_raw_data()
        elif target == 'red_data':
            content = SD.get_red_data()
        elif target == 'recon_data':
            content = SD.get_recon_data()
        else:
            return None
        content['path'] = self._sensors[index]['path']
        content['sensor_id'] = self._sensors[index]['sensor_id']
        content['sensor_name'] = self._sensors[index]['sensor_name']
        content['sensor_type'] = self._sensors[index]['sensor_type']

        return content

    def write_sensor_file(self, index, path=None, target='red_data', ftype='csv', zip_c=0):
        """ 
        API for write data to a file

        Args: 
            index: index of the SensorData
            path:  filename
            target: datatype, red_data or recon_data

        Returns: 
            SensorData Instance
        """
        print "****************************** WARNING *********************************"
        print "write_sensor_file(index, path, datatype, ftype, zip_c) is replaced by : "
        print ">>> write_sensor_data_files(sensor_id)"
        print "*****************************END OF WARNING *****************************"
        SD = self._sensors[index]['sensor_data']
        if target == 'red_data':
            content = SD.get_red_content()
        elif target == 'recon_data':
            content = SD.get_recon_content()
        elif target == 'raw_data':
            content = SD.get_raw_content()
        elif target == 'red_delta':
            content = SD.get_red_delta_content()
        else:
            return None
        f = open(path, 'w')
        f.write(content)
        f.close()
        return SD

    def add_processed_red_data(self, sensor_id, filename):
        """ 
        API for add reduced data from a file

        Args: 
            sensor_id: sensor_id
            filename:  filename for reduced delta file

        Returns: 
            Process ID
        """
        sensor_info = self.get_sensor_info(sensor_id)
        if sensor_info is None:
            print("Sensor id " + str(sensor_id) + " does not exist!")
            return None
        SD = SensorData()
        SD._raw_data = sensor_info['sensor_data']._raw_data
        meta, red_delta = SD.read_red_delta_file(sensor_id, filename)
        SD.adv_decoding()
        process_info = {}
        process_id = len(sensor_info['processed_data'])
        process_info['process_id'] = process_id
        process_info['reduce_info'] = {}
        process_info['framesize'] = meta['n']
        process_info['reduction'] = 1 - meta['s']
        process_info['sparsity'] = meta['s']
        process_info['eng_name'] = meta['e']
        process_info['block'] = meta['b']
        process_info['pre_process'] = meta['pr']
        process_info['post_process'] = meta['po']
        process_info['thelist'] = meta['l']
        process_info['sensor_data'] = SD
        process_info['deviations'] = []
        sensor_info['processed_data'].append(process_info)
        sensor_info['processed_data'][process_id]['deviations'] = self.calculate_deviations(sensor_id, process_id)
        return process_id

    def add_processed_sensor_data(self, sensor_id, values, tss=[], datatype='recon_data', meta={}):
        """ 
        API for add processed sensor data into DataBank class

        Args: 
            sensnor_id:  sensor id
            values: a list of values with data format: 
                    [ [value_x[, value_y...]], [value_x[, value_y]...]
            tss: a list of timestamps with data format: [ts1, ts2....]
            datatype: datatype, 'recon_data' for reconstructed data
            meta: other information about the data

        Returns: 
            Process ID
        """
        sensor_info = self.get_sensor_info(sensor_id)
        if sensor_info is None:
            print("Sensor id " + str(sensor_id) + " does not exist!")
            return None
        if datatype == 'recon_data':
            SD = SensorData()
            SD._raw_data = sensor_info['sensor_data']._raw_data
            SD._recon_data['values'] = [x for x in values]
            SD._recon_data['tss'] = [ts for ts in tss]
            SD._recon_data['dof'] = len(values[0])
            SD._recon_data['count'] = len(values)
            process_info = {}
            process_id = len(sensor_info['processed_data'])
            process_info['process_id'] = process_id
            process_info['source'] = 'file'
            process_info['reduce_info'] = {}
            process_info['framesize'] = 0
            process_info['reduction'] = 0
            process_info['sparsity'] = 0
            process_info['eng_name'] = 'unknow'
            process_info['pre_process'] = {}
            process_info['post_process'] = {}
            process_info['block'] = 0
            process_info['sensor_data'] = SD
            for key in meta.keys():
                process_info[key] = meta[key]
            sensor_info['processed_data'].append(process_info)
            sensor_info['processed_data'][process_id]['deviations'] = self.calculate_deviations(sensor_id, process_id)
            return process_id
        return None

    def sensor_data_process(self, sensor_id, framesize, reduction, eng_name='aii_v010', pre_process={}, post_process={}, block=None, epsilon=None, batch=0):
        """ 
        Common API for process the data: encoding and decoding

        Args: 
            sensor_id: sensor_id
            framesize: framesize
            reduction: reduction
            eng_name:  engine name
            pre_process: parameters in dictionary for pre_process
            post_process: parameters in dictionary for post_process
            block: block used for engine A/C
            epsilon: epsilon used for engine RDP
            batch: batch process for engine A/C

        Returns: 
            Process ID
        """ 
        if eng_name.startswith('rdp'):
            result = self.rdp_data_process(sensor_id, framesize, eng_name=eng_name, epsilon=epsilon)
        else:
            result = self.eng_ac_data_process(sensor_id, framesize, reduction, eng_name, pre_process, post_process, block, batch)
        return result

    def rdp_data_process(self, sensor_id, framesize=200, sparsity=0.5, eng_name='rdpi_v001', epsilon=None):
        """ 
        API for RDP data process 

        Args: 
            sensor_id: sensor_id
            framesize: framesize
            sparsity: not used
            eng_name:  engine name
            epsilon: epsilon

        Returns: 
            Process ID
        """ 
        sys.path.append('../tksvr/packages/')
        import importlib
        sensor_info = self.get_sensor_info(sensor_id)
        if sensor_info is None:
            print("Sensor id " + str(sensor_id) + " does not exist!")
            return None
        if len(sensor_info['sensor_data']._raw_data['values']) < framesize:
            print("Raw data is less than a framesize!" )
            return None            
        engine = 'tk_eng_' + eng_name
        E = importlib.import_module(engine)
        D = E.Decomposer(epsilon, framesize)
        R = E.Reconstructor(epsilon, framesize)
        SD = SensorData()
        SD._raw_data = sensor_info['sensor_data']._raw_data
        SD._red_data = {}
        #print(SD._raw_data['values'])
        SD._red_data['red_values'], SD._rdp_list = D.decompose(SD._raw_data['values'])
        SD._red_data['tss'] = [SD._raw_data['tss'][i] for i in SD._rdp_list]
        SD._red_data['count'] = len(SD._red_data['red_values'])
        SD._red_data['meta'] = {'rdp_list': SD._rdp_list}
        SD._red_data['raw_value'] = []
        SD._red_meta_data = {}
        SD._red_delta = []
        reduce_info = {}
        reduce_info['thelist'] = SD._rdp_list
        reduce_info['count_raw'] = len(SD._raw_data['values'])
        reduce_info['count_red'] = len(SD._rdp_list)
        reduce_info['ratio'] = 1.0 - 1.0 * reduce_info['count_red'] / reduce_info['count_raw']

        SD._recon_data = {}
        SD._recon_data['values'] = R.reconstruct(SD._red_data['red_values'], SD._rdp_list)
        SD._recon_data['tss'] = SD._raw_data['tss']
        SD._recon_data['dof'] = len(SD._recon_data['values'][0])
        SD._recon_data['count'] = len(SD._recon_data['values'])
        SD._recon_data['meta'] = {'ts':1}

        process_info = {}
        process_id = len(sensor_info['processed_data'])
        process_info['process_id'] = process_id
        process_info['reduce_info'] = reduce_info
        process_info['framesize'] = framesize
        process_info['reduction'] = reduce_info['ratio']
        process_info['sparsity'] = sparsity
        process_info['eng_name'] = eng_name
        process_info['pre_process'] = {}
        process_info['post_process'] = {}
        process_info['block'] = 0
        process_info['sensor_data'] = SD
        sensor_info['processed_data'].append(process_info)
        sensor_info['processed_data'][process_id]['deviations'] = self.calculate_deviations(sensor_id, process_id)
        return process_id

    def eng_ac_data_process(self, sensor_id, framesize, reduction, eng_name='aii_v010', pre_process={}, post_process={}, block=None, batch=0):
        """ 
        API for process the data: encoding and decoding

        Args: 
            sensor_id: sensor_id
            framesize: interger in range of [1:2000]
            reduction: float in range of (0 : 1]

        Returns: 
            Process ID
        """
        if framesize < 1:
            print "framesize must be an interger in range [1, 2000]"
            return None
        if framesize > 2000:
            print "framesize must be an interger in range [1, 2000]"
            return None
        if reduction < 0:
            print "reduction must be a float in range [0, 1)"
            return None
        if reduction  >= 1:
            print "reduction must be a float in range [0, 1)"
            return None
        sensor_info = self.get_sensor_info(sensor_id)
        if sensor_info is None:
        	print("Sensor id " + str(sensor_id) + " does not exist!")
        	return None
        if len(sensor_info['sensor_data']._raw_data['values']) < framesize:
            print("Raw data is less than a framesize!" )
            return None            
        SD = SensorData()
        SD._raw_data = sensor_info['sensor_data']._raw_data
        sparsity = 1 - reduction
        SD.encoding(framesize, sparsity)
        import copy
        pr = copy.deepcopy(pre_process)
        po = copy.deepcopy(post_process)
        reduce_info = SD.adv_encoding_decoding(sensor_id, eng_name, block, pr, po, batch=batch)
        process_info = {}
        process_id = len(sensor_info['processed_data'])
        process_info['process_id'] = process_id
        process_info['reduce_info'] = reduce_info
        process_info['framesize'] = framesize
        process_info['reduction'] = reduction
        process_info['sparsity'] = sparsity
        process_info['eng_name'] = eng_name
        process_info['pre_process'] = pr
        process_info['post_process'] = po
        process_info['block'] = block       
        process_info['sensor_data'] = SD
        sensor_info['processed_data'].append(process_info)
        sensor_info['processed_data'][process_id]['deviations'] = self.calculate_deviations(sensor_id, process_id)
        return process_id

    def sensor_data_post_process(self, sensor_id, process_id, post_process_para={}):
        """ 
        API for post process the processed data: 

        Args: 
            sensor_id: sensor_id
            process_id: process_id
            post_process_para: a dict for process_para

        Returns: 
            Process ID
        """
        sensor_info = self.get_sensor_info(sensor_id)
        if sensor_info is None:
            print("Sensor id " + str(sensor_id) + " does not exist!")
            return None
        if len(sensor_info['processed_data']) <= process_id:
            print("Process id " + str(process_id) + " out of range! ")
            return None
        recon_data = self.get_sensor_recon_data(sensor_id, process_id)
        current_process_info = self.get_sensor_process_info(sensor_id, process_id)
        SD = SensorData()
        SD._raw_data = sensor_info['sensor_data']._raw_data
        SD._recon_data['values'] = self.adv_post_process(recon_data['values'], post_process_para)
        SD._recon_data['tss'] = recon_data['tss']
        SD._recon_data['dof'] = recon_data['dof']
        SD._recon_data['count'] = recon_data['count']
        process_info = {}
        new_process_id = len(sensor_info['processed_data'])
        process_info['process_id'] = new_process_id
        process_info['source'] = 'post_process'
        process_info['reduce_info'] = current_process_info['reduce_info']
        process_info['framesize'] = current_process_info['framesize']
        process_info['reduction'] = current_process_info['reduction']
        process_info['sparsity'] = current_process_info['sparsity']
        process_info['eng_name'] = current_process_info['sparsity']
        process_info['pre_process'] = current_process_info['pre_process']
        process_info['post_process'] = current_process_info['post_process']
        process_info['block'] = current_process_info['block']
        process_info['sensor_data'] = SD
        sensor_info['processed_data'].append(process_info)
        sensor_info['processed_data'][new_process_id]['deviations'] = self.calculate_deviations(sensor_id, new_process_id)
        return new_process_id

    def adv_post_process(self, recon_data, post_process_para={}):
        """ 
        API for post data process: 

        Args: 
            recon_data: reconstructed data
            post_process_para: a dict for process_para

        Returns: 
            the data after post processed
        """
        result = recon_data
        if 'method' in post_process_para.keys():
            if post_process_para['method'] == 'savgol_filter':
                window_length = post_process_para['window_length']
                polyorder = post_process_para['polyorder']
                result = self.post_process_savgol_filter(recon_data, window_length, polyorder)
        return result

    def post_process_savgol_filter(self, recon_data, window_length, polyorder):
        """ 
        API for savgol_filter post data process: 

        Args: 
            recon_data: reconstructed data
            window_length: parameters for savgol filter
            polyorder: parameters for savgol filter

        Returns: 
            the data after post processed
        """
        from scipy.signal import savgol_filter
        value_di = data_m_n_convert(recon_data)
        post_value_di = []
        for item in value_di:
            post_item = savgol_filter(item, window_length, polyorder)
            post_value_di.append(post_item)
        result = data_m_n_convert(post_value_di)
        return result

    def process_sensor_data(self, index, framesize, reduction, eng_name='aii_v010', pre_process={}, post_process={}, block=None):
        """ 
        API for process the data: encoding and decoding

        Args: 
            index: index of the SensorData
            framesize: interger in range of [1:2000]
            reduction: float in range of (0 : 1]

        Returns: 
            SensorData Instance
        """
        print "****************************** WARNING *********************************"
        print "process_sensor_data(index, n, reduction, eng_n, pre_p, post_p) is replaced by : "
        print ">>> sensor_data_process(sensor_id, n, reduction, eng_n, pre_p, post_p, block)"
        print "*****************************END OF WARNING *****************************"
        if framesize < 1:
            print "framesize must be an interger in range [1, 2000]"
            return None
        if framesize > 2000:
            print "framesize must be an interger in range [1, 2000]"
            return None
        if reduction < 0:
            print "reduction must be a float in range [0, 1)"
            return None
        if reduction  >= 1:
            print "reduction must be a float in range [0, 1)"
            return None
        SD = self._sensors[index]['sensor_data']
        sparsity = 1 - reduction
        SD.encoding(framesize, sparsity)
        SD.adv_encoding_decoding(self._sensors[index]['sensor_id'], eng_name, block, pre_process, post_process)
        return SD

    def get_input_files(self):
        """ 
        API for get all the input files

        Args: 
            None

        Returns: 
            None
        """ 
        return self._input_files

    def data_process(self, config_para):
        """ 
        API for bath process the data

        Args: 
            config_para: config paramters

        Returns: 
            None
        """ 
        content = ''
        self.read_files(config_para['input'])
        if config_para['action'] == 'encoding':
            target = 'red_data'
            for pf in config_para['decomposes']:
                self.encoding_files(pf['framesize'], pf['sparsity'])
                content += self.get_target_content()
        elif config_para['action'] == 'decoding':
            self.decoding_files()
            content += self.get_target_content()
            target = 'recon_data'
        else:
            print("Unknow action!")
            return
        f = open(config_para['output']['filename'], 'w')
        f.write(content)
        f.close()
        return

    def read_file(self, path):
        """ 
        API for read one file, the file can be raw data, reduced data or reconstructed data

        Args: 
            path: path to the file

        Returns: 
            list of sensor data
        """ 
        self._input_files = [path]
        self._sensor_data = []
        self._current_sd = None
        self.read_onefile(path)
        return self._sensor_data

    def get_files_from_path_name(self, filename=None, f_type='csv', rootpath="."):
        """ 
        get list of files in one folder and its subdirectory

        Args: 
            filename: the specific file name
            f_type: the file type, e.g. "csv", "tka"...
            rootpath: the rootpath to load the files

        Returns: 
            list of file with fullpaths
        """ 
        result = []
        for path, subdirs, files in os.walk(rootpath):
            for name in files:
                fullpath = os.path.join(path, name)
                if filename is None:
                    result.append(fullpath)
                elif name == filename + "." + f_type:
                    result.append(fullpath)
        return result

    def load_files(self, name=None, f_type='csv', path='.'):

        """ 
        API for read list of files in one folder and its subdirectory
        Args: 
            name: the specific file name, e.g. "intel_raw", by default load all files
            f_type: the file type, e.g. "csv", "tka"...
            path: the root path to load the files, using current folder by default

        Returns: 
            list of sensor data
        """ 
        self._input_files = self.get_files_from_path_name(name, f_type, path)
        result = self.read_files(self._input_files)
        return result

    def read_files(self, paths):
        """ 
        API for read list of files, the file can be raw data, reduced data or reconstructed data

        Args: 
            paths: list of path to the file

        Returns: 
            list of sensor data
        """ 
        self._input_files = paths
        self._sensor_data = []
        self._current_sd = None
        for path in paths:
            self.read_onefile(path)
        return self._sensor_data

    def read_onefile(self, path):
        """ 
        read onefile, the file can be raw data, reduced data or reconstructed data

        Args: 
            path: path to the file

        Returns: 
            list of sensor data, the results are read into _sensor_data
        """ 
        self._input_file = path
        f = open(path, 'r')
        current_line = 0
        for line in f:
            current_line += 1
            text = line.rstrip()
            ret = self.read_one_line(text)
            if ret < 0:
                print("Error when reading line " + str(current_line) + " in file " + path)
                f.close()
                exit(1)
        f.close()
        return self._sensor_data

    def read_one_line(self, text):
        """ 
        read one line from the file

        Args: 
            text: the text of line in the file

        Returns: 
            0 if success and -1 if error
        """ 
        if len(text) < 1:
            ret = 0
        elif text[0] == '#':
            ret = self.read_headline(text)
        else:
            ret = self.read_dataline(text)
        return ret

    def read_headline(self, text):
        """ 
        read headline from the file

        Args: 
            text: the text of line in the file

        Returns: 
            0 if success and -1 if error
        """ 
        SD = SensorData()
        self._sensor_data.append(SD)
        self._current_sd = SD
        ret = self._current_sd.read_headline(text)
        return ret

    def read_dataline(self, text):
        """ 
        read data line from the file

        Args: 
            text: the text of line in the file

        Returns: 
            0 if success and -1 if error
        """ 
        if self._current_sd is None:
           SD = SensorData()
           self._sensor_data.append(SD)
           self._current_sd = SD
        ret = self._current_sd.read_dataline(text)
        return ret

    def encoding_files(self, framesize=100, sparsity=0.3):
        """ 
        encoding the read data

        Args: 
            framesize, spstsity

        Returns: 
            None
        """ 
        for SD in self._sensor_data:
            SD.encoding(framesize, sparsity)

    def decoding_files(self):
        """ 
        encoding the read data

        Args: 
            None

        Returns: 
            None
        """ 
        for SD in self._sensor_data:
            SD.decoding()

    def write_file(self, path=None, target='red_data', ftype='csv', zip_c=0):
        """ 
        write data to the file

        Args: 
            path: the path to wrote
            target: the target datatype to be wrote to file
            ftype: csv or excel
            zip_c: if 1, zip compression

        Returns: 
            None
        """ 
        content = self.get_target_content(target)
        f = open(path, 'w')
        f.write(content)
        f.close()
        return

    def write_red_file(self, path=None):
        """ 
        write data to the file

        Args: 
            path: the path to wrote

        Returns: 
            None
        """ 
        content = self.get_target_content()
        f = open(path, 'w')
        f.write(content)
        f.close()
        return

    def write_recon_file(self, path=None):
        """ 
        write data to the file

        Args: 
            path: the path to wrote

        Returns: 
            None
        """ 
        content = self.get_target_content('recon_data')
        f = open(path, 'w')
        f.write(content)
        f.close()
        return

    def get_target_content(self, target='red_data'):
        """ 
        get the target text conetnt

        Args: 
            target: the datatype of the target content
            
        Returns: 
            None
        """ 
        result = ''
        for SD in self._sensor_data:
            if target == 'red_data':
                result += SD.get_red_content()
            elif target == 'recon_data':
                result += SD.get_recon_content()
        return result

    def get_wirte_profile(self, para):
        result = para
        if 'zip' not in result.keys():
            result['zip'] = 0
        if 'filename' not in result.keys():
            result['filename'] = 100
        return result

    def print_out(self):
        for SD in self._sensor_data:
            SD.print_out()


#-------------------------------------------------------------------------------
"""                 test case 1 
Step 1: Create a Databank and initiate 3 datasets:
    index   filename            sensor_id,    sensor_name,  sensor_type,   dof  
      0     test1.csv               1          intel_raw    temperature     1
      1     edison_gyro.csv         2          edison       gyro            3
      2     None(data add)          5          test_sensor  range           3
    The csv file is formated as following:
    <timestamp>;<value>[;<value>...]

Step 2: Data Decompose and Reconstruction and get raw, red, and recon_data
    index   filename            framesize      reduction         print_result
      0     test1.csv              100            0.7          raw_data/red_data
      1     edison_gyro.csv        240            0.8          red_data
      2     None(data add)         500            0.9          raw_data/recon_data

Step 3: Process Each Dataset with following framesize and reduction
     framesize      reduction      red_file_name                    recon_file_name
       200             0.7      red200_0.7_<sensor_name>     recon200_0.7_<sensor_name>.csv
       200             0.8      red200_0.8_<sensor_name>     recon200_0.8_<sensor_name>.csv
       200             0.9      red200_0.9_<sensor_name>     recon200_0.9_<sensor_name>.csv
       500             0.7      red500_0.7_<sensor_name>     recon500_0.7_<sensor_name>.csv
       500             0.8      red500_0.8_<sensor_name>     recon500_0.8_<sensor_name>.csv
       500             0.9      red500_0.9_<sensor_name>     recon500_0.9_<sensor_name>.csv
************* TEST1 IS NOT USED ANY MORE, DO NOT REFER TO TEST1 *******************
def test1():        
    #Step 1: create a Databank and initiate 3 datasets
    D = DataBank()
    S1 = D.read_sensor_file("test1.csv", 1, "intel_raw", "temperature")
    S2 = D.read_sensor_file("edison_gyro.csv", 2, "edison", "gyro")
    values = [[x, x+x, x*x] for x in range(10000)]
    tss = [1700000000 + x*50 for x in range(10000)]
    S3 = D.add_sensor_data(values, tss, 9, "test_sensor", "random")
    print D.get_sensor_list()

    #Step 2: Data Decompose and Reconstruction and get raw, red, and recon_data
    D.process_sensor_data(0, 100, 0.7)
    D.process_sensor_data(1, 240, 0.8)
    D.process_sensor_data(2, 500, 0.9)
    print("*****raw data for temperature sensor ")
    print D.get_sensor_data(0, 'raw_data')
    print("*****red data for temperature sensor ")
    print D.get_sensor_data(0, 'red_data')
    print("*****red data for gyro sensor ")
    print D.get_sensor_data(1, 'red_data')
    print("*****raw data for random data set ")
    print D.get_sensor_data(2, 'raw_data')
    print("*****recon data for random data set ")
    print D.get_sensor_data(2, 'recon_data')

    #Step 3: Process Each Dataset with following framesize and reduction
    process_cases = [[200, 0.7], [200, 0.8], [200, 0.9], [500, 0.7], [500, 0.8], [500, 0.9]]
    for sensor in D.get_sensor_list():
        index = sensor['index']
        for item in process_cases:
            framesize = item[0]
            reduction = item[1]
            D.process_sensor_data(index, framesize, reduction)
            rawdata_filename = 'raw'+str(framesize)+'_'+str(reduction)+sensor['sensor_name']+'.csv'
            reddata_filename = 'red'+str(framesize)+'_'+str(reduction)+sensor['sensor_name']+'.csv'
            recondata_filename = 'recon'+str(framesize)+'_'+str(reduction)+sensor['sensor_name']+'.csv'
            D.write_sensor_file(index, rawdata_filename, 'raw_data')
            D.write_sensor_file(index, reddata_filename, 'red_data')
            D.write_sensor_file(index, recondata_filename, 'recon_data')
            print("Success process and write data to " + recondata_filename + ' !') 
"""
def test2():        
    #Step 1: create a Databank and initiate 3 datasets
    D = DataBank()
    S1 = D.read_sensor_file("test1.csv", 1, "intel_raw", "temperature")
    S2 = D.read_sensor_file("test2.csv", 2, "edison", "gyro", ['v_x', 'v_y'], ['D', 'D'], ['hours','EUROs'])
    values = [[x, x+x, x*x] for x in range(10000)]
    tss = [1700000000 + x*50 for x in range(10000)]
    S3 = D.add_sensor_data(values, tss, 9, "test_sensor", "random", ['x', 'x+x', 'x*x'], ['C', 'D', 'E'], ['meter','degree','KG'])
    #print D.get_sensor_list()

    #Step 2: Process Each Dataset with following framesize and reduction, deviation is calculated 
    #        after each data process
    process_cases = [[200, 0.7], [200, 0.8], [200, 0.9], [500, 0.7], [500, 0.8], [500, 0.9]]
    for sensor_id in [1, 2, 9]:
        for item in process_cases:
            framesize = item[0]
            reduction = item[1]
            D.sensor_data_process(sensor_id, framesize, reduction, 'aii_v010', block=5)
    #print D.get_sensor_list()

    #Step 3: Get process result
    print('*'*50)
    for sensor_id in [1, 2, 9]:
        for item in D.get_sensor_process_info(sensor_id):
        	print('+'*50)
        	print(item['process_id'], item['framesize'], item['reduction'], item['eng_name'])
        	print D.get_deviations(sensor_id, item['process_id'])
    print D.get_sensor_list()
    print("*"*50)
    print D.get_sensor_process_info(2)
    print("*"*50)
    print D.get_sensor_process_info(2, 0)

    #Step 4: Print data result or write data to files
    print D.get_sensor_raw_data(1)
    print D.get_sensor_red_data(2, 0)
    print D.get_sensor_recon_data(9, 1)
    D.write_sensor_data_files(9)

def test3():
    from tk_gdfr import get_ts_value_from_file        
    gps_data = get_ts_value_from_file("bmw1.csv", ',', 0, 2, 1, [3,4])
    heading_data = get_ts_value_from_file("bmw1.csv", ',', 0, 2, 1, [5])
    speed_data = get_ts_value_from_file("bmw1.csv", ',', 0, 2, 1, [6])

    #Step 1: create a Databank and initiate 3 datasets
    D = DataBank()
    S = D.add_sensor_data(gps_data['pos']['values'], 
        None, 8, "gps", "bmw_dataset_pos")
    S = D.add_sensor_data(heading_data['pos']['values'], 
        heading_data['pos']['tss'], 9, "heading", "bmw_dataset_pos")
    S = D.add_sensor_data(speed_data['pos']['values'], 
        speed_data['pos']['tss'], 10, "gps", "bmw_dataset_pos")
    pre_process = {
        "multiple": [1000000, 1000000],
        "integer": [1, 1]
    }
    post_process = {
        "divide": [1000000.0, 1000000.0]
    }
    D.sensor_data_process(8, 500, 0.9, 'aii_v010', pre_process, post_process, 5)
    pre_process = {
        "multiple": [1000],
        "integer": [1],
        "value_exceptions": [
            [
                {
                    "e_name": "red_peak_points",
                    "e_id":  4,
                    "v_types": ["red"],
                    "condition": "(abs(delta_v)>30000)",
                    "para": "delta_v"
                },
                {
                    "e_name": "rest_peak_points",
                    "e_id":  5,
                    "v_types": ["rest"],
                    "condition": "(abs(delta_v)>30000)",
                    "para": "str(delta_v)+'_'+str(ts)+'_'+str(v)"
                }
            ] 
        ]
    }
    post_process = {
        "divide": [1000.0]
    }
    D.sensor_data_process(9, 500, 0.9, 'aii_v010', pre_process, post_process, 5)
    D.sensor_data_process(10, 500, 0.9, 'aii_v010', pre_process, post_process, 5)
    D.write_sensor_data_files(8)
    D.write_sensor_data_files(9)
    D.write_sensor_data_files(10)

    D.add_processed_red_data(8, "red_delta_8_0.csv")
    gps_deviation = D.get_gps_deviations(8, 0)
    print "*" * 50
    print gps_deviation['pp_deviations']

    gps_deviation = D.get_gps_deviations(8, 1)
    print "*" * 50
    print gps_deviation['pp_deviations']
    D.write_sensor_data_files(8)

    D.add_processed_red_data(9, "red_delta_9_0.csv")
    D.write_sensor_data_files(9)


def test4():
    from tk_gdfr import get_ts_value_from_file        
    gps_data = get_ts_value_from_file("bmw1.csv", ',', 0, 2, 1, [3,4])
    D = DataBank()
    S = D.add_sensor_data(gps_data['pos']['values'], 
        None, 8, "gps", "bmw_dataset_pos")
    D.sensor_data_process(8, 100, 0.9, 'rdpi_v001', epsilon=0.00001)
    gps_deviation = D.get_gps_deviations(8, 0)
    print "*" * 50
    print gps_deviation['pe_deviations']
    
    print "Reduced Ratio : " + str(D.get_sensor_process_info(8, 0)['reduce_info']['ratio'])
    D.write_sensor_data_files(8)

def test_dataset_process():
    from datasets.datasets import synthsets, tempsets, process_synth, process_temp
    D = DataBank()
    D.process_dataset(synthsets, process_synth)
    D.process_dataset(tempsets, process_temp)

def test_batch_process():
    from tk_gdfr import get_ts_value_from_file
    D = DataBank()    
    sensor_data = get_ts_value_from_file("bmw1.csv", ',', 0, 2, 1, [3,4])
    S = D.add_sensor_data(sensor_data['pos']['values'], 
        sensor_data['pos']['tss'], 8, "gps", "bmw_dataset_pos")
    pre_process = {
        "multiple": [1000000, 1000000]
    }
    post_process = {
        "divide": [1000000.0, 1000000.0]
    }
    D.sensor_data_process(8, 500, 0.9, 'cii_dct_v011', pre_process, post_process, 5, batch=0)
    D.sensor_data_process(8, 500, 0.9, 'cii_dct_v011', pre_process, post_process, 5, batch=1)
    D.write_sensor_data_files(8)

    print "*" * 50
    print "gps distance devition process 0"
    gps_deviation = D.get_gps_deviations(8, 0)
    print "Reduction Ratio : " + str(D.get_sensor_process_info(8, 0)['reduce_info']['ratio'])
    print "edge distance : " + str(gps_deviation['pe_deviations'])
    print "point distance : " + str(gps_deviation['pp_deviations'])
    print "wgs point distance : " + str(gps_deviation['wgs84_pp_deviations'])
    print gps_deviation['pe_distances'][3]
    print gps_deviation['pp_distances'][3]
    print gps_deviation['wgs84_pp_distances'][3]

    print "*" * 50
    print "gps distance devition process 1"
    gps_deviation = D.get_gps_deviations(8, 1)
    print "Reduction Ratio : " + str(D.get_sensor_process_info(8, 1)['reduce_info']['ratio'])
    print "edge distance : " + str(gps_deviation['pe_deviations'])
    print "point distance : " + str(gps_deviation['pp_deviations'])
    print "wgs point distance : " + str(gps_deviation['wgs84_pp_deviations'])
    print gps_deviation['pe_distances'][3]
    print gps_deviation['pp_distances'][3]
    print gps_deviation['wgs84_pp_distances'][3]

def test_get_exp_data():
    from tk_gdfr import get_ts_value_from_file
    D = DataBank()    	
    sensor_data = get_ts_value_from_file("bmw1.csv", ',', 0, 2, 1, [5])
    S = D.add_sensor_data(sensor_data['pos']['values'], 
    sensor_data['pos']['tss'], 9, "speed", "bmw_dataset_pos")
    pre_process = {
        "multiple": [1000],
        "value_exceptions": [
            [
                {
                    "e_name": "red_peak_points",
                    "e_id":  4,
                    "v_types": ["red"],
                    "condition": "(abs(delta_v)>10000)",
                    "para": "delta_v"
                },
                {
                    "e_name": "rest_peak_points",
                    "e_id":  5,
                    "v_types": ["rest"],
                    "condition": "(abs(delta_v)>10000)",
                    "para": "str(delta_v)+'_'+str(ts)+'_'+str(v)"
                }
            ] 
        ]
    }
    post_process = {
        "divide": [1000.0]
    }
    D.sensor_data_process(9, 200, 0.8, 'cii_dct_v011', pre_process, post_process, 5)
    D.sensor_data_process(9, 200, 0.8, 'cii_dct_v011', pre_process, post_process, 5, batch=1)
    D.write_sensor_data_files(9)

    print("*"*100)
    for item in D.get_sensor_exp_data(9, 0, 0):
    	print(item)
    print("-"*100)
    for item in D.get_sensor_exp_data(9, 1, 0):
    	print(item)


if __name__ == '__main__':
    test_get_exp_data()	