"""
Project Teraki csv File APIs
Author: qingzhou

it provides the APIs handling the values for one sensor
"""

import os, sys, time, json
from tk_mvdec import MultiValueDecomposer
from tk_mvrec import MultiValueReconstructor
sys.path.append('../tksvr/packages/')
sys.path.append('../tkmw/packages/')
from tk_process import SensorProcessor
from tk_cdp import DataPlane


def unicode_to_string(input_data):
    """ convert the unicode to string.
    Args: unitcode
    Returns: string
    """
    if isinstance(input_data, dict):
        return {unicode_to_string(key):unicode_to_string(value) for key, value in input_data.iteritems()}
    elif isinstance(input_data, list):
        return [unicode_to_string(element) for element in input_data]
    elif isinstance(input_data, unicode):
        return input_data.encode('utf-8')
    else:
        return input_data

def str_to_int_float(string): 
    """ convert the string to int, float.
    Args: string text format of the values
    Returns:  int, float or text.
    """
    try:
        return int(string)
    except ValueError:
        pass
    try:
        return float(string)
    except ValueError:
        return string


class SensorData(object):
    """ SensorData Class Definition
    private data:
        _read_para: the parameter for reading the file
        _headtext: the text in the headline
        _raw_data: the dictionary for the raw data
        _red_data: the dictionary for the reduced data
        _recon_data: the dictionary for the reconstructed data
    private functions:
        get_read_para(): get read parameter for reading the file
        get_red_meta_data(): get meta data for reduced data file
        get_recon_meta_data(): get meta data for reconstructed data file
        get_recon_timestamp(): get timestamp for reconstructed data
    public functions:
        get_raw_data(): get a dictionary with raw data
        get_red_data(): get a dictionary with reduced data
        get_recon_data(): get a dictionary with reconstructed data
        print_out_raw_data(): print out raw data
        print_out_red_data(): print out reduced data 
        print_out_recon_data(): print out reconstructed data
        print_out_raw_data_10(): print out first 10 raw data
        print_out_red_data_10(): print out first 10 reduced data
        print_out_recon_data_10(): print out first 10 reconstructed data
        print_out_raw_data_50(): print out first 50 raw data
        print_out_red_data_50(): print out first 50 reduced data
        print_out_recon_data_50(): print out first 50 reconstructed data
        read_headline(): read headline from the file
        read_dataline(): read dataline from the fine
        read_raw_data(): read raw data line in raw data file
        read_red_data(): read one data line in reduced data file
        read_red_values(): read reduced data line in reduced data file
        read_raw_value(): read raw data line in reduced data file
        encoding(): encoding raw data file to reduced data file
        decoding(): decoding reduced data file to reconstructed data file 
        get_red_content(): get text content for reduced data file
        get_recon_content(): get text content for reconstructed data file
    """
    def __init__(self):
        """ initiate SensorData instance
        Args: None
        Returns: None
        """ 
        self._read_para = self.get_read_para()
        self._headtext = None
        self._raw_data = {'dof':None, 'count':0, 'values':[], 'tss':[]}
        self._red_data = {'meta':{}, 'red_values':[], 'raw_value':[]}
        self._recon_data = {'recon_para':{}, 'dof':None, 'count':0, 'values':[], 'tss':[]}

    def get_min_max_value(self, values):
        min_v = values[0]
        max_v = values[0]
        for item in values:
            if item > max_v:
                max_v = item
            elif item < min_v:
                min_v = item
        return [min_v, max_v]

    def raw_data_process(self):
        values = self._raw_data['values']
        self._raw_data['min_v'] = []
        self._raw_data['max_v'] = []   
        for i in range(len(values[0])):
            div = [x[i] for x in values]
            mvs = self.get_min_max_value(div)
            self._raw_data['min_v'].append(mvs[0])
            self._raw_data['max_v'].append(mvs[1])
        return

    def sort_raw_data(self):
        """ sort the raw data based on timestamp """
        self._raw_data['values'] = [x for (y, x) in sorted(zip(self._raw_data['tss'], self._raw_data['values']))]
        self._raw_data['tss'] = sorted(self._raw_data['tss'])
        return

    def clean_duplicates(self):
        """ clean the duplicated raw data based on timestamp """
        clean_tss = []
        clean_values = []
        for i in range(len(self._raw_data['tss'])):
            if i == 0:
                clean_tss.append(self._raw_data['tss'][i])
                clean_values.append(self._raw_data['values'][i])
            elif not (self._raw_data['tss'][i-1] == self._raw_data['tss'][i]):
                clean_tss.append(self._raw_data['tss'][i])
                clean_values.append(self._raw_data['values'][i])
        self._raw_data['values'] = clean_values
        self._raw_data['tss'] = clean_tss
        self._raw_data['count'] = len(clean_tss)
        return                

    def get_raw_data(self):
        """ get a dictionary with raw data """
        return self._raw_data

    def get_red_data(self):
        """ get a dictionary with reduced data """
        try: 
            return self._org_red_data
        except:
            return None

    def get_recon_data(self):
        """ get a dictionary with reconstructed data """
        return self._recon_data

    def print_out_raw_data(self, count=100):
        """ print out raw data
        Args: 
            count: count of values to be print out
        Returns: 
            None
        """
        print("************ start printout of raw data ********************")
        print("------------ DOF : " + str(self._raw_data['dof']) + " ---------------")
        print("------------ Count : " + str(self._raw_data['count']) + " ------------")
        for i in range(count):
            if i >= len(self._raw_data['values']):
                print("************ end printout of raw data ********************")
                return
            value_text = ';'.join(str(x) for x in self._raw_data['values'][i])
            if i < len(self._raw_data['tss']):
                str_text = str(i) + ': ' + str(self._raw_data['tss'][i]) + ';' + value_text
            else:
                str_text = str(i) + ': ' + value_text
            print(str_text)
        print("************ end printout of raw data ********************")

    def print_out_red_data(self, count=100):
        """ print out reduced data
        Args: 
            count: count of values to be print out
        Returns: 
            None
        """
        print("************ start printout of reduced data ********************")
        print("-----------  Meta : " + str(self._red_data['meta']) + " -------------")
        for i in range(count):
            if i >= len(self._red_data['red_values']):
                print('Raw Value : ' + ';'.join(str(x) for x in self._red_data['raw_value'][i]))
                print("************ end printout of reduced data ********************")
                return
            print('Red Value ' + str(i) + ': ' + ';'.join(str(x) for x in self._red_data['red_values'][i]))
        print("************ end printout of reduced data ********************")

    def print_out_recon_data(self, count=100):
        """ print out reconstructed data
        Args: 
            count: count of values to be print out
        Returns: 
            None
        """
        print("************ start printout of reconstructed data ********************")
        print("-------------- ReconPara : " + str(self._recon_data['recon_para']) + " ------------")
        print("-------------- DOF : " + str(self._recon_data['dof']) + " --------------")
        print("-------------- Count : " + str(self._recon_data['count']) + " -------------")
        for i in range(count):
            if i >= len(self._recon_data['values']):
                print("************ end printout of reduced data ********************")
                return
            value_text = ';'.join(str(x) for x in self._recon_data['values'][i])
            if i < len(self._recon_data['tss']):
                str_text = str(i) + ': ' + str(self._recon_data['tss'][i]) + ';' + value_text
            if i < len(self._recon_data['tss']):
                str_text = str(i) + ': ' + value_text
            print(str_text)
        print("************ end printout of reduced data ********************")

    def print_out_raw_data_10(self):
        """ print out first 10 raw data """
        self.print_out_raw_data(10)

    def print_out_red_data_10(self):
        """ print out first 10 reduced data """
        self.print_out_red_data(10)

    def print_out_recon_data_10(self):
        """ print out first 10 reconstructed data """
        self.print_out_recon_data(10)

    def print_out_raw_data_50(self):
        """ print out first 50 raw data """
        self.print_out_raw_data(50)

    def print_out_red_data_50(self):
        """ print out first 50 reduced data """
        self.print_out_red_data(50)

    def print_out_recon_data_50(self):
        """ print out first 50 reconstructed data """
        self.print_out_recon_data(50)

    def get_read_para(self, para={}):
        """ get read parameters
        Args: read parameters
        Returns: read parameters with default values 
        """ 
        result = para
        if 'datatype' not in result.keys():
            result['datatype'] = 'raw_data'
        if 'sep' not in result.keys():
            result['sep'] = ';'
        if 'ts' not in result.keys():
            result['ts'] = 1
        return result

    def read_headline(self, text):
        """ read headline 
        Args: 
            text: text of the head line, the format of headline is
            #{JSON_FORMAT}
            the meaning of the keys is defined in the file specification document 
        Returns:
            0 if Success, and -1 if Error.
        """
        self._headtext = text
        json_data = json.loads(text[1:])
        header_data = unicode_to_string(json_data)
        self._read_para = self.get_read_para(header_data)
        if header_data['datatype'] == 'red_data':
            self._red_data['meta'] = header_data           
        return 0

    def read_dataline(self, text):
        """ read dataline from file 
        Args: 
            text: text of the data line, the format of data line is
            <value>;<value>;<value>.....;<value>
            the size of values in one line depends on the data type 
        Returns:
            0 if Success, and -1 if Error.
        """
        if self._read_para['datatype'] == 'raw_data':
            ret = self.read_raw_data(text)
        elif self._read_para['datatype'] == 'red_data':
            ret = self.read_red_data(text)
        elif self._read_para['datatype'] == 'recon_data':
            ret = self.read_raw_data(text)
        else:
            print("Reading unexpected datatype : " + self._read_para['datatype'])
            ret = -1
        return ret

    def read_raw_data(self, text):
        """ read dataline from raw data file 
        Args: 
            text: text of the data line, the format of data line is
            [<timestamp>;]<value>;<value>;<value>.....;<value>
            the size of values in one line is dof+1, or dof if timestamp not present
        Returns:
            0 if Success, and -1 if Error.
        """
        value_list = text.split(self._read_para['sep'])
        if self._read_para['ts'] == 1:
            t = str_to_int_float(value_list[0])
            self._raw_data['tss'].append(t)
            v = [str_to_int_float(element) for element in value_list[1:]]
        else:
            v = [str_to_int_float(element) for element in value_list]
        if self._raw_data['dof'] is None:
            self._raw_data['dof'] = len(v)
        elif self._raw_data['dof'] != len(v):
            print("DOF does not match!")
            return -1
        self._raw_data['values'].append(v)
        self._raw_data['count'] += 1
        return 0

    def read_red_data(self, text):
        """ read dataline from reduced data file 
        Args: 
            text: text of the data line, the format of reduced data line is
            <value>;<value>;<value>.....;<value>
            the size of values in the line is framesize * sparsity * dof
        Returns:
            0 if Success, and -1 if Error.
        """
        if text[-1] == '#':
            ret = self.read_raw_value(text)
        else:
            ret = self.read_red_values(text)
        return 0

    def read_red_values(self, text):
        """ read reduced data line from reduced data file 
        Args: 
            text: text of the data line, the format of reduced data line is
            <value>;<value>;<value>.....;<value>
            the size of values in the line is framesize * sparsity * dof
        Returns:
            0 if Success, and -1 if Error.
        """
        dof = self._red_data['meta']['dof']
        value_list = text.split(self._red_data['meta']['sep'])
        v = [str_to_int_float(e) for e in value_list]
        self._red_data['red_values'].append(v)
        return 0

    def read_raw_value(self, text):
        """ read raw data line from reduced data file 
        Args: 
            text: text of the data line, the format of raw data line is
            <value>;<value>;<value>.....;<value>#
            NOTE: '#' is the start and end mark for reduced data
        Returns:
            0 if Success, and -1 if Error.
        """
        dof = self._red_data['meta']['dof']
        value_list = text[:-1].split(self._red_data['meta']['sep'])
        v = [str_to_int_float(e) for e in value_list]
        pkg_num = len(v) / dof
        for p in range(pkg_num):
            start = p * dof
            end = (p + 1) * dof
            self._red_data['raw_value'].append(v[start:end])
        return 0

    def read_red_delta_file(self, sensor_id, filename):
        self._red_meta_data = {}
        self._red_delta = []
        f = open(filename, 'r')
        for line in f:
            text = line.rstrip()
            if len(text) < 1:
                pass
            elif text[0] == '#':
                json_data = json.loads(text[1:])
                self._red_meta_data = unicode_to_string(json_data)
            else:
                item = text.split('|')
                self._red_delta.append(item)
        f.close()
        return self._red_meta_data, self._red_delta

    def get_red_content(self): 
        """ get text content for reduced data file 
        Args: 
            None
        Returns: 
            text content can be wrote to reduced data file. the format of 
            reduced data file is defined in file specification document 
        """
        result = '#' + json.dumps(self._red_data['meta']) + '\n'
        for line_value in self._red_data['red_values']:
            red_data_line = ';'.join(str(x) for x in line_value)
            result += red_data_line + '\n'
        if len(self._red_data['raw_value']) > 0:
            raw_value_list = []
            for item in self._red_data['raw_value']:
                for value in item:
                    raw_value_list.append(value)
            raw_data_line = ';'.join(str(x) for x in raw_value_list)
            result += raw_data_line +'#\n'
        return result 

    def get_recon_content(self): 
        """ get text content for reconstructed data file 
        Args: 
            None
        Returns: 
            text content can be wrote to reconstructed data file. the format of 
            reconstructed data file is defined in file specification document 
        """
        result = '#' + json.dumps(self._recon_data['meta']) + '\n'
        for n in range(len(self._recon_data['values'])):
            line_text = ''
            if self._recon_data['meta']['ts'] == 1:
                line_text += str(self._recon_data['tss'][n]) + ';'
            recon_value_text = ';'.join(str(x) for x in self._recon_data['values'][n])
            line_text += recon_value_text + '\n'
            result += line_text
        return result 

    def get_raw_content(self): 
        """ get text content for raw data file 
        Args: 
            None
        Returns: 
            text content can be wrote to raw data file. the format of 
            raw data file is defined in file specification document 
        """
        result = ''
        for n in range(len(self._raw_data['values'])):
            line_text = ''
            if len(self._raw_data['tss']) == len(self._raw_data['values']):
                line_text += str(self._raw_data['tss'][n]) + ';'
            raw_value_text = ';'.join(str(x) for x in self._raw_data['values'][n])
            line_text += raw_value_text + '\n'
            result += line_text
        return result 

    def get_red_meta_data(self, n, s):
        """ get meta data in headline for reduced data file 
        Args: 
            n: framesize
            s: sparsity
        Returns: 
            meta data targeted to the headline of the reduced data file
        """
        result = {}
        result['datatype'] = 'red_data'
        result['e'] = 'TKA'
        result['v'] = 3
        result['n'] = n
        result['s'] = s
        result['sep'] = ';'
        result['ts'] = self._read_para['ts']
        result['dof'] = self._raw_data['dof']
        result['count'] = self._raw_data['count']
        result['org_head'] = self._headtext
        result['read_para'] = self._read_para
        if len(self._raw_data['tss']) > 0:
            result['start_ts'] = self._raw_data['tss'][0]
            result['ts_delta'] = self._raw_data['tss'][-1] - self._raw_data['tss'][0]
        return result

    def get_recon_meta_data(self):
        """ get meta data in headline for reconstructed data file 
        Args: 
            None
        Returns: 
            meta data targeted to the headline of the reconstructed data file
        """
        result = {}
        result['datatype'] = 'recon_data'
        result['e'] = 'TKA'
        result['v'] = 3
        result['n'] = self._red_data['meta']['n']
        result['s'] = self._red_data['meta']['s']
        result['ts'] = self._red_data['meta']['ts']
        result['dof'] = self._red_data['meta']['dof']
        result['count'] = self._red_data['meta']['count']
        result['org_head'] = self._red_data['meta']['org_head']
        return result

    def get_recon_timestamp(self):
        """ get reconstructed timestamp for reconstructed data file,
        the reconstructed timestamp are saved in self._recon_data['tss']
        Args: 
            None
        Returns: 
            None
        """
        if 'start_ts' not in self._red_data['meta'].keys():
            return
        if 'ts_delta' not in self._red_data['meta'].keys():
            return
        if 'count' not in self._red_data['meta'].keys():
            return
        result = []
        start_ts = self._red_data['meta']['start_ts']
        count = self._red_data['meta']['count']
        interval = self._red_data['meta']['ts_delta'] / float(count-1)
        for n in range(count):
            timestamp = start_ts + int(interval * n)
            result.append(timestamp)
        return result

    def encoding(self, n, s):
        """ encoding raw data to reduced data 
        Args: 
            n: framesize
            s: sparsity
        Returns: 
            the reduced data
        """
        self._red_data['meta'] = self.get_red_meta_data(n, s)
        red_lines = self._raw_data['count'] / n
        end_line = red_lines * n
        if end_line > 0:
            FD = MultiValueDecomposer(n, s, self._raw_data['dof'])
            self._red_data['red_values'] = FD.get_reduced_values(self._raw_data['values'])
            self._red_data['meta']['thelist'] = FD.get_list()
            self._red_data['meta']['xs1'] = FD.get_xs1()
            self._red_data['meta']['red_lines'] = red_lines
        if end_line < self._raw_data['count']:
            self._red_data['raw_value'] = self._raw_data['values'][end_line:]
            self._red_data['meta']['raw_lines'] = 1
        return self._red_data

    def decoding(self):
        """ decoding reduced data to reconstructed data 
        Args: 
            None
        Returns: 
            the reconstructed data
        """
        self._recon_data['meta'] = self.get_recon_meta_data()
        self._recon_data['tss'] = self.get_recon_timestamp()
        if len(self._red_data['red_values']) > 0:
            n = self._red_data['meta']['n']
            s = self._red_data['meta']['s']
            dof = self._red_data['meta']['dof']
            thelist = self._red_data['meta']['thelist']
            xs1 = self._red_data['meta']['xs1']
            FR = MultiValueReconstructor(n, s, dof, thelist, xs1)
            self._recon_data['values'] = FR.get_reconstruct_values(self._red_data['red_values'])
        self._recon_data['values'] += self._red_data['raw_value']
        self._recon_data['count'] = len(self._recon_data['values'])
        return self._recon_data

    def adv_decoding(self):
        n = self._red_meta_data['n']
        s = self._red_meta_data['s']
        e = self._red_meta_data['e']
        b = self._red_meta_data['b']
        po = self._red_meta_data['po']
        thelist = self._red_meta_data['l']
        Server = SensorProcessor({'framesize': n, 'reduction': 1-s, 'eng_name': e, 'block': b, 'post_process': po})
        Server._thelist = thelist
        Server._R.set_list(thelist, thelist)

        self._org_red_data = {}
        self._org_red_data['tss'] = []
        self._org_red_data['values'] = []
        self._recon_data['meta'] = self._red_meta_data
        self._recon_data['meta']['ts'] = 1
        self._recon_data['values'] = []
        self._recon_data['tss'] = []
        for item in self._red_delta:
            topic_items = [str_to_int_float(x) for x in item[0].split('/')]
            recv_data = [str_to_int_float(x) for x in item[1].split(';')]
            sensor_id = topic_items[2]
            list_id = topic_items[3]
            seq_no = topic_items[4] 
            exp_list = topic_items[5:]
            ret = Server.red_data_process(recv_data, list_id, seq_no, exp_list)
            if ret[1] and len(ret[1]) > 1:
                self._recon_data['tss'] += [x[0] for x in ret[1]]
                self._recon_data['values'] += [x[1:] for x in ret[1]]
            if ret[2]:
                self._org_red_data['tss'].append(ret[2][0])
                self._org_red_data['values'].append(ret[2][1:])
        self._org_red_data['dof'] = len(self._org_red_data['values'][0])
        self._org_red_data['count'] = len(self._org_red_data['values'])
        self._recon_data['dof'] = len(self._recon_data['values'][0])
        self._recon_data['count'] = len(self._recon_data['values'])
        return None

    def adv_encoding_decoding(self, sensor_id=0, eng_name='aii_v010', block=None, pre_process={}, post_process={}, last_frame_flag=0):
        result = {}
        self._raw_data_sent = []
    	self._red_delta = []
        self._org_red_data = {}
        self._recon_data['meta'] = self.get_recon_meta_data()
        self._recon_data['tss'] = self._raw_data['tss']
        n = self._red_data['meta']['n']
        s = self._red_data['meta']['s']
        Client = DataPlane(None, {'client_id':0, 'ts0':0, 'default_topic':'6'})
        Server = SensorProcessor({'framesize': n, 'reduction': 1-s, 'eng_name': eng_name, 'block': block, 'post_process': post_process})
        thelist = Server._thelist
        select_list = [1, n] + thelist
        config_para = {'sensors': {str(sensor_id): {'process_id':8, 'select_list': select_list, 'pre_process': pre_process, 'post_process': post_process}}}
        Client.reset_config(config_para)
        result['framesize'] = n
        result['red_framesize'] = len(thelist)
        result['thelist'] = thelist
        result['count_raw'] = 0
        result['len_raw'] = 0
        result['count_red'] = 0
        result['len_red'] = 0
        #print(sensor_id)
        #print(self._raw_data)
        for i in range(self._raw_data['count']):
            #print([sensor_id])
            #print([self._raw_data['tss'][i]])
            #print(self._raw_data['values'][i])
            #data = [sensor_id] + [self._raw_data['tss'][i]] + self._raw_data['values'][i]
            values = [float(x) for x in self._raw_data['values'][i]]
            data = [sensor_id] + [self._raw_data['tss'][i]] + values
            print(data)
            topic, payload = Client.get_topic_payload_from_sensor_data(data)
            if topic:
                self._red_delta.append((topic, payload))
                result['count_red'] += 1
                result['len_red'] += (len(topic) + len(payload))
            print(data)
            topic, payload = Client.get_topic_payload_from_sensor_raw_data(data)
            self._raw_data_sent.append((topic, payload))
            result['count_raw'] += 1
            result['len_raw'] += (len(topic) + len(payload))
        result['ratio'] = 1.0 - 1.0 * result['len_red'] / result['len_raw']
        self._red_meta_data = {}
        self._red_meta_data['n'] = n
        self._red_meta_data['s'] = s
        self._red_meta_data['e'] = eng_name
        self._red_meta_data['b'] = block
        self._red_meta_data['l'] = thelist
        self._red_meta_data['pr'] = pre_process
        self._red_meta_data['po'] = post_process

        self._org_red_data['tss'] = []
        self._org_red_data['values'] = []
        self._recon_data['values'] = []
        for item in self._red_delta:
            topic_items = [str_to_int_float(x) for x in item[0].split('/')]
            recv_data = [str_to_int_float(x) for x in item[1].split(';')]
            sensor_id = topic_items[2]
            list_id = topic_items[3]
            seq_no = topic_items[4] 
            exp_list = topic_items[5:]
            ret = Server.red_data_process(recv_data, list_id, seq_no, exp_list)
            if ret[1] and len(ret[1]) > 1:
            	self._recon_data['values'] += [x[1:] for x in ret[1]]
            if ret[2]:
                self._org_red_data['tss'].append(ret[2][0])
                self._org_red_data['values'].append(ret[2][1:])
        if last_frame_flag == 1:
            self._recon_data['values'] += self._red_data['raw_value']
        self._org_red_data['dof'] = len(self._org_red_data['values'][0])
        self._org_red_data['count'] = len(self._org_red_data['values'])
        self._recon_data['dof'] = len(self._recon_data['values'][0])
        self._recon_data['count'] = len(self._recon_data['values'])
        #print(len(self._recon_data['tss']), len(self._recon_data['values']), len(self._red_data['raw_value']), len(self._red_delta))
        if (len(self._recon_data['values']) > len(self._recon_data['tss'])):
        	self._recon_data['values'] = self._recon_data['values'][:len(self._recon_data['tss'])]
        else:
            self._recon_data['tss'] = self._recon_data['tss'][:len(self._recon_data['values'])]
        return result

    def get_red_delta_content(self): 
        result = '#' + json.dumps(self._red_meta_data) + '\n'
        for item in self._red_delta:
            result += item[0] + '|' + item[1] + '\n'
        return result 

    def print_out(self):
        """ print out data information
        Args: 
            None
        Returns: 
            None
        """
        print(self._raw_data)
        print('-'*10)
        print(self._red_data)


