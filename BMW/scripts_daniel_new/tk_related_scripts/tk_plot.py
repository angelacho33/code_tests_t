import os, sys, time, datetime
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.dates as md
import matplotlib.style
matplotlib.style.use('ggplot')
import tk_err
import numpy as np

def ts_to_dt(ts, dt_format):
    s = ts / 1000.0
    dt = datetime.datetime.fromtimestamp(s)
    return dt

value_names = {
    'emg': ['x', 'y', 'z'],
    'gps': ['lat', 'lon', 'alt']
}

units = {
    'gps': 'Degrees'
}

quantity = {
    'emg': 'Voltage',
    'acceleration': '???',
    'gps': ['deg', 'deg', 'deg']
}


def plot_boxplot_values(ax, data, color=None, **kwargs):
    """Helper function to create a box plot showing properties of plain values

    Args:
        ax (axes): The axes to draw to
        data (list or numpy array): Original (raw) data
        **kwargs (dict): Dictionary of kwargs to pass to ax.plot

    Returns:
        out (): Boxplot object handle
    
    Example:
    
    """
    meanpointprops = dict(marker='D', markerfacecolor='w')
    out = ax.boxplot(data, showmeans=True, meanprops=meanpointprops, labels=['values'], widths=None, 
                     boxprops={'color': color}, **kwargs)
    #ax.yaxis.grid(True, linestyle='-', which='major', color='lightgrey', alpha=0.5)
    return out

def plot_boxplot_residuals(ax, O, R, **kwargs):
    """Helper function to create a boxplot showing properties of the residuals (raw-rec)

    Args:
        O (list or numpy array): Original (raw) data
        R (list or numpy array): Reconstructed data
        kwargs (dict): Dictionary of kwargs to pass to ax.plot

    Returns:
        out (): Boxplot object hanndle
    
    Example:
        fig = plt.figure()
        ax = fig.add_subplot(111)
        raw = ...
        rec = ...
        plot_boxplot_residuals(ax, raw, rec)
        plt.show()
    """
    if len(O) != len(R):
        print 'Original (raw) and reconstructed data vectors need to have same length!'
    else:
        res = np.array(O) - np.array(R)
        meanpointprops = dict(marker='D', markerfacecolor='w')
        out = ax.boxplot(res, showmeans=True, meanprops=meanpointprops , labels=['residuals'],whis=[1, 99])
        #ax.yaxis.grid(True, linestyle='-', which='major', color='lightgrey', alpha=0.5)
        #ax.set_axisbelow(True)
        return out
    
class TextPlot(object):
    def __init__(self, ax=None):
        if ax == None:
            self.fig = plt.figure(figsize=(8, 6))
            self.ax = fig.add_subplot(1, 1, 1)
        else:
            self.ax = ax
        self._colors = {'raw':'g', 'rec':'b', 'red':'r', 'subsampled':'c', 'deviation':'y', 'other':'grey'}
        #ax.axes.get_xaxis().set_visible(False)
        #ax.axes.get_yaxis().set_visible(False)
        ax.set_axis_off()

    def write(self, info):
        for i, (key, value) in enumerate(info):
            string = key+':  '+str(value)
            self.ax.text(0.0, 0.9-(i*0.07), string, va='top', ha='left', transform=self.ax.transAxes, fontsize=11)

class BoxPlot(object):
    """Helper Class for creating a box plot displaying statistics for sigal values, deviations, or others

    """
    def __init__(self, ax=None):
        if ax == None:
            self.fig = plt.figure(figsize=(8, 6))
            self.ax = fig.add_subplot(1, 1, 1)
        else:
            self.ax = ax
        self._colors = {'raw':'g', 'rec':'b', 'red':'r', 'subsampled':'c', 'deviation':'y', 'other':'grey'}
        self._meanprops = dict(marker='D', markerfacecolor='w')
        
    def set_params(self, title=None):
        self.ax.set_title(title)

    def set_box_colors(self, bp, color):
        plt.setp(bp['boxes'][0], color=color)
        plt.setp(bp['whiskers'][0], color=color)
        plt.setp(bp['whiskers'][1], color=color)
        #plt.setp(bp['fliers'][0], color=color)
        #plt.setp(bp['fliers'][1], color=color)
        #plt.setp(bp['medians'][0], color=color)
    
    def boxplot(self, data, labels=None, signal_type='other', *args, **kwargs):
        bp = self.ax.boxplot(data, showmeans=True, meanprops=self._meanprops , labels=labels, *args, **kwargs)
        self.set_box_colors(bp, self._colors[signal_type])


def sliding_window_rmse(data, window_size):
    data_squared = np.power(data, 2)
    window = np.ones(window_size) / float(window_size)
    return np.sqrt(np.convolve(data_squared, window, 'valid'))


class FftPlot(object):
    def __init__(self, ax=None):
        if ax == None:
            self.fig = plt.figure(figsize=(8, 6))
            self.ax = fig.add_subplot(1, 1, 1)
        else:
            self.ax = ax
        self._colors = {'raw':'g', 'rec':'b', 'red':'r', 'sub':'c', 'dev':'y'}
        self.ax.set_title('Fourier transform')
        self.ax.set_xlabel('Frequency (Hz)')
        self.ax.set_ylabel('Amplitude')
        self._legend = True
        
    def set_params(self, title='Fourier transform', legend=True):
        self.ax.set_title(title)
        self._legend = legend
        
    def plot(self, tss, y, signal_type, skip_zero=False, yscale='linear', color=None, sw_rmse=True, *args, **kwargs):
        if skip_zero: 
            start = 1 
        else:
            start = 0

        if isinstance(tss[0], datetime.datetime):
            dt = (tss[1] - tss[0]).total_seconds()
        else:
            dt = tss[1] - tss[0]

        # For fft time deltas need to be equal, therefore check for it first
        for i in range(2, len(tss)):
            if (tss[i] - tss[i-1]) != dt:
                print 'Time stamps are not equally spaced!'
                break
        yf  = np.fft.fft(y)
        xf  = np.fft.fftfreq(len(yf), dt)
        N   = len(yf)

        # Not that xf (yf corresponginly) starts at 0 and goes to pos values, then in middle of 
        # the list it goes from 0 to negative values
        ln, = self.ax.plot( xf[start:N/2], 2.0/N * np.abs(yf[start:N/2]), color=self._colors[signal_type], label=signal_type, zorder=1, **kwargs )

        if sw_rmse == True:          # Sliding window RMSE
            import detect_peaks
            # Overplot window wise averaged RMS
            window_size = 150
            # create fecotor holding neg. and pos. fourier spectrum for RMS averaging
            tmp = np.concatenate(( yf[N/2+start:], yf[start:N/2] ), axis=0)
            windowed_rms = sliding_window_rmse(4.0/N * np.abs(tmp), window_size)

            y = windowed_rms[len(windowed_rms)/2:]        
            x = xf[:N/2][:len(y)]       # 1. take first pos half of xf, then trim off some points in the end due to window (window_size/2)

            ls = 'solid' if signal_type=='raw' else 'dashed'
            color = 'red' if signal_type=='raw' else 'orange'

            self.ax.plot( x, y, color=color, lw=1, label=signal_type, zorder=2, linestyle=ls)
            peak_idxs = detect_peaks.detect_peaks(y, mpd=400, mph=np.max(y)*0.2, edge='rising')

            self.ax.plot( x[peak_idxs], y[peak_idxs], color=color, marker='o', markersize=5, zorder=3, linestyle='None')
            self.ax.vlines(x[peak_idxs], y[peak_idxs]*0., y[peak_idxs], color=color, zorder=3, lw=1.5, linestyle=ls)

        if self._legend: self.ax.legend()
        self.ax.set_yscale(yscale)

class DeviationPlot(object):
    def __init__(self, ax=None):
        if ax == None:
            self.fig = plt.figure(figsize=(8, 6))
            self.ax = fig.add_subplot(1, 1, 1)
        else:
            self.ax = ax
        self._colors = {'raw':'g', 'rec':'b', 'red':'r', 'sub':'c', 'dev':'y'}

    def set_params(self, title=None, legend=True, dt_format='%Y-%m-%d %H:%M:%S'):
        if title != None: self.ax.set_title(title)
        self.ax.set_xlabel('Time')
        self._legend = legend
        self._dt_format = dt_format

    def plot(self, t, dev, dev_type, *args, **kwargs):
        if len(t) != len(dev):
            print 'Timestams, and data vectors need to have same length!'

        if isinstance(t[0], datetime.datetime):
            sr = str(1. / (t[1] - t[0]).total_seconds())
        else:
            sr = str(1. / t[1] - t[0])

        myplot = self.ax.plot(t, dev, color=self._colors['dev'], label=dev_type, *args, **kwargs)

        if self._legend: self.ax.legend()
    
        return(myplot)

class SignalPlot(object):

    def __init__(self, ax=None):
        if ax == None:
            self.fig = plt.figure(figsize=(8, 6))
            self.ax = self.fig.add_subplot(1, 1, 1)
        else:
            self.ax = ax
        self._colors = {'raw':'g', 'rec':'b', 'red':'r', 'sub':'c', 'dev':'y'}
        self._ls = {'raw':'solid', 'red':'dotted', 'rec':'dashed', 'sub':'dashed'}
        self._legend = True
        
    def set_params(self, title=None, quantity=None, unit=None, legend=True, dt_format='%Y-%m-%d %H:%M:%S'):
        self.ax.set_title(title)
        self.ax.set_xlabel('Time')
        self.ax.set_ylabel(quantity)
        self._legend = legend
        self._dt_format = dt_format

    def add_signal(self, t, y, signal_type, show_sr=True, *args, **kwargs):
        if isinstance(t[0], datetime.datetime):
            sr = str(1. / (t[1] - t[0]).total_seconds())
        else:
            sr = str(1. / t[1] - t[0])
            
        myplot = self.ax.plot(t, y, color=self._colors[signal_type], ls=self._ls[signal_type], label=signal_type, *args, **kwargs)

        if show_sr == True:
            self.ax.text(0.02, 0.96, 'sr: '+sr+' Hz', verticalalignment='top', 
                         horizontalalignment='left', transform=self.ax.transAxes, fontsize=11)

        if isinstance(t[0], datetime.datetime):
            ax.xaxis.set_major_formatter( md.DateFormatter(self._dt_format) )
            labels = ax.get_xticklabels()
            plt.setp(labels, rotation=25)
    
        if self._legend:
            self.ax.legend()

        return myplot

class HistPlot(object):
    pass
    
def plot_hist_of_values(ax, data, orientation='horizontal', log=True, color=None, edgecolor='w', **kwargs):
    """Helper function to create a histogram showing distribution of the input values

    Args:
        ax (Axes): The axes to draw to
        data (list or numpy array): Vecotr containing the values to histogram
 
    Returns:
        None
    """
    from matplotlib.ticker import FuncFormatter
    
    # apply weights to histogram to present in percentage
    weights = np.zeros_like(data) + 1. / np.array(data).size
    ax.hist(data, bins=50, orientation=orientation, weights=weights, log=log, color=color, edgecolor=edgecolor, **kwargs)

    ax.set_xlabel('Frequency of occurrence (%)')
    ax.set_title('Distribution of values')

class MapPlot(object):

    def __init__(self, ax=None, region=None, basemap_res='l'):
        import mpl_toolkits.basemap.pyproj as pyproj
        from mpl_toolkits.basemap import Basemap

        from matplotlib.ticker import FuncFormatter
        
        if ax == None:
            self.fig = plt.figure(figsize=(8, 6))
            self.ax = fig.add_subplot(1, 1, 1)
        else:
            self.ax = ax
        self._colors = {'raw':'g', 'rec':'b', 'red':'r', 'sub':'c', 'dev':'y'}
        self._ls = {'raw':'solid', 'red':'dotted', 'rec':'dashed', 'sub':'dashed'}
        self.ax.grid(b=None)     # turn default grid of ggplot style off

        if region == None:
            # Create region around first point of first track
            print 'Choosing default region (germany)'
            region = [[5, 47], [15.5, 55.2]] # Germany

        M = Basemap(
            projection = 'merc',
            llcrnrlon = region[0][0], llcrnrlat=region[0][1],
            urcrnrlon = region[1][0], urcrnrlat=region[1][1],
            resolution = basemap_res, #'f'
            suppress_ticks = False,
            ax = ax
        )
        self.M = M
        M.drawcoastlines(linewidth=1)
        M.drawcountries(linewidth=1)

        M.drawmeridians([6, 8, 10, 12, 14], color='lightgrey', linewidth=0.25, dashes=[0.5,100], labels=[1,0,1,0])
        M.drawparallels([46, 48, 50, 52, 54, 56], color='lightgrey', linewidth=0.25, dashes=[0.5,100], labels=[0,1,1,0])
    
#        M.drawrivers(linewidth=0.25, linestyle='solid', color='b')
#        M.shadedrelief(scale=0.5)

        xff = FuncFormatter(self._m2km)
        yff = FuncFormatter(self._m2km)
        ax.xaxis.set_major_formatter(xff)
        ax.yaxis.set_major_formatter(yff)
        ax.set_xlabel('(km)')
        ax.set_ylabel('(km)')
        ax.xaxis.tick_bottom()
        ax.yaxis.tick_left()

    def set_params(self, title=None):
        pass
        
    def _m2km(self, m, pos):
        """Format a value in metres into a string in km.
        """
        km = float(m) / 1000
        return('%.0f' % km)
    
    def add_marker(self, marker):
        for label, coord in marker.iteritems():
            pt = M.plot(coord[0], coord[1], 'ro', latlon=True)
        
    def add_track(self, tracks, signal_type='other'):
        from matplotlib.collections import LineCollection
        for label, track in tracks.iteritems():
            lat, lon = zip(*track)
#            pline = self.M.plot(lon, lat, color=self._colors[signal_type], linestyle=self._ls[signal_type], latlon=True, linewidth=1)
            
            points = np.array([lon, lat]).T.reshape(-1, 1, 2)
            segments = np.concatenate([points[:-1], points[1:]], axis=1)

            lc = LineCollection(segments, cmap=plt.get_cmap('Spectral'), norm=plt.Normalize(0, 100))

            z = np.ones(len(lon))
            z[0:500] = 90
            lc.set_array(z)
            lc.set_linewidth(2)
            
            self.ax.add_collection(lc)
        #axcb = self.fig.colorbar(lc)
#        for i in trajectorias:
#            # for each i, the x (longitude), y (latitude) and z (height)
#            # are read from a file and stored as numpy arrays
#
#            points = np.array([x, y]).T.reshape(-1, 1, 2)
#            segments = np.concatenate([points[:-1], points[1:]], axis=1)
#
#            lc = LineCollection(segments, cmap=plt.get_cmap('Spectral'),
#                                norm=plt.Normalize(250, 1500))
#            lc.set_array(z)
#            lc.set_linewidth(2)
#
#            plt.gca().add_collection(lc)
#
#        axcb = fig.colorbar(lc)
#        axcb.set_label('cota (m)')
        
        
        

def plot_map(ax, tracks=None, marker=None, region=None, basemap_res='l'):
    import mpl_toolkits.basemap.pyproj as pyproj
    from mpl_toolkits.basemap import Basemap
    from matplotlib.ticker import FuncFormatter

    ax.grid(b=None)     # turn default grid of ggplot style off

    if region == None:
        # Create region around first point of first track
        print 'Choosing default region (germany)'
        region = [[5, 47], [15.5, 55.2]] # Germany

    M = Basemap(
        projection = 'merc',
        llcrnrlon = region[0][0], llcrnrlat=region[0][1],
        urcrnrlon = region[1][0], urcrnrlat=region[1][1],
        resolution = basemap_res, #'f'
        suppress_ticks = False,
        ax = ax
    )
    
    #http://overpass-api.de/api/map?bbox=5.000,47.000,15.000,56.000
    #im = plt.imread('background.png')
    #M.imshow(im, interpolation='lanczos', origin='upper')


    #M.fillcontinents()
    M.drawcoastlines(linewidth=1)
    M.drawcountries(linewidth=1)
    #m.bluemarble(scale=0.5)

    M.drawmeridians([6, 8, 10, 12, 14], color='lightgrey', linewidth=0.25, dashes=[0.5,100], labels=[1,0,1,0])
    M.drawparallels([46, 48, 50, 52, 54, 56], color='lightgrey', linewidth=0.25, dashes=[0.5,100], labels=[0,1,1,0])
    
    M.drawrivers(linewidth=0.25, linestyle='solid', color='b')
    M.shadedrelief(scale=0.5)

    if tracks:
        for label, track in tracks.iteritems():
            lat, lon = zip(*track)
            pline = M.plot(lon, lat, 'g-', latlon=True, linewidth=1)

    if marker:
        for label, coord in marker.iteritems():
            pt = M.plot(coord[0], coord[1], 'ro', latlon=True)
    
    def m2km(m, pos):
        """Format a value in metres into a string in km.
        """
        km = float(m) / 1000
        return('%.0f' % km)

    xff = FuncFormatter(m2km)
    yff = FuncFormatter(m2km)
    ax.xaxis.set_major_formatter(xff)
    ax.yaxis.set_major_formatter(yff)
    ax.set_xlabel('(km)')
    ax.set_ylabel('(km)')
    ax.xaxis.tick_bottom()
    ax.yaxis.tick_left()

    
def plot_hist_of_timestamp_intervals(timestamps):
    diff_tss = [timestamps[1]-timestamps[0]]
    
    for i in range(1,len(timestamps)):
        diff_tss.append(timestamps[i]-timestamps[i-1]) 
        #print "This is the timestamp",diff_tss
    return diff_tss


class Plot():
    """ 
    """
    def __init__(self, databank=None):
        """Initiate Plot instance
        Args:
            None
        Returns:
            None
        """
        self._db = databank
        self._colors = {'raw':'g', 'red':'r', 'rec':'b', 'sub':'c', 'dev':'y'}
        self._ls = {'raw':'solid', 'red':'dotted', 'rec':'dashed', 'sub':'dashed'}

    def show(self):
        plt.show()

    def load_databank(self, filename):
        """Load databnk object from file into Plot object
           Set Environment variable TKDIR
        """
        import cPickle as pickle
        try:
            tkdir = os.environ['TKDIR']
        except Exception:
            print "ERROR: You need to set the environment variable TKDIR (path of your tk directory)"
        os.environ['LD_LIBRARY_PATH'] = tkdir+'/tklib'
        sys.path.append(tkdir+'/tkmw/packages')
        sys.path.append(tkdir+'/tksvr/packages')
        
        databank = pickle.load( open(filename, "rb") )
        self._db = databank

    def plot(self, sensor_id=0, process_id=None, dt_format='%Y-%m-%d %H:%M:%S', deviation=None, suptitle=None, colors=None, save_png=False):
        import matplotlib.dates as md
        """Plot data for individual sensor

        Args:
            path: string
                   The path including filename and extension
            sensor_id: string
                ID of sensor to be plotted
        
            deviation: string
                Deviation kind: 'ratio','difference', 'rel_error' or 'all'
        """
        data = self._db.get_sensor_process_info(sensor_id, process_id=process_id)
        sensor_name = self._db.get_sensor_info(sensor_id)['sensor_name']
                
        raw_data = data['sensor_data'].get_raw_data()
        recon_data = data['sensor_data'].get_recon_data()
        
        if colors == None:
            colors = self._colors
        
        dof = raw_data['dof']
        if deviation==None:
            fig, ax = plt.subplots(dof,sharex=True)
        else:
            if deviation=='all':
                fig, ax = plt.subplots(4,dof,figsize=(20, 7),sharex=True)
                plt.subplots_adjust(left=0.125,right=0.9,bottom=0.1,top=0.9,wspace=0.3,hspace=0.0)
            else:
                fig, ax = plt.subplots(2*dof, sharex=True, gridspec_kw={'height_ratios': [0.7, 0.3]*dof})
                plt.subplots_adjust(left=0.125, right=0.9, bottom=0.1, top=0.9, wspace=0.3, hspace=0.2)
        plt.xticks( rotation=25 )
        xfmt = md.DateFormatter(dt_format)
        
        #if dof==1:
        ax = np.array([ax])
            
        for i in range(dof):        # loop over components (coordinates)
            tss_raw   = raw_data['tss']
            tss_recon = recon_data['tss']
            component_raw   = list(zip(*raw_data['values'])[i])
            component_recon = list(zip(*recon_data['values'])[i])
            
            # reconstructed might contain fewer data points (due to last uncomplete frame) therefore trim raw
            n = len(tss_recon)
            tss_raw         = tss_raw[0:n]
            component_raw   = component_raw[0:n]
            component_recon = component_recon[0:n]
            
            tss_raw_dt = [ts_to_dt(elm, dt_format) for elm in tss_raw]
            tss_recon_dt = [ts_to_dt(elm, dt_format) for elm in tss_recon]
            
        
            #Interpolated recontructed component
            component_recon_interpol = tk_err.interpolate(tss_raw, tss_recon, component_recon)
            #Convert lists to numpy arrays
            component_raw_array=np.asarray(component_raw)
            component_recon_array=np.asarray(component_recon)
            component_recon_interpol_array=np.asarray(component_recon_interpol)
            

            if(deviation==None):
                #Plot components and no deviations
                if(dof==1):
                    axd=ax[0]
                else:
                    axd=ax[0][i]
                axd.plot(tss_raw_dt, component_raw, label='raw', color=self._colors['raw'], ls=self._ls['raw'], lw=1)
                axd.plot(tss_recon_dt, component_recon, label='rec', color=self._colors['rec'], ls=self._ls['rec'], lw=1)
                axd.set_title(raw_data['value_names'][i])
                axd.set_ylabel(raw_data['quantity'][i])
                axd.xaxis.set_major_formatter(xfmt)
                axd.legend()
                
            else:
                ##Plot components and ratio
                j=2*i
                k=2*i+1
                ##Components plot
                if (deviation=='all'):
                    if(dof==1):
                        ax0=ax[0][0]
                    else:
                        ax0=ax[0][0][i]
                    ax0.plot(tss_raw_dt, component_raw, label='raw', color=self._colors['raw'], ls=self._ls['raw'], lw=1)
                    ax0.plot(tss_recon_dt, component_recon, label='rec', color=self._colors['rec'], ls=self._ls['rec'], lw=1)
                    ax0.set_title(raw_data['value_names'][i])
                    ax0.set_ylabel(raw_data['quantity'][i])
                    ax0.xaxis.set_major_formatter(xfmt)
                    ax0.legend()
                else:
                    ax[0][j].plot(tss_raw_dt, component_raw, label='raw', color=self._colors['raw'], ls=self._ls['raw'], lw=1)
                    ax[0][j].plot(tss_recon_dt, component_recon, label='rec', color=self._colors['rec'], ls=self._ls['rec'], lw=1)
                    ax[0][j].set_title(raw_data['value_names'][i])
                    ax[0][j].set_ylabel(raw_data['quantity'][i])
                    ax[0][j].xaxis.set_major_formatter(xfmt)
                    ax[0][j].legend()
                
                if(deviation=='ratio'):
                    ##Ratio plot
                    ax[0][k].plot(tss_recon_dt,component_raw_array/component_recon_interpol_array,
                               label='', color='r', ls=self._ls['raw'],)
                    ax[0][k].axhline(y=1.0,color='k',lw=1,ls="dashed",label='raw/rec=1') #Constant line at raw/rec=1
                    ax[0][k].set_ylabel('Ratio raw/rec')
                    ax[0][k].xaxis.set_major_formatter(xfmt)
#                    ax[0][k].set_ylim([-1000, 1000])
#                    ax[0][k].legend(loc=1)
                    
                if(deviation=='difference'):
                    ##Difference plot
                    ax[0][k].plot(tss_recon_dt,component_raw_array-component_recon_interpol_array,'ro',
                                  label='', color=colors['dev'], ls=self._ls['raw'],)
                    ax[0][k].axhline(y=0.0,color='k',lw=1,ls="dashed",label='raw-rec=0') #Constant line at raw/rec=0
                    ax[0][k].set_ylabel('raw-rec')
                    ax[0][k].xaxis.set_major_formatter(xfmt)
                    #ax[k].set_ylim([-100, 100])
#                    ax[0][k].legend(loc=1)
                    
                if (deviation=='rel_error'):
                    ##Rel. error plot
                    ax[0][k].plot(tss_recon_dt,100*np.abs(component_raw_array-component_recon_interpol_array)/component_raw_array, 
                          label='', color='r', ls=self._ls['raw'],)
                    ax[0][k].axhline(y=0.0,color='k',lw=1,ls="dashed",label='Rel. error(%)=0') #Constant line at raw/rec=0
                    ax[0][k].set_ylabel('Relative error (%)')
                    ax[0][k].xaxis.set_major_formatter(xfmt)
#                    ax[0][k].set_ylim([-100, 100])
#                    ax[0][k].legend(loc=1)
                
                elif(deviation=='all'):
                    
                    if(dof==1):
                        ax1=ax[0][1]
                        ax2=ax[0][2]
                        ax3=ax[0][3]
                    else:
                        ax1=ax[0][1][i]
                        ax2=ax[0][2][i]
                        ax3=ax[0][3][i]
                    ##Ratio plot
                    ax1.plot(tss_recon_dt,component_raw_array/component_recon_interpol_array,
                                  label='Ratio raw/rec', color='r', ls=self._ls['raw'],)
                    ax1.axhline(y=1.0,color='k',lw=1,ls="dashed",label='raw/rec=1') #Constant line at raw/rec=1
                    ax1.set_ylabel('Ratio raw/rec')
#                    ax1.set_ylim([-1000, 1000])
#                    ax1.legend(loc=1)
                    
                    #Difference plot
                    ax2.plot(tss_recon_dt,component_raw_array-component_recon_interpol_array, 
                                  label='raw-rec', color='r', ls=self._ls['raw'],)
                    ax2.axhline(y=0.0,color='k',lw=1,ls="dashed",label='raw-rec=0') #Constant line at raw/rec=0
                    ax2.set_ylabel('raw-rec')
                    #ax[2][i].set_ylim([-100, 100])
#                    ax2.legend(loc=1)
                    
                    #Relative error plot
                    ax3.plot(tss_recon_dt,100*np.abs(component_raw_array-component_recon_interpol_array)/component_raw_array, 
                                  label='Rel. error(%)', color='r', ls=self._ls['raw'],)
                    ax3.axhline(y=0.0,color='k',lw=1,ls="dashed",label='Rel. error(%)=0') #Constant line at raw/rec=0
                    ax3.set_ylabel('Relative error (%)')
#                    ax3.set_ylim([-100, 100])
#                    ax3.legend(loc=1)
                    
        
        plt.xlabel('Time')
        
        # Useful info to put in the title
        framesize = data['framesize']
        reduction = data['reduction']
        deviations = self._db.get_deviations(sensor_id, process_id)
        #relerr=round(deviations['relerr'][i],2)
        title = sensor_name+'; Framesize='+str(framesize)+', Reduction='+str(reduction)#+' , Relerr%= '+str(relerr) 

                
        if suptitle == None:
            #suptitle = sensor_name
            suptitle = title
        fig.suptitle(suptitle, fontsize=11)

        # Option to produce a png figure with a 'formatted' name
        if save_png==True:
            output_name='sensor_'+str(sensor_id)+'_processID_'+str(process_id)+'_'+str(framesize)+'_'+str(reduction)+'.png'
            plt.savefig(output_name)
            
        plt.show()


    def plot_raw_inspection(self, sensor_id, output='screen', fft_yscale='linear', dt_format='%Y-%m-%d %H:%M:%S'):
        """Plot different graphs in one figure to get a first insight into raw data

        
        Args:
            sensor_id (int): Sensor ID as set when loading data into databank.
            output ([string]): Optional. Output can be 'screen' (default) or 'png'.
            
        Returns:
            ln (line): line object handle
        
        """
        raw = self._db.get_sensor_raw_data(sensor_id)
        ts = raw['tss']
        values = zip(*raw['values'])

        if output == 'screen':
            ratio = [16, 9] #[4, 3]
            scale = 0.9  #2.5
            figsize = [x * scale for x in ratio]
            dpi = None
        elif output == 'png':
            ratio = [16, 9] #[4, 3]
            scale = 0.8
            figsize = [x * scale for x in ratio]
            dpi = 150
        else:
            print 'Output not supported:', output

        for i in range(raw['dof']):
            fig = plt.figure(figsize=figsize, dpi=dpi, facecolor='w') # figsize default: (8, 6), dpi=150
            fig.suptitle(raw['sensor_name'], fontsize=14)

            gs = gridspec.GridSpec(2, 3)
            if isinstance(ts[0], datetime.datetime):
                # x axis labels are longer and will be rotated, therefore increase space
                gs.update(hspace=0.45)
            else:
                gs.update(hspace=0.3)
            
            ax1 = plt.subplot(gs[0:-1, :-1])
            ax2 = plt.subplot(gs[-1:, :-1])
            ax3 = plt.subplot(gs[0:-1, -1:])
            ax4 = plt.subplot(gs[-1:, -1:])

            # The signal
            sp = SignalPlot(ax=ax1)
            sp.set_params(title=raw['value_names'][i], quantity=raw['quantity'][i], dt_format=dt_format)
            sp.add_signal(ts, values[i], 'raw')

            # Fourier spectrum
            fftp = FftPlot(ax2)
            fftp.plot(ts, values[i], 'raw', skip_zero=True, yscale=fft_yscale)

            # Plot either geographic map or distribution (hist) of values, depending on sensor_type.
            if raw['sensor_type'] == 'gps':
                track = {'1': raw['values']}
                plot_map(ax3, tracks=track, marker=None, region=None, basemap_res='l')
            else:
                # Distribution of values
                plot_hist_of_values(ax3, values[i], color=self._colors['raw'])
                ax3.yaxis.tick_right()      #ax3.tick_params(labelleft='off')

            # Boxplot for raw values            
            bp = BoxPlot(ax4)
            bp.boxplot(values[i], signal_type='raw')
            ax4.yaxis.tick_right()

            if output == 'png':
                if not os.path.exists('./figures'):
                    os.makedirs('./figures')
                fig.savefig('./figures/'+raw['sensor_name']+'_'+raw['value_names'][i]+'.png', dpi=dpi)
                plt.close(fig)
                
        if output == 'screen':
            plt.show()






    def plot_rec_inspection(self, sensor_id, process_id, output='screen', fft_yscale='linear', dt_format='%Y-%m-%d %H:%M:%S'):
        """Plot different graphs in one figure to get a first insight into raw data

        Args:
            sensor_id (int): Sensor ID as set when loading data into databank.
            output ([string]): Optional. Output can be 'screen' (default) or 'png'.
            
        Returns:
            ln (line): line object handle
        
        """
        raw = self._db.get_sensor_raw_data(sensor_id)
        raw_tss = raw['tss']
        raw_values = zip(*raw['values'])
        
        rec = self._db.get_sensor_recon_data(sensor_id, process_id)
        rec_tss = rec['tss']
        rec_values = zip(*rec['values'])
        
        sensor_process_info = self._db.get_sensor_process_info(sensor_id, process_id)
        
        if output == 'screen':
            ratio = [16, 9] #[4, 3]
            scale = 0.95  #2.5
            figsize = [x * scale for x in ratio]
            dpi = None
        elif output == 'png':
            ratio = [16, 9] #[4, 3]
            scale = 0.8
            figsize = [x * scale for x in ratio]
            dpi = 150

        for i in range(raw['dof']):
            fig = plt.figure(figsize=figsize, dpi=dpi, facecolor='w') # figsize default: (8, 6), dpi=150
            fig.suptitle(raw['sensor_name'], fontsize=14)

            gs = gridspec.GridSpec(4, 4)
            if isinstance(raw_tss[0], datetime.datetime):
                # x axis labels are longer and will be rotated, therefore increase space
                gs.update(hspace=0.45)
            else:
                gs.update(hspace=0.5)
            
            ax1 = plt.subplot(gs[0:-2, :-2])
            ax2 = plt.subplot(gs[2:-1, :-2], sharex=ax1)
            ax3 = plt.subplot(gs[-1:, :-2], sharex=ax1)
            
            ax4 = plt.subplot(gs[0:-2, -2:])
            ax5 = plt.subplot(gs[-2:-1, -2:-1])
            ax6 = plt.subplot(gs[-1:, -2:-1])
            ax7 = plt.subplot(gs[-2:, -1:])

            # The signal
            sp = SignalPlot(ax=ax1)
            sp.set_params(title=raw['value_names'][i], quantity=raw['quantity'][i], dt_format=dt_format)
            sp.add_signal(raw_tss, raw_values[i], 'raw')
            sp.add_signal(rec_tss, rec_values[i], 'rec')
            ax1.tick_params(labelbottom='on')
            
            # Deviations
            n = len(rec_values[i])
            diff = tk_err.e(raw_values[i][:n], rec_values[i])
            rel_diff = diff / raw_values[i][:n] * 100


            dp1 = DeviationPlot(ax2)
            dp1.set_params(legend=True, dt_format='%Y-%m-%d %H:%M:%S')
            dp1.plot(raw_tss[:n], diff, 'difference')
            dp1.ax.tick_params(labelbottom='off')

            dp2 = DeviationPlot(ax3)
            dp2.set_params(legend=True, dt_format='%Y-%m-%d %H:%M:%S')
            dp2.plot(raw_tss[:n], rel_diff, 'relative difference')
            dp2.ax.set_ylabel('Diff. (%)')

            # Plot either geographic map or distribution (hist) of values, depending on sensor_type.
            if raw['sensor_type'] == 'gps':
                # GPS
                raw_track = {'raw': raw['values']}
                rec_track = {'rec': rec['values']}
#                plot_map(ax4, tracks=track, marker=None, region=None, basemap_res='l')
                mp = MapPlot(ax=ax4, region=None, basemap_res='l')
                mp.add_track(raw_track, signal_type='raw')
                mp.add_track(rec_track, signal_type='rec')
            else:
                # Fourier spectrum
                fftp = FftPlot(ax4)
                fftp.plot(raw_tss, raw_values[i], 'raw', skip_zero=True, yscale=fft_yscale)
                fftp.plot(rec_tss, rec_values[i], 'rec', skip_zero=True, yscale=fft_yscale)
                fftp.ax.yaxis.tick_right()
                fftp.ax.yaxis.set_label_position('right')

            # Boxplot for raw values
            bp = BoxPlot(ax5)
            bp.boxplot(diff, signal_type='rec', labels=['difference'])
            bp.ax.tick_params(labelleft='off')

            bp = BoxPlot(ax6)
            bp.boxplot(rel_diff, signal_type='rec', labels=['rel. difference'])
            bp.ax.tick_params(labelleft='off')

            # Print some error measures into the plot.
            tp = TextPlot(ax7)
            info = [
                ('frame_size', sensor_process_info['framesize']),
                ('reduction', sensor_process_info['reduction']),
                ('eng_name', sensor_process_info['eng_name']),
                ('RRMSE%', sensor_process_info['deviations']['RRMSE%'][i]),
                ('MAPE%', sensor_process_info['deviations']['MAPE%'][i]),
                ('SMAPE%', sensor_process_info['deviations']['SMAPE%'][i]),
                ('WAPE%', sensor_process_info['deviations']['WAPE%'][i])
            ]
            tp.write(info)

            if output == 'png':
                if not os.path.exists('./figures'):
                    os.makedirs('./figures')
                fig.savefig('./figures/'+raw['sensor_name']+'_pid'+str(rec['process_id'])+'_'+raw['value_names'][i]+'.png', dpi=dpi)
                plt.close(fig)
                
        if output == 'screen':
            plt.show()

    def plot_difference_histogram(self, sensor_id=None, process_id=None):
        data = self._db.get_sensor_process_info(sensor_id, process_id=process_id)
        raw_data = data['sensor_data'].get_raw_data()
        devs = data['deviations']

        dof = raw_data['dof']
        fig, ax = plt.subplots(dof, sharex=True)

        for i in range(dof):        # loop over components (coordinates)        
            e = devs['E'][i]
            values_component = devs['values_component'][i]
            e_hist_norm = e['hist']
            e_bin_edges = e['bin_edges']

            width = 0.95 * (e_bin_edges[1] - e_bin_edges[0])
            center = (e_bin_edges[:-1] + e_bin_edges[1:]) / 2.
            ax[i].bar(center, e_hist_norm, width=width, align='center')

            ax[i].set_title('component: '+str(values_component))
            ax[i].set_ylabel( 'Frequency (%)' )

            text_str = 'Differences:\n'+'min: '+str(round(e['min'],2))+'\n'+'max: '+str(round(e['max'],2))+'\n'+'mean: '+str(round(e['mean'],2))+'\n'+'median: '+str(round(e['median'],2))
            ax[i].text(0.025, 0.95, text_str, transform=ax[i].transAxes, verticalalignment='top', horizontalalignment='left', bbox=dict(facecolor='lightgrey', edgecolor='white', alpha=1))

        fig.suptitle('Frequency of occurrence of differences', fontsize=14)
        plt.show()


    def print_deviations_as_table(self, sensor_id=None, out='stdout'):
        if out == 'stdout':
            print "-"*170
            print 'sensor_id'.ljust(11), 'process_id'.ljust(11), 'reduct.'.ljust(9), 'frames.'.ljust(9), 'MAPE%'.ljust(10), 'WPE%'.ljust(10), 'WAPE%'.ljust(10), 'RRMSE%'.ljust(10), 'relerr%'.ljust(10), 's. type1'.ljust(10), 's. type2'.ljust(10), 'engine'.ljust(12), 'v. comp.'.ljust(12), 'sparsity'.ljust(10), 'max_diff'.ljust(12)
            print "-"*170
            for sensor_process_info in self._db.get_sensor_process_info(sensor_id):
                tmp = sensor_process_info['deviations'].copy()
                process_id = sensor_process_info['process_id']
                for i in range(len(tmp['sensor_id'])):
                    print str(tmp['sensor_id'][i]).ljust(11), str(process_id).ljust(11), str(tmp['reduction'][i]).ljust(9), str(tmp['framesize'][i]).ljust(9), str(round(tmp['MAPE%'][i], 3)).ljust(10), str(round(tmp['WPE%'][i], 3)).ljust(10), str(round(tmp['WAPE%'][i], 3)).ljust(10), str(round(tmp['RRMSE%'][i], 3)).ljust(10), str(round(tmp['relerr'][i], 3)).ljust(10), str(tmp['series_type1'][i]).ljust(10), str(tmp['series_type2'][i]).ljust(10), str(tmp['engine'][i]).ljust(12), str(tmp['values_component'][i]).ljust(12), str(tmp['sparsity'][i]).ljust(10), str(round(tmp['E'][i]['max'], 5)).ljust(12)
            print "-"*170        

        if out == 'xlsx':
            import xlsxwriter
            workbook = xlsxwriter.Workbook('deviations_sid'+str(sensor_id)+'.xlsx')
            worksheet = workbook.add_worksheet('A')
            header = ['sensor_id', 'process_id', 'reduction', 'framesize', 'MAPE%', 'WPE%', 'WAPE%', 'RRMSE%', 'relerr%', 'series_type1', 'series_type2', 'engine', 'values_component', 'sparsity', 'max']
            for i, key in enumerate(header):
                worksheet.write(0, i, key) #(row, col, key)

            row = 1
            for sensor_process_info in self._db.get_sensor_process_info(sensor_id):
                tmp = sensor_process_info['deviations'].copy()
                for i in range(len(tmp['sensor_id'])):        
                    worksheet.write(row, 0, tmp['sensor_id'][i])
                    worksheet.write(row, 1, sensor_process_info['process_id'])
                    worksheet.write(row, 2, tmp['reduction'][i])
                    worksheet.write(row, 3, tmp['framesize'][i])
                    worksheet.write(row, 4, tmp['MAPE%'][i])
                    worksheet.write(row, 5, tmp['WPE%'][i])
                    worksheet.write(row, 6, tmp['WAPE%'][i])
                    worksheet.write(row, 7, tmp['RRMSE%'][i])
                    worksheet.write(row, 8, tmp['relerr'][i])
                    worksheet.write(row, 9, tmp['series_type1'][i])
                    worksheet.write(row, 10, tmp['series_type2'][i])
                    worksheet.write(row, 11, tmp['engine'][i])
                    worksheet.write(row, 12, tmp['values_component'][i])
                    worksheet.write(row, 13, tmp['sparsity'][i])
                    worksheet.write(row, 14, tmp['E'][i]['max'])
                    row += 1
            workbook.close()

        
    def print_summary_sensors(self):
        D = self._db
        n_sensors = len(D.get_sensor_list())
        print "-------------------------------------------------------------"
        print "Total number of sensors: ", n_sensors #Number of sensors
        sensor_data=[]
        Total_points=0
        for i in range(n_sensors):
            ID=D.get_sensor_list()[i]['sensor_id']
            sensor_name = D.get_sensor_list()[i]['sensor_name']
            N_points=D.get_sensor_raw_data(ID)['count']
            Total_points+=N_points
            sensor_i_data=(ID,N_points)
            sensor_data.append(sensor_i_data)

        dtype = [('sensor_id', int), ('sensor_points', int)]
        sensor_data_array=np.array(sensor_data,dtype=dtype)
        sensor_data_array_sort=np.sort(sensor_data_array,order='sensor_points')
        sensor_data_array_sort=sensor_data_array_sort[::-1]
        print 'Total points:', Total_points
        print "-------------------------------------------------------------"
        #print 'sensor_id  ','  Npoints','  %', '  cum %'
        print 'sensor_id'.ljust(10),"# points per sensor".ljust(20),"%".ljust(10), "cum %".ljust(10)
        cum=0
        for i in range(n_sensors):
            ID=sensor_data_array_sort[i][0]
            number_points=sensor_data_array_sort[i][1]
            percentage=100.*sensor_data_array_sort[i][1]/Total_points
            cum+=100.*sensor_data_array_sort[i][1]/Total_points
            #print sensor_data_array_sort[i][0], sensor_data_array_sort[i][1], 100.*sensor_data_array_sort[i][1]/Total_points, cum
            print repr(ID).ljust(10), repr(number_points).ljust(20), ("%.3f" % percentage).ljust(10), ("%.3f" % cum).ljust(10)
        print "-------------------------------------------------------------"


    def print_sensor_summary(self):
        # Replacement for print_summary_sensors() above
        D = self._db
        sensor_list = D.get_sensor_list()
        n_sensors = len(sensor_list)

        sensor_stats = {'min_values': [], 'max_values': [], 'mean_values': []}
        sensor_stats['sensor_id'] = [elm['sensor_id'] for elm in sensor_list]
        sensor_stats['sensor_type'] = [D.get_sensor_raw_data(sensor_id)['sensor_type']for sensor_id in sensor_stats['sensor_id']]
        sensor_stats['sensor_name'] = [D.get_sensor_raw_data(sensor_id)['sensor_name']for sensor_id in sensor_stats['sensor_id']]
        sensor_stats['n'] = [D.get_sensor_raw_data(sensor_id)['count'] for sensor_id in sensor_stats['sensor_id']]
        n_total = sum(sensor_stats['n'])
        sensor_stats['%'] = [ n/float(n_total)*100 for n in sensor_stats['n'] ]

        sensor_stats['tss_min'] = [D.get_sensor_raw_data(sensor_id)['tss'][0] for sensor_id in sensor_stats['sensor_id']]
        sensor_stats['tss_max'] = [D.get_sensor_raw_data(sensor_id)['tss'][-1] for sensor_id in sensor_stats['sensor_id']]
        ##/1000 to get value in seconds if the input is ms of course :P
        sensor_stats['timespan'] = [(D.get_sensor_raw_data(sensor_id)['tss'][-1]-D.get_sensor_raw_data(sensor_id)['tss'][0])/1000. for sensor_id in sensor_stats['sensor_id']]
        #print sensor_stats['timespan']
        sensor_stats['sampling_rate'] = np.asarray(sensor_stats['n'])/np.asarray(sensor_stats['timespan'])
        #print sensor_stats['sampling_rate']
        
        #print sensor_stats['tss_min']
         
        # Derive min, max, mean of values
        for sensor_id in sensor_stats['sensor_id']:
            raw_data = D.get_sensor_raw_data(sensor_id)['values']
            # Derive for each component (coordinate)
#            print 'sid, min_v >>>>>', sensor_id, D.get_sensor_raw_data(sensor_id)['min_v']
            sensor_stats['min_values'].append( [round(min(comp_values), 3) for comp_values in zip(*raw_data)] )
            sensor_stats['max_values'].append( [round(max(comp_values), 3) for comp_values in zip(*raw_data)] )
            sensor_stats['mean_values'].append( [round(sum(comp_values)/float(len(comp_values)), 3) for comp_values in zip(*raw_data)] )
        
        # Sort accroding to percentage "column"
        idx_for_sorting = sorted( range(len(sensor_stats['%'])), key=lambda k: sensor_stats['%'][k], reverse=True)
        for key in sensor_stats.keys():
            sensor_stats[key] = [sensor_stats[key][idx] for idx in idx_for_sorting]

        # Calculate cumulative percentage
        tmp = 0; cum = []
        for elm in sensor_stats['%']:
            tmp = tmp + elm
            cum.append(tmp)
        sensor_stats['cum_%'] = cum

        # Print out data as table
        print "-"*230
        print "Total number of sensors: ", n_sensors
        print 'Total points:', n_total
        print "-"*230
        print 'sensor_id'.ljust(11), 'n'.ljust(9), '%'.ljust(9), 'cum_%'.ljust(10),'timespan(s)'.ljust(15),'sampling rate(Hz)'.ljust(20),'min_values (round)'.ljust(20), 'max_values (round)'.ljust(20), 'mean_values (round)'.ljust(20), 'sensor_type'.ljust(20), 'sensor_name'.ljust(20)
        print "-"*230
        for i in range(n_sensors):
            print str(sensor_stats['sensor_id'][i]).ljust(11), str(sensor_stats['n'][i]).ljust(9), str(round(sensor_stats['%'][i], 2)).ljust(9), str(round(sensor_stats['cum_%'][i], 2)).ljust(10), str(sensor_stats['timespan'][i]).ljust(15),str(round(sensor_stats['sampling_rate'][i],3)).ljust(20),str(sensor_stats['min_values'][i]).ljust(20), str(sensor_stats['max_values'][i]).ljust(20), str(sensor_stats['mean_values'][i]).ljust(20), sensor_stats['sensor_type'][i].ljust(20), sensor_stats['sensor_name'][i].ljust(20)
        print "-"*230    
        
def test2():
    import scipy.signal as signal
    
    def smothing_average(data, n_average):
        n_average = 9              # Number of points in window to average
        raw_mean = np.convolve(data, np.ones(n_average)/n_average, mode='same')
    
    
    def gen_sin():
        dt            = 0.01    # in s
        sample_length = 30       # in s
        frequency     = 1       # in Hz
        amplitude     = 1

        # relative timestamps in ms
        #t = np.linspace(0, sample_length * 1000, sample_length/dt+1, dtype=np.int)  # im ms
        #y = np.sin( t * 2 * np.pi * frequency / 1000)

        N = 600
        # sample spacing
        T = 1.0 / 800.0
        x = np.linspace(0.0, N*T, N)
        y = np.sin(50.0 * 2.0*np.pi*x) + 0.5*np.sin(80.0 * 2.0*np.pi*x)
        t = x
    
        return t, y
    
    colors = {'raw':'g', 'red':'r', 'rec':'b', 'sub':'c', 'dev':'y'}
    ls = {'raw':'solid', 'red':'dotted', 'rec':'dashed', 'sub':'dashed'}
    
    # Generate initial noise fee signal
    t, ref = gen_sin()

    # Generate "raw" signal containing noise
    SNR = 10
    np.random.seed(1)
    raw = ref + np.random.normal(0, 1./SNR, len(ref))

    # Get "mean" signal by ablying a averageing sliding window
    n_ave = 9                   # Number of points in window to average
    raw_mean = np.convolve(raw, np.ones(n_ave)/n_ave, mode='same')
    

    # First, design the Buterworth filter
    N  = 2    # Filter order
    Wn = 0.05 # Cutoff frequency
    B, A = signal.butter(N, Wn, output='ba')
 
    # Second, apply the filter
    raw_buterworth = signal.filtfilt(B, A, raw)


    dof = 1

    # 2 rows, 2:1
    #gs = gridspec.GridSpec(3, 1)
    #gs.update(hspace=0.1)
    #ax1 = plt.subplot(gs[:-1, :])
    #ax2 = plt.subplot(gs[-1:, :])

    # 2 rows, 3:1
    #gs = gridspec.GridSpec(4, 1)
    #gs.update(hspace=0.15)
    #ax1 = plt.subplot(gs[:-1, :])
    #ax2 = plt.subplot(gs[-1:, :])

    output = 'screen'
    if output == 'screen':
        ratio = [16, 9] #[4, 3]
        scale = 0.9  #2.5
        figsize = [x * scale for x in ratio]
        dpi = None
    elif output == 'png':
        figsize = (8, 6)
        dpi = 200

    fig = plt.figure(figsize=figsize, dpi=dpi, facecolor='w') # figsize default: (8, 6), dpi=150

    gs = gridspec.GridSpec(3, 4)
    #gs.update(hspace=0.1)
    ax1 = plt.subplot(gs[:-1, :-1])
    ax2 = plt.subplot(gs[-1:, :-1], sharex=ax1)
    ax3 = plt.subplot(gs[:-1, -1:])
    ax4 = plt.subplot(gs[-1:, -1:])

    ax3.yaxis.tick_right()
    ax4.yaxis.tick_right()


    ax1.plot(t, raw-0.5, label='raw', color=colors['raw'])
    ax1.plot(t, ref, label='ref', color='m')
    ax1.plot(t, raw_mean, label='average')
    ax1.plot(t, raw_buterworth, label='buterw')
    ax1.legend()

    dp = DeviationPlot(ax2)
    dp.set_params(title='Deviations', legend=True, dt_format='%H:%M:%S')
    dp.plot(t, raw, raw_mean, dev_type='residuals')
    
    fftp = FftPlot(ax3)
    fftp.plot(t, raw, 'raw', skip_zero=True, yscale=linear)
    
    
    plot_boxplot_residuals(ax4, raw, raw_mean)

    plt.tight_layout()
    plt.show()
    

def test_plot_map():
    gps_track = [
        [48.24626862, 11.64362013],
        [48.24161549, 11.64481656],
        [48.24580033, 11.64041363],
        [48.24371667, 11.64042168],
        [48.24346739, 11.6402962] ,
        [48.23065624, 11.63390919],
        [48.22303072, 11.62799115],
        [48.22298705, 11.62768948],
        [48.22661247, 11.61383839],
        [48.22684868, 11.61278747],
        [48.22840687, 11.60335011],
        [48.22846664, 11.60270672],
        [48.22866713, 11.58714069],
        [48.2261472,  11.54085506],
        [48.22425967, 11.5280481] ,
        [48.21983889, 11.51283084],
        [48.21411279, 11.50129692],
        [48.21390341, 11.50096005],
        [48.21056306, 11.49438789],
        [48.21046851, 11.4940967] ,
        [48.2103753,  11.49380987],
        [48.21022384, 11.49334359],
        [48.20919756, 11.48380087],
        [48.20941717, 11.46596904],
        [48.20600163, 11.45880989],
        [48.1999773,  11.43687931],
        [48.18815714, 11.43697604],
        [48.18075768, 11.4310502] ,
        [48.17164001, 11.4467055] ,
        [48.17149979, 11.44690617],
        [48.16972073, 11.44958326],
        [48.16607921, 11.45464451],
        [48.16429135, 11.45631929],
        [48.16181785, 11.45638769],
        [48.15592076, 11.45754833],
        [48.15589687, 11.45754456],
        [48.15209492, 11.45654209],
        [48.14984857, 11.4552149]
    ]
    
    region = [[5, 47], [15.5, 55.2]]

    marker = {
        'Munich': [11.64362013, 48.24626862]
    }

    tracks = {
        '1': gps_track
    }

    fig, ax = plt.subplots(figsize=(7, 8))
    #plot_map(ax, tracks=tracks, marker=marker, region=region)
    plot_map(ax, tracks=None, marker=marker, region=region)
    
    plt.show()
    
def test_plot_raw_inspect():
    from tools import tk_databank
    from tools import tk_gdfr

    D = tk_databank.DataBank()

    filename = '../../BMW/data/use_case_1/probes/highway_probes/2eef208-63c2-4a9f-8098-28d8207d1bb_1.csv'
    sensor_data = tk_gdfr.get_ts_value_from_file(filename, sep=',', pos_sid=0, pos_mt=2, pos_ts=1, pos_value=[3,4])
    D.add_sensor_data(sensor_data['pos']['values'], sensor_data['pos']['tss'], 0, filename, sensor_type='gps', 
                      value_names=['lat', 'lon'], quantity=['Latitude (deg)', 'Longitude (deg)'])

    P = Plot(D)
    P.plot_raw_inspection(0, output='screen', fft_yscale='log', dt_format='%Y-%m-%d %H:%M:%S')

def test_plot_rec_inspection():
    from tools import tk_databank
    from tools import tk_gdfr

    D = tk_databank.DataBank()

    filename = '../../BMW/data/use_case_1/probes/highway_probes/2eef208-63c2-4a9f-8098-28d8207d1bb_1.csv'
    sensor_data = tk_gdfr.get_ts_value_from_file(filename, sep=',', pos_sid=0, pos_mt=2, pos_ts=1, pos_value=[3,4])
    D.add_sensor_data(sensor_data['pos']['values'], sensor_data['pos']['tss'], 0, filename, sensor_type='gps', 
                      value_names=['lat', 'lon'], quantity=['Latitude (deg)', 'Longitude (deg)'])

#    D.load('/Users/markus/Teraki/7_Programing/7.1_Development/BMW/data/use_case_1/databank3/highway_gps.pkl')

    D.sensor_data_process(0, 500, 0.6) #eng_name

    P = Plot(D)
    P.plot_rec_inspection(0, 0, output='screen', fft_yscale='log', dt_format='%Y-%m-%d %H:%M:%S')


def test1():
    from tools import tk_databank
    #Step 1: create a Databank and initiate 3 datasets
    D = tk_databank.DataBank()
    S1 = D.read_sensor_file("test1.csv", 1, "intel_raw", "temperature")
    S2 = D.read_sensor_file("edison_gyro.csv", 2, "edison", "gyro", ['v_x', 'v_y'], ['D', 'D'])
    values = [[x, x+x, x*x] for x in range(10000)]
    tss = [1700000000 + x*50 for x in range(10000)]
    S3 = D.add_sensor_data(values, tss, 9, "test_sensor", "random", ['x', 'x+x', 'x*x'], ['C', 'D', 'E'])
    
    #Step 2: process 
    process_cases = [[200, 0.7],[200, 0.8]]
    for sensor_id in [1, 2, 9]:
        for item in process_cases:
            framesize = item[0]
            reduction = item[1]
            D.sensor_data_process(sensor_id, framesize, reduction)
    
    #Step 3: Plots
    P=Plot(D)
    #Print summary
    P.print_summary_sensors()
    #Print deviation for sensor_id=9
    P.print_deviations_as_table(sensor_id=9)
    
    #Make plots for sensor_id=9 and process_id=0
    P.plot(sensor_id=9,process_id=0)
    #Add subplot for differences raw-rec
    P.plot(sensor_id=9,process_id=0,deviation='difference')
    #Add subplot for ratio raw-rec
    P.plot(sensor_id=9,process_id=0,deviation='ratio')
    #Add subplot for relative error 
    P.plot(sensor_id=9,process_id=0,deviation='rel_error')
    #Add subplots for all deviations= difference, ratio and rel. error 
    P.plot(sensor_id=9,process_id=0,deviation='all')
    
    
if __name__ == '__main__':
#    test2()
    #test3()
    #test_plot_map()
#    test_plot_raw_inspect()
    test_plot_rec_inspection()
