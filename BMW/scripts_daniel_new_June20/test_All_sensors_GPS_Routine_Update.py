#First test using the Teraki software
import os, sys, time
#os.chdir('../')
from tk_databank import DataBank
#from tk_plot import Plot
import numpy as np
#from linear import LinearInterpolation
#from lat_lon import Latitude, Longitude, LatLon, string2geocoord, string2latlon, GeoVector
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('Agg') ### Force matplotlib to not use any Xwindows backend.
from tk_plot import Plot


import tk_gdfr 

import  glob
import csv

scenario_input=int(sys.argv[1]) ##0:highway_probes, 1:innerCity ,2:rural 3:InnercityCrossing
scenario=['city','overland']

##Input Files
#folder='/home/angel/Files/BMW/probes/'
#folder='/Users/danielrichart/Documents/Projects/Research/Data/BMW/UseCase3/2016-05-19_Sample_02_Original/'
folder='/home/angelh/Documents/DataScienceRelated/Teraki/Files/BMW_UC3/2016_05_19_Sample_02_Original_3/'
#folder='/home/angelh/Documents/DataScienceRelated/Teraki/Files/BMW/probes/'
filenamelist = sorted(glob.glob(folder+scenario[scenario_input]+'/*positions.csv'),key=os.path.getsize,reverse=True)
#filenamelist = filenamelist[-1:] #last file by size (smallest file)
#filenamelist = filenamelist[:1]   #first fileS by size (biggest file)
#filenamelist = filenamelist[100:]   #first fileS by size (biggest file)
#filenamelist = [filenamelist[13]] #13: File with speed differene peak!
#filenamelist = [filenamelist[2]] #2 file that Daniel reach 2m presision for GPS he show in Slack.
#filenamelist = [filenamelist[66]] #13: File with heading difference peak!
#filenamelist = [filenamelist[390]] #390: File with 110 lines
#filenamelist = [filenamelist[243]] #390: File with 1100 lines
#filenamelist = [filenamelist[323]] #390: File with 520 lines

#filenamelist=[folder+scenario[0]+'/2eef208-63c2-4a9f-8098-28d8207d1bb_1.csv']##1M file and intersting file for Daniel
#filenamelist=['/Users/danielrichart/Documents/Projects/Research/Data/BMW/UseCase3/2016-05-19_Sample_02_Original/city/54b95068-c519-4c69-96c5-f2db75041ad1-positions.csv']##16k file
#del filenamelist[0]

filenamelist=filenamelist[:1]
print filenamelist

##Process cases
#framesizes=[100,500,1000]
#reductions=[0.6,0.8,0.9]   
#Short test
framesizes=[500]
reductions=[0.8]

process_cases=[]
for framesize in framesizes:
    for reduction in reductions:
        process_cases.append([framesize,reduction])
        

#########Pre and post process gps
pre_process_gps = {
    "multiple": [1000000, 1000000],
    'integer': [1,1]
}
post_process_gps = {
        "divide": [1000000.0, 1000000.0]
}
#########Pre and post process heading
pre_process_heading = {
    "multiple": [1000],
    'integer': [1],
    "value_exceptions": [
        [
            {
                "e_name": "red_peak_points",
                "e_id":  4,
                "v_types": ["red"],
                "condition": "(abs(delta_v)>10000)",
                "para": "delta_v"
            },
            {
                "e_name": "rest_peak_points",
                "e_id":  5,
                "v_types": ["rest"],
                "condition": "(abs(delta_v)>10000)",
                "para": "str(delta_v)+'_'+str(ts)+'_'+str(v)"
            }
        ]
    ]
}
post_process_heading = {
    "divide": [1000.0]
}

    
#########Pre and postprocess Speed
pre_process_speed = {
    "multiple": [1000],
    'integer': [1],
    "value_exceptions": [
        [
            {
                "e_name": "red_peak_points",
                "e_id":  4,
                "v_types": ["red"],
                "condition": "(abs(delta_v)>3000)",
                "para": "delta_v"
            },
            {
                "e_name": "rest_peak_points",
                "e_id":  5,
                "v_types": ["rest"],
                "condition": "(abs(delta_v)>3000)",
                "para": "str(delta_v)+'_'+str(ts)+'_'+str(v)"
            }
        ]
    ]
}
post_process_speed = {
    "divide": [1000.0]
}

##array components::
#             0=gps             ,1=heading            ,2=speed
pre_process= [pre_process_gps   ,pre_process_heading  ,pre_process_speed]
post_process=[post_process_gps  ,post_process_heading ,post_process_speed]
block=       [10                ,15                   ,10]


pre_process= [pre_process_gps]
post_process=[post_process_gps]
block=       [10              ]


output_file='summary_'+scenario[scenario_input]+'_output_recon.csv'

with open(output_file, 'w',0) as f_recon:
    #Output file
    writer_recon = csv.writer(f_recon,quoting=csv.QUOTE_NONNUMERIC)
    writer_recon.writerow(('file_id','npoints','sensor_id', 'sensor_name','value_name','process_id' ,'framesize','reduction', 
                           'tss_recon_min','tss_recon_max', 'values_recon_min','values_recon_max', 'values_recon.mean', 
                           'zlib_ratio', 'count_red','red_info_ratio',
                           'dev_max','dev_mean','dev_min',
                           'relerr','mape','smape','wpe','wape','rrmse','mae',
                           'file_name') )
    
    
    file_id=0 
    for file in filenamelist:    
        print "This is the current file",file
        D = DataBank()
        P = Plot(D)
	   
	   
        # Old data files
        #file = "./probes/innercity/28f21acc-3c8c-49fc-bf29-4acd40891bf0_1.csv"
        #sensor_data = get_ts_value_from_file(file, ',', 0, 2, 1, [3,4])
    
        #S = D.add_sensor_data(sensor_data['pos']['values'], 
        #   sensor_data['pos']['tss'], 0, "gps", "bmw_dataset_pos")
	   
        #Load GPS Data for UC3
        #value_names=['lat','lon']
        #sensor_data1 = tk_gdfr.get_ts_value_from_file(file, ',', 0, 2, 1, [3,4])
        #sensor_data1 = tk_gdfr.get_ts_value_from_file(file, ',', 'pos', 'pos', 0, [1,2])
        #S1 = D.add_sensor_data(sensor_data1['pos']['values'],
        #                       sensor_data1['pos']['tss'], 0, "gps", "bmw_dataset_pos",value_names=value_names)
        #S1 = D.add_sensor_data(sensor_data1['pos']['values'][1:],
        #      sensor_data1['pos']['tss'][1:], 0, "gps", "pos")
		    
        #pre_process = {
        #    "multiple": [1000000, 1000000]
        #}
        #post_process = {
        #    "divide": [1000000.0, 1000000.0]
        #}
		    
        #D.sensor_data_process(0, 100, 0.8, 'cii_dct_v011', pre_process, post_process, 5)
		    
        #D.write_sensor_data_files(0)
	   
	  
        from tk_gdfr import get_ts_value_from_file
        sensor_data = get_ts_value_from_file(file, ',', 'pos', 'pos', 0, [1, 2])
        S = D.add_sensor_data(sensor_data['pos']['values'][1:], 
        sensor_data['pos']['tss'][1:], 5, "bbb", "bbb")
        print sensor_data['pos']['values'][1:]
        D.sensor_raw_data_clean(5, 1000)
        distance_pre_process = {
            "multiple": [1000],
            "integer": [1],
            "value_exceptions": [
                [
                    {
                        "e_name": "red_peak_points",
                        "e_id":  4,
                        "v_types": ["red"],
                        "condition": "(abs(delta_v)>350)",
                        "para": "delta_v"
                    },
                    {
                        "e_name": "rest_peak_points",
                        "e_id":  5,
                        "v_types": ["rest"],
                        "condition": "(abs(delta_v)>350)",
                        "para": "str(delta_v)+'_'+str(ts)+'_'+str(v)"
	               }
	           ] 
            ]
        }
        distance_post_process = {
	   #    "range_min": [0],
	       "divide": [1000.0]
	   }

        pid = D.gps_sensor_data_process(5, 500, 0.8, 'cii_dct_v011', distance_pre_process, distance_post_process, 15, gps_sync_rate=10, heading_digits=1, gps_digits=5)
        D.write_sensor_data_files(5)
	       #print D.get_deviations(500, 0)
        reduce_info = D.get_sensor_process_info(5, pid)['reduce_info']
        print (reduce_info)
        gps_deviation = D.get_gps_deviations(5, pid)
        print "edge distance : " + str(gps_deviation['pe_deviations'])
        print "point distance : " + str(gps_deviation['pp_deviations'])
        print "wgs point distance : " + str(gps_deviation['wgs84_pp_deviations'])
	   
        reduce_ratio = D.get_sensor_process_info(5, pid)['reduce_info']['ratio']

        #pid = D.gps_conversion_process(5)
        #gps_deviation = D.get_gps_deviations(5, pid)
        #print "Do not process the distance : "
        #print "edge distance : " + str(gps_deviation['pe_deviations'])
        #print "point distance : " + str(gps_deviation['pp_deviations'])
        #print "wgs point distance : " + str(gps_deviation['wgs84_pp_deviations'])
        
        process_id=0
        for item in process_cases:        
            framesize = item[0]
            reduction = item[1]
            #if sensor['sensor_id'] == 14:
            for sensor in D.get_sensor_list():
                print "This is the sensor",sensor['sensor_id']
                if (sensor['sensor_id'] not in [5]):# | (sensor['sensor_id'] != 2):
				continue
                index=sensor['index']
                dof=sensor['dof']
                sensor_id=sensor['sensor_id']
                print "The sensor id",sensor_id
                sensor_name=sensor['sensor_name']
                
                #Raw data info
                raw_data=D.get_sensor_raw_data(sensor_id)
                tss=np.asarray(raw_data['tss'])
                values=np.asarray(raw_data['values'])
                npoints=raw_data['count']
                if npoints<framesize:
                    break
                
                ##Process Data
                #D.sensor_data_process(sensor_id, framesize, reduction,'cii_dct_v011', pre_process=pre_process[sensor_id], post_process=post_process[sensor_id],block=block[sensor_id])
                #if (sensor['sensor_id'] == 1):
                meta = {'reduce_info': {'ratio': reduce_ratio}}
                print "This happens here",D.add_processed_sensor_data(sensor_id, raw_data['values'], tss,meta=meta)# Recon data from sensor_id 8
                #if (sensor['sensor_id'] == 2):
                print "This is the RDP process id",D.sensor_data_process(sensor_id, 100, 0.9, 'rdpi_v001', epsilon=0.00001)
                #P.plot(sensor_id,process_id=0,deviation='difference')
	           #gps_Recovered_deviation = D.get_gps_deviations(1, 0)
                
                ##Plots
                #P=Plot(D)
                #P.plot(sensor_id,process_id,deviation='difference',save_png=True) 
            
                #Recon data info
                print "This is the current sensor id",sensor['sensor_id']
                recon_data=D.get_sensor_recon_data(sensor_id,process_id)
                tss_recon=np.asarray(recon_data['tss'])
                values_recon=np.asarray(recon_data['values'])
                zlib_ratio=D.get_zip_reduction_ratio(sensor_id,framesize)
            
                #Reduce info
                reduce_info=D.get_sensor_process_info(sensor_id, process_id)['reduce_info']
                count_red = D.get_sensor_process_info(sensor_id, 0)['reduce_info']['len_red']
                red_info_ratio= D.get_sensor_process_info(sensor_id, 0)['reduce_info']['ratio']#reduce_ratio #  This is the updated calculation for the reduction
            
                #Deviation_info
                deviations=D.get_deviations(sensor_id,process_id)
                
                for i in range(dof):
                    value_name=sensor['value_names'][i]
                    values_comp=values_recon.T[i]
                    dev_max=deviations['E'][i]['max']
                    dev_mean=deviations['E'][i]['mean']
                    dev_min=deviations['E'][i]['min']
                    
                    relerr=deviations['relerr'][i]
                    mae=deviations['MAE'][i]
                    mape=deviations['MAPE%'][i]
                    smape=deviations['SMAPE%'][i]
                    rrmse=deviations['RRMSE%'][i]
                    wape=deviations['WAPE%'][i]
                    wpe=deviations['WPE%'][i]
                    
                    
                    
                    #Write output
                    writer_recon.writerow((file_id,npoints,sensor_id,sensor_name, value_name,process_id ,framesize,reduction, 
                                   tss_recon.min(),tss_recon.max(), float("%.3f"%values_comp.min()), float("%.3f"%values_comp.max()), float("%.3f"%values_comp.mean()), 
                                   float("%.3f"%zlib_ratio), count_red,float("%.3f"%red_info_ratio), 
                                   dev_max,dev_mean,dev_min,
                                   relerr,mape,smape,wpe,wape,rrmse,mae,
                                   file))
            
            process_id=process_id+1
        print '** Goes in file: ',file_id,''
        ##
        name='output_file_id_'+scenario[scenario_input]+'file_id_'+str(file_id)+'.dbk'
        D.save(name)
        ##
        file_id=file_id+1
            
