#First test using the Teraki software
import matplotlib
matplotlib.use('Agg') ### Force matplotlib to not use any Xwindows backend.

import os, sys, time
from tk_databank import DataBank
from tk_plot import Plot
import numpy as np
import matplotlib.pyplot as plt
from linear import LinearInterpolation

import tk_gdfr 

import  glob
import csv
import gc

scenario_input=int(sys.argv[1]) ##0:city ##1 overland
scenario=['city','overland']

#folder='/Users/danielrichart/Documents/Projects/Research/Data/BMW/UseCase3/2016-05-19_Sample_02_Original/'
#folder='/home/angelh/Documents/DataScienceRelated/Teraki/Files/BMW_UC3/2016_05_19_Sample_02_Original_3/'
folder='/home/angel/Files/BMW_UC3/2016_05_19_Sample_02_Original_3/'

filenamelist_velocity = sorted(glob.glob(folder+scenario[scenario_input]+'/*velocity.csv'),key=os.path.getsize,reverse=True)

#######Data Files to be read!! ##comment to loop over all the data files in a folder i,e the line above
#filenamelist_velocity = filenamelist_velocity[-1:] ## -1 to test the lightest file
#filenamelist_velocity = [filenamelist_velocity[0]] ##Tthe largest file!!

#Velocity file daniel used
#filenamelist_velocity = [folder+scenario[scenario_input]+'/82a8535b-4bb8-4899-9923-04b6d7a39b45-velocity.csv']
##Process cases

for file in filenamelist_velocity:
    print file,' ,size:',os.path.getsize(file)


######## Short test
#framesizes=[100]
#reductions=[0.8]
#blocks=[5]

######### Parameters used in the second run for the largest file....
#framesizes=[100,500,1000]
#reductions=[0.60,0.70,0.80,0.90]   
#blocks=[5,15,25]

######### Parameters choosed to loop over all the files
framesizes=[200,500]
reductions=[0.80]   
blocks=[5]


##Define process cases!!!
process_cases=[]
number_cases=0
for framesize in framesizes:
    for reduction in reductions:
        for block in blocks:
            process_cases.append([framesize,reduction,block])
            number_cases=number_cases+1

print '********Processing: ', number_cases, ' process cases'

############   Pre and post process velocity
pre_process_velocity = {
    #"multiple": [1],
    #'integer': [1],
    "value_exceptions": [
        [
            {
                "e_name": "red_peak_points",
                "e_id":  4,
                "v_types": ["red"],
                "condition": "(abs(delta_v)>10)",
                "para": "delta_v"
            },
            {
                "e_name": "rest_peak_points",
                "e_id":  5,
                "v_types": ["rest"],
                "condition": "(abs(delta_v)>10)",
                "para": "str(delta_v)+'_'+str(ts)+'_'+str(v)"
            }
        ]
    ]
}
post_process_velocity = {
    #"divide": [1.0]
}

output_file='summary_'+scenario[scenario_input]+'_output_recon.csv'        
with open(output_file, 'w',0) as f_recon:
    #Output file
    writer_recon = csv.writer(f_recon,quoting=csv.QUOTE_NONNUMERIC)
    writer_recon.writerow(('file_id','npoints','sensor_id', 'sensor_name','value_name','process_id' ,'framesize','reduction','block', 
                           'tss_recon_min','tss_recon_max', 'values_recon_min','values_recon_max', 'values_recon.mean', 
                           'zlib_ratio', 'count_red','red_info_ratio',
                           'dev_max','dev_mean','dev_min',
                           'relerr','mape','smape','wpe','wape','rrmse','mae',
                           'file_name') )
    
    ################## Loop over files
    file_id=0 
    for file in filenamelist_velocity:    
        D = DataBank()
        #Load turnarte data****************** Sensor 0 ******************************
        value_names=['velocity']
        
        sensor_data1 = tk_gdfr.get_ts_value_from_file(file, ',', 'pos', 'pos', 0, [1])
    
        sensor_id=0
        ##Add data to a databank
        S1 = D.add_sensor_data(sensor_data1['pos']['values'][1:],
                               sensor_data1['pos']['tss'][1:], sensor_id, "velocity", "pos",value_names=value_names)
    
        
        ####################   Interpolation Daniel implemented
        #Raw data info
        raw_data=D.get_sensor_raw_data(sensor_id)
        tss=np.asarray(raw_data['tss'])
        values=np.asarray(raw_data['values'])
        npoints=raw_data['count']
        
        from tk_plot import plot_hist_of_timestamp_intervals
        tss_plot = plot_hist_of_timestamp_intervals(tss)
        #plt.plot(tss_plot)
        #plt.show()
        
        values_full = []
        tss_full = []
        for i in range(len(values)):
            #print "This is the treshhold interval",tss_plot[i]
            if tss_plot[i] < 25:
                values_full.append(values[i])
                tss_full.append(tss[i])
            if tss_plot[i] > 25:
                #print "This is the treshhold interval",tss_plot[i]
                #print "This is the rounded data point",round(tss_plot[i]/20)
                data_range = range(0,int(round(tss_plot[i]/20)))
                int_start = values[i-1]
                int_stop = values[i]
                table = LinearInterpolation(
                    x_index=[0,int(round(tss_plot[i]/20))],
                    values=[int_start,int_stop],
                    extrapolate=True)
                values_int = []
                tss_int = []
                #print "This is the range",data_range
                n = 0
                for j in data_range:
                    values_int.append(table(j).tolist())
                    #print tss[i-1]
                    tss_int.append(tss[i-1] + n*20)
                    #print tss_int[j]
                    n = n + 1
                    
                #print "This is the new interval",values_int
                #for i in range(len(int(tss_plot[i]/20)):
                values_full.extend(values_int)
                tss_full.extend(tss_int)
    
        #plt.plot(values_full)
        #plt.show()
		  
		  
        # from tk_plot import plot_hist_of_timestamp_intervals
        # tss_plot_int = plot_hist_of_timestamp_intervals(tss_full)
        #plt.plot(tss_plot_int)
        #plt.show()
        
        values_full = [x for x in values_full]
        tss_full = [x for x in tss_full]
        #print(values_full)
        
        ################## Add interpolated data to sensor_id+1 i.e sensor_id=1
        #add interpolated turnarte data****************** Sensor 1 ******************************
        S2 = D.add_sensor_data(values_full,
                               tss_full, sensor_id + 1, "velocity", "pos",value_names=value_names)
        
        
        ##Process sensors in the Databan
        
        ##Loop over sensors added to a DataBank
        for sensor in D.get_sensor_list():
            index=sensor['index']
            dof=sensor['dof']
            sensor_id=sensor['sensor_id']
            sensor_name=sensor['sensor_name']
            raw_data=D.get_sensor_raw_data(sensor_id)
            npoints=raw_data['count']
            
            ######### Skip the raw data without interpolation...the interpolated is sensor_id=1
            if sensor_id==0:
                continue
                
            #sensor_id=1 ##Just to safe...
            ######### For each sensor loop over process cases considered
            process_id_counter=0
            for item in process_cases:        
                framesize = item[0]
                reduction = item[1]
                block=item[2]
                
                if npoints<framesize:
                    continue
                print 'Processing....file:', file_id, ' ,npoints:', npoints,' ,sensor_id:', sensor_id,'sensor_name:', sensor_name, ', process_id_counter:',process_id_counter
                ########## Process Interpolated data! i.e sensor_id=1
            
                pre_process= pre_process_velocity
                post_process=post_process_velocity
                
                D.sensor_data_process(sensor_id, framesize, reduction,'cii_dct_v011', pre_process=pre_process, post_process=post_process,block=block)
                #D.sensor_data_process(sensor_id, framesize, reduction,'cii_dct_v011')
                #print "This is the reduction info",D.get_sensor_process_info(sensor_id + 1, 0)['reduce_info']
                
                ######################## Note!!!!  each time sensor_data_proces is called process_id increments by one for a given sensor_id
                ##the next returns the current process id...
                process_id=len(D.get_sensor_info(sensor_id)['processed_data'])-1
                process_id_counter=process_id_counter+1
                
                print '***Goes in file:', file_id, 'sensor_id:', sensor_id,'sensor_name', sensor_name,' ,process_id:', process_id
                print '     *framesize:', framesize, ' ,reduction:',reduction, ', block:', block
                
                ############################ Post process data!!...process_id WILL INCREMENT by one!!
                ##      the next process case then will be process id+2....and the post process of that is process_id+3 :S
                post_process_para = {
                    "method": "savgol_filter",
                    "window_length": 49,
                    "polyorder": 2
                }
                
                D.sensor_data_post_process(sensor_id,process_id, post_process_para)
                
                
                #######################################################################################################
                
                #          From here on process_id+1 contains the relevant data to print the csv tables and make plots
                
                #######################################################################################################
                
                print 'After post processing....Goes in file:', file_id, 'sensor_id:', sensor_id,'sensor_name', sensor_name,' ,process_id:', process_id+1
                
                
                #Recon data info
                raw_data=D.get_sensor_raw_data(sensor_id)
                recon_data=D.get_sensor_recon_data(sensor_id,process_id + 1)
                tss_recon=np.asarray(recon_data['tss'])
                values_recon=np.asarray(recon_data['values'])
                zlib_ratio=D.get_zip_reduction_ratio(sensor_id,framesize)
                #print "This is the zlib ratio",zlib_ratio
                
                #Reduce info
                reduce_info=D.get_sensor_process_info(sensor_id, process_id + 1)['reduce_info']
                count_red=reduce_info['count_red']
                red_info_ratio=reduce_info['ratio']
                
                ###############################Plots!!
                P=Plot(D)
                suptitle='File_id:'+str(file_id)+' ,sensor '+str(sensor_id)+', '+sensor_name+' ,process_id:'+str(process_id+1)+' ,framesize: '+str(framesize)+' ,reduction: '+str(round(red_info_ratio*100,3))+' ,block:'+ str(block)
                #P.plot(sensor_id,process_id,deviation='difference',save_png=True)
                P.plot(sensor_id,process_id + 1,deviation='difference',save_png=True, suptitle=suptitle)
                
                ##Box plot!!
                #import tk_plot
                #raw_data = [x for x in raw_data]
                #recon_data = [x for x in recon_data]
                #fig = plt.figure()
                #ax = fig.add_subplot(111)
                #tk_plot.plot_boxplot_residuals(ax, raw_data, recon_data)
                #plt.show()
                
                #Deviation_info
                deviations=D.get_deviations(sensor_id,process_id+1)
                
                for i in range(dof):
                    value_name=sensor['value_names'][i]
                    values_comp=values_recon.T[i]
                    dev_max=deviations['E'][i]['max']
                    dev_mean=deviations['E'][i]['mean']
                    dev_min=deviations['E'][i]['min']
                    
                    relerr=deviations['relerr'][i]
                    mae=deviations['MAE'][i]
                    mape=deviations['MAPE%'][i]
                    smape=1#deviations['SMAPE%'][i]
                    rrmse=deviations['RRMSE%'][i]
                    wape=deviations['WAPE%'][i]
                    wpe=deviations['WPE%'][i]
                    
                    #Write output
                    writer_recon.writerow((file_id,npoints,sensor_id,sensor_name, value_name,process_id+1,framesize,reduction,block, 
                                           tss_recon.min(),tss_recon.max(), float("%.3f"%values_comp.min()), float("%.3f"%values_comp.max()), 
                                           float("%.3f"%values_comp.mean()), 
                                           float("%.3f"%zlib_ratio), count_red,float("%.3f"%red_info_ratio), 
                                           dev_max,dev_mean,dev_min,
                                           relerr,mape,smape,wpe,wape,rrmse,mae,
                                           file))
                    
                    #close figures in matplotlib not to have a massive memory leak!!
                    plt.close('all')
                    gc.collect()
                    #Finish loop over dof
                #process_id=process_id+1
                #Finish loop over process cases
            #Finish loop over sensors
        
        name='output_file_id_'+str(file_id)+'.dbk'
        D.save(name)
        file_id=file_id+1
        #Finish loop over files
