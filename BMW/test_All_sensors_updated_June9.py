#First test using the Teraki software
import os, sys, time
from tk_databank import DataBank
#from tk_plot import Plot
import numpy as np

import tk_gdfr 

import  glob
import csv

scenario_input=int(sys.argv[1]) ##0:highway_probes, 1:innerCity ,2:rural 3:InnercityCrossing
scenario=['highway_probes','innerCity','rural','InnercityCrossing']

##Input Files
folder='/home/angel/Files/BMW/probes/'
#folder='/home/angelh/Documents/DataScienceRelated/Teraki/Files/BMW/probes/'
filenamelist = sorted(glob.glob(folder+scenario[scenario_input]+'/*.csv'),key=os.path.getsize,reverse=True)
#filenamelist = filenamelist[-1:] #last file by size (smallest file)
#filenamelist = filenamelist[:1]   #first fileS by size (biggest file)
#filenamelist = filenamelist[100:]   #first fileS by size (biggest file)
#filenamelist = [filenamelist[0]] #13: File with speed differene peak!
#filenamelist = [filenamelist[2]] #2 file that Daniel reach 2m presision for GPS he show in Slack.
#filenamelist = [filenamelist[66]] #13: File with heading difference peak!
#filenamelist = [filenamelist[390]] #390: File with 110 lines
#filenamelist = [filenamelist[243]] #390: File with 1100 lines
#filenamelist = [filenamelist[323]] #390: File with 520 lines

#filenamelist=[folder+scenario[0]+'/2eef208-63c2-4a9f-8098-28d8207d1bb_1.csv']##1M file and intersting file for Daniel
#filenamelist=[folder+scenario[0]+'/f88520-2efa-46e6-8fc0-35c353f84922_1.csv']##16k file
print filenamelist, os.path.getsize(filenamelist[0])

##Process cases
#framesizes=[100,500,1000]
#reductions=[0.6,0.8,0.9]   
#Short test
framesizes=[500]
reductions=[0.80]

process_cases=[]
for framesize in framesizes:
    for reduction in reductions:
        process_cases.append([framesize,reduction])
        

#########Pre and post process gps
pre_process_gps = {
    "multiple": [1000000, 1000000],
    'integer': [1,1]
}
post_process_gps = {
        "divide": [1000000.0, 1000000.0]
}
#########Pre and post process heading ##New parameters as suggested by Daniel
pre_process_heading = {
    "multiple": [1000],
    #'integer': [1],
    "min_delta":[6000],
    "value_exceptions": [
        [
            {
                "e_name": "red_peak_points",
                "e_id":  4,
                "v_types": ["red"],
                "condition": "(abs(delta_v)>7000)",
                "para": "delta_v"
            },
            {
                "e_name": "rest_peak_points",
                "e_id":  5,
                "v_types": ["rest"],
                "condition": "(abs(delta_v)>7000)",
                "para": "str(delta_v)+'_'+str(ts)+'_'+str(v)"
            }
        ]
    ]
}
post_process_heading = {
    "divide": [1000.0]
}

    
#########Pre and postprocess Speed
pre_process_speed = {
    "multiple": [1000],
    #'integer': [1],
    "min_delta":[1000],
    "value_exceptions": [
        [
            {
                "e_name": "red_peak_points",
                "e_id":  4,
                "v_types": ["red"],
                "condition": "(abs(delta_v)>2000)",
                "para": "delta_v"
            },
            {
                "e_name": "rest_peak_points",
                "e_id":  5,
                "v_types": ["rest"],
                "condition": "(abs(delta_v)>2000)",
                "para": "str(delta_v)+'_'+str(ts)+'_'+str(v)"
            }
        ]
    ]
}
post_process_speed = {
    "divide": [1000.0]
}

##array components::
#             0=gps             ,1=heading            ,2=speed
pre_process= [pre_process_gps   ,pre_process_heading  ,pre_process_speed]
post_process=[post_process_gps  ,post_process_heading ,post_process_speed]
block=       [10                ,15                   ,15] ##


output_file='summary_'+scenario[scenario_input]+'_output_recon.csv'

with open(output_file, 'w',0) as f_recon:
    #Output file
    writer_recon = csv.writer(f_recon,quoting=csv.QUOTE_NONNUMERIC)
    writer_recon.writerow(('file_id','npoints','sensor_id', 'sensor_name','value_name','process_id' ,'framesize','reduction', 
                           'tss_recon_min','tss_recon_max', 'values_recon_min','values_recon_max', 'values_recon.mean', 
                           'zlib_ratio', 'count_red','red_info_ratio',
                           'dev_max','dev_mean','dev_min',
                           'relerr','mape','smape','wpe','wape','rrmse','mae',
                           'file_name') )
    
    
    file_id=0 
    for file in filenamelist:    
        D = DataBank()
        #Load GPS Data
        value_names=['lat','lon']
        sensor_data1 = tk_gdfr.get_ts_value_from_file(file, ',', 0, 2, 1, [3,4])
        S1 = D.add_sensor_data(sensor_data1['pos']['values'],
                               sensor_data1['pos']['tss'], 0, "gps", "bmw_dataset_pos",value_names=value_names)
        
        #Load Heading Data                                                                                                                                                                                      
        sensor_data2 = tk_gdfr.get_ts_value_from_file(file, ',', 0, 2, 1, [5])
        S2 = D.add_sensor_data(sensor_data2['pos']['values'],
                               sensor_data2['pos']['tss'], 1, "heading", "bmw_dataset_heading")
        
        #Load Speed Data                                                                                                                                                                                        
        sensor_data3 = tk_gdfr.get_ts_value_from_file(file, ',', 0, 2, 1, [6])
        S3 = D.add_sensor_data(sensor_data3['pos']['values'],
                               sensor_data3['pos']['tss'], 2, "speed", "bmw_dataset_speed")
        
        process_id=0
        for item in process_cases:        
            framesize = item[0]
            reduction = item[1]

            for sensor in D.get_sensor_list():
                index=sensor['index']
                dof=sensor['dof']
                sensor_id=sensor['sensor_id']
                sensor_name=sensor['sensor_name']
                
                if sensor_id==0: ##For this test no need to process GPS data...just heading and velocity with some new parameters as suggested by daniel
                    continue
                #Raw data info
                raw_data=D.get_sensor_raw_data(sensor_id)
                tss=np.asarray(raw_data['tss'])
                values=np.asarray(raw_data['values'])
                npoints=raw_data['count']
                if npoints<framesize:
                    break
                
                ##Process Data
                D.sensor_data_process(sensor_id, framesize, reduction,'cii_dct_v011', pre_process=pre_process[sensor_id], post_process=post_process[sensor_id],block=block[sensor_id])
                
                ##Plots
                #P=Plot(D)
                #P.plot(sensor_id,process_id,deviation='difference',save_png=True) 
            
                #Recon data info
                recon_data=D.get_sensor_recon_data(sensor_id,process_id)
                tss_recon=np.asarray(recon_data['tss'])
                values_recon=np.asarray(recon_data['values'])
                zlib_ratio=D.get_zip_reduction_ratio(sensor_id,framesize)
            
                #Reduce info
                reduce_info=D.get_sensor_process_info(sensor_id, process_id)['reduce_info']
                count_red=reduce_info['count_red']
                red_info_ratio=reduce_info['ratio']
            
                #Deviation_info
                deviations=D.get_deviations(sensor_id,process_id)
                
                for i in range(dof):
                    value_name=sensor['value_names'][i]
                    values_comp=values_recon.T[i]
                    dev_max=deviations['E'][i]['max']
                    dev_mean=deviations['E'][i]['mean']
                    dev_min=deviations['E'][i]['min']
                    
                    relerr=deviations['relerr'][i]
                    mae=deviations['MAE'][i]
                    mape=deviations['MAPE%'][i]
                    smape=deviations['SMAPE%'][i]
                    rrmse=deviations['RRMSE%'][i]
                    wape=deviations['WAPE%'][i]
                    wpe=deviations['WPE%'][i]
                    
                    
                    
                    #Write output
                    writer_recon.writerow((file_id,npoints,sensor_id,sensor_name, value_name,process_id ,framesize,reduction, 
                                   tss_recon.min(),tss_recon.max(), float("%.3f"%values_comp.min()), float("%.3f"%values_comp.max()), float("%.3f"%values_comp.mean()), 
                                   float("%.3f"%zlib_ratio), count_red,float("%.3f"%red_info_ratio), 
                                   dev_max,dev_mean,dev_min,
                                   relerr,mape,smape,wpe,wape,rrmse,mae,
                                   file))
            
            process_id=process_id+1
        print '** Goes in file: ',file_id,''
        ##
        name='output_file_id_'+str(file_id)+'.dbk'
        D.save(name)
        ##
        file_id=file_id+1
            
