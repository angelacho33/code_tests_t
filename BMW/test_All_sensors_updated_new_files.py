import matplotlib
matplotlib.use('Agg') ### Force matplotlib to not use any Xwindows backend

import os, sys, time
from tk_databank import DataBank
from tk_plot import Plot
import numpy as np
import tk_gdfr 

import  glob
import csv

scenario_input=int(sys.argv[1]) ##0:city ##1 overland
scenario=['city','overland']

##Input Files
folder='/home/angelh/Documents/DataScienceRelated/Teraki/Files/BMW_UC3/2016_05_19_Sample_02_Original_3/'

filenamelist_positions = sorted(glob.glob(folder+scenario[scenario_input]+'/*positions.csv'),key=os.path.getsize,reverse=True)
filenamelist_positions = [filenamelist_positions[0]] #Just one file
print filenamelist_positions

filenamelist_turnrate = sorted(glob.glob(folder+scenario[scenario_input]+'/*turnrate.csv'),key=os.path.getsize,reverse=True)
filenamelist_lanes = sorted(glob.glob(folder+scenario[scenario_input]+'/*lanes.csv'),key=os.path.getsize,reverse=True)


##Process cases
#framesizes=[100,500,1000]
#reductions=[0.6,0.8,0.9]   

#Short test
framesizes=[500]
reductions=[0.8]

process_cases=[]
for framesize in framesizes:
    for reduction in reductions:
        process_cases.append([framesize,reduction])
        

#########Pre and post process gps
pre_process_positions = {
    "multiple": [1000000, 1000000],
    'integer': [1,1]
}
post_process_positions = {
        "divide": [1000000.0, 1000000.0]
}
#########Pre and post process turnrate
pre_process_turnrate = {
    "multiple": [1000],
    'integer': [1],
    "value_exceptions": [
        [
            {
                "e_name": "red_peak_points",
                "e_id":  4,
                "v_types": ["red"],
                "condition": "(abs(delta_v)>20000)",
                "para": "delta_v"
            },
            {
                "e_name": "rest_peak_points",
                "e_id":  5,
                "v_types": ["rest"],
                "condition": "(abs(delta_v)>20000)",
                "para": "str(delta_v)+'_'+str(ts)+'_'+str(v)"
            }
        ]
    ]
}
post_process_turnrate = {
    "divide": [1000.0]
}

    
#########Pre and postprocess velocity
pre_process_velocity = {
    "multiple": [1000],
    'integer': [1],
    "value_exceptions": [
        [
            {
                "e_name": "red_peak_points",
                "e_id":  4,
                "v_types": ["red"],
                "condition": "(abs(delta_v)>3000)",
                "para": "delta_v"
            },
            {
                "e_name": "rest_peak_points",
                "e_id":  5,
                "v_types": ["rest"],
                "condition": "(abs(delta_v)>3000)",
                "para": "str(delta_v)+'_'+str(ts)+'_'+str(v)"
            }
        ]
    ]
}
post_process_velocity = {
    "divide": [1000.0]
}

##array components::
#             0=gps             ,1=heading            ,2=speed
pre_process= [pre_process_positions   ,pre_process_turnrate  ,pre_process_velocity]
post_process=[post_process_positions  ,post_process_turnrate ,post_process_velocity]
block=       [10                ,15                   ,10]




    
    
file_id=0 
D = DataBank()
for file in filenamelist_positions:    
    
    #Load GPS Data****************** Sensor 0: position ******************************
    value_names=['lat','lon']
    
    ##Next two lines work
    #sensor_data1 = tk_gdfr.get_ts_value_from_file(file, ',', '', '', 0, [1,2]) 
    #S1 = D.add_sensor_data(sensor_data1['']['values'],
    #                       sensor_data1['']['tss'], 0, "position", "bmw_dataset_posision_file",value_names=value_names)
    
    #As suggested by quing
    sensor_data1 = tk_gdfr.get_ts_value_from_file(file, ',', 'pos', 'pos', 0, [1,2])
    S1 = D.add_sensor_data(sensor_data1['pos']['values'][1:],
                               sensor_data1['pos']['tss'][1:], 0, "gps", "pos",value_names=value_names)
    
    file_id=file_id+1
        
    
for file in filenamelist_lanes:           
    #Load Heading Data ************  Sensor heading******************************
    
    sensor_data2 = tk_gdfr.get_ts_value_from_file(file, ',', 0, 'heading', 0, [1])
    ##Caution!!! there are null values...
    print sensor_data2['heading']['values'][58] ##Has a value
    
    S2 = D.add_sensor_data(sensor_data2['heading']['values'][1:],
                           sensor_data2['heading']['tss'][1:], 1, "heading", "bmw_dataset_heading")
    
    #Load Speed Data Sensor 2 *****  Sensor distances ******************************          
    sensor_data3 = tk_gdfr.get_ts_value_from_file(file, ',', 0, 'nextLeft_y', 0, [3])
    ## Caution!!! there are null values...
    print sensor_data3['nextLeft_y']['values'][1040] ##Has a value
    
    S3 = D.add_sensor_data(sensor_data3['nextLeft_y']['values'][1:],
                           sensor_data3['nextLeft_y']['tss'][1:], 2, "speed", "bmw_dataset_speed")
         
        
#Process_data
for sensor in D.get_sensor_list():
    index=sensor['index']
    dof=sensor['dof']
    sensor_id=sensor['sensor_id']
    sensor_name=sensor['sensor_name']
    
    #framesize=400
    ##Test gps at lower reduction....
    #if sensor_name=='gps':
    #    reduction=0.5
    #else:
    #reduction=0.9
    
    #Raw data info
    raw_data=D.get_sensor_raw_data(sensor_id)
    tss=np.asarray(raw_data['tss'])
        values=np.asarray(raw_data['values'])
        npoints=raw_data['count']
        if npoints<framesize:
            break
    
    ##Loop over process cases and write file...
    output_file='summary_'+scenario[scenario_input]+'_output_recon.csv'
    with open(output_file, 'w',0) as f_recon:
        #Output file
        writer_recon = csv.writer(f_recon,quoting=csv.QUOTE_NONNUMERIC)
        writer_recon.writerow(('file_id','npoints','sensor_id', 'sensor_name','value_name','process_id' ,'framesize','reduction', 
                               'tss_recon_min','tss_recon_max', 'values_recon_min','values_recon_max', 'values_recon.mean', 
                               'zlib_ratio', 'count_red','red_info_ratio',
                               'dev_max','dev_mean','dev_min',
                               'relerr','mape','smape','wpe','wape','rrmse','mae',
                               'file_name') )
            
        
        for item in process_cases:        
            framesize = item[0]
            reduction = item[1]    
            ##Process Data
            print 'Goes in file:', file_id, 'Goes in sensor: ', sensor_id
            D.sensor_data_process(sensor_id, framesize, reduction,'cii_dct_v011', pre_process=pre_process[sensor_id], post_process=post_process[sensor_id],block=block[sensor_id])
            process_id=len(D.get_sensor_info(sensor_id)['processed_data'])-1
            
            #Plots
            P=Plot(D)
            P.plot(sensor_id,process_id,deviation='difference',save_png=True) 
            
            #Recon data info
            recon_data=D.get_sensor_recon_data(sensor_id,process_id)
            tss_recon=np.asarray(recon_data['tss'])
            values_recon=np.asarray(recon_data['values'])
            zlib_ratio=D.get_zip_reduction_ratio(sensor_id,framesize)
            
            #Reduce info
            reduce_info=D.get_sensor_process_info(sensor_id, process_id)['reduce_info']
            count_red=reduce_info['count_red']
            red_info_ratio=reduce_info['ratio']
            
            #Deviation_info
            deviations=D.get_deviations(sensor_id,process_id)
            
            for i in range(dof):
                value_name=sensor['value_names'][i]
                values_comp=values_recon.T[i]
                dev_max=deviations['E'][i]['max']
                dev_mean=deviations['E'][i]['mean']
                dev_min=deviations['E'][i]['min']
                
                relerr=deviations['relerr'][i]
                mae=deviations['MAE'][i]
                mape=deviations['MAPE%'][i]
                smape=deviations['SMAPE%'][i]
                rrmse=deviations['RRMSE%'][i]
                wape=deviations['WAPE%'][i]
                wpe=deviations['WPE%'][i]
                
                
                
                #Write output
                writer_recon.writerow((file_id,npoints,sensor_id,sensor_name, value_name,process_id ,framesize,reduction, 
                                       tss_recon.min(),tss_recon.max(), float("%.3f"%values_comp.min()), float("%.3f"%values_comp.max()), 
                                       float("%.3f"%values_comp.mean()), 
                                       float("%.3f"%zlib_ratio), count_red,float("%.3f"%red_info_ratio), 
                                       dev_max,dev_mean,dev_min,
                                       relerr,mape,smape,wpe,wape,rrmse,mae,
                                       file))
                
            #process_id=process_id+1
            #print '** Goes in file: ',file_id,''
            ##
name='output_file_id_'+str(file_id)+'.dbk'
D.save(name)
##

            
