"""
Project Teraki: BOsch HTML Image Output Funtion
Author: xucao

it accept command line parameter: filename, Number of columns in output html file
print the multiple png images in one html file
"""


"""
for testing, the following commands can type in ternimal :
        python bosch_test.py 20160413-RBCD_CDMA-2016-05-24-0xB_head.CSV.csv
    or  python bosch_test.py 20160413-RBCD_CDMA-2016-05-24-0xB_head.CSV.csv 2
"""


import os, sys, time, datetime
import csv
import zipfile
from tools.tk_databank import DataBank
from tools.tk_plot import Plot
from tools.tk_plot import gen_html_grid
import glob
from dominate import document
from dominate.tags import *


def str_to_int_float(string): 
    try:
        return int(string)
    except ValueError:
        pass
    try:
        return float(string)
    except ValueError:
        return string


def get_single_sensor_data(filename,sensor_pos):
    result = {}
    with open(filename) as f:
        reader = csv.reader(f)
        headers = next(reader)
        key = headers[sensor_pos]
        for line in reader:
            try:
                ts = datetime.datetime.strptime(line[1], '%H:%M:%S')
                value = [str_to_int_float(line[sensor_pos])]
                if key not in result.keys():
                    result[key] = {}
                    result[key]['mt'] = headers[sensor_pos]
                    result[key]['tss'] = []
                    result[key]['values'] = []
                result[key]['tss'].append(ts)
                result[key]['values'].append(value)

            except:
                pass
    return result



def get_csv_list_len(filename):
    with open(filename) as f:
        reader = csv.reader(f)
        headers = next(reader)
        return len(headers)


def get_sensor_name(filename,sensor_pos):
    with open(filename) as f:
        reader = csv.reader(f)
        headers = next(reader)
        key = headers[sensor_pos]
        return key

def html_process(directory, outputfile, column):
    photos = glob.glob(directory)
    num = len(photos)
    with document(title='Photos') as doc:
        h = html()
        with h.add(body()).add(div(id='content')):
            h1('PNG')
            t = table()
            with t.add(tbody()):
                for i in range(0,num,column):
                    l = tr()
                    for j in range(column):
                        if i+j >= num:
                            break
                        l += td(div(img(src=photos[i+j],width=700,high=700), _class='photo'))

    with open(outputfile, 'w') as f:
        f.write(doc.render())


def process():

    if len(sys.argv) < 2:
        print 'Arguments fault, please type in following command:'
        print 'python output_html.py [filename] [Columns number -defual is 3]'
        return
    else:
        if len(sys.argv) == 2:
            print 'process file name is: '+ str(sys.argv[1])
            print 'Number of columns in output html file is 3'
        elif len(sys.argv) == 3:
            print 'process file name is: '+ str(sys.argv[1])
            print 'Number of columns in output html file is '+ str(sys.argv[2])
        else:
            print 'Arguments fault, please type in following command:'
            print 'python output_html.py [filename] [Row PNG number -defual is 3]'
            return


    filename = str(sys.argv[1])
    D = DataBank()
    for i in range(4,get_csv_list_len(filename)):
        sensor_data = get_single_sensor_data(filename,i)
        sensor_name = get_sensor_name(filename,i)
        output_name = 'Bosch_'+sensor_name
        D.add_sensor_data(sensor_data[sensor_name]['values'],sensor_data[sensor_name]['tss'], i-4, output_name, sensor_name)

    Pl = Plot(D)
    # leave out column which contains strings
    for sensor_id in range(0,2):
        Pl.plot_raw_inspection(sensor_id,'png', dt_format='%H:%M:%S')
    for sensor_id in range(3,(get_csv_list_len(filename)-4)):
        Pl.plot_raw_inspection(sensor_id,'png', dt_format='%H:%M:%S')


    directory = 'figures/*.png'
    outputfile = 'Bosch_Overview.html'
    if len(sys.argv) == 3:
        column = int(sys.argv[2])
    else :
        column = 3
    gen_html_grid(out_filename=outputfile, title='Bosch Data Overview', headline='Bosch Data Overview', n_cols=column, img_width=700)


if __name__ == '__main__':
    process()

