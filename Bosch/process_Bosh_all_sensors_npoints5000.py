import matplotlib
matplotlib.use('Agg') ### Force matplotlib to not use any Xwindows backend.

import matplotlib.pyplot as plt 

import os, sys, time
from tk_databank import DataBank
from tk_plot import Plot
import numpy as np
import glob
import csv
#import read_airbus_sar_data
import gc

from bosch_overview import * ##File to read bosch data....

#file_id=int(sys.argv[1])
file_id=0
folder='/home/angelh/Documents/DataScienceRelated/Teraki/Bosch/'
filenamelist=sorted(glob.glob(folder+'*.CSV'),key=os.path.getsize,reverse=True)

filenamelist=[filenamelist[file_id]] 
print filenamelist

##Sensor names copied from the header of the Bosh file...
all_sensors = ["Longitude","Latitude","IMEI","ASMod_dvolPFltEG",
               "Exh_pAdapPPFltDiff","ASMod_tPFltSurfSim","PFltLd_stSimOn",
               "GlbDa_lTotDst","PFltLd_resFlwFlt","PFltLd_mSotMeasBas_mp",
               "PFltLd_mSotCor","PFltLd_mSotMeasRaw","PFltLd_mSotMeas",
               "PFltLd_mSotMlg","VehV_v","PFltPOp_stVehPOp",
               "Exh_tAdapTPFltUs","PFltLd_resFlw","Exh_pDiffOfsValAct",
               "Exh_pDiffOfsValOld","PFltLd_mSotSimNoCont",
               "PFltRgn_lSnceRgn","Epm_nEng","InjCtl_qSetUnBal",
               "DFES_numDFC_[0]","DFES_numDFC_[1]","DFES_numDFC_[2]",
               "DFES_stEntry_[0]","DFES_stEntry_[1]","DFES_stEntry_[2]",
               "CEngDsT_t","PthSet_trqInrSet","APP_r",
               "Tra_numGear","AirCtl_mDesVal","AFS_mAirPerCylFlt",
               "AirCtl_stMon","EGRVlv_r","EGRVlv_rAct",
               "ThrVlv_rAct","TrbCh_rPs","EnvT_t",
               "LSU_rLam_[0]","PFltLd_dmO2Aprx","PFltLd_dvolPFltEGRstnRed_mp",
               "PFltLd_mSotSim","FMO_qEmiCtlCor","EnvP_p",
               "Exh_tAdapTOxiCatUs","CoEng_st","CoEOM_stOpModeAc"]

default_exception=1e15 ##If I do not want to use exception detection as suggested by Qing I just set a very large number....

                                            ##min delta is the factor multiplying the amplitude for each sensor
                                           #:[framesize,reduction, multiple, min_delta, delta] 
all_sensors_parameters = {"Longitude":[500,0.85,1,0,10],
                          "Latitude":[500,0.85,1,0,10],
                          "IMEI":[500,0.85,1,0,  default_exception],
                          "ASMod_dvolPFltEG":[500,0.7,1,5,20],
                          "Exh_pAdapPPFltDiff":[500,0.75,1,1,2.5],
                          "ASMod_tPFltSurfSim":[500,0.85,1,0,25],
                          "PFltLd_stSimOn":[500,0.95,1,0,0.5],
                          "GlbDa_lTotDst":[500,0.9,1,0,  default_exception], 
                          "PFltLd_resFlwFlt":[500,0.7,1,0,0.005],
                          "PFltLd_mSotMeasBas_mp":[500,0.8,1,0,  default_exception], 
                          "PFltLd_mSotCor":[500,0.9,1,0,  default_exception], 
                          "PFltLd_mSotMeasRaw":[500,0.9,1,0,  default_exception], 
                          "PFltLd_mSotMeas":[500,0.7,1,0,  0.005], 
                          "PFltLd_mSotMlg":[500,0.6,1,0,  0.005], 
                          "VehV_v":[1000,0.75,1,0,25],
                          "PFltPOp_stVehPOp":[500,0.7,1,0,25],
                          "Exh_tAdapTPFltUs":[500,0.8,1,0,20],
                          "PFltLd_resFlw":[500,0.99,1,0,0.005],
                          "Exh_pDiffOfsValAct":[500,0.7,1,0,0.2],
                          "Exh_pDiffOfsValOld":[500,0.7,1,0,0.2],
                          "PFltLd_mSotSimNoCont":[500,0.6,1,0,  0.005], 
                          "PFltRgn_lSnceRgn":[500,0.85,1,0,  default_exception], 
                          "Epm_nEng":[500,0.85,1,0,100],
                          "InjCtl_qSetUnBal":[250,0.8,1,0,2],
                          "DFES_numDFC_[0]":[500,0.9,1,0,  default_exception], 
                          "DFES_numDFC_[1]":[500,0.9,1,0,  default_exception], 
                          "DFES_numDFC_[2]":[500,0.9,1,0,  default_exception], 
                          "DFES_stEntry_[0]":[500,0.9,1,0,  default_exception], 
                          "DFES_stEntry_[1]":[500,0.9,1,0,  default_exception], 
                          "DFES_stEntry_[2]":[500,0.9,1,0,  default_exception], 
                          "CEngDsT_t":[500,0.85,1,0,10],
                          "PthSet_trqInrSet":[500,0.80,1,1,20],
                          "APP_r":[500,0.6,1,1,10],
                          "Tra_numGear":[500,0.8,1,0,0.5],
                          "AirCtl_mDesVal":[500,0.8,1,0,100],
                          "AFS_mAirPerCylFlt":[500,0.95,1,0,25],
                          "AirCtl_stMon":[500,0.85,1,0,0.05],
                          "EGRVlv_r":[500,0.6,1,0,25],
                          "EGRVlv_rAct":[500,0.4,1,0,25],
                          "ThrVlv_rAct":[500,0.4,1,0,25],
                          "TrbCh_rPs":[500,0.9,1,0,  default_exception], 
                          "EnvT_t":[500,0.99,1,0,0.05],
                          "LSU_rLam_[0]":[500,0.85,1,0.1,1],
                          "PFltLd_dmO2Aprx":[500,0.7,1,0,5],
                          "PFltLd_dvolPFltEGRstnRed_mp":[500,0.7,1,1,25],
                          "PFltLd_mSotSim":[500,0.7,1,0,  0.005], 
                          "FMO_qEmiCtlCor":[500,0.9,1,0,0.25],
                          "EnvP_p":[500,0.1,1,0,0],
                          "Exh_tAdapTOxiCatUs":[1000,0.85,1,0,25],
                          "CoEng_st":[500,0.7,1,0,0.5],
                          "CoEOM_stOpModeAct":[500,0.9,1,0,  default_exception]
                      }

print 'len(all_sensors)', len(all_sensors)
print 'len(all_sensors_parameters)', len(all_sensors_parameters)
######Classification:
sensors_with_good_profile=[]
sensors_for_exception=[]
sensors_for_min_delta=[]

##The next were the first 12 sensors tested and the corresponding optimal block and framesizes (Not used below)
#relevant =         ['BLATACC', 'BNACC', 'BPTCHACC', 'BROLLACC', 'BYAWACC', 'BPTCHR', 'BROLLR', 'BYAWR', 'AOAC', 'SDSLPC', 'ELEV.R', 'ELEV.L']
#************************* Need_updated_framesizes for file1!! These optimal that Daniel sent for wich file? file1? 
#Note on blocks: Daniel values or all sensors expect ELEV.R and ELEV.L which I optimized in a separate run
#block=             [15       ,  15    ,  15       ,  15       ,  15      ,  15     ,  15     ,  15    ,   5   ,  10     ,  10    , 10      ]
#************************* Need_updated_framesizes for file1: 
#Note on framesizes: Taken from the first run without any pre_processing i.w output_updated2/ for file2!!
#optimal_framesizes=[1000      , 1000  ,  1000     ,  1000     ,  1000    ,  1000   ,  1000   ,  1000  ,   200 ,  200    ,  200  , 200     ]


##################
##Process_parameters
#reductions=[0.4,0.5,0.6,0.7,0.8]
#min_deltas=[0,0.01,0.05,0.10] #in percentage!! 
#framesizes=[100,500,1000]

#short test...no loop
reductions=[0.85]
framesizes=[500] ##I the sampling rate is actually 
min_deltas=[0] #in percentage!! 

##For the moment these parameters for all sensors
multiple=1000
block=15 

process_cases=[]
for reduction in reductions:
    for framesize in framesizes:
        for min_delta in min_deltas:
            process_cases.append([reduction,framesize,min_delta])

#####################################################

######### Pre and post process, exception point function
def set_pre_process(multiple,min_delta,delta):
    pre_process = {
        "multiple": [multiple],
        #'integer': [1],
        "min_delta":[min_delta],
        "value_exceptions": [
            [
                {
                    "e_name": "red_peak_points",
                    "e_id":  4,
                    "v_types": ["red"],
                    "condition": "(abs(delta_v)>"+str(delta)+")",
                    "para": "delta_v"
                },
                {
                    "e_name": "rest_peak_points",
                    "e_id":  5,
                    "v_types": ["rest"],
                "condition": "(abs(delta_v)>"+str(delta)+")",
                    "para": "str(delta_v)+'_'+str(ts)+'_'+str(v)"
                }
            ]
        ]
    }
    post_process = {
        "divide": [float(multiple)]
    }
    return pre_process, post_process

###############################################################    
output_file='summary_file_id_'+str(file_id)+'_output_recon.csv'

with open(output_file, 'w',0) as f_recon:
    #Output file
    writer_recon = csv.writer(f_recon,quoting=csv.QUOTE_NONNUMERIC)
    writer_recon.writerow(('file_id','npoints','sensor_id', 'sensor_name','value_name','process_id' ,'framesize','reduction',
                           'multiple','min_delta','delta(excp.)', 
                           'tss_recon_min','tss_recon_max',
                           'raw_min_value','values_recon_min',
                           'raw_max_value','values_recon_max', 
                           'values_recon.mean', 
                           'zlib_ratio', 'count_red','red_info_ratio',
                           'dev_max','dev_mean','dev_min','amplitude',
                           'relerr','mape','smape','wpe','wape','rrmse','relative_to_the_max_error','mae',
                           'file_name') )
    
    
    file_id=0 
    for file in filenamelist:    
        D = DataBank()
        ##Next for loop was taken from the bosch overview Cao sent around with some modifications I import these functions above...
        for i in range(4,get_csv_list_len(file)):
            #if i!=5:
            #    continue
            sensor_id=i-4
            #if sensor_id!=48:
            #    continue
            #if sensor_id<=45: ##Since the last crash happened at sensor 45 this run I start from that sensor...
            #    continue
            sensor_data = get_single_sensor_data(file,i)
            sensor_name = get_sensor_name(file,i)
            value_names= [sensor_name]
            #output_name = 'Bosch_'+sensor_name
            # output_name = sensor_name
             
            #print 'sensor_id', i-4 , ' , sensor_name:',sensor_name
            print 'sensor_id', sensor_id , ' , sensor_name:',sensor_name
            
            
            
            #D.add_sensor_data(sensor_data[sensor_name]['values'],sensor_data[sensor_name]['tss'], i-4, output_name, sensor_name)
            ####Note!!! The timestamps converted to seconds are negative!!! wtf?
            timestamps= sensor_data[sensor_name]['tss']
            #print timestamps
            timestamps_in_miliseconds=[ (time.mktime(dt.timetuple()) + (dt.microsecond / 1000000.0))*1000 for dt in timestamps]
            if sensor_name=='IMEI': ##This sensor has some code entries with letters...so not a number...
                continue
            #D.add_sensor_data(sensor_data[sensor_name]['values'][:1000],timestamps_in_miliseconds[:1000], i-4, sensor_name, sensor_name,value_names=value_names)
            D.add_sensor_data(sensor_data[sensor_name]['values'][:5000],timestamps_in_miliseconds[:5000], sensor_id, sensor_name, sensor_name,value_names=value_names)
            
            #D.add_sensor_data(sensor_data[sensor_name]['values'],timestamps_in_miliseconds, sensor_id, sensor_name, sensor_name,value_names=value_names)
            #print timestamps_in_miliseconds

            ##In seconds as used by Markus in tk_plot
            #ts = [int( time.mktime(x.timetuple()) * 1.e3 + x.microsecond / 1.e3 ) for x in timestamps]
            #D.add_sensor_data(sensor_data[sensor_name]['values'],ts, i-4, output_name, sensor_name)
            #print ts
            
            
            
            
        print '************************************************************************************'
        print '*********** There are ', len(D.get_sensor_list()), ' sensors in DataBank ***********'
        print '************************************************************************************'
       
        ##Print the sensor_summary i.e #sensors cumulative functions and sampling rate...x
        P=Plot(D)
        P.print_sensor_summary()
        
        
        ##################### Process each sensor added to the DataBank...
        for sensor in D.get_sensor_list():
            index=sensor['index']
            dof=sensor['dof']
            sensor_id=sensor['sensor_id']
            sensor_name=sensor['sensor_name']
            
            #Raw data info
            raw_data=D.get_sensor_raw_data(sensor_id)
            tss=np.asarray(raw_data['tss'])
            values=np.asarray(raw_data['values'])
            npoints=raw_data['count']
            
            ##The next works if there dof=1
            max=raw_data['max_v'][0]
            min=raw_data['min_v'][0]
            
            #print 'max: ',max
            #if npoints<=400: ##Not process sensors with less than 400 data points
            #    continue
            
            #counter_process_exception=0
            counter_process_good_profile=0 ##This variables are to process exception and good profile only once!...no need to loop over reductions
            #process_id=0 #Not used as an additive variable! just for the fist process
            for item in process_cases:  
                reduction=item[0]
                framesize=item[1]
                min_delta=item[2]
                

                #################### Set process parameters depending on the kind of sensor....
                ###########    Note!! If these conditions are changed they also have to be changed below in the suptitle for the plots...
                #if sensor_name in sensors_for_exception:
                #    delta=10
                #    min_delta=0
                #    pre_process,post_process=set_pre_process(multiple=multiple,min_delta=min_delta,delta=delta)
                    
                #if sensor_name in sensors_for_min_delta:
                #    amplitude= np.abs(max-min)
                #    min_delta= amplitude*min_delta*multiple ##min delta is then a fraction of the amplitude...but in principle should be less than the noise level not to cut away frecuencies that might be interesting
                    
                #    delta=1e15 ##As suggested by Qing...if no exception detection is required..set this number to a high value...
                #    pre_process,post_process=set_pre_process(multiple=multiple,min_delta=min_delta,delta=delta)
                #else:
                #    min_delta=0
                #    delta=1e15
                #    pre_process={}
                #    post_process={}
                
                framesize=all_sensors_parameters[sensor_name][0]
                reduction=all_sensors_parameters[sensor_name][1]
                multiple=all_sensors_parameters[sensor_name][2]
                min_delta=all_sensors_parameters[sensor_name][3]
                delta=all_sensors_parameters[sensor_name][4]
                
                pre_process,post_process=set_pre_process(multiple=multiple,min_delta=min_delta,delta=delta)
                
                #if sensor_name!='Latitude':
                    #if sensor_name!="AFS_mAirPerCylFlt":
                    #if sensor_name!="LSU_rLam_[0]":
                #if sensor_name!='DFES_numDFC_[1]':
                #if sensor_name=="PFltLd_mSotSimNoCont" or sensor_name=="PFltLd_mSotSim":
                #    continue ##This sensor shows an error if all the data is processed....why?...skiped for the moment...
                    
                print 'Processing sensor, ',sensor_id, ' ,', sensor_name
                print 'npoints', npoints
                print 'block:' , block
                print 'pre_process:', pre_process
                print 'post_process:', post_process
                
                ##Process Data
                D.sensor_data_process(sensor_id, framesize, reduction,'cii_dct_v011', pre_process=pre_process, post_process=post_process,block=block)
                process_id=len(D.get_sensor_info(sensor_id)['processed_data'])-1
                print 'Processed sensor, ',sensor_id, ' ,', sensor_name, ', framesize: ', framesize, ' ,reduction: ', reduction
                print 'process_id=', process_id
                #print 'block:' , block
                #print 'pre_process:', pre_process
                #print 'post_process:', post_process

                
                #Recon data info
                recon_data=D.get_sensor_recon_data(sensor_id,process_id)
                tss_recon=np.asarray(recon_data['tss'])
                values_recon=np.asarray(recon_data['values'])
                zlib_ratio=D.get_zip_reduction_ratio(sensor_id,framesize)
                
                #Reduce info
                reduce_info=D.get_sensor_process_info(sensor_id, process_id)['reduce_info']
                count_red=reduce_info['count_red']
                red_info_ratio=reduce_info['ratio']
                
                #Deviation_info
                deviations=D.get_deviations(sensor_id,process_id)
            
                for i in range(dof):
                    value_name=sensor['value_names'][i]
                    values_comp=values_recon.T[i]
                    dev_max=deviations['E'][i]['max']
                    dev_mean=deviations['E'][i]['mean']
                    dev_min=deviations['E'][i]['min']
                
                    relerr=deviations['relerr'][i]
                    mae=deviations['MAE'][i]
                    mape=deviations['MAPE%'][i]
                    smape=deviations['SMAPE%'][i]
                    rrmse=deviations['RRMSE%'][i]
                    wape=deviations['WAPE%'][i]
                    wpe=deviations['WPE%'][i]
                    
                    ####### Plots##################
                    P=Plot(D)
                    ##Set titles of the plots depending on the pre_process used...
                    #if sensor_name in sensors_for_exception:
                    #    suptitle='sensor '+str(sensor_id)+', '+sensor_name+' ,process_id:'+str(process_id)+' ,framesize: '+str(framesize)+' ,reduction: '+str(round(red_info_ratio*100,3))+'%'+' ,Delta for exception: '+str(delta)+', rrmse='+str(round(rrmse,4))+'%'
                        
                    #if sensor_name in sensors_for_min_delta:
                    #    suptitle='sensor '+str(sensor_id)+', '+sensor_name+' ,process_id:'+str(process_id)+' ,framesize: '+str(framesize)+' ,reduction: '+str(round(red_info_ratio*100,3))+'%'+' ,min_delta '+str(min_delta)+', rrmse='+str(round(rrmse,4))+'%'
                        
                    #else:
                    #    suptitle='sensor '+str(sensor_id)+', '+sensor_name+' ,process_id:'+str(process_id)+' ,framesize: '+str(framesize)+' ,reduction: '+str(round(red_info_ratio*100,3))+'%'+', rrmse='+str(round(rrmse,4))+'%'
                     
                    #suptitle='sid_'+str(sensor_id)+'_'+sensor_name+'_pid'+str(process_id)+'_f'+str(framesize)+'_red'+str(round(red_info_ratio*100,2))+'_rrmse'+str(round(rrmse,2))+'_mindelta'+str(round(min_delta,2))+'_delta'+str(round(delta,2))
                    
                    suptitle='sid_'+str(sensor_id)+'_'+sensor_name+'_pid'+str(process_id)+'_f'+str(framesize)+'_corered'+str(round(reduction*100,2))+'_red'+str(round(red_info_ratio*100,2))+'_rrmse'+str(round(rrmse,2))+'_mindelta'+str(round(min_delta,2))+'_delta'+str(round(delta,2))

                    P.plot(sensor_id,process_id,deviation='difference',dt_format='%H:%M:%S',save_png=True,suptitle=suptitle) 
                    
                    #P.plot_rec_inspection(sensor_id, process_id, output='png', fft_yscale='log', dt_format='%M:%S')
                    ##Close plt figures to avoid a massive memory leak...
                    plt.close('all')
                    gc.collect()
                    #################################

                    ##A relative error measurement they want to see
                    amplitude=np.abs(max-min)
                    if np.abs(dev_min)>np.abs(dev_max):
                        relative_error=np.abs(dev_min/amplitude)*100 
                    else:
                        relative_error=np.abs(dev_max/amplitude)*100 
                    
                    #Write output
                    writer_recon.writerow((file_id,npoints,sensor_id,sensor_name, value_name,process_id ,framesize,reduction, 
                                           multiple, min_delta,delta,
                                           tss_recon.min(),tss_recon.max(),min,float("%.3f"%values_comp.min()),max, float("%.3f"%values_comp.max()), 
                                           float("%.3f"%values_comp.mean()), float("%.3f"%zlib_ratio), count_red,float("%.3f"%red_info_ratio), 
                                           dev_max,dev_mean,dev_min,amplitude,
                                           relerr,mape,smape,wpe,wape,rrmse,relative_error,mae,
                                           file))
                    
                    #process_id=process_id+1
        print '** Goes in file: ',file_id, ' ', file
        name='output_file_id_'+str(file_id)+'.dbk'
        D.save(name)
        file_id=file_id+1
            
