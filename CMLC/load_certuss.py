##Starting from the original script Daniel Sent me via slack
from tk_databank import DataBank

from tk_plot import Plot,SignalPlot,ts_to_dt
#from tk_plot import *

from sensor_data.cmlc_dataset import cmlc_dataset
import numpy as np
import csv
import os, sys, time, datetime
import gc

import matplotlib
matplotlib.rcParams.update({'font.size': 30})
matplotlib.rcParams['agg.path.chunksize'] = 10000

import matplotlib.pyplot as plt
import matplotlib.dates as md



D = DataBank()
print 'Loading data set...'
D.load_dataset(cmlc_dataset, True, True)

print D._sensors

pre_process = {
    "multiple": [1]
    #"value_exceptions": [
    #    [
    #        {
    #            "e_name": "red_peak_points",
    #            "e_id":  4,
    #            "v_types": ["red"],
    #            "condition": "(abs(delta_v)>1000)",
    #            "para": "delta_v"
    #        },
    #        {
    #            "e_name": "rest_peak_points",
    #            "e_id":  5,
    #            "v_types": ["rest"],
    #            "condition": "(abs(delta_v)>1000)",
    #            "para": "str(delta_v)+'_'+str(ts)+'_'+str(v)"
    #        }
    #    ]
    #]
   }
pre_process['integer'] = [1]
post_process = {
    "divide": [1.0]
}
pre_process['min_delta'] = [-1]

#D.sensor_data_process(10, 50, 0.5, 'aii_PCA_v011', pre_process, post_process, 5)
#D.write_sensor_data_files(10)

P=Plot(D)
P.print_sensor_summary()

##Print summary of raw data!!...and make inspection plots...

output_file_name='summary_certus_output_raw.csv'
with open(output_file_name, 'w',0) as f_raw:
    writer_raw = csv.writer(f_raw,quoting=csv.QUOTE_NONNUMERIC)
    writer_raw.writerow(('sensor_id','index','sensor_type','sensor_name','value_name','unit','npoints', 'tss.min()','tss.max()','value_min','value_max', 'file_name'))
    
    for sensor in D.get_sensor_list():
    
        index=sensor['index']
        dof=sensor['dof']
        sensor_id=sensor['sensor_id']
        sensor_name=sensor['sensor_name']
        
        #Two new entries!
        sensor_file=sensor['path']
        sensor_unit=sensor['unit']
        sensor_type=sensor['sensor_type']
        
        
        raw_data=sensor['sensor_data'].get_raw_data()
        npoints=raw_data['count']
        tss_raw=raw_data['tss']
        tss_raw_array=np.asarray(tss_raw)
        
        timespan_seconds=(tss_raw[-1]-tss_raw[0])/1000.
        sampling_rate=npoints/timespan_seconds
        
        #dt_format='%Y-%m-%d %H:%M:%S'
        dt_format='%Y-%m-%d %H:%M'
        xfmt = md.DateFormatter(dt_format)
        
        tss_raw_dt = [ts_to_dt(elm, dt_format=dt_format) for elm in tss_raw] #convert to time stamps...Taken from tk_plot.plot
        print 'len(tss_raw_dt)', len(tss_raw_dt)
        
        for i in range(dof):
            value_name=sensor['value_names'][i]
            values=zip(*raw_data['values'])[i]#Taken from tk_plot.plot
            print 'len(values)', len(values)
            #values_array=np.asarray(values)
            value_min=raw_data['min_v'][i]
            value_max=raw_data['max_v'][i]
            
            writer_raw.writerow((sensor_id,index,sensor_type,sensor_name,value_name,sensor_unit,npoints, tss_raw_array.min(),tss_raw_array.max(),float("%.3f"%value_min),float("%.3f"%value_max), sensor_file))
            
            ############ Make Plots for raw using tk_plot function################
            fig1, ax1 = plt.subplots(1,1,figsize=(35,25))
            fig1.suptitle(sensor_name+'  , sensor type: '+sensor_type+'  , Number of points: '+str(npoints)+'  , Sampling Rate: '+str(round(sampling_rate*1e3,3))+' mHz', fontsize=40)
            

            ##Attempt to use SignalPlot class
            #sp = SignalPlot(ax=ax1)
            ##Value names here are actually the units...(tk_databank bug!!)
            #sp.set_params(title=value_name, dt_format=dt_format)
            #sp.add_signal(tss_raw_dt, values, 'raw')
            
            if npoints>100:
                ax1.plot(tss_raw_dt[:100],values[:100],label='raw',marker='o',markersize=15,color='g',lw=2)
            else:
                ax1.plot(tss_raw_dt,values,label='raw',marker='o',markersize=15,color='g',lw=2)
            #ax1.set_title(value_name)
            ax1.set_xlabel('Time')
            ax1.set_ylabel(sensor_unit)
            ax1.xaxis.set_major_formatter(xfmt)
            
            plt.xticks( rotation=25 )
            
            #if npoints>1000:
            folder='raw_plots_100_points'
            #else:
            #    folder='raw_plots'
                
            if not os.path.exists('./'+folder):
                os.makedirs('./'+folder)
            fig1.savefig(folder+'/sensor_id_'+str(sensor_id)+'_sensor_name_'+sensor_name+'.png')
            ####################################################################
            print 'Making plot for sensor_id: ', sensor_id, 'sensor_name:', sensor_name
            
            ##close everything
            plt.clf()
            plt.close('all')
            #file_id=file_id+1
            gc.collect()
                                
        #Make inspaection plots!
        #print 'Making plot for sensor_id: ', sensor_id, 'sensor_name:', sensor_name
        #P.plot_raw_inspection(sensor_id, output='png', fft_yscale='log', dt_format='%Y-%m-%d %H:%M:%S')
        


    
    
