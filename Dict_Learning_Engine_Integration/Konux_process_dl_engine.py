##Original from: encode_decode_example_DL.py Daniel sent me
##Removed unnecesary lines...

import tk_gdfr
from tk_databank import DataBank
from tk_plot import Plot
import numpy as np
import sys
import os
import glob
import csv 

if __name__ == '__main__':
            
    Dat = DataBank()  #call it Dat because D represents the Dictionary in what follows
    P = Plot(Dat)
    
    value_names=['sensor3']
    sensor_name='sensor3'
    sensor_id=0
    
    
    
    folder='/home/angelh/Documents/DataScienceRelated/Teraki/Konux/Files/'
    filenamelist=sorted(glob.glob(folder+'*.csv'),key=os.path.getsize,reverse=True)
    
    file = filenamelist[0]
    print file
    sensor_data1 = tk_gdfr.get_ts_value_from_file(file, ',', 'pos', 'pos', 0, [3]) ##[3] is the 4th Column of the Konux Data
    
    number_of_points=2000
    len_data=len(sensor_data1['pos']['values'][:number_of_points])
    #len_data=len(sensor_data1['pos']['values'])
    #timestamps=np.arange(0,len_data)*1000 
    timestamps=np.arange(0,len_data) ##every ms
    
    ##Add data to a databank
    #S1 = Dat.add_sensor_data(sensor_data1['pos']['values'],
    #                       sensor_data1['pos']['tss'], sensor_id,sensor_name, sensor_name,value_names=value_names)
    S1 = Dat.add_sensor_data(sensor_data1['pos']['values'][:number_of_points],
                             timestamps, sensor_id, sensor_name, "pos",value_names=value_names)
					  
					  
    raw_data = Dat.get_sensor_raw_data(sensor_id) 
    tss = np.asarray(raw_data['tss'])
    values = np.asarray(raw_data['values'])
    npoints = raw_data['count']
    
    #hh=sensor_data1['pos']['values'][2:]
    #maxmin=np.max(hh)-np.min(hh)
    maxmin=np.max(values)-np.min(values) ##maxmin is how Ettine call this amplitude...is used in the definition of tol_1 for DictLearning
    ##Saved Data
    #np.savetxt('./Data_Konux_sensor_%d.txt'%sensor_id,values)
    
    ##Convert values=[[1],[2],[3],[4]...] to [1,2,3,4...] ala Ettine...this should be the same as np.concatenate(values)
    data_tmp = values
    data_tmp = [x[0] for x in data_tmp] 
    data_tmp = np.asarray(data_tmp)
    
    pre_process = {
        "multiple": [1],
        #"multiple": [1000],
        #'integer': [1],
        #'min_delta': [50],
        "value_exceptions": [
            [
                {
                    "e_name": "red_peak_points",
                    "e_id":  4,
                    "v_types": ["red"],
                    #"condition": "(abs(delta_v)>100)",
                    "condition": "(abs(delta_v)>1e15)",
                    "para": "delta_v"
                },
                {
                    "e_name": "rest_peak_points",
                    "e_id":  5,
                    "v_types": ["rest"],
                    #"condition": "(abs(delta_v)>100)",
                    "condition": "(abs(delta_v)>1e15)",
                    "para": "str(delta_v)+'_'+str(ts)+'_'+str(v)"
                }
            ]
        ]
    }
    post_process= {
        #"divide": [1000.0]
        "divide": [1.0]
        
    }
					  
    #Dat.sensor_raw_data_clean(sensor_id, 20)
    
    ##Default parameters Daniel was using in his original scripts
    framesize=50
    core_reduction=0.7 
    block=10
    
    ##############################################################################
    ##############################################################################
    ##Generate Dictionary using dedicated class...
    ##The next lines are a Copy paste from Ettine Example...encode_decode_example_AirBus.py
    
    ##################Note!!!!#################################
    #To mantain order i.e creating the dictionary together with loading it if already exists should be a function of tk_dictionary_learning!!
    ############################################################################
    
    #loadthedictionary
    n_components=framesize
    from dict_learning_methods.dict_analysis import dict_read
    if not os.path.exists('./Dictionaries'):
        os.makedirs('./Dictionaries')
    file_dict='./Dictionaries/dictionary_konux'+'_sensorid'+str(sensor_id)+'_omp_%s_%s.csv' % (n_components, framesize)
    try:
        D=dict_read(file_dict)
    except IOError as e:
        ##If the dictionary doesn exists...create one...
        print "Unable to open Dictionary. Generating one..." #Does not exist OR no read permissions
        write=True ##Write dictionary to a file...
        plot_dict=False ##If True plot the dictionary components...
        
        config_train={
            'engine_id': "encoding_0",
            'engine_type': "encoder",
            'engine_layer': "signal",
            'did': 1,
            'f' : framesize,
            'tol_2': 0.005,
            #'tol_1': 0.01*maxmin,
            'tol_1': None,
            'tol' : -1,
            'n_iter' : 50,##50,
            'n_components' : framesize,
            'factor' : 10,
            'n_nonzero': None,
            'init' : 'svd'} 
        
        print 'Training the dictionary ...'
        ##From Ettiene Only half of the data is used to train the Dictionary...
        y_1=data_tmp[:len(data_tmp)/2] 
        from dict_learning_methods.tk_dictionary_learning import DictionaryTraining
        dictio=DictionaryTraining(config_train)
        D=dictio.fit(y_1)
        #print "Dictionary", D
        print 'Dictionary trained.'
        
        ##Write the created dictionary to a file..
        if (write):
            with open(file_dict, "wb") as f:
                csvfile = csv.writer(f, delimiter=';')
                for i in range(len(D[0])):
                    csvfile.writerow((D[:,i]))
            f.close()    
        
    ##############################################################################
    ##############################################################################
    #Process Data with the new engine that Used the generated Dictionary... cii_dl_v013
    #########
    process_id=Dat.sensor_data_process(sensor_id, framesize, core_reduction, 'cii_dl_v013', Dictionary=D,pre_process=pre_process, post_process=post_process,raw_tss=True,block=block)
    #process_id=Dat.sensor_data_process(sensor_id, framesize, core_reduction, 'cii_dct_v012', pre_process, post_process, block)
    
    reduce_info=Dat.get_sensor_process_info(sensor_id, process_id)['reduce_info']
    count_red=reduce_info['count_red']
    red_info_ratio=reduce_info['ratio']

    #Deviation_info
    deviations=Dat.get_deviations(sensor_id,process_id)
    rrmse=deviations['RRMSE%'][0] ##First degree of freedom....
    
    print "*********************************"
    print 'Effective reduction: ' , red_info_ratio
    print 'rrmse: ', rrmse
    print "*********************************"
    
    suptitle='Eff.Reduction: %.3f , rrmse: %.3f, sid: %d'%(red_info_ratio,rrmse,sensor_id)    
    #P.plot(sensor_id,0,deviation='difference',save_png=True)
    P.plot(sensor_id,process_id,deviation='difference',output='screen',dt_format="%S.%f",suptitle=suptitle)
    P.plot(sensor_id,process_id,deviation='difference',output='png',dt_format="%S.%f",suptitle=suptitle)
    

    
    
    
    
    
    
    
    
