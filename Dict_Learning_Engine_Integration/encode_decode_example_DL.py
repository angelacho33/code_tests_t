# -*- coding: utf-8 -*-
"""
Created on Thu Aug 04 10:20:59 2016

@author: Etienne
"""

from tk_dictionary_encoder import SignalDictionaryEncoder
from tk_dictionary_decoder import SignalDictionaryDecoder
import tk_gdfr
from tk_databank import DataBank
from tk_plot import Plot
import numpy as np
from dict_analysis import dict_read

if __name__ == '__main__':
	
    import sys
    import os
    import glob
    
    Dat = DataBank()  #call it Dat because D represents the Dictionary in what follows
    P = Plot(Dat)
    value_names = 'velocity'
    numberofdata=2
    frame_size=15
    n_components=frame_size
    file = 'Data/BMW/%s_data_%s.csv' %(value_names, numberofdata)
    
    scenario_input=int(sys.argv[1]) ##0:city ##1 overland
    scenario=['city','overland']

    #folder='/Users/danielrichart/Documents/Projects/Research/Data/BMW/UseCase3/2016-05-19_Sample_02_Original/'
    #folder='/home/angelh/Documents/DataScienceRelated/Teraki/Files/BMW_UC3/2016_05_19_Sample_02_Original_3/'
    #folder='/Users/danielrichart/Documents/Projects/Research/Code/ml_code/ml_dev/dict_learning methods/Data/BMW_Velocity/'
    folder='/home/angelh/Documents/DataScienceRelated/Teraki/Files/BMW_UC3/2016_05_19_Sample_02_Original_3/'
    #filenamelist_positions = sorted(glob.glob(folder+scenario[scenario_input]+'/*positions.csv'),key=os.path.getsize,reverse=True)
    filenamelist_turnrate = sorted(glob.glob(folder+scenario[scenario_input]+'/*velocity.csv'),key=os.path.getsize,reverse=True)
    
    file = filenamelist_turnrate[1]
    
    working_folder="/home/angelh/Documents/DataScienceRelated/Teraki/ml_dev_tests/test2_engine/"
    
    sensor_data1 = tk_gdfr.get_ts_value_from_file(file, ',', 'pos', 'pos', 0, [1])
    sensor_id=1
    ##Add data to a databank
    S1 = Dat.add_sensor_data(sensor_data1['pos']['values'][1:],
                           sensor_data1['pos']['tss'][1:], sensor_id, "velocity", "pos",value_names=value_names)
					  
					  
    raw_data = Dat.get_sensor_raw_data(sensor_id) 
    tss = np.asarray(raw_data['tss'])
    values = np.asarray(raw_data['values'])
    npoints = raw_data['count']
					  
					  
    np.savetxt('./Data_BMW_Velocity_City.txt',values)
    
    pre_process_velocity = {
        "multiple": [1000],
        'integer': [1],
        #'min_delta': [50],
        "value_exceptions": [
            [
                {
                    "e_name": "red_peak_points",
                    "e_id":  4,
                    "v_types": ["red"],
                    "condition": "(abs(delta_v)>100)",
                    "para": "delta_v"
                },
                {
                    "e_name": "rest_peak_points",
                    "e_id":  5,
                    "v_types": ["rest"],
                    "condition": "(abs(delta_v)>100)",
                    "para": "str(delta_v)+'_'+str(ts)+'_'+str(v)"
                }
            ]
        ]
    }
    post_process_velocity = {
        "divide": [1000.0]
    }
					  
    #Dat.sensor_raw_data_clean(sensor_id, 20)
    
    Dat.sensor_data_process(sensor_id, 50, 0.1, 'cii_dl_v012', pre_process_velocity, post_process_velocity, 10)
    
    P.plot(sensor_id,0,deviation='difference',save_png=True)
    #print "*********** MIN_VALUES ***************"
    #print D.get_sensor_raw_data(8)['min_v']
    #print "*********** MAX_VALUES ***************"
    #print D.get_sensor_raw_data(8)['max_v']
    #print "********** REDUCATION INFO ***********"
    #print D.get_sensor_process_info(8, 0)['reduce_info']
    #print "This is the ratio",D.get_sensor_process_info(8, 0)['reduce_info']['ratio']
    #print D.get_zip_reduction_ratio(8, 500)
    #print P.print_deviations_as_table(8)
    #print "********** End of SENSOR 8 ***********"
    #print D.get_sensor_red_data(8, 0)
    
    #D.write_sensor_data_files(sensor_id)
    
    
    ####################   Interpolation Daniel implemented
    #Raw data info
    #raw_data=Dat.get_sensor_raw_data(sensor_id)
    #tss=np.asarray(raw_data['tss'])
    #values=np.asarray(raw_data['values'])
    #npoints=raw_data['count']
    
    
    data_tmp = values
    #print data_tmp
    data_tmp = [x[0] for x in data_tmp]
#    print data_tmp
    data_tmp = np.asarray(data_tmp)
    
    #data_tmp = data_tmp[0:,1]#.tolist()

    #data_tmp = []
    #for row in data.value:
    #    data_tmp.append(row)
    ##    data_codename_all.append(row)
    ##        if row not in CodeNames_All:
    ##           CodeNames_All.append(row)
    
#    print "This is the new dataset",data_tmp
    print "This is the dataset length",len(data_tmp)
    len_recon=len(data_tmp)     
    if (len_recon%frame_size>0): #this step is to make sure that we can divide the length of the signal by the number of segments we set
        for j in range (frame_size-(len_recon%frame_size)):
            data_tmp=np.concatenate((data_tmp, [data_tmp[len_recon-1]]))
  
    

    #loadthedictionary
    #file_dict='Dictionaries/dictionary_bmw%s_omp_%s_%s.csv' % (value_names, n_components, frame_size)
    file_dict='./dictionary_bmw%s_omp_%s_%s.csv' % (value_names, n_components, frame_size)
    try:
        D=dict_read(file_dict)
    except IOError as e:
        print "Unable to open file" #Does not exist OR no read permissions
        print 'Training the dictionary ...'
        y_1=data_tmp[:len(data_tmp)/2]
        if (len(y_1)%frame_size>0): #this step is to make sure that we can divide the length of the signal by the number of segments we set
            for j in range (frame_size-(len(y_1)%frame_size)):
                y_1=np.concatenate((y_1, [y_1[len(y_1)-1]]))
        variance_1=(np.mean(y_1**2)-np.mean(y_1)**2) 
    
        y_1=y_1.reshape(len(y_1)/frame_size, frame_size)
        #from sklearn.decomposition import DictionaryLearning
        from dict_learning import DictionaryLearning ##Modified routine...
        #from ksvd import KSVD
        #D_0,c_0=KSVD(y_1, n_components, frame_size/3, 5, enable_threading=False)
        D_0=None
        c_0=None
        dictio = DictionaryLearning(n_components=n_components, alpha=1.0*np.sqrt(variance_1), 
                                    max_iter=2, tol=-1,
                                    fit_algorithm='omp', transform_algorithm='omp', transform_n_nonzero_coefs=None, transform_alpha=0.02*0.02*variance_1, n_jobs=1, 
                                    code_init=c_0, dict_init=D_0, verbose=False, 
                                    split_sign=False, random_state=None, tol_2=0.02*0.02*variance_1*5, tol_1=0.01*5, n_nonzero_coefs=None)
                                    
                                    
        #On this step is the dictionary learned, and x is then transformed in the new basis
        x=dictio.fit_transform(y_1)
        
        D=dictio.components_ #here is the final dictionary    
        
        dictio = DictionaryLearning(n_components=n_components, alpha=1.0*np.sqrt(variance_1), 
                                    max_iter=50, tol=-1,
                                    fit_algorithm='omp', transform_algorithm='omp', transform_n_nonzero_coefs=None, transform_alpha=0.02*0.02*variance_1, n_jobs=1, 
                                    code_init=x, dict_init=D, verbose=False, 
                                    split_sign=False, random_state=None, tol_2=0.02*0.02*variance_1, tol_1=0.1, n_nonzero_coefs=None)
                                    
                                    
        #On this step is the dictionary learned, and x is then transformed in the new basis
        x=dictio.fit_transform(y_1)
        #Analyses the results        
        D=dictio.components_ #here is the final dictionary
        print 'Dictionary trained.'
    
    
    config_enc={'engine_id': "encoding_0",
                'engine_type': "encoder",
                'engine_layer': "signal",
                'did': 1,
                'f': frame_size,
                'd': D,
                'tol_2': 0.02,
                'tol_1': 0.01,
                'n_nonzero': None}    
    encoder=SignalDictionaryEncoder(config_enc)
    
    red,_=encoder.single_encoding(data_tmp)
#    red=np.array(red)
    
    print "This is the reduced data shape", red.shape
    
    
    config_dec={'engine_id': "encoding_0",
                'engine_type': "encoder",
                'engine_layer': "signal",
                'did': 1,
                'f': frame_size,
                'd': D}
    decoder=SignalDictionaryDecoder(config_dec)
    recon,_=decoder.single_decoding(red, length=len(data_tmp))
    recon=recon[:len_recon]
    
    process_id=Dat.add_processed_sensor_data(sensor_id, recon, tss, datatype='recon_data', meta={'reduction' : 100*(1-(red.shape[0]+0.0)/len(data_tmp)),
                                                                                                'framesize' : frame_size})
  
    #Dat.save("./Saved_Databank/BMW_%s_%s.dbk" %(value_names, numberofdata))   
    Dat.save("./BMW_%s_%s.dbk" %(value_names, numberofdata))   
    
    print 'Databank has been saved.'
    P.plot(sensor_id, process_id=process_id, deviation='difference', save_png=True)
