# -*- coding: utf-8 -*-
"""
Created on Thu Aug 04 10:20:59 2016

@author: Etienne
"""

from tk_dictionary_encoder import SignalDictionaryEncoder
from tk_dictionary_decoder import SignalDictionaryDecoder
import tk_gdfr
from tk_databank import DataBank
from tk_plot import Plot
import numpy as np
from dict_analysis import dict_read

if __name__ == '__main__':
	
    import sys
    import os
    import glob
    
    Dat = DataBank()  #call it Dat because D represents the Dictionary in what follows
    P = Plot(Dat)
    value_names = 'velocity'
    numberofdata=2
    frame_size=15
    n_components=frame_size
    file = 'Data/BMW/%s_data_%s.csv' %(value_names, numberofdata)
    
    scenario_input=int(sys.argv[1]) ##0:city ##1 overland
    scenario=['city','overland']

    folder='/home/angelh/Documents/DataScienceRelated/Teraki/Files/BMW_UC3/2016_05_19_Sample_02_Original_3/'
    #filenamelist_positions = sorted(glob.glob(folder+scenario[scenario_input]+'/*positions.csv'),key=os.path.getsize,reverse=True)
    filenamelist = sorted(glob.glob(folder+scenario[scenario_input]+'/*velocity.csv'),key=os.path.getsize,reverse=True)
    
    file = filenamelist[1]
    
    #working_folder="/home/angelh/Documents/DataScienceRelated/Teraki/ml_dev_tests/test2_engine/"
    
    sensor_data1 = tk_gdfr.get_ts_value_from_file(file, ',', 'pos', 'pos', 0, [1])
    sensor_id=1
    
    ##Add data to a databank
    S1 = Dat.add_sensor_data(sensor_data1['pos']['values'][1:],
                           sensor_data1['pos']['tss'][1:], sensor_id, "velocity", "pos",value_names=value_names)
					  
					  
    raw_data = Dat.get_sensor_raw_data(sensor_id) 
    tss = np.asarray(raw_data['tss'])
    values = np.asarray(raw_data['values'])
    npoints = raw_data['count']
					  
					  
    np.savetxt('./Data_BMW_Velocity_City.txt',values)
    
    pre_process_velocity = {
        "multiple": [1000],
        'integer': [1],
        #'min_delta': [50],
        "value_exceptions": [
            [
                {
                    "e_name": "red_peak_points",
                    "e_id":  4,
                    "v_types": ["red"],
                    "condition": "(abs(delta_v)>100)",
                    "para": "delta_v"
                },
                {
                    "e_name": "rest_peak_points",
                    "e_id":  5,
                    "v_types": ["rest"],
                    "condition": "(abs(delta_v)>100)",
                    "para": "str(delta_v)+'_'+str(ts)+'_'+str(v)"
                }
            ]
        ]
    }
    post_process_velocity = {
        "divide": [1000.0]
    }
					  
    #Dat.sensor_raw_data_clean(sensor_id, 20)
    
    ##Default parameter Daniel was using in his original scripts
    core_reduction=0.1
    framesize=50
    block=10
    #############################################################

    Dat.sensor_data_process(sensor_id, framesize, core_reduction, 'cii_dl_v012', pre_process_velocity, post_process_velocity, block)
    
    P.plot(sensor_id,0,deviation='difference',save_png=True)
    
    reduce_info=Dat.get_sensor_process_info(sensor_id, process_id)['reduce_info']
    count_red=reduce_info['count_red']
    red_info_ratio=reduce_info['ratio']

    #Deviation_info
    deviations=Dat.get_deviations(sensor_id,process_id)
    rrmse=deviations['RRMSE%'][0] ##First degree of freedom....
    
    print "*********************************"
    print 'Effective reduction: ' , red_info_ratio
    print 'rrmse: ', rrmse
    print "*********************************"
    
    
    
    
    
    
    
    
    
