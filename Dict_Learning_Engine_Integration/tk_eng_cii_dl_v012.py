"""
Project TERAKI Middleware
Author: Markus K.
Class for data decomposition and reconstruction using the selective sampling scheme
   Dedcomposer:
   1) Initialize the Decomposer
   2) get list, xs1 and make it available to the reconstructor on server side
   3) Decompose data (continously (in a loop))
   Reconstructor:
   1) Initialize Reconstructor
   2) set list and xs1, provided by decomposer
   3) reconstruct decomposed data (continously (in a loop))
"""

import numpy as np
#import pandas as pd
#import numpy.core._dotblas
import random
import time
#import scipy.fftpack
import ctypes
from os import uname
from math import cos
import scipy.fftpack
from scipy.linalg import hadamard
from math import sqrt
import math as mt
import scipy.sparse as sparse
import EngineC # This is the improved EngineB library
import scipy.fftpack as sf

working_folder="/home/angelh/Documents/DataScienceRelated/Teraki/ml_dev_tests/test2_engine/"

platform = uname()[0].lower()
if (platform == 'darwin'):
	platform = 'mac'

shared_lib = 'tk_lasso_a2_'+platform+'.so'


#-------------------------------------------------------------------------------
def tk_eye(n):
	""" Creates identity matrix as a list of lists
	Args:
		n: Spcifies size of the n-by-n identity matrix
	Returns:
		Identity matrix (ones on the main diagonal, zeros otherwise) 
		of size n-by-n as list of lists. Elements are of type float.
	"""
	identity = [[float(i==j) for i in range(n)] for j in range(n)]
	return identity


#-------------------------------------------------------------------------------
def dct_iii(X):
	X = np.array(X)
	PI = 3.14159265359
	N = X.shape[0]
	x = [None] * N
	for n in range(N):
		summation = 0.
		for k in range(N):
			s = 0.5**0.5 if k==0 else 1.0
			summation += s * X[k] * cos(PI * (n + 0.5) * k / N)
			x[n] = summation * (2.0 / N)**0.5
	x_reform = [list(a) for a in zip(*x)]
	return x_reform
	
def dwt_iii(X):
	
	X = np.array(X)
	PI = 3.14159265359
	N = X.shape[0]
	s = (N,N)
	s2 = (0,N-1)
	ih = np.zeros(s)
	h = np.zeros(s)
	
	for k in range(0,N-1):
		p = mt.floor(mt.log(k+1,2))
		q = (k+1) - 2**p
		k1 = 2**p
		t1 = N/k1
		k2 = int(2**(p+1))
		t2 = N/k2
		if k == 0:
			for i in range(N):
				h[0,i] = 1 / mt.sqrt(N)
		for i in range(t2):
			h[k+1,i+q*t1] = (2**(p/2))/mt.sqrt(N)
			h[k+1,i+q*t1+t2] = -(2**(p/2))/mt.sqrt(N)

	return h
				#haar.append(h)
			#haar = [list(a) for a in zip(*h)]


#-------------------------------------------------------------------------------
class Decomposer(object):
	""" Decomposer class definition
    private data:
        _sparsity: the sparsity of the original data
		_n: number of points in raw data frame
		_m: number of points in resulting decomposed data
		_meas: matrix containing basis for generating measurment matrix A
		_list: m random values, representing index of basis vecotrs 
		_xs1: starting vector for iterative optimization process in reconstr.
	public functions:
		get_list():
		get_xs1():
		decompose:
	 """
	
	def __init__(self, sparsity, n, opt='EngC'):
		""" Initialize Decomposer instance
		Args:
			sparsity: the sparsity of the original data
			n: number of points in raw data frame
		Returns:
			None
		"""
		self._sparsity       = sparsity
		self._n              = n
		self._m              = int(sparsity * n)		
		self._meas           = None
		self._meas_cs        = None
		self._list           = None

		if opt == 'dct':
			#eye = np.identity(self._n)
			#self._meas = scipy.fftpack.dct(eye, type=3, axis=-1, norm='ortho', overwrite_x=False)
			eye = tk_eye(self._n)		# return value is a list of lists (not numpy array)
			self._meas = scipy.fftpack.dct(eye, type=3, axis=-1, norm='ortho', overwrite_x=False)	# DCT type III (inverse of type II), normalized #dct_iii(eye)
		elif opt == 'fourier':
			eye = np.identity(self._n)
			self._meas = np.fft.fft(eye)
		elif opt == 'BWHT':
			blk_size = 8
			self._eye = np.identity(self._n)
			self._eye_blk = np.identity(self._n/blk_size)
			self._meas = np.kron(self._eye_blk,hadamard(blk_size)/sqrt(blk_size))
			self._psi = scipy.fftpack.idct(self._eye, type=3, axis=-1, norm='ortho', overwrite_x=False)
			#q3 = randperm(N);
			#p3 = randperm(N);
		elif opt == 'PCA':
			Sample_size = 100
			blk_size = 8
			self._eye = np.identity(self._n)
			self._eye_blk = np.identity(self._n/blk_size)
			self._meas = np.kron(self._eye_blk,hadamard(blk_size)/sqrt(blk_size))
			self._psi = np.genfromtxt('PCA_Decomposition_Intel_S'+str(Sample_size)+'.csv', delimiter=";")
			#self._psi = scipy.fftpack.idct(self._eye, type=3, axis=-1, norm='ortho', overwrite_x=False)
		elif opt == 'DWT':
			self._eye = np.identity(self._n)		# return value is a list of lists (not numpy array)
			f = open ( 'W_db20_matrix.csv' , 'r')
			self._psi = [ map(float,line.split(',')) for line in f ]
			#self._psi = np.transpose(self._psi)
		if opt == 'EngC':
			#y        = np.dot(self._meas_cs, data.T)
			self._eye = np.identity(self._n)
			self._psi = sparse.rand(self._n,self._n, density=0.05) # Sparse matrix with random entries. To replace by binary entries to make computation even more efficient.
			self._psi = self._psi.toarray()
		else:
			raise Exception('Transformation type (parameter opt) is not supported')

		if (opt == 'dct') | (opt == 'fourier'):
			# Inverse measurement matrix. Commented out in order to speed up!!!???
			meas_inv       = np.linalg.inv(self._meas)

			# Create random measurements
			random.seed(1)
			self._list = random.sample(xrange(self._n), self._m)
			self._meas_cs = meas_inv[self._list, :]

		if (opt == 'BWHT') | (opt == 'PCA') | (opt == 'DWT') | (opt == 'EngC') | (opt == 'DL'):
			random.seed(1)
			self._list_m = random.sample(xrange(self._n), self._m)
			random.seed(1)
			self._list_n = random.sample(xrange(self._n), self._n)
			#self._MeasBWHT = self._meas[self._list_m,:];
			#self._MeasBWHT = self._meas[:,self._list_n]
			self._MeasBWHT = self._eye[self._list_m,:]
			
		
	def get_list(self):
		""" Get random list
		Args: 
			None
		Returns:
			List of random indice values, 1d numpy array
		"""
		return self._list_m, self._list_n

	def get_psi(self):
		""" Get random list
		Args:
			None
		Returns:
			List of random indice values, 1d numpy array
		"""
		return self._psi
	
	def calc_ic(self, data_in):
		""" Get initial condition (starting vector) for iterative optimization
		Args:
			None
		Returns:
			Starting vector for optimization, 1d numby array
		"""
		data     = np.array(data_in,dtype=np.double)
		#print len(self._meas)
		#print len(data)
		#dataT_ft = np.dot(self._psi, data.T)
		#y        = np.dot(self._meas_cs, dataT_ft)
		#xs1_loc      = np.dot(self._meas_cs.T.conjugate(), y)
		#xs1_loc      = np.squeeze(xs1_loc)		# Remove single-dimensional entry
		return(data.tolist())
	
	def decompose(self, data_in):
		""" Fuction for the actual decomposition
		Args:
			Raw data to be decomposed
		Return:
			Decomposition values (1d numpy array)
		"""
		data    = np.array(data_in, dtype=np.float32)
		#print self._MeasBWHT
		# Perform only Trafo into frequency domain of data
		dataT_ft = np.dot(self._MeasBWHT, data.T)
		#t1 = time.time()
		#y        = np.dot(self._meas_cs, dataT_ft)
		#elapsed_enc = time.time() - t1
		#print "This is CS compression time per 100k", (elapsed_enc*96000/self._n)

		return dataT_ft, self._list_m#.tolist()


#-------------------------------------------------------------------------------
class Reconstructor(object):
	""" Reconstructor class definition
    private data:
		_sparsity: ...
	public functions:
		set_list():
		...
	"""

	def __init__(self, sparsity, n, opt='DL', max_iter=300, tol=1e-6, lmda=0.1, block=15):
		self._sparsity     = sparsity
		self._n            = n		# n defines the size of the input data vector
		self._m            = int(sparsity * n)
		self._meas         = None
		self._meas_cs      = None
		self._MeasBWHT      = None
		self._meas_inv     = None
		self._A1_mtx       = None
		self._list         = None
		self._max_iter     = max_iter
		self._tol          = tol
		self._lambda       = lmda
		self._block        = block
		

		
		if opt == 'dct':
			#eye = np.identity(self._n)
			#self._meas = scipy.fftpack.dct(eye, type=3, axis=-1, norm='ortho', overwrite_x=False)
			eye = tk_eye(self._n)		# return value is a list of lists (not numpy array)
			self._meas = scipy.fftpack.dct(eye, type=3, axis=-1, norm='ortho', overwrite_x=False)	# DCT type III (inverse of type II), normalized #dct_iii(eye)
		elif opt == 'fourier':
			eye = np.identity(self._n)
			self._meas = np.fft.fft(eye)
		elif opt == 'BWHT':
			blk_size = 8
			self._eye = np.identity(self._n)
			self._eye_blk = np.identity(self._n/blk_size)
			self._meas = np.kron(self._eye_blk,hadamard(blk_size)/sqrt(blk_size))
			self._psi = scipy.fftpack.idct(self._eye, type=3, axis=-1, norm='ortho', overwrite_x=False)
		elif opt == 'PCA':
			blk_size = 8
			Sample_size = 100
			self._eye = np.identity(self._n)
			self._eye_blk = np.identity(self._n/blk_size)
			self._meas = np.kron(self._eye_blk,hadamard(blk_size)/sqrt(blk_size))
			self._psi = np.genfromtxt('PCA_Decomposition_Intel_S'+str(Sample_size)+'.csv', delimiter=";")
			#self._psi = scipy.fftpack.idct(self._eye, type=3, axis=-1, norm='ortho', overwrite_x=False)
		elif opt == 'DWT':
			self._eye = np.identity(self._n)		# return value is a list of lists (not numpy array)
			f = open ( 'W_db20_matrix.csv' , 'r')
			self._psi = [ map(float,line.split(',')) for line in f ]
			#self._psi = np.transpose(self._psi)
			#print l
			#self._psi = pd.read_csv('W_db20_matrix.csv',sep=',')#dwt_iii(self._eye)
			#print "This is the length",len(self._psi)
			print self._psi
		elif opt == 'EngC':
			self._eye = np.identity(self._n)
			self._psi = sparse.rand(self._n,self._n, density=0.05) # Sparse matrix with random entries. To replace by binary entries to make computation even more efficient.
			self._psi = self._psi.toarray()
			self._psi = scipy.fftpack.idct(self._eye, type=3, axis=-1, norm='ortho',overwrite_x=False)
			#self._psi = scipy.fftpack.idct(self._eye, type=3, axis=-1, norm='ortho', overwrite_x=False)
		elif opt == 'DL':
			self._eye = np.identity(self._n)
			notrainrestart = 0
			plot_dict=True
			import os
			import csv
			from dict_analysis import dict_read
			import plot_dictionary as pd
			import matplotlib.pyplot as pl
			#if notrainrestart == 0:
			
			frame_size = self._n
			n_components=frame_size
				
		     #loadthedictionary
			folder_write = 'Dictionaries'
			file_dict = folder_write+'/dictionary%s_%s.csv' %(n_components, frame_size)
			try:
				self._psi=dict_read(file_dict)
			except IOError as e:
			#if notrainrestart == 0:
				#data_tmp = np.loadtxt('../Data/Data_BMW_Velocity_City.txt')
                                data_tmp = np.loadtxt(working_folder+'Data_BMW_Velocity_City.txt')
				# Check this data: It is not processed according to the multiple, exception detection. The processed data should be input here instead!!
				
				
				y_1=data_tmp[:len(data_tmp)/2]
				if (len(y_1)%frame_size>0): #this step is to make sure that we can divide the length of the signal by the number of segments we set
					for j in range (frame_size-(len(y_1)%frame_size)):
						y_1=np.concatenate((y_1, [y_1[len(y_1)-1]]))
				variance_1=(np.mean(y_1**2)-np.mean(y_1)**2) 
    
				y_1=y_1.reshape(len(y_1)/frame_size, frame_size)
				#from sklearn.decomposition import DictionaryLearning
				from dict_learning import DictionaryLearning ##Modified routine...
                                from ksvd import KSVD
				D_0,c_0 = KSVD(y_1, n_components, frame_size/3, 5, enable_threading=False)
				#D_0=None#KSVD(y_1, n_components, frame_size/3, 5, enable_threading=False)
				#c_0=None
				dictio = DictionaryLearning(n_components=n_components, alpha=1.0*np.sqrt(variance_1), 
								max_iter=50, tol=-1,
								fit_algorithm='omp', transform_algorithm='omp', transform_n_nonzero_coefs=None, transform_alpha=0.02*0.02*variance_1, n_jobs=1, 
								code_init=c_0, dict_init=D_0, verbose=False, 
								split_sign=False, random_state=None, tol_2=0.02*0.02*variance_1*5, tol_1=0.01*5, n_nonzero_coefs=None)
                                    
                                    
				#On this step is the dictionary learned, and x is then transformed in the new basis
				x=dictio.fit_transform(y_1)
        
				D=dictio.components_ #here is the final dictionary    
        
				dictio = DictionaryLearning(n_components=n_components, alpha=1.0*np.sqrt(variance_1), 
								max_iter=50, tol=-1,
								fit_algorithm='omp', transform_algorithm='omp', transform_n_nonzero_coefs=None, transform_alpha=0.02*0.02*variance_1, n_jobs=1, 
								code_init=x, dict_init=D, verbose=False, 
								split_sign=False, random_state=None, tol_2=0.02*0.02*variance_1, tol_1=0.1, n_nonzero_coefs=None)
                                    
                                    
				#On this step is the dictionary learned, and x is then transformed in the new basis
				x=dictio.fit_transform(y_1)
				#Analyses the results        
				self._psi=dictio.components_ #here is the final dictionary. Check the normalization and orthogonalization of this code.
				print 'Dictionary trained.'
				
				if not (notrainrestart):
					folder_write='Dictionaries'
					file_path = folder_write+'/dictionary%s_%s.csv' %(n_components, frame_size)
					if not os.path.exists(folder_write):
						os.makedirs(folder_write)#
					with open(file_path, "wb") as f_c:   
						csvfile = csv.writer(f_c, delimiter=';')
						for i in range(len(self._psi[0])):
							csvfile.writerow((self._psi[:,i]))
					f_c.close()
					
					
			if (plot_dict):
				fi2=pl.figure(2, figsize=(4.2, 4))
				fi2.suptitle("Generated dictionary", fontsize=18)
				pd.plot_dic(self._psi)
			
          
		else:
			raise Exception('Transformation type (parameter opt) is not supported')

		if (opt == 'dct') | (opt == 'fourier'):
			# Inverse measurement matrix. Commented out in order to speed up!!!???
			self._meas_inv = np.linalg.inv(self._meas)

		#if opt == 'BWHT':
		#	print "This is BWHT"

		self._lib = ctypes.cdll.LoadLibrary(shared_lib)

		# Define matrix c types
		A1_type     = np.ctypeslib.ndpointer(dtype=np.double, ndim=2, shape=(self._n, self._m), flags='CONTIGUOUS')
		b_type      = np.ctypeslib.ndpointer(dtype=np.double, ndim=1, shape=(self._m), flags='CONTIGUOUS')
		xs1_type    = np.ctypeslib.ndpointer(dtype=np.double, ndim=1, shape=(self._n), flags='CONTIGUOUS')
		result_type = np.ctypeslib.ndpointer(dtype=np.double, ndim=1, shape=(self._n), flags='CONTIGUOUS')

		self._lib.mylib.restype = ctypes.c_int # return type	
		self._lib.mylib.argtypes = (ctypes.c_int, ctypes.c_int, A1_type, b_type, xs1_type, ctypes.c_int, ctypes.c_double, ctypes.c_double, result_type)

		#print 'Initialization done.'

	def set_block(self, block):
		""" Set block, called before reconstruct 
		Args: None			
		Returns: None
		"""
		self._block = block
				
	def set_list(self, in_list_m, in_list_n):
		""" Set list with random indices to access basis-matrix
		Args:
			
		Returns:
			
		"""
		self._list_m = in_list_m
		self._list_n = in_list_n
		
		# Define measurement matrix with only sparsity many measurements. This defines
		# the number of measurements. Important: Here the inverse matrix is used
		psi_real = (self._psi)
		#print psi_real
		#self._MeasBWHT = self._meas[:,self._list_n]
		self._binary = sparse.rand(self._n,self._n, density=0.05) # Sparse matrix with random entries. To replace by binary entries to make computation even more efficient.
		self._binary = self._binary.toarray()
		self._MeasBWHT = self._eye[self._list_m,:]
		

		A1_mtx = np.dot(self._MeasBWHT,psi_real)
		self._A1_mtx = np.ascontiguousarray(A1_mtx)
		self._HI = None
		self._HXI = None
		
		#self._meas_cs = self._meas_inv[self._list, :]
		#A1_mtx = self._meas_cs
		#self._A1_mtx = np.ascontiguousarray(A1_mtx.T)
		
		#self._list = in_list
		#self._Phi = Phi
		#self._Phi = self._Phi.toarray()
		##self._meas_inv = np.linalg.inv(self._Phi)
		#self._meas_inv = self._Phi
		
		# Define measurement matrix with only sparsity many measurements. This defines
		# the number of measurements. Important: Here the inverse matrix is used
		#self._meas_cs = self._meas_inv[self._list, :]
		#A1_mtx = self._meas_cs
		#self._A1_mtx = np.ascontiguousarray(A1_mtx)
		# For EngB mode enter here the Gaussian measurement matrix. Otherwise enter an empty array []
				
	def set_max_iter(self, max_iter_in):
		""" Set maximal number of iterations
		Args:
			maximal number of iterations (int)
		Returns:
			None
		"""
		self._max_iter = max_iter_in

	def set_lambda(self, lambda_in):
		""" Set regularization parameter lambda
		Args: 
			lambda: Regularization parameter (float)
		Returns:
			None
		"""
		self._lambda = lambda_in
	
	def mpi_finalize(self):
		rc = self._lib.mpi_finalize()
		print 'MPI finalized with return code: ', rc
		
	def reconstruct(self, data, xs1_in, opt='DL'):
		""" Function performing the reconstruction
		Args:
			data: decomposed data
		Returns:
			Reconstructed data
		"""
		if (opt == 'BWHT') | (opt == 'PCA') | (opt == 'DWT'):
			result = np.zeros(self._n)

			b_mtx = np.array(data, dtype=np.double)
			xs1_in = np.array(xs1_in, dtype=np.double)
			ret = self._lib.mylib(ctypes.c_int(self._m), ctypes.c_int(self._n), self._A1_mtx, b_mtx, xs1_in, 
						ctypes.c_int(self._max_iter), ctypes.c_double(self._tol), ctypes.c_double(self._lambda), result)

			result_converted = [round(float(element),6) for element in result]
		
			data_reconstructed = np.dot(self._psi, result_converted)
			return( data_reconstructed.tolist())
		elif (opt == 'EngC') | (opt == 'DL'):
			b_mtx = np.array(data, dtype=np.double)		
			# Define block partition
			#Block = 15 # This is the size of the blocks
			BlockStart = np.arange(0,self._n,self._block)
			
			#print "This is the matrix",self._A1_mtx
			#print "This is the data",b_mtx
			
			
			#self._psi_inv = np.linalg.inv(self._psi)
			Run = EngineC.bs(verbose=1, learn_type=1, lambda_init=1e-3, r_init=0.96,
					prune_gamma=-1, epsilon=1e-8, max_iters=100)
			data_reconstructed = Run.reconstruct(self._A1_mtx, b_mtx, BlockStart)
			#data_reconstructed = clf.fit_transform(A, b_mtx, blk_start_loc=groupStartLoc)
			#data_reconstructed = np.dot(self._psi, result_converted)
			#data_reconstructed = result_converted
			#data_reconstructed = sf.idct(data_reconstructed,norm='ortho');
			data_reconstructed = np.dot(self._psi,data_reconstructed)
			#print(self._HI)
			#print(data)
			#print(data_reconstructed)
			return( data_reconstructed )

#-------------------------------------------------------------------------------
def test2():
	import matplotlib.pyplot as plt
	import os

	sparsity1  = 0.2
	frame_size1 = 200


	# Generate a raw test data set
	raw_data1 = np.sin(np.arange(frame_size1) / float(frame_size1) * 2 * np.pi)
	raw_data2 = np.cos(np.arange(frame_size1) / float(frame_size1) * 4 * np.pi)
	
	decomposer1 = Decomposer(sparsity1, frame_size1)
	the_list1   = decomposer1.get_list()
	xs11        = decomposer1.get_xs1(raw_data1)
	
	decomposed_data1 = decomposer1.decompose(raw_data1)
	decomposed_data2 = decomposer1.decompose(raw_data2)


	reconstructor1 = Reconstructor(sparsity1, frame_size1)
	reconstructor1.set_list(the_list1)
#	reconstructor1.set_xs1(xs11)
	
	
	reconstructed_data1 = reconstructor1.reconstruct(decomposed_data1, xs11)
	reconstructed_data2 = reconstructor1.reconstruct(decomposed_data2, xs11)



	if ('DISPLAY' in os.environ):
		line_raw1 = plt.plot(raw_data1)
		line_rec1 = plt.plot(reconstructed_data1)
		plt.setp(line_rec1, linestyle='--')
		line_raw1 = plt.plot(raw_data2)
		line_rec2 = plt.plot(reconstructed_data2)
		plt.setp(line_rec2, linestyle=':')
		plt.show()
	else:
		print 'Difference between raw and reconstructed data:'
		print raw_data1 - reconstructed_data1



#-------------------------------------------------------------------------------
def test():
	import matplotlib.pyplot as plt
	import os

	sparsity1  = 0.2
	frame_size1 = 200
	sparsity2  = 0.5
	frame_size2 = 100


	# Generate a raw test data set
	raw_data1 = np.sin(np.arange(frame_size1) / float(frame_size1) * 2 * np.pi)
	raw_data2 = np.sin(np.arange(frame_size2) / float(frame_size2) * 4 * np.pi)
	
	decomposer1 = Decomposer(sparsity1, frame_size1)
	the_list1   = decomposer1.get_list()
	xs11        = decomposer1.get_xs1(raw_data1)

	decomposer2 = Decomposer(sparsity2, frame_size2)
	the_list2   = decomposer2.get_list()
	xs12        = decomposer2.get_xs1(raw_data2)

	
	decomposed_data1 = decomposer1.decompose(raw_data1)
	decomposed_data2 = decomposer2.decompose(raw_data2)



	reconstructor1 = Reconstructor(sparsity1, frame_size1)
	reconstructor1.set_list(the_list1)
#	reconstructor1.set_xs1(xs11)
	
	reconstructor2 = Reconstructor(sparsity2, frame_size2)
	reconstructor2.set_list(the_list2)
#	reconstructor2.set_xs1(xs12)
	
	reconstructed_data1 = reconstructor1.reconstruct(decomposed_data1, xs11)
	reconstructed_data2 = reconstructor2.reconstruct(decomposed_data2, xs12)
	reconstructed_data2 = reconstructor2.reconstruct(decomposed_data2, xs12)
	reconstructed_data1 = reconstructor1.reconstruct(decomposed_data1, xs11)



	if ('DISPLAY' in os.environ):
		line_raw1 = plt.plot(raw_data1)
		line_rec1 = plt.plot(reconstructed_data1)
		plt.setp(line_rec1, linestyle='--')
		line_raw1 = plt.plot(raw_data2)
		line_rec2 = plt.plot(reconstructed_data2)
		plt.setp(line_rec2, linestyle=':')
		plt.show()
	else:
		print 'Difference between raw and reconstructed data:'
		print raw_data1 - reconstructed_data1

#-------------------------------------------------------------------------------
if __name__ == '__main__':
	test2()
