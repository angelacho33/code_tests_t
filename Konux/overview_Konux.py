#import matplotlib
#matplotlib.use('Agg') ### Force matplotlib to not use any Xwindows backend.

import matplotlib.pyplot as plt 

import os, sys, time
from tk_databank import DataBank
from tk_plot import Plot
import tk_gdfr as tk_gdfr
import numpy as np
import glob
import csv
import gc
import numpy as np
import matplotlib.pyplot as plt

file_id=0
folder='/home/angelh/Documents/DataScienceRelated/Teraki/Konux/Files/'
filenamelist=sorted(glob.glob(folder+'*.csv'),key=os.path.getsize,reverse=True)
filenamelist=[filenamelist[file_id]] 
print filenamelist

##Sensor names
all_sensors = ['sensor0',
               'sensor1',
               'sensor2',
               'sensor3',
]
print 'len(all_sensors)', len(all_sensors)

###############################################################    
output_file='summary_file_id_'+str(file_id)+'_output_recon.csv'
for file in filenamelist:    
    D = DataBank()
    P=Plot(D)
    ##Read the file
    #number_of_points=1000
    for index,sensor in enumerate(all_sensors):
        sensor_id=index
        sensor_name=sensor
        sensor_data = tk_gdfr.get_ts_value_from_file(file, ',', sensor_name, sensor_name, index, [index])
        #timestamps= sensor_data[sensor_name]['tss']
        #print timestamps
        
        ##Fake time stamps in ms assuming a sampling rate of 1 Hz
        len_data=len(sensor_data[sensor_name]['values'])
        #timestamps=np.arange(0,len_data)*1000 
        timestamps=np.arange(0,len_data)
        
        value_names= [sensor_name]
        #timestamps_in_miliseconds=[ (time.mktime(dt.timetuple()) + (dt.microsecond / 1000000.0))*1000 for dt in timestamps]
        #D.add_sensor_data(sensor_data[sensor_name]['values'][:number_of_points],timestamps[:number_of_points], sensor_id, sensor_name, sensor_name,value_names=value_names)
        D.add_sensor_data(sensor_data[sensor_name]['values'],timestamps, sensor_id, sensor_name, sensor_name,value_names=value_names)
        #P.plot_raw_inspection(sensor_id, output='png', fft_yscale='linear', dt_format='%Y-%m-%d %H:%M:%S', sw_rmse=False)
        P.plot_raw_inspection(sensor_id, output='screen', fft_yscale='linear', dt_format='%Y-%m-%d %H:%M:%S', sw_rmse=False)
        
        
    print '************************************************************************************'
    print '*********** There are ', len(D.get_sensor_list()), ' sensors in DataBank ***********'
    print '************************************************************************************'
    
    ##Print the sensor_summary i.e #sensors cumulative functions and sampling rate...x
    P.print_sensor_summary()
    D.save('output_file%d.dbk'%file_id)
    file_id=file_id+1
            
