import matplotlib
matplotlib.use('Agg') ### Force matplotlib to not use any Xwindows backend.

from tk_databank import * 
from tk_plot import Plot
import tk_err
import numpy as np
import glob
import csv
import sys
#matplotlib.rcParams.update({'font.size': 25})
import matplotlib.pyplot as plt 
import os
import gc

folder='dbk_files'

#Load and plot databanks in the folder
number_of_dbk_files=195
#number_of_dbk_files=3

differences=[]
reductions=[]
count_reds=[]
all_raw_arrays=[]
all_recon_arrays=[]
all_timestamp_arrays=[]

#Loop over dbk files (one per chunk)
for counter in np.arange(0,number_of_dbk_files,1):
    file=folder+'/output_chunk_id_'+str(counter)+'.dbk'
    if not os.path.exists(file):
        continue
    print file
    
    #os.rename('difference_plots','difference_plots_per_data_segment')
    
    E=DataBank()
    E.load(file)
    P=Plot(E)
    
    for sensor in E.get_sensor_list(): ##It should be just one sensor per databak stored
        index=sensor['index']
        dof=sensor['dof']
        sensor_id=sensor['sensor_id']
        sensor_name=sensor['sensor_name']
        
        #Raw data info
        raw_data=E.get_sensor_raw_data(sensor_id)
        tss=np.asarray(raw_data['tss'])
        values=np.asarray(raw_data['values'])
        npoints=raw_data['count']
        
        all_raw_arrays.append(values)
        #print all_raw_arrays[:10]
        
        ##The next works if there dof=1
        max=raw_data['max_v'][0]
        min=raw_data['min_v'][0]
        
        
        process_id=0
        
        #Recon data info
        recon_data=E.get_sensor_recon_data(sensor_id,process_id)
        tss_recon=np.asarray(recon_data['tss'])
        values_recon=np.asarray(recon_data['values'])
        #zlib_ratio=E.get_zip_reduction_ratio(sensor_id,framesize)
        
        all_recon_arrays.append(values_recon)
        all_timestamp_arrays.append(tss_recon)
        
        print '***********************************'
        print 'sensor_id',sensor_id
        print "first_raw_tss",tss[0]
        print "first_recon_tss",tss_recon[0]
        print '***********************************'
        
        
        #Reduced info
        reduce_info=E.get_sensor_process_info(sensor_id, process_id)['reduce_info']
        count_red=reduce_info['count_red']
        red_info_ratio=reduce_info['ratio']
        count_reds.append(count_red)
        reductions.append(red_info_ratio)
        
        #Deviation_info
        deviations=E.get_deviations(sensor_id,process_id)
        recon_data_len=len(values_recon)
        differences=tk_err.e(values[:recon_data_len],values_recon)
        
        for i in range(dof):
            value_name=sensor['value_names'][i]
            values_comp=values_recon.T[i]
            dev_max=deviations['E'][i]['max']
            dev_mean=deviations['E'][i]['mean']
            dev_min=deviations['E'][i]['min']
            
            relerr=deviations['relerr'][i]
            mae=deviations['MAE'][i]
            mape=deviations['MAPE%'][i]
            smape=deviations['SMAPE%'][i]
            rrmse=deviations['RRMSE%'][i]
            wape=deviations['WAPE%'][i]
            wpe=deviations['WPE%'][i]
            
            #suptitle='sid_'+str(sensor_id)+'_'+sensor_name+'_red'+str(round(red_info_ratio*100,2))+'_rrmse'+str(round(rrmse,2))
            suptitle=sensor_name+' ,Red:'+str(round(red_info_ratio*100,2))+'%'+' ,rrmse:'+str(round(rrmse,2))+'%'+' ,data segment:'+str(sensor_id)
            
            ##Difference plot
            #P.plot(sensor_id,process_id,deviation='difference',dt_format='%H:%M:%S',output='png',suptitle=suptitle) 
            
            ##Integral plot (from Daniel)
            #P.plot(sensor_id,process_id,deviation='difference_integral',dt_format='%H:%M:%S',output='png',suptitle=suptitle) 
            
            plt.close('all')
            gc.collect()
            

###########After Loading all the dbk files for eeach data chunk...concatenate the results in a big databamk
sensor_id=0
process_id=0

Dat_concatenated_data=DataBank()
P_concatenated_data=Plot(Dat_concatenated_data)


all_raw_data=np.concatenate(all_raw_arrays) ##concatenate raw_data per chunk
all_raw_data=all_raw_data.reshape(len(all_raw_data), 1) ##Put [1,2,3...] in format [[1],[2],[3]...]

all_recon_data=np.concatenate(all_recon_arrays) ##concatenate recon_data per chunk
all_recon_data=all_recon_data.reshape(len(all_recon_data), 1) ##Put [1,2,3...] in format [[1],[2],[3]...]

all_timestamps=np.concatenate(all_timestamp_arrays)

value_names=[sensor_name]
Dat_concatenated_data.add_sensor_data(all_raw_data,all_timestamps, sensor_id, sensor_name, sensor_name,value_names=value_names)

recon_data=np.concatenate(all_recon_arrays) ##concatenate recon_data per chunk
recon_data=recon_data.reshape(len(recon_data), 1) ##Put [1,2,3...] in format [[1],[2],[3]...]


print '**red_points'
print count_reds
print "\n"
print '**Sum of red points'
all_red_points=sum(count_reds)
print all_red_points

print '***Over All reduction***'
overall_reduction=(1-all_red_points/float(len(all_raw_data)))*100
print overall_reduction,"%"

process_id=Dat_concatenated_data.add_processed_sensor_data(sensor_id, recon_data, all_timestamps, datatype='recon_data', meta={'reduction' : overall_reduction})

deviations=Dat_concatenated_data.get_deviations(sensor_id=sensor_id, process_id=process_id)
rrmse=deviations['RRMSE%'][0]

suptitle=sensor_name+' ,Reduction: '+str(round(overall_reduction,3))+'% ,rrmse: '+str(round(rrmse,3))+'%'
P_concatenated_data.plot(sensor_id=sensor_id, 
                         process_id=process_id, 
                         deviation='difference', 
                         output='png', 
                         suptitle=suptitle,
                         dt_format='%H:%M:%S')

P_concatenated_data.plot(sensor_id=sensor_id, 
                         process_id=process_id, 
                         deviation='difference_integral', 
                         output='png', 
                         suptitle=suptitle,
                         dt_format='%H:%M:%S')
            

Dat_concatenated_data.save("BMW_%s_concatenated_chunks.dbk"%sensor_name)  
        
        
        
        

