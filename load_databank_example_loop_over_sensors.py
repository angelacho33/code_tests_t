#import matplotlib
#matplotlib.use('Agg') ### Force matplotlib to not use any Xwindows backend.

import matplotlib.pyplot as plt
from tk_databank import * 
from tk_plot import Plot
import numpy as np
import glob
import csv
import read_airbus_sar_data
import sys
import gc
import tk_err as tk_err
file_name='output_file.dbk'

E=DataBank()
E.load(file_name)
P=Plot(E)
process_id=0

for sensor in E.get_sensor_list():
    sensor_id=sensor['sensor_id']
    sensor_name=sensor['sensor_name']
    dof=sensor['dof']
    
    if sensor_name!='Perform_CleaningActVal':
        continue
    
        
    ##Lines extracted from tk_plot
    raw = E.get_sensor_raw_data(sensor_id)
    raw_tss = raw['tss']
    raw_values = zip(*raw['values'])
    
    rec = E.get_sensor_recon_data(sensor_id, process_id)
    rec_tss = rec['tss']
    rec_values = zip(*rec['values'])
    
    
    i=0
    n = len(rec_values[i])
    diff = tk_err.e(raw_values[i][:n], rec_values[i])
    rel_diff = tk_err.pe(raw_values[i][:n], rec_values[i], norm='avg')
    
    #print raw_values[:10]
    #print rec_values[:10]
    #print np.asarray(rel_diff[:10])
    print 'max rel diff:', np.asarray(rel_diff).max()
    
    for i,value in enumerate(rel_diff):
        if value>15000:
            diff=raw_values[0][i]-rec_values[0][i]
            mean=np.mean([raw_values[0][i],rec_values[0][i]])
            rel_diff=(diff/mean)*100
            print '**************'
            print str(value).ljust(5),str(raw_values[0][i]).ljust(5),str(rec_values[0][i]).ljust(5)
            print str(diff).ljust(5) ,str(mean).ljust(5),str(rel_diff).ljust(5)
            print '**************'
    #Reduce info
    reduce_info=E.get_sensor_process_info(sensor_id, process_id)['reduce_info']
    count_red=reduce_info['count_red']
    red_info_ratio=reduce_info['ratio']
    
    deviations=E.get_deviations(sensor_id,process_id)
    rrmses=[]
    rrmses_string=""
    for i in range(dof):
        rrmse=deviations['RRMSE%'][i]
        rrmses.append(rrmse)
        rrmses_string=rrmses_string+str(round(rrmse,1))+"%"
        if i<dof-1:
            rrmses_string=rrmses_string+","
        else:
            rrmses_string=rrmses_string
    
    
    ##Plots
    suptitle=sensor_name+ ',reduction: '+str(int(red_info_ratio*100))+'%\n rrmse:  ('+rrmses_string+') ,sid:'+str(sensor_id)
    #P.plot(sensor_id,process_id,deviation='difference',dt_format="%Y-%m-%d %H",output='png',suptitle=suptitle)
    #P.plot(sensor_id,process_id,deviation='difference',dt_format="%H:%M",output='png',suptitle=suptitle)
    #P.plot_rec_inspection(sensor_id, process_id, output='png', fft_yscale='linear', dt_format='%H:%M')
    P.plot_rec_inspection(sensor_id, process_id, output='screen', fft_yscale='linear', dt_format='%H:%M')
    plt.close('all')
    gc.collect()


