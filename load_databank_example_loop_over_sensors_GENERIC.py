import matplotlib
matplotlib.use('Agg') ### Force matplotlib to not use any Xwindows backend.

import matplotlib.pyplot as plt
from tk_databank import * 
from tk_plot import Plot
import numpy as np
import glob
import csv
import read_airbus_sar_data
import sys
import gc
import tk_err as tk_err
file_name=sys.argv[1]

E=DataBank()
E.load(file_name)
P=Plot(E)
process_id=0

for sensor in E.get_sensor_list():
    sensor_id=sensor['sensor_id']
    sensor_name=sensor['sensor_name']
    dof=sensor['dof']
    
    #Reduce info
    reduce_info=E.get_sensor_process_info(sensor_id, process_id)['reduce_info']
    count_red=reduce_info['count_red']
    red_info_ratio=reduce_info['ratio']
    
    deviations=E.get_deviations(sensor_id,process_id)
    rrmses=[]
    rrmses_string=""
    for i in range(dof):
        rrmse=deviations['RRMSE%'][i]
        rrmses.append(rrmse)
        rrmses_string=rrmses_string+str(round(rrmse,1))+"%"
        if i<dof-1:
            rrmses_string=rrmses_string+","
        else:
            rrmses_string=rrmses_string
        
        dev_max=deviations['E'][i]['max']
        dev_mean=deviations['E'][i]['mean']
        dev_min=deviations['E'][i]['min']
        
        relerr=deviations['relerr'][i]
        mae=deviations['MAE'][i]
        mape=deviations['MAPE%'][i]
        smape=deviations['SMAPE%'][i]
        rrmse=deviations['RRMSE%'][i]
        wape=deviations['WAPE%'][i]
        wpe=deviations['WPE%'][i]
    
    ##Plots!
    suptitle=sensor_name+ ',reduction: '+str(int(red_info_ratio*100))+'%\n rrmse:  ('+rrmses_string+') '+ ',dev_max='+str(round(dev_max,3))+' ,sid:'+str(sensor_id)
    P.plot(sensor_id,process_id,deviation='difference',dt_format="%H:%M:%S",output='png',suptitle=suptitle)
    P.plot(sensor_id,process_id,deviation='rel_error',dt_format="%H:%M:%S",output='png',suptitle=suptitle)    
    #P.plot_rec_inspection(sensor_id, process_id, output='screen', fft_yscale='linear', dt_format='%H:%M:%S')
    
    plt.close('all')
    gc.collect()


