from tk_databank import * 
from tk_plot import Plot
import numpy as np
import glob
import csv
import read_airbus_sar_data
import sys
import numpy as np
import matplotlib.pyplot as plt

#file_name=str(sys.argv[1])
folder='/home/angelh/Documents/DataScienceRelated/Teraki/BMW_output/twelfth_run/innercity/'
file_name='post_processed_output_file_id_0.dbk' ##File for innercity with small pe_distances that do not make sense when look at the map!!

E=DataBank()
E.load(folder+file_name)
#E.get_sensor_list()

sensor_id=0
process_id=1

recon_data=E.get_sensor_recon_data(sensor_id,process_id)
#red_data=E.get_sensor_red_data(sensor_id,process_id)
raw_data=E.get_sensor_raw_data(0)

#print red_data
recon_data_array=np.asarray(recon_data['values']).T[0]
recon_tss_array=np.asarray(recon_data['tss'])

#red_data_array=np.asarray(red_data['values']).T[0]
#red_tss_array=np.asarray(red_data['tss'])

plt.plot(recon_tss_array,recon_data_array,label='recon data')
#plt.plot(red_tss_array,red_data_array,label='reduced data')
plt.show()




#raw_data['values'][:10]
#recon_data['values'][:10]
#gps_deviation1['pe_distances'][:10]

##Qing line to write reduced and reconstructed csv files for sensor_id=1
#E.write_sensor_data_files(sensor_id)

##Plot
#P=Plot(E)
#P.plot(sensor_id,process_id,deviation='difference',dt_format='%M:%S')
#P.plot_raw_inspection(sensor_id, output='screen', fft_yscale='log', dt_format='%Y-%m-%d %H:%M:%S')
#P.plot_rec_inspection(sensor_id, process_id, output='screen', fft_yscale='log', dt_format='%Y-%m-%d %H:%M:%S')
#P.plot_rec_inspection(sensor_id, process_id, output='png', fft_yscale='log', dt_format='%Y-%m-%d %H:%M:%S')
#P.show()



